(ns advent-of-code-2017.day5-test
  (:require
   [clojure.test :refer :all]
   [advent-of-code-2017.day5 :as sut]))

(deftest part1-test
  (testing "Part 1"
    (testing "should exit after the expected number of steps"
      (is (= (sut/part1 [0 3 0 1 -3]) 5))))

  (testing "Part 2"
    (testing "should exit after the expected number of steps"
      (is (= (sut/part2 [0 3 0 1 -3]) 10))))

  (testing "read-input"
    (testing "should read input correctly"
      (is (= (sut/read-input "0 3 0 1 -3") [0 3 0 1 -3])))))
