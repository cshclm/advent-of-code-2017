(ns advent-of-code-2017.day8-test
  (:require [advent-of-code-2017.day8 :as sut]
            [clojure.test :refer :all]))

(def sample-input
  "b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10")

(deftest read-input-test
  (testing "Read input"
    (let [output (sut/read-input sample-input)]
      (is (= (count output) 4))
      (is (every? identity
                  (mapcat #(vector (function? (:op %))
                                   (function? (:cond %)))
                          output))))))

(deftest part1-test
  (testing "Part 1"
    (is (= (sut/part1 (sut/read-input sample-input)) 1))))

(deftest part2-test
  (testing "Part 2"
    (is (= (sut/part2 (sut/read-input sample-input)) 10))))
