(ns advent-of-code-2017.day2-test
  (:require [clojure.test :refer :all]
            [advent-of-code-2017.day2 :refer [part1 part2 read-input]]))

(deftest part1-test
  (testing "Part 1"
    (are [i a] (= (part1 i) a)
      [[5 1 9 5]] 8
      [[7 5 3]] 4
      [[2 4 6 8]] 6
      [[5 1 9 5]
       [7 5 3]
       [2 4 6 8]] 18)))

(deftest part2-test
  (testing "Part 2"
    (are [i a] (= (part2 i) a)
      [[5 9 2 8]
       [9 4 7 3]
       [3 8 6 5]] 9)))

(deftest read-input-test
  (testing "Read input"
    (are [i a] (= (read-input i) a)
      "5 1 9 5
       7 5 3
       2 4 6 8"
      [[5 1 9 5]
       [7 5 3]
       [2 4 6 8]])))
