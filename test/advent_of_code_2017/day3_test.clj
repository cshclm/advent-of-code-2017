(ns advent-of-code-2017.day3-test
  (:require
   [clojure.test :refer :all]
   [advent-of-code-2017.day3 :refer [part1 part2]]))

(deftest part1-test
  (testing "Part 1"
    (are [i a] (= (part1 i) a)
      1 0
      12 3
      23 2
      1024 31
      312051 430)))

(deftest part2-test
  (testing "Part 2"
    (are [i a] (= (part2 i) a)
      1 2
      42 54
      132 133
      600 747)))
