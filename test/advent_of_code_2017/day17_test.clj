(ns advent-of-code-2017.day17-test
  (:require [advent-of-code-2017.day17 :as sut]
            [clojure.test :refer :all]))

(deftest part1-test
  (testing "Part 1"
    (is (= (sut/part1 3) 638))))

(deftest part2-test
  (testing "Part 2"
    (is (= (sut/part2 1000 3) 517))))
