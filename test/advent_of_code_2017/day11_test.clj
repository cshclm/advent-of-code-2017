(ns advent-of-code-2017.day11-test
  (:require
   [advent-of-code-2017.spec-util :refer [defspec-test]]
   [clojure.test.check :as tc]
   [clojure.spec.gen.alpha :as gen]
   [clojure.test :refer :all]
   [advent-of-code-2017.day11 :as sut]
   [clojure.spec.test.alpha :as stest]))

(deftest part1-test
  (testing "Part 1"
    (is (= (sut/part1 [:ne :ne :ne]) 3))
    (is (= (sut/part1 [:ne :ne :sw :sw]) 0))
    (is (= (sut/part1 [:ne :ne :s :s]) 2))
    (is (= (sut/part1 [:se :sw :se :sw :sw]) 3))
    (is (= (sut/part1 [:ne :ne :ne :n :n]) 5))))

(deftest part2-test
  (testing "Part 2"
    (is (= (sut/part2 [:ne :ne :ne]) 3))
    (is (= (sut/part2 [:ne :ne :sw :sw]) 2))
    (is (= (sut/part2 [:ne :ne :s :s]) 2))
    (is (= (sut/part2 [:se :sw :se :sw :sw]) 3))
    (is (= (sut/part2 [:ne :ne :ne :n :n :s :s :s :ne]) 5))))

(defspec-test part1-spec
  'advent-of-code-2017.day11/part1)
(defspec-test part2-spec
  'advent-of-code-2017.day11/part2)
