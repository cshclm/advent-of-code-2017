(ns advent-of-code-2017.day16-test
  (:require
   [advent-of-code-2017.spec-util :refer [defspec-test]]
   [clojure.test.check :as tc]
   [clojure.spec.gen.alpha :as gen]
   [clojure.test :refer :all]
   [advent-of-code-2017.day16 :as sut]
   [advent-of-code-2017.data :refer [day16]]
   [clojure.spec.test.alpha :as stest]))

(deftest part1-test
  (testing "Part 1"
    (is (= ((sut/part1 (sut/read-input "s1,x3/4,pe/b"))
            (into [] "abcde")) (into [] "baedc")))))

(deftest part2-test
  (testing "Part 2"
    (is (= (sut/part2 "s1,x3/4,pe/b" "abcde" 0) "abcde"))
    (is (= (sut/part2 "s1,x3/4,pe/b" "abcde" 1) "baedc"))
    (is (= (sut/part2 "s1,x3/4,pe/b" "abcde" 999) "ecbda"))
    (is (= (sut/part2 "s1,x3/4,pe/b" "abcde" 10000002) "ceadb"))))
