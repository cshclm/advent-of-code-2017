(ns advent-of-code-2017.day4-test
  (:require [advent-of-code-2017.day4 :as sut]
            [clojure.test :refer :all]))

(deftest part1-test
  (testing "Part 1"
    (testing "should only count phrases with unique terms"
      (are [i a] (= (sut/part1 i) a)
        [["aa" "bb" "cc" "dd" "ee"]
         ["aa" "bb" "cc" "dd" "aa"]
         ["aa" "bb" "cc" "dd" "aaa"]] 2))))
