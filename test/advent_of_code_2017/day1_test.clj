(ns advent-of-code-2017.day1-test
  (:require [clojure.test :refer :all]
            [advent-of-code-2017.day1 :refer [part1 part2 read-input]]))

(deftest part1-test
  (testing "Part 1"
    (are [i a] (= (part1 i) a)
      [1 1 2 2] 3
      [1 1 1 1] 4
      [1 2 3 4] 0
      [9 1 2 1 2 1 2 9] 9)))

(deftest part2-test
  (testing "Part 2"
    (are [i a] (= (part2 i) a)
      [1 2 1 2] 6
      [1 2 2 1] 0
      [1 2 3 4 2 5] 4
      [1 2 3 1 2 3] 12
      [1 2 1 3 1 4 1 5] 4)))

(deftest read-input-test
  (testing "Read input"
    (are [i a] (= (read-input i) a)
      "1212" [1 2 1 2]
      "12131415" [1 2 1 3 1 4 1 5])))
