(ns advent-of-code-2017.day9-test
  (:require
   [clojure.test.check :as tc]
   [clojure.spec.gen.alpha :as gen]
   [clojure.test :refer :all]
   [advent-of-code-2017.day9 :as sut]
   [clojure.spec.test.alpha :as stest]))

(deftest part1-test
  (testing "Part 1"
    (testing "should give correct results for"
      (testing "empty string"
        (is (= (sut/part1 "") 0)))
      (testing "single group"
        (is (= (sut/part1 "{}") 1)))
      (testing "nested group"
        (is (= (sut/part1 "{{}}") 3)))
      (testing "nested group containing garbage"
        (is (= (sut/part1 "{{<some garbage here>}}") 3)))
      (testing "nested group with escaped garbage end"
        (is (= (sut/part1 "{{<some !>garbage here>}}") 3)))
      (testing "nested group with braces in garbage garbage end"
        (is (= (sut/part1 "{{<some !>garbage {{}}>}}") 3))))))

(deftest part2-test
  (testing "Part 2"
    (testing "should give correct results for"
      (testing "empty string"
        (is (= (sut/part2 "") 0)))
      (testing "single group"
        (is (= (sut/part2 "{}") 0)))
      (testing "nested group"
        (is (= (sut/part2 "{{}}") 0)))
      (testing "nested group containing garbage"
        (is (= (sut/part2 "{{<some garbage here>}}") 17)))
      (testing "nested group with escaped garbage end"
        (is (= (sut/part2 "{{<some !>garbage here>}}") 17)))
      (testing "nested group with braces in garbage garbage end"
        (is (= (sut/part2 "{{<some !>garbage {{}}>}}") 17))))))

(deftest specs-test
  (testing "Specs"
    (let [r (-> (stest/enumerate-namespace 'advent-of-code-2017.day9)
                stest/check
                stest/summarize-results)]
      (is (= (:total r) (:check-passed r))))))
