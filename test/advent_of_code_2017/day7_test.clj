(ns advent-of-code-2017.day7-test
  (:require
   [clojure.test :refer :all]
   [advent-of-code-2017.day7 :as sut]))

(def sample-data
  "pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)")

(deftest read-input-test
  (testing "Read input"
    (is (= (sut/read-input sample-data)
           [["pbga" 66 []]
            ["xhth" 57 []]
            ["ebii" 61 []]
            ["havc" 66 []]
            ["ktlj" 57 []]
            ["fwft" 72 ["ktlj" "cntj" "xhth"]]
            ["qoyq" 66 []]
            ["padx" 45 ["pbga" "havc" "qoyq"]]
            ["tknk" 41 ["ugml" "padx" "fwft"]]
            ["jptl" 61 []]
            ["ugml" 68 ["gyxo" "ebii" "jptl"]]
            ["gyxo" 61 []]
            ["cntj" 57 []]]))))

(deftest part1
  (testing "Part 1"
    (is (= (sut/part1 (sut/read-input sample-data)) "tknk"))))

(deftest part2
  (testing "Part 2"
    (is (= (sut/part2 (sut/read-input sample-data)) 60))))
