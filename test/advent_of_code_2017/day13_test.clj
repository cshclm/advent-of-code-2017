(ns advent-of-code-2017.day13-test
  (:require
   [advent-of-code-2017.spec-util :refer [defspec-test]]
   [clojure.test.check :as tc]
   [clojure.spec.gen.alpha :as gen]
   [clojure.test :refer :all]
   [advent-of-code-2017.day13 :as sut]
   [clojure.spec.test.alpha :as stest]))

(def input
  {:ranges {0 3
            1 2
            4 4
            6 4}
   :steps 0
   :times-caught 0
   :severity 0})

(deftest part1-test
  (testing "Part 1"
    (is (= (sut/part1 input) 24))))

(deftest part2-test
  (testing "Part 2"
    (is (= (sut/part2 input) 10))))
