(ns advent-of-code-2017.day6-test
  (:require
   [clojure.test :refer :all]
   [advent-of-code-2017.day6 :as sut]))

(deftest read-input-test
  (testing "Read input"
    (is (= (sut/read-input "1 2 3") [1 2 3]))))

(deftest part1-test
  (testing "Part 1"
    (is (= (sut/part1 [0 2 7 0]) 5))))

(deftest part2-test
  (testing "Part 2"
    (is (= (sut/part2 [0 2 7 0]) 4))))
