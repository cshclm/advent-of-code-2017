(ns advent-of-code-2017.day10-test
  (:require
   [advent-of-code-2017.spec-util :refer [defspec-test]]
   [clojure.test.check :as tc]
   [clojure.spec.gen.alpha :as gen]
   [clojure.test :refer :all]
   [advent-of-code-2017.day10 :as sut]
   [clojure.spec.test.alpha :as stest]))

(deftest part1-test
  (testing "Part 1"
    (is (= (apply * (take 2 (first (sut/part1 [3 4 1 5] [0 1 2 3 4])))) 12))))

(deftest part2-test
  (testing "Part 2"
    (is (= (sut/part2 "") "a2582a3a0e66e6e86e3812dcb672a272"))
    (is (= (sut/part2 "AoC 2017") "33efeb34ea91902bb2f59c9920caa6cd"))
    (is (= (sut/part2 "1,2,4") "63960835bcdc130f0b66d7ff4f6a5a8e"))))

(defspec-test part1-spec
  'advent-of-code-2017.day10/part1)
