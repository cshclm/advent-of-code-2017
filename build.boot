(def project 'advent-of-code-2017)
(def version "0.1.0-SNAPSHOT")

(set-env! :resource-paths #{"resources" "src"}
          :source-paths   #{"test"}
          :dependencies   '[[org.clojure/clojure "1.9.0"]
                            [adzerk/boot-test "1.2.0" :scope "test"]
                            [org.clojure/math.combinatorics "0.1.4"]
                            [criterium "0.4.4"]
                            [org.clojure/test.check "0.9.0" :scope "test"]])

(task-options!
 pom {:project     project
      :version     version
      :description "Advent of Code solutions, in Clojure"
      :url         "http://gitlab.com/cshclm/advent-of-code-2017"
      :scm         {:url "http://gitlab.com/cshclm/advent-of-code-2017"}
      :license     {"Eclipse Public License"
                    "http://www.eclipse.org/legal/epl-v10.html"}})

(require '[adzerk.boot-test :refer [test]])
