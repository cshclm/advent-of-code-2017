# Advent of Code 2017

Solutions, in Clojure, for [Advent of Code 2017](http://adventofcode.com/).

## License

Copyright © 2017 Chris Mann

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
