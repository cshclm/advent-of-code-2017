(ns advent-of-code-2017.day7
  (:require
   [clojure.string :as s]
   [clojure.edn :as edn]
   [clojure.set :as cljset]
   [advent-of-code-2017.data :as data]
   [criterium.core :refer [quick-bench]]))

(defn read-input
  [input]
  (mapv
   #(let [[node-str deps-str] (s/split % #"->")
         [node weight-str] (s/split (s/trim node-str) #"\s+")
          deps (if deps-str (mapv s/trim (s/split deps-str #",")) [])
         weight (first (edn/read-string weight-str))]
      [node weight deps])
   (s/split input #"\n")))

(defn part1
  [input]
  (let [all (into #{} (map first input))
        refd (into #{} (mapcat #(nth % 2) input))]
    (first (into [] (cljset/difference all refd)))))

(defn check-weights
  [tree root]
  (let [cws (mapv (fn [node]
                   (+ (first (get tree node))
                      (check-weights tree node)))
                 (second (get tree root)))]
    (if (> (count (distinct cws)) 1)
      (let [ws (mapv #(first (get tree %)) (second (get tree root)))]
        (throw (RuntimeException. (str [cws ws]))))
      (reduce + 0 cws))))

(defn part2
  [input]
  (let [root (part1 input)
        tree (reduce-kv (fn [acc k v] (assoc acc k (-> v first rest))) {}
                        (group-by first input))]
    (try
      (check-weights tree root)
      (catch Exception e
        (let [[totals node-weights] (edn/read-string (.getMessage e))
              rs (->> totals
                      frequencies
                      (partition-by (fn [[k v]] (= v 1)))
                      (mapcat identity)
                      (sort-by second))
              fault (ffirst rs)
              correct (first (second rs))
              offset (- correct fault)]
          (+ offset (nth node-weights (.indexOf totals fault))))))))

(defn solve
  []
  ((juxt part1 part2) (read-input data/day7)))

(comment
  (let [i (read-input data/day7)]
    (quick-bench (part1 i)) ;; 1.4ms
    (quick-bench (part2 i)))) ;; 4.2ms
