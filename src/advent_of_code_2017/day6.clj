(ns advent-of-code-2017.day6
  (:require
   [clojure.string :as s]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [criterium.core :refer [quick-bench]]))

(defn read-input
  [input]
  (mapv edn/read-string (s/split input #"\s+")))

(defn max-index
  [input]
  (let [v (apply max input)]
    [v (.indexOf input v)]))

(defn redistribute
  [input v idx]
  (reduce (fn [acc i]
            (update acc i inc))
          input
          (map #(mod (+ idx %) (count input)) (range 1 (inc v)))))

(defn part1
  [input]
  (loop [seen #{} cur input cycles 1]
    (let [[v idx] (max-index cur)
          rdist (redistribute (assoc cur idx 0) v idx)]
      (if (contains? seen rdist)
        cycles
        (recur (conj seen cur) rdist (inc cycles))))))

(defn part2
  [input]
  (loop [seen #{} hist [] cur input cycles 1]
    (let [[v idx] (max-index cur)
          rdist (redistribute (assoc cur idx 0) v idx)]
      (if (contains? seen rdist)
        (- cycles (.indexOf hist rdist))
        (recur (conj seen cur) (conj hist cur) rdist (inc cycles))))))

(defn solve
  []
  ((juxt part1 part2) (read-input data/day6)))

(comment
  (let [i (read-input data/day6)]
    (quick-bench (part1 i)) ;; 100ms
    (quick-bench (part2 i)))) ;; 102ms
