(ns advent-of-code-2017.day8
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.alpha :as s]
   [clojure.test.check.generators :as gen]))

(def op-ops
  {"inc" +
   "dec" -})

(def cond-ops
  {"<=" <=
   ">=" >=
   "!=" not=
   ">" >
   "<" <
   "==" =})

(defn ->op-expr
  [op]
  (let [matcher (re-matcher #"(?<reg>\w+)\s+(?<op>\w+)\s+(?<qty>-?\d+|)" op)]
    (if (.matches matcher)
      #(update %
               (.group matcher "reg")
               (fn [n]
                 ((get op-ops (.group matcher "op"))
                  (or n 0)
                  (edn/read-string (.group matcher "qty")))))
      (throw (ex-info (str "Cannot parse operator") {:input op})))))

(defn ->cond-expr
  [cond]
  (let [matcher (re-matcher #"(?<reg>\w+)\s+(?<op>.+)\s+(?<qty>-?\d+)" cond)]
    (if (.matches matcher)
      #((get cond-ops (.group matcher "op"))
        (or (get % (.group matcher "reg")) 0)
        (edn/read-string (.group matcher "qty")))
      (throw (ex-info (str "Cannot parse condition") {:input cond})))))

(defn ->expr
  [op cond]
  {:op (->op-expr op)
   :cond (->cond-expr cond)})

(defn read-input
  [input]
  (mapv #(let [[op cond] (cstr/split % #" if ")]
           (->expr op cond))
        (cstr/split input #"\n")))

(s/def ::cond (s/with-gen ifn?
                #(gen/one-of [(gen/return (fn [m] (>= (or (get m "a") 0) 0)))
                              (gen/return (fn [m] (> (or (get m "b") 0) 5)))])))
(s/def ::op (s/with-gen ifn?
              #(gen/one-of [(gen/return (fn [m] (update m "a" (fn [v] (+ (or v 0) 2)))))
                            (gen/return (fn [m] (update m "b" (fn [v] (+ (or v 0) 2)))))
                            (gen/return (fn [m] (update m "b" (fn [v] (- (or v 0) 3)))))])))
(s/def ::operation (s/keys :req-un [::cond ::op]))
(s/def ::input (s/coll-of ::operation))
(s/fdef part1
        :args (s/cat :input ::input)
        :ret (s/nilable int?))

(defn part1
  [input]
  (let [vs (->> input
                (reduce #(if ((:cond %2) %1)
                           ((:op %2) %1)
                           %1) {})
                vals)]
    (if (empty? vs)
      nil
      (apply max vs))))

(defn part2
  [input]
  (->> input
       (reduce #(if ((:cond %2) (:state %1))
                  (let [state ((:op %2) (:state %1))]
                    {:state state
                     :max (apply max (:max %1) (vals state))})
                  %1) {:max 0 :state {}})
       :max))

(defn solve
  []
  ((juxt part1 part2) (read-input data/day8)))

(comment
  (let [i (read-input data/day8)]
    (quick-bench (part1 i)))  ;; 2.5ms
    (quick-bench (part2 i))) ;; 6.0ms
