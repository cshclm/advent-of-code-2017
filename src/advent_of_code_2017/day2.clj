(ns advent-of-code-2017.day2
  "http://adventofcode.com/2017/day/2"
  (:require
   [advent-of-code-2017.data :as data]
   [clojure.string :as s]
   [clojure.edn :as edn]
   [clojure.math.combinatorics :refer [cartesian-product]]
   [criterium.core :refer [quick-bench]]))

(defn read-input
  [input]
  (map #(map edn/read-string (s/split (s/trim %) #"\s+"))
       (s/split input #"\n+")))

(defn part1
  [input]
  (->> input
       (map #(- (apply max %) (apply min %)))
       (reduce + 0)))

(defn p2-solve-row
  [row]
  (let [s (->> row
               (cartesian-product row)
               (filter #(and (> (count (distinct %)) 1)
                             (= (mod (apply max %) (apply min %)) 0)))
               first)]
    (/ (apply max s) (apply min s))))

(defn part2
  [input]
  (->> input
       (map #(p2-solve-row %))
       (reduce + 0)))

(defn solve
  []
  ((juxt part1 part2)
   (read-input data/day2)))

(comment
  (let [i (read-input data/day2)]
    (quick-bench (part1 i)) ;; 21µs
    (quick-bench (part2 i)))) ;; 4.5ms
