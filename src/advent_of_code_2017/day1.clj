(ns advent-of-code-2017.day1
  "http://adventofcode.com/2017/day/1"
  (:require
   [advent-of-code-2017.data :as data]
   [criterium.core :refer [quick-bench]]))

(defn read-input
  [input]
  (mapv #(- (int %) (int \0)) input))

(defn cycler
  [cyclefn]
  (fn [input]
    (let [xform (comp (map-indexed (fn [i x] [x (cyclefn input i)]))
                      (filter #(= (count (distinct %)) 1))
                      (map first))]
      (reduce + 0 (sequence xform input)))))

(defn rotate-by
  [rfn]
  (fn [input i]
    (->> input
         count
         (mod (+ i (rfn input)))
         (nth input))))

(def part1 (cycler (rotate-by (constantly 1))))
(def part2 (cycler (rotate-by #(int (/ (count %) 2)))))

(defn solve
  []
  (let [f (juxt part1 part2)]
    (f (read-input data/day1))))

(comment
  (let [i (read-input data/day1)]
    (quick-bench (part1 i)) ;; 3.3ms
    (quick-bench (part2 i)))) ;; 3.6ms
