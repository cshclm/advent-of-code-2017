(ns advent-of-code-2017.day12
  "http://adventofcode.com/2017/day/12"
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [clojure.set :as set]
   [advent-of-code-2017.data :as data]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]))

(defn- read-input
  [input]
  (->> (cstr/split input #"\n")
       (map #(map edn/read-string (cstr/split % #"<->|,\s*")))))

(defn part1
  [cs]
  (let [g (reduce
           (fn [acc [n & pipes]]
             (reduce #(update (update %1 n conj %2)
                           %2
                           conj n)
                     acc
                  pipes))
           {}
           cs)]
    (loop [seen #{} rem [] node 0]
      (let [nrem (concat rem (into [] (set/difference (into #{} (get g node)) seen)))]
        (if (empty? nrem)
          (count (inc seen))
          (recur (conj seen node) (rest nrem) (first nrem)))))))

(defn part2
  [cs]
  (let [g (reduce
           (fn [acc [n & pipes]]
             (reduce #(update (update %1 n conj %2)
                           %2
                           conj n)
                     acc
                  pipes))
           {}
           cs)]
    (:count (reduce
             (fn [acc n]
               (if (contains? (:seen acc) n)
                 acc
                 (update (update acc :seen set/union
                                 (loop [seen #{} rem [] node n]
                                   (let [nrem (concat rem (into [] (set/difference (into #{} (get g node)) seen)))]
                                     (if (empty? nrem)
                                       (conj seen node)
                                       (recur (conj seen node) (rest nrem) (first nrem))))))
                         :count inc)))
             {:seen #{} :count 0}
             (keys g)))))

(comment
  (quick-bench (part1 (read-input data/day12))) ;; 1.6ms
  (quick-bench (part2 (read-input data/day12)))) ;; 69.7ms


