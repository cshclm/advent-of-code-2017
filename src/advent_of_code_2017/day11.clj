(ns advent-of-code-2017.day11
  "http://adventofcode.com/2017/day/11"
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]))

(def ^:private coords
  {:n (fn [[x y]] [x (inc y)])
   :s (fn [[x y]] [x (dec y)])
   :nw (fn [[x y]] [(dec x) (+ y 0.5)])
   :sw (fn [[x y]] [(dec x) (- y 0.5)])
   :ne (fn [[x y]] [(inc x) (+ y 0.5)])
   :se (fn [[x y]] [(inc x) (- y 0.5)])})

(defn- read-input
  [input]
  (map keyword (cstr/split input #",")))

(defn- dist
  [coords]
  (let [[dx dy] (map #(Math/abs %) coords)]
    (Math/round (double (+ dx (max (- dy (double (/ dx 2))) 0))))))

(let [ks (into #{} (keys coords))]
  (s/def ::direction
    (s/with-gen #(contains? ks %)
      #(gen/one-of (map (fn [d] (gen/return d)) ks)))))

(defn part1
  [input]
  (dist (reduce #(let [f (get coords %2)]
                   (f %1))
                [0 0]
                input)))

(s/fdef part1
        :args (s/cat ::input (s/coll-of ::direction))
        :ret nat-int?
        :fn #(<= (:ret %) (count (-> % :args ::input))))

(defn part2
  [input]
  (:max (reduce #(let [f (get coords %2)
                       p (f (:cur %1))]
                   (update (assoc %1 :cur p)
                           :max max (dist p)))
                {:max 0
                 :cur [0 0]}
                input)))

(s/fdef part2
        :args (s/cat ::input (s/coll-of ::direction))
        :ret nat-int?
        :fn #(let [i (-> % :args ::input)]
               (<= (part1 i) (:ret %) (count i))))

(comment
  (quick-bench (part1 (read-input data/day11))) ;; 1.6ms
  (quick-bench (part2 (read-input data/day11)))) ;; 69.7ms
