(ns advent-of-code-2017.day3
  "http://adventofcode.com/2017/day/3"
  (:require
   [clojure.math.combinatorics :refer [cartesian-product]]
   [criterium.core :refer [quick-bench]])
  (:import
   (java.util.Math)))

;; Part 1

(defn part1
  [n]
  (if (= n 1)
    0
    (loop [count 1 step 8 dist 0]
      (if (< (+' count step) n)
        (recur (+' count step) (+' step 8) (inc dist))
        (let [edge (/ step 4)
              spiral-pos (- n count)
              side (int (/ spiral-pos edge))
              offset (Math/abs (- (* edge (+' 0.5 side)) spiral-pos))]
          (int (+' offset (inc dist))))))))

;; Part 2

(def directions
  {:east #(vector (inc (first %)) (second %))
   :north #(vector (first %) (inc (second %)))
   :west #(vector (dec (first %)) (second %))
   :south #(vector (first %) (dec (second %)))})

(defn next-dir
  [coord dir bound]
  (let [nbound (- bound)
        [x y] coord]
    (cond
      (and (= x bound) (= y bound)) :west
      (and (= x nbound) (= y bound)) :south
      (and (= x nbound) (= y nbound)) :east
      (and (= x bound) (= y (inc nbound))) :north
      :else dir)))

(def adj-ops
  (->> [inc dec identity]
       (repeat 2)
       (apply cartesian-product)
       (remove #(= [identity identity] %))))

(defn adjacent-vals
  [board [x y]]
  (->> adj-ops
       (map (fn [[dx dy]] (get board [(dx x) (dy y)])))
       (remove nil?)
       (reduce +' 0)))

(defn next-bound
  [[x y] bound]
  (if (and (= x bound) (= y (- bound)))
    (inc bound)
    bound))

(defn part2
  [target]
  (loop [board {[0 0] 1}
         coord [0 0]
         dir :east
         bound 1]
    (let [next-coord ((get directions dir) coord)
          v (adjacent-vals board next-coord)]
      (if (> v target)
        v
        (recur (assoc board next-coord v)
               next-coord
               (next-dir next-coord dir bound)
               (next-bound next-coord bound))))))

(comment
  (let [i 312051]
    (quick-bench (part1 i)) ;; 2.3µs
    (quick-bench (part2 i)))) ;; 475µs
