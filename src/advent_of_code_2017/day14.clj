(ns advent-of-code-2017.day14
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [advent-of-code-2017.util :as util]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]
   [advent-of-code-2017.day10 :as day10]))

(def ^:private magic [17,31,73,47,23])
(def ^:private default-range (range 0 256))

(defn ^:private move-pos
  [input pos]
  (take (count input) (drop pos (concat input input))))

(defn read-input
  [input]
  (apply str (map char (map edn/read-string (cstr/split input #",")))))

(defn part1
  [input]
  (count (filter #(= \1 %)
                 (seq (apply str (mapcat (fn [n]
                                           (let [nums (day10/part2-nums (str input "-" n))]
                                             (map #(Integer/toBinaryString %) nums)))
                                         (range 128)))))))

(defn consume-at
  [board row col]
  (if (and (<= 0 row (dec (count board)))
           (<= 0 col (dec (count (first board))))
           (= (get-in board [row col]) \1))
    (-> board
        (assoc-in [row col] \0)
        (consume-at (dec row) col)
        (consume-at (inc row) col)
        (consume-at row (inc col))
        (consume-at row (dec col)))
    board))

(defn part2
  [input]
  (let [c (mapv (fn [n]
                  (let [nums (day10/part2-nums (str input "-" n))]
                    (into [] (seq (mapcat #(util/padded-binary 8 %) nums)))))
                (range 128))]
    (loop [board c row 0 col 0 count 0]
      (if (= row 128)
        count
        (let [ncol (mod (inc col) 128)
              nrow (if (zero? ncol) (inc row) row)]
          (if (= (get-in board [row col]) \1)
            (recur (consume-at board row col) nrow ncol (inc count))
            (recur board nrow ncol count)))))))



(comment
  (quick-bench (part1 (map int (seq (read-input data/day10))))) ;; 1.6ms
  (quick-bench (part2 data/day10))) ;; 325.4ms
