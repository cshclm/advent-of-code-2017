(ns advent-of-code-2017.day10
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]))

(def ^:private magic [17,31,73,47,23])
(def ^:private default-range (range 0 256))

(defn ^:private move-pos
  [input pos]
  (take (count input) (drop pos (concat input input))))

(defn read-input
  [input]
  (apply str (map char (map edn/read-string (cstr/split input #",")))))

(s/fdef part1
        :args (s/alt ::unary (s/cat ::input (s/coll-of nat-int?))
                     ::binary (s/cat ::input (s/coll-of nat-int?)
                                    ::nums (s/coll-of nat-int? :min-count 1))
                     ::more (s/cat ::input (s/coll-of nat-int?)
                                  ::nums (s/coll-of nat-int? :min-count 1)
                                  ::pos nat-int?
                                  ::skip nat-int?))
        :ret (s/tuple (s/coll-of nat-int?) nat-int? nat-int?)
        :fn (fn [{:keys [ret args]}]
              (let [ns (or (::nums (second args)) default-range)]
                (and (= (into #{} ns) (into #{} (first ret)))
                     (= (count ns) (count (first ret)))))))

(defn part1
  ([input]
   (part1 input default-range))
  ([input nums]
   (part1 input nums 0 0))
  ([input nums pos skip]
   (if (or (empty? input) (empty? nums))
     [nums pos skip]
     (loop [rem input pos pos skip skip res nums]
       (if (empty? rem)
         [(vec (take (count res) (drop (- (count res) (mod pos (count res))) (concat res res))))
          (long pos) (long skip)]
         (let [n (first rem)
               rable (take n res)
               nrable (drop n res)]
           (let [p (mod (+' n skip) (count res))]
             (recur (rest rem)
                    (mod (+' p pos) (count res))
                    (inc skip)
                    (let [nl (concat (reverse rable) nrable)]
                      (move-pos nl p))))))))))

(defn part2
  [input]
  (let [i (concat (map int (seq input)) magic)]
    (->> (reduce (fn [[n p s] x] (part1 i (move-pos n p) p s))
                 [default-range 0 0]
                 (range 0 64))
         first
         (partition 16)
         (map #(reduce (fn [acc x] (bit-xor acc x)) (first %1) (rest %)))
         (map #(format "%02x" %))
         (apply str))))

(defn part2-nums
  [input]
  (let [i (concat (map int (seq input)) magic)]
    (->> (reduce (fn [[n p s] x] (part1 i (move-pos n p) p s))
                 [default-range 0 0]
                 (range 0 64))
         first
         (partition 16)
         (map #(reduce (fn [acc x] (bit-xor acc x)) (first %1) (rest %))))))

(comment
  (quick-bench (part1 (map int (seq (read-input data/day10))))) ;; 1.6ms
  (quick-bench (part2 data/day10))) ;; 325.4ms
