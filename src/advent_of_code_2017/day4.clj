(ns advent-of-code-2017.day4
  (:require
   [clojure.string :as s]
   [advent-of-code-2017.data :as data]
   [criterium.core :refer [quick-bench]]))

(defn read-input
  [input]
  (map #(s/split (s/trim %) #"\s+") (s/split input  #"\n")))

(defn part1
  [input]
  (count (filter #(= (count (distinct %)) (count %)) input)))

(defn part2
  [input]
  (count (filter #(= (count (distinct (map (fn [x] (into #{} x)) %))) (count %)) input)))

(defn solve
  []
  ((juxt part1 part2) (read-input data/day4)))

(comment
  (let [i (read-input data/day4)]
    (quick-bench (part1 i)) ;; 2.6ms
    (quick-bench (part2 i)))) ;; 7.6ms
