(ns advent-of-code-2017.day15
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [advent-of-code-2017.util :as util]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]))

(def ^:private factor 2147483647)

(defn- generate
  [f n]
  (mod (* f n) factor))

(defn- build-generator [fac seed pred]
  (loop []
    (let [v (atom seed)]
      (fn []
        (let [n (swap! v generate fac)]
          (if (pred n)
            (util/padded-binary 32 n)
            (recur)))))))

(defn- gen-pairs
  [iter gen-a gen-b]
  (let [xform (comp
               (map (fn [_] (vector (drop 16 (gen-a)) (drop 16 (gen-b)))))
               (map (fn [[a b]] (= a b)))
               (filter identity))]
    (count (sequence xform (range iter)))))

(def ^:private gen-iter-limit 10)
(s/fdef gen-pairs
        :input (s/cat ::iter (s/with-gen nat-int?
                               (gen/returns gen-iter-limit))
                      ::a nat-int?
                      ::b nat-int?)
        :ret nat-int?)

(defn part1
  []
  (gen-pairs 40000000
             (build-generator 16807 722 (constantly true))
             (build-generator 48271 354 (constantly true))))

(defn part2
  []
  (gen-pairs 5000000
             (build-generator 16807 722 #(zero? (mod % 4)))
             (build-generator 48271 354 #(zero? (mod % 8)))))
