(ns advent-of-code-2017.data)

(def day1
  (apply str
         '("7385764686251444473997915123782972536343732657517834671759462795"
           "4612137824283429318961816955789962743213174192423595347839573729"
           "3295377433633811848896717272765186249883819531765428979755868345"
           "8511126996217953322817229372373455862177844478443391835484591525"
           "2356518634648911779272449549258277867994365365925613742692994747"
           "3832129357538589943844655856924123627877977998358791243139547524"
           "4796538888373287186921647426866237756737342731976763959499149996"
           "3155915847161221991832952774398729113713139245944867664794385444"
           "1741652974349511481982598452443736722523418477261794252595496113"
           "6976875325182725754768372684531972614455134523596338355374444273"
           "5221153622387343831647781293766286214976629654567616317961783535"
           "9962988766593952189244736121947964648397879839271611979328271773"
           "9524897385958273726776318154977675546287789874265339688753977185"
           "1293349297154863818752862785282476964641622976916981547127755895"
           "4194526357489726657599645554762553794792797249797933393211516515"
           "1462742216327321116291372396585618664475715321298122335789262942"
           "2845713284145693754643864468248825519188431851958295473739154826"
           "8753443294277831254275279831343462849829521669264671313724419812"
           "3219531693559848915834623825919191532658735422176965451741869666"
           "7148741584925564459548522991618686514481238258217753632192462445"
           "1594639268627554556198935557394692476744225346534275399576479192"
           "7951158771231944177692469531494559697911176613943396258141822244"
           "5784574983613523815181665875833422338169893295444156211273979967"
           "2399739721967648696668472965376352576865532444399112986212918121"
           "5339947555257279592921258246646215764736698583211625887436176149"
           "2513564523582114583434393746883411165297269724346973247345251141"
           "9222964146422798658284547774174778767358884843971361932688962432"
           "6944553386782821633538775371915973899959295232927996742218926514"
           "3741689475824418927314629934818772777144368875972238718811496932"
           "2892844242761166465577233347189373593241993783293795349592951483"
           "7663883938416644387342825836673733778119481514427512453357628396"
           "6667915475318148441763426963624168429937619193699947798973573483"
           "34197721735231299249116477")))

(def day2
  "626	2424	2593	139	2136	163	1689	367	2235	125	2365	924	135	2583	1425	2502
  183	149	3794	5221	5520	162	5430	4395	2466	1888	3999	3595	195	181	6188	4863
  163	195	512	309	102	175	343	134	401	372	368	321	350	354	183	490
  2441	228	250	2710	200	1166	231	2772	1473	2898	2528	2719	1736	249	1796	903
  3999	820	3277	3322	2997	1219	1014	170	179	2413	183	3759	3585	2136	3700	188
  132	108	262	203	228	104	205	126	69	208	235	311	313	258	110	117
  963	1112	1106	50	186	45	154	60	1288	1150	986	232	872	433	48	319
  111	1459	98	1624	2234	2528	93	1182	97	583	2813	3139	1792	1512	1326	3227
  371	374	459	83	407	460	59	40	42	90	74	163	494	250	488	444
  1405	2497	2079	2350	747	1792	2412	2615	89	2332	1363	102	81	2346	122	1356
  1496	2782	2257	2258	961	214	219	2998	400	230	2676	3003	2955	254	2250	2707
  694	669	951	455	2752	216	1576	3336	251	236	222	2967	3131	3456	1586	1509
  170	2453	1707	2017	2230	157	2798	225	1891	945	943	2746	186	206	2678	2156
  3632	3786	125	2650	1765	1129	3675	3445	1812	3206	99	105	1922	112	1136	3242
  6070	6670	1885	1994	178	230	5857	241	253	5972	7219	252	806	6116	4425	3944
  2257	155	734	228	204	2180	175	2277	180	2275	2239	2331	2278	1763	112	2054")

(def day4
  "una bokpr ftz ryw nau yknf fguaczl anu
  tvay wvco bcoblpt fwzg sfsys zvuqll mcbhwz ovcw fgdy
  ynsocz vid rfmsy essqt fpbjvvq sldje qfpvjvb
  yvh nxc kla vhy vkbq cxfzgr
  kljv ymobo jlvk ebst cse cmlub tavputz omoby psif
  ebfuvvu paqgzri uvvubef hglk jvn gzeln bdl ziqgpra bzcfa
  tclq ytw tclq tezcqys
  qae eofr yzwcwqf wqm ytcdnc pxnmkw
  krryi irykr ycp lbeed ykrir lhq rdd tyujwd
  hms pii lxoa dchsvz bepjwst bllxkqg hsm yrdj myzvju msh lwnnc
  yxqh hqxy xkn ljjsqjh jjljshq
  mhgsehj urbvnvf gbz ykxsd hsmgehj wtoc ujpfaos eir vifog tsy kdo
  wfruf wwijme pxbbsvf asmgs ccbn vwtc mkhah oxxfh
  lxqy jzvcvd cfgg uahxrwr dqmaqr bwzm wruxhra lrb lmk
  jspgxo yuhgni njzqtn zglkzz ybc itj orqr zgqwuoo mjagh erll srqrk
  cbrtnbx ukblei ienmdm sinzq lytyliz mma lizylty zeumwgu
  aeggz eljcry buqdeog dvjzn ilvw arz vep kxdzm mvh szkf
  imn sfty ugg flqq nydky mni bkqzlok wye lehwlmp xeyfmj
  beyv oamrpkc tebdkwv zlq jes mqvif sej qpsnmjz edvtbkw
  hylmacl wwlra xntnvg ppvb bzof cymllha
  qktxomf ngfpuz qqz malc zxuqz szg zis vzro rfpgk
  phru sxlg qzqlw uej vmd omzga jue
  drzgojf ttqdqar weikik wvrjtxi gbj jramqh nlwoj drzgojf bgabmn xqlaeun
  aiuohu pca apkmv cpa kmvpa nmdn
  gelymv eto itcnuhn ote teo
  oxiz xzio kqu wwgow
  picoyb coibpy acsw ehlirq deyz gymqvz opicyb vuet lrl
  zerg rezg miwtjgw gezr cui
  mlh qlu ktil tnlgnrk bfqbk pgg qxeyd noadmjo nonlsh eqxdy
  yqqaq yqqqa xod oss
  mkotw bznvs xowoofq sebp wsgpsmn fytcpc vvmzr vmzrv xwtxz zrvvm
  dvs twhz teqmlow oqg sjetxd aku cwl qfvrkex mndsio hfg
  itdl qwdagkk wli tdil vlgur dyl xvfm
  wlmyd dwmlx zhmqd zqmhd edzsvmz yclg umpq
  petva gazw byrca pvaet epkoqh nlp vqfl vatpe
  rykn ckr dijkme kmiedj ozlh deikmj
  kuecjh sfqv pojfzf fjopzf fjpfzo amxtc
  hri gglmial lrwbyc memgszu hir cfwlg ylcrwb
  rxrfbtv pwpra fngt auh rapwp zrruuq uah
  cevc utfd ysrfcw nnlg fnqtx aua htteunu mrjhhj
  tvnxznj mvpagt nqmxvnl mutn ntmu eybh vkqeaj frayclp
  ygktzxo lzwwy qsipu nwb jdmw pahao paow mwjd uivqbnj woap nyyogcc
  log zihz rsmx ixfr xwvd osg zxc gol ufnbig
  dogve cnb osa xbafl vefr nxlw yjgquui
  ucyl aaoae ktjvi fyps xvjhpbh iiur tuc
  pqt jasxg ehhs lzjzzzl sqmmj vwyva eklbtv hksanb fuesnd oyw fuesdn
  uankv wesi frhpau qiucu lim uzbdapf ciwjd tifbqxh tfbtsdi
  vgjd izpofu fqjpcct phuz
  cfg cfg rgahl frm emn pbfsmgy frm jemwqgn sfpm azunntj igl
  daps hpe fqg err sapd dci vbzlqx gsigq eyp rre
  iuqyqdy djprdj mgtkdxr pwmkzv wmkvzp hppisd pidphs
  rlr rrl vhevh cucprc xki urte lrr zfc xrqydzk ipjkyxj kytul
  jwbkaee rgyjl rjljbwe ppq plp pfts ijd ckpvmw mbdrqh zolt lzmr
  alw law awl wknavtb esklsbj wvssyai
  aqy ldf qdht soxkg qtfipe nsdm aqe rtlc fbqrk ius gybbhxr
  xteov wgqoqim nlz szlj oxevt xwb
  tmgdst fyn oul tsohzbq ltmxji fgops gatssx zxdzfc talb
  zkvjpu jnhtc nxs jqv pyoqz zsj ckwd xot ykai
  fxfarre yjbxvj lqfaglo mbxuv bmuxv bxumv
  yrhi uosldj hryi fwres ycygw ycvxze zevxyc iyk
  yphev xisbai xdb hzrbg ayxbhdx qnvbus pwc
  wytqraw yvurj erqckl rvrvda xsh gsd bxtm acxle gpndk
  kpvjtu vacggk oabcuoq qusf zitqpgn pbyq ocabouq ntpgizq gaiiry dke
  frz ceozajo ljltawq tjepzp iflv
  zntejm dkfgc ysno noys sony muy
  qdnyvvw oykq bnmldt zjgauw pviurd cbcnl tnkhq ebsey bccln arvwe
  iqazt xidjgom wcrdz itht lfh ifzwkj rwqxhy ervcgmt vojg lzntz ezg
  tlcxioh qvvkan wpi ody
  mhsy dwm hyms yegvvc
  hbkvi wvemc uwxgqf pwrgu wcy wxqfgu qkzppc vxcwdwd rcgp ivjd wmevc
  upc ucp cpu unmr pyod
  bqfew ebwfq paccwh phgc fchhr jrnio
  abh bjalls bdtac zzvt totdlc yptqmgu rpcin bch cqklqly
  bsnss qcsgi tegyz lqoqkpf qvatlyu ibza mzfotsk lye oqqf mnor
  lzrxca stkbn axhr wckbip bsntk ahrx
  oricdw cnpte dbz nqx xloxc bdz fdsl uyvgi nvoy ezbi
  nlcqye ofta jcvqvtg yxduyh btawc tjgvqvc tcvqjvg
  nji znctdfm kkmp pmt ikhl jjoubc xnp zdctnmf covvmsh ydh ircplcm
  yafkuk yasqsyt ytqayss nusgb ukfyka
  yogcf emg jlkd blupemf axg wihhrb ysernt yznhz
  gmc yyqtgp use lohoit
  lclwa ojjkm rxr rrx
  punyfv iphw ddbc jghx lrssszc bepexv sicpy syicp lszrscs vrqjb
  rjanra juh hljdmad scu usc baifu ijs suc bgdbbv
  ogs hehi lgiwowc wwezznc ogs lorcl naunpll wlmal ajcbj ciujw
  slenm xxod vhf amhts
  mrrduda mrrduda lwecwjv lwecwjv vvzmjla cjipv
  ixnv invx inmzz aoxghpv
  ulyvfrf zsup zfryjy xoo agdsd giw papa ljtu rzbjiq wrex
  bzpv svpuyov mklrct uzox
  fxs iypd uaqch kxazj ksjyv
  uxaommf xtq uubbfk bmlq kdhgjzg oxwpag itfij irmkjx ggod sddcyo bopn
  lch plmvfni qbjr dnu zjcod qlwax gejmyj gxjqm mfzkb gejmyj
  yoa thrfbto wposvrm amulogu mcqzfax fjquli oay
  kywj kqqhney ozuljxz wqwfte ubo mjj anhhg aphy ocfnef yhin ywnx
  vxuledm wllj obqtsrr jwll uvmelxd xvj gbwte
  hevc bitwu ydw ywd btiwu iuether gfe
  dzn ssmfpel wbbdeyt xge hrfi
  zebz ifhq euto ppfnrwc winkkj
  utuly wtdt iahpe ihtxwmh zxun bqluj hsaxgcs ytluu jlfnnuv drxlctr myhp
  kwxgy hreul rsnh wdmsx kkajywb
  bond djq kccazc zvzcie hndm myx cmhyhkc ove ord dqj
  zcong tekgn pbzs ywqgqgu eizrx ypydeku yqyjdjp dwsu zxire zcgon iggnvf
  tkioh hflkm fsjz gisjbi otikh
  ccdqqm fdabbne fyd lbyqm cyzgtd puitvjz nluf hirrpxd tgxrg vvl
  hjnygbz fnu twwbp xqw pfdlt uoalyme rsd muayeol ezcq
  kubeooi bxgwoun paogjs twvwlji opsajg higbdfi aazo evmj
  sfipxe mqbkmrn plwqd zvq nmvom fyfbs nbs qkrbmmn eym kqnrmbm
  ruhsp hurps mqws umm sphru
  ksjs pgpxh qozg enplxbn oqzg rvjnaje sjsk
  rbwbvog mhgtgg uld twrqz rbf kpop
  lwho lohw ylhd dej lydh vsfffsm
  icljgu gluijc vthqx orynv xhvqt
  biav elxkct mtaw nlafk snyr cbqdwim blim rtrqmc wze cxktel
  fgpeia ebkhga azlfsr bsj pipvwsd nry bayrjzl ftth ynr mfhd
  ymlowur nighqgk yjv pyxf nan xamb ohm jvy owrxbg icbz
  iyyt linaqu httt zyfeo udap mllq pdxo lpl djhqaou zkit llp
  dxspk yge kcqjqpz ulb hoe mfx nwayo
  rdnmmh gyqd qhxrzj dgizu lyittbv txngpdg fiu mwd ndp oks vxnxybi
  eul ztpe evnz yxx iuwon rkbbsw liy mqhxt
  qahp zwn ualtk txbt cbthj xchqy pirucp povdwq
  mqwu mwww muiafa miaafu hzripx wmww
  auw hyyi ylewfi ihva jknbrry voxzooq xaos xymv qzzjw hjc
  enpb jqa ajciy cbeopfs tqrlqj ecudb gso cyjai gxoet
  yohkjj yohjjk xapawgo rtgnjj
  lnlxxn nxllnx vhjrev uoew zts smkd kynlrg
  bkblpr vgafcy alju aiyqe eebtsyu buve hdesodl pspbohw
  aacmw qpndwo tcwsfqy qecnms wondpq sto
  wdsyxe edsxyw jnzruiw pfqdrhi
  pfgxekl vswgxhb qyn mykn jimiatq zkcz jimiatq kaexgxm mykn
  xegwn upudt dew uqjrcl abyof hbtiko wxgne sorgno etm
  xzojs zxsjo szjox gumjtwi
  gttngkk bwdgce bhuw fgo rcbf byw
  ngtzwqx ptx xodmy ctmtf oioahmm qajlhje jzilpk cvypp ijaefz
  arlx slcugvm hyuo zoptsh emyr tndx rroecp tdnx xea rtkpd
  sfckdx ktyrjju ruwjtp zhqznj vncun
  oqqh xpc itrdg gtrid hoqq tuo yijh ncp suvck jic
  brrlqu twdt urblrq twtd
  brfuh arwtkpu mzlj wdyqk
  pmag dtwnva nect azld axqrwy apgm xbv gdq ienubsy andvwt
  hqb qbh gxdl mwjn cmfsmik
  yiwma utlor qxjfjsn aomlvu gxp ryj rfkdsw kuguhyi qxxpg
  ifq wcvcgly jdalgzx lgcycwv rajmnqw
  latakk yxxbw evy vey
  odkvw ojgveb jhg qwhkyoh btvu qbfot nouv owgtsi pdwbmfn pmvcv dnqbo
  tmka qqnty knz swi fyvmt ikcdu jfjzsfu dshgi cosacuj szjjuff
  eanet uua fbztk bzkft
  jepi hyo jgzplr jyqk zzcs iepj hfamvu bfgbz sjsnf lprgzj
  mlca ywko mtg vkfv ojka zbelq qkaujs simt kafq qtycfzo
  sqh omv llvra ecvxmtx dpnafv llvszx xzlsvl quj ufnhvod faraf fivmnl
  pvxlls fzeoav ahgv uhq nodcr cohy vqisgaj jsfcyur dbohh
  ztccbwk okv vok kov ywel
  xyu cmaikc jgqu ozccdzk ybn yoeq fky aetrkj eyoyvla laiu cermo
  sssnb kxly mgvaz jpffkq bysbwwu rfnkm eycp ipyd hyi wjew
  obdfqmh flkm semednj iafewg lvh uwa ciepe
  zmldp ykffe gtehz qlmvule edrtzg prund oagwto qia bvyxur
  kjok neno qbxh wqgkkt ympclso poyclsm cajgnnn
  paqili kur sfit jbqchzx bhjqzxc
  fhghm ubtaana qbn autnaab aaaunbt vmz
  exlrl hfnpq zgdwx smix nyg ogccrhj iimhhwc uhcldo oydwxp kqc jxxpycv
  wtdqjfh ialoqr zeej ipoh qtjdwhf wdhqftj
  jcgug cmtvmu ynhnilj txlv uemowyu cvrool oolcvr njr cxqntdh
  uhtwtp tgnc jmmjl utiu jfxtsoz cxwqcz
  qntxl lyownp tsp tps mixyge rqfqumc bxjiry zmaj azjm
  abt bat tftvm nyjs jyns
  hzsdh pwthfvm cedg hzsdh rsxtehn owh cedg
  hcoiny aqbeme eeuigt pocpvox tiugee rwb tvgmyc ojg hgdaf
  mzlwcfc uoobo bouoo tvgvmiu evsfkm popgm evmfsk ehxvits vebxbmd qhmz jzj
  mypgg jbpx vgeb ahvjl zbm ancdzfy wytkcq
  bivscw zmzmjxu jzm fwb ujefxp jzsiskp cgx atcj sszikjp cxg nqvxga
  vvurbxp iry zlz gfnlpuy npyugfl
  fpmee mhj iul lui liu
  xjoesn ggsdc vnisnmw kyxmmv xphfq
  zcsml ngzlpy udaoab eotbv ylca bfmums izx
  pdi bpyoep cofsy qazl oaovek fvfbe sotc lfdmaea smvs
  zajm bskaqhj qxyiptb bdyeuqr dqjrekn iywj
  hzhky hukvpve iqcbwju nyiwb rvutxlb hyuah urnhxg savicaw hexr ptedk
  qndji wrr sin ljogf ncrb zvt tvz
  kvfke tjpzhrl zvd doq kxyw fdgr oqd egkybdh rqpfxks nja
  escstpv ccc ryzdv gxkjuyt gkhw jxnfda awpzg csestpv
  cpcd onxeae nimbrpt zyji qnuo ktxgwbj vtjfglz skcozd zgii zgii nimbrpt
  lwq iue hfbv hgbg aeqc
  vzgbod yjkoc ckt bpiaz
  eyco ecoy uzousjp faxj utu yoec
  fhqdi myd tvex bzizkcx pifcfhz fczhpif eofzv bqzd knbhbgj dok ffcizhp
  qlqlgmz hofmnc cwtk ahgnpne acn prwdh hwdrp rfofhl atavrf afkcbk
  sgl apyfr pwxzptv osuwy vmqqh soyuw lqilked oinhh
  eglqdox gcxfxql ejtnwu wvho can eyu uetwnj elgdxqo atvpkk eailsnn cwosyn
  mylxhuj kbc apnllw qbmtj sqy hxtnvoe ins iyudo aagezrq nsi ikvn
  lpmzo tkdeg zilkm vdkmtf yulbdd dkfmtv
  fzrv grq zfvr ychga gqr
  vdjxx wew pdxgp cjywsc meoffrj pgpdx chxmw eizgz ruea
  iaklyhx btqqik tbiqqk ynmq laxykhi qatrnsh lnmtm plz
  sfogup jtdsx tsxjd wwzkyy wzywky vgdsgr
  paupqb kyy bccklmr vopviup lctcxza yyk yky
  gduuia doek hqcr upvb szeewnu rrrdz
  lhnsaf lut kzf allu dvj tyngx zkf aqsgz rtkzzdz
  xxqj msg xxqj ezmm tmyji msg cco tain ctpel
  pvcfzv rhn hlhxyu bghzzpx dlhvr hrvdl lhuxhy
  npzhkp voghdv rvezqff hvgvdo jndf gpa wevrwpu
  faixq aecax hxdouer yqlngzd grf wid iwd cce xnmmr
  ifqwiah dib ibd dtvkwqj mpn dtwjkqv kyntq xwlv
  rwoiz dja cvv lvza kfdblq bgtwgh ongdub wggthb lvaz
  xajf eyasx rupsyqx wubjwx bsrqi ripghci sbzxp sbz dhooax
  ydnv tvhwgp uvrh yryhl yxdlwa ladwxy awi mkwyn ghvpwt
  qrl vwdvwic ocbhpi bcmz dor lrq hokg gokh
  adz echnlju ebnmw twjl cfw loq fqklyyq clgqq jtgpsu wltj
  vwlgisb murtsw ldkacqv wxfcke vcqkald ldhh gsl kpzn
  itnvo lyddd saewfse spnri vtmst iblx
  qsgv qni wvqiih mneg lkpb quhbkyi
  efwaaa huu fslzwpc uuh szflwpc
  sgmj ajh vcwpcua enltaks aquooh bwoda txbuve
  vbe astgezx xqbxkdj evb bev yuuesdc fvohzq
  gpn oqxfz pbwibjw gljdbf gbldfj sis dpk iss
  pha ebybvye ntxhs wcuce
  odnnywv qpcjll aslxqjm injfs vkbturz atxi
  zzncfj kbhk jzzvnwf kqipx qkxpi rzb czfnzj
  ygu vqpnxkw trdtv rrte
  hrutley ljxuuq yonbpmk hmpc etyrhlu
  odxp fpvizzx dxop jjbr skhxq mpzawhe zptdxuu erxk adbbuk zfzipvx
  qjovasi yutjpg rcp bykpctm fqmmg pckbymt hqj
  ssqc cype tkowxb fbh rsluu zjk scrukwv pkuexk qlgjtdq oulrke
  bkcd nnf hdj sdlweyr uyf kmvzq
  sgeg moy png blv bblree ufe uqknuqd lnjwbh
  snpex hrbcfok pffv cwrvhcs fpk uprhn gbpy zkxyi esug ccnnj
  bmwue dugcrdu uifiy clid rdmodec jodp hti xptj tpopl vuwhdyi hnoq
  cwlkg qsz nnp lfyk pwn dpe oeuzp jusxxkq qlnchc
  tsmkvge pxauyc cxypua boi hybq rzf iioyi rtedkc gjmk iob mqb
  cvip wgbjhe ulwg jckkwd gdu bmaoisp
  drpl xbliszf rpld ngnvgxl xnrd xsmd oetrcmn xvfohp mtocren
  habmf dmfxq qitw xxtybla cxvb colcvpj sowoeuh bhmfa plccvjo naftjgw
  cix soo icx ahx cdrjyxe htcnp
  acoksaf cgahlg tdj euchwnj bdn lunouq aewrk uktre kklwqy lnouuq
  ibsel hwjbah vxuk bjxa dvzbpq tffqvo bjax qfoftv
  iynms tzv jakuuw cmz cjnyr ddibtd gcb
  tmgerk pvwizc lxoma ergmtk xmg loaxm
  ajazon yjwt viyemnk wobzwwm jcucn nopymyq znaajo qcjtmlq ccjun ywvt oqczod
  kdhgnv kdnvgh rpyrxx xpyrxr
  qftmshx hrbr kcggxw jwtuk qxbzkn
  ddi fjekwxs xxua cmmkrso
  ptrsv favvfh innnnx nninnx
  kzsnd pnf evtazgw wmjk gvxp bslajo
  nphqtka umlxu ymw whqiab whqiab vwigkz pevtps
  vhje cnu uzfzum rwucy mxr wyrdqfi gnkuwz dkrwc otfc vbfc
  ubtzio vlijsst anruj qyntadb fnic klz ywqq fnic vlijsst
  rprj ybyqawb tgeieih nzcr rjpr bjfpozh tpevsx flvjdq
  kvqqzvm cfdm wzjmkq sbcfx vzmkvqq
  zqtt elpg eglp uoe glep
  lqv madhtch xevl fal ijmx chcpra lzl afl cndbvnq
  yjx jyx xjy otwklfj
  cur egsdzaz ocbx wvky coxb pgiysbh lclhvy gfu oxbc vqyjvhh
  gtd pytdaz kecsku nkiud ytt bmgobx tyt pfleji ebqlifv lqp ytzadp
  bopfdvy eovszvw krgu jhuhyqi kaczafr krgu zlfxtl
  yixprs zqai oprgw vcsjoc pgorw ypx ijo urjcjqv
  estg oqnhw xgwajp mpbsag ibzi
  zggbt jmmtkru sye ybd ztggb
  tzryuqb blyxnnu sjpmf yxe zimf uyzqtbr qbyrtzu
  rzivz rxn invxqd nazw efzun bwnw ywx rfuda jhnoww mketav
  zxfw zcaqi qaciz ktefiwk xwzf
  ntougl fvyaxfr obml obml bjkm lgsqj yfcggdu rqcpgt ntougl nveto
  rma dakifg pvfc ticvff iffctv difkga
  wpnt eclov vmmoqh qvw mljlvnj hxrx
  ijnpo uhgxrfe xxopw xuzwyd powlpo ctduj eepw gubnepv
  rxcmve myxckxk ecid nxe xevrmc juzaonr ilkx zpb pbz mvgos yzr
  yfecm wqkh defqqa mnzgx nwe ixxg rjdhe new
  awztgx vqgnfd iwcakr ajaiwn jiwnaa uqfrim wrgbjon ufqrmi vdu yjwy gwkdc
  vrqf yzmvnr jkjji ghya pdlye ygha qlcm twmkex frqv
  hjb xgypw vtr mgj asa owcuks fnllp ommrhng senv iix
  usw iyuatv ondiwh neac ttge tzw bvjkfe neac usw
  qwehwhj idrwo vex zopkjd lrcc sfqyz smte qrfh lccr qwjhewh vlb
  efnlhsj ltm xirn nctwio cin
  zocc cpfo czoc oiz tftk
  rlzvqe inivone kptyumn eatw urjxc aetw
  qavvqa jvvc yux cvvj
  bfr fcpc xpkphcf irak bfr nuhsooj nniugf bfr gllq ipo
  ekd dydxs rnmgb dek yowk
  ipdll wdgx gjiigd uleiwg buvv vdhuzg gigidj gkyigmx lpdli lzyode fqdpvms
  ycna rhyz bsipz lit rmc mrofb cyan mrc wujk
  tjrk cwdsvf srkdjy lsyvryj nko syjvlry fgqq srdykj pgb koh dyle
  sflcxt wmgdgej akllaoa bbsvve nsxnt nsxnt kgm akllaoa btqbez
  bzmoci agemx mdtiol pyohvf zwtx aqblx oibmcz ypcmz lfg ckssn ozx
  cuojke joekcu eusr dxqk xxwob klpsm
  byspz lyunt eojrx ubh rjxoe ypzsb
  ibok bkrtut wzcdk ppm qekhvy aupaic vswwul lmlxrv ainigy sasurbx
  jeigcyc cycgjie resio ncz
  xvxr lmlaje ebmtn cvms xrvx vsmc
  cfjbffj xvo hpsbu nfm jhlsk mnf fmn
  xncxo iwuon voj aebv jks nynzl hwjwo womejo ugzmr tdfaep olpdiaf
  jlnusc hgpprf nulcjs kwiwypu kitjjbj snlcju
  buqzm kpuoxel ajlqke qqdur jecuibn leajqk qudrq usi
  ahbnjf uuzecdv yfyrsxv eoqey oonue vyyrxfs jixmvao
  wjdi cfgurdl usdnlk jmao qnus cicxnux vtdxxkx nusq
  mlvfz twu unj mar qpiz fhjczpz ytl npwjys ppq koa
  ippdky pvwthzj qlkhl pvwthzj
  kfm lcedomr xgdkrzo hfxyoe rafcby uwe pzjyuja weu nso erdwc fjvc
  peep oufzlb fsgn kxj uge xvufb zsnrxss lere gfsn gvwajkj fmh
  mexvi kgkktz kgkktz auyln ghvqc mexvi
  wit qxtewrk qdaz oljeb wowb suergyt hxq pfnfbei rdu qrxkwte fyw
  qjjzkd oxedeu uoxbehs zder vvjnn ocxkiz wkblzy eyzksc waiiqo fut raak
  dhojys qkusuxs kzicui dcsxo
  hsnexb yoz inza gqxtbc rya jqfe ufzlqva
  scpquf gbloz ceol eclo qpvzavo rwfnxa
  jyg edqf vdpsihl edqf
  mbyjg yjgbm mgybj mhgi grw
  ler oxssrel jhw jwh sfa hdhlo gng auzoan
  hmkuis iaxf muhisk ikshum
  eodbpo prajasi zsu hyb npr tbcntup uzs bxchne
  zpyr kxmvz frlzwnb tzqrw vdh ndbwqmu cadwsv adq bzfnrwl qfgf
  dybnn dmarc mknr fovokgj ukrp cuwag
  khweq eljs rllijp pizevm lwws kehqw mkjcu otqodz
  fvsrb kzbjbcc kzbjbcc mee dhyedb kzbjbcc
  cnlmjd dvnd vhlvsa rsrtc scrrt tqhx vke jqmxpd udkjqc qxriw pfqpk
  tyureg urteyg rutyge rmoihs
  pccxeak tkmyane qqggpr tbdmpi ieb
  wjucbi wjm hais pcxjd kkzh qns hmf mhf mnsv ifigsc
  lqeyr pnopcig cpgoinp pncpigo mjfkjus cko zedvvyq
  ofsnspv goj wqm ynolb qczly brl lrupzg buof zladwte
  xzn zxn yaseulw qwhxb easyluw vvlmh
  aiybip ydfczwh wkl rjsu xreokl qov mko pna fkfu
  zjlwozs nxke ozwlzjs jybx jpsxp qtkll idsrad savpoe
  xph lpvkmvy afq uqhg qqjgm smg tmhem mxdyqx bvhot lpvmkyv
  jxkkzil pkjheow fewr ggbfy fuol cuzud wnx fxujfwh srjsmic
  lzbjx vfx sncis xuv unoa nlgs stdhf oibxsgk uhogsb
  hfqzkms bzyfnz npcd yhfdo myqukug pjq adle sqkfhmz
  czhainb iazcnhb hhaqr cyrwj zzow luuvt zdyhnh uppysr
  fyw dulbxa drewqsr tldlaac kyaw datclal ylplup hdzbj
  kiiv tly gua lfg
  gphbvwc lqdd jqja ffpkz hafal eiapksw wsikpea vphgbcw lkcpm zjxcx
  viapp rxt vdgbm ezphp pcqr bll mklgx epzhp
  favz bwmczgg zoyns pens wpgi mrwxel
  ozwjjn kbzaozc cuaa igbfyq swi uypx bczaozk pyux odvawqx
  npnpw nwppn egnpj fkzh wppnn
  asu mlqmwa npewa cjluw qmalmw newpa fznx dzypi yiy
  hleh usw bgmgscg cqc fijfuw idtyh cgmsbgg zjhr wus hymbju
  tmre fvm cgowgb eduyfla ttez vdzp vtmtaog ezxsfi gyxgzi pvzd
  acvarlu hkmfzdg jsnil hpv wjj rljpk pygl wjhhohk rkplj spvgx psgvx
  wyz rvuobq kbmhaf bec bec
  zosyz psuo lgihdo mjtftk fjkttm ddmcd
  pqm qpswpb opviwxg ppqsbw waco jpx
  yzgumgq kqv hqjghnl jixhoyg ufer kvq lzi rojm gbhvsd urd tuy
  sxc jndqc ozpex wkchpu tmwv utcxzd piecpma cmppeia
  ifjc lttj tltj rxmgxqa jcif lzhxkg zqb mdq kbjavv
  isyxn zjbj uiw avziqxf zpezazx iuw
  rjaudu amtpuk gufogys xiqs
  gau sndrkv cmiolti cdxm ikkcisu xusnfbp jxffy ffcizj psye sgd
  mvx onzmy oynzm mwfgvs
  mrdg roanty dljs jlil gzcj dqitnfb gxb vzzqf ooweeh pgs oyntra
  yna xgok fvbdl xgko udqp sorfo hmhl yan
  kycl ule blupejp kycl fhpkoe pqkptw cfzpv nkncl
  snugkyw zfdbsfs aehb olq kkoi xpsvy jqcspf lajjyu jtm
  hifhfa mii clukcbc fhhifa wcts tgai vvqsyr kclcbcu
  ordjftj dokk hdhytwc fjodrtj ojrjfdt san ajxrwy ked jfrqc
  eylx cohd biswq xgiibz gzcptbf eylx icunv bfg jqanbv rntp sbfkiey
  kub gdpbdms qnnto bit visqop
  tywk clicj daics cbuewkx yyjjjka vxzk afsdyqg
  bmxzll wqjnyr mxlzbl yutkaa qmpz hiqkf lqqqle jagj qlqelq
  jgdeatg qubj jsu bhgbm swmgy lwgnuh qjbu dqwiikp mgwys
  ryisldg asrrhz vxvrnk ibjr kebyx dwbx qdrpa tgakt
  dfvgzk hifan dpjdnqc ncnvf xmqjjao npjq vobt evtaety kvufds pcugx oyjo
  ezionjg ioznegj cykxy igeojzn ezm
  dkv dkv vfqyl dkv dtjhrem
  xfgh brsjcdw auvq fibb gcbecl
  eet qdnrymr ndqmyrr tpfqxoi kbkxw
  qhmaj maukf uygg hqmaj tfmtv irao wsari
  ofoywus wxs leemkn wfko dwzqv txg qsiiiss aiiffe fadmdq zjtaovt
  fgfms oazi sam msgff qif evxca reho
  gpzhy qxh sco qeax wtabxwv sjd oev
  xsmpi wskvku xspmi smipx
  ghgf tbpeun qdivuvq dump umdp khxcxtx laljpv lownp egovve
  vhmu eziabt hnz neko nkz hfmizn
  vqhb lax zzyf lxa lik jrqynr rgcos
  zjgbfzv ieufyz kjxad qxeuewx
  ufl drkaac hoyic pqutop wqzdk eewabsr mqspcr ewarbse dzqkw
  bgatanj xmddwv mwlmw scgzboo myignm lkfl fsqr
  xkrx otjzfk wek dpbwk cromg fclmhg pkvw wsln
  yyqjs lifg gifl cfv lfig fycza
  dfup fkfeiqq rcmuv dly iforzi lngkjc rzifio oiifrz mlhor puwm qrthoa
  nzfaeto punt rtmlg dwdk hyig
  mds ueoyclh lxb axgea wqt wwqqglf tqw yvzji ryr dst bojf
  ayoj yzj lyctgnc woxz gqltw lkzkwte wysb mjyeu hrwso
  gymmvtt lhskza lsb nhlijnt men zphurrw oftksy zxs ykerwue hnijltn iknqyz
  xuaxkc lgzeef fwb nlzzhjj lsieg qdr ews rue rdq
  xnf lljcmod kyuercp kvlvd lkvh ncn afaq
  bjytofa ltz mkyy bwt uxca somiz rhgdg keaqu ybr
  aipljn qpq nilajp udgkchc dirvxg rrbmi mxfxkk kmfxkx
  sfzjemk hjsvnmb hfd hprfmvg pbhkc
  cvxi srj ucy yuc euswuns jpox
  tajlnn ivuecv fdfce rakjq bfuxirh eibde tajnln nlajtn
  ndvm mlnhy bfqlzn nmdv ohto
  jysyvwy xbcyt lbbm osoye swwtwa emfznci qnzc qsgk
  xcm jbqsuo xmc mtrk bojuqs
  ukshrrh xhfl ooxgq vadlcrg ydva hugplg mnqbd wkyouq
  mnmqys bhws megar krgoke modxe krgoke clovh dlo
  ejl qzc agxph jcn zcq zqc
  jgh yhh hjg jhg
  tarm jboyg gbyjo pgalg xugzt bph mart
  yur wrrahr fnnfqu rwhrar cdq
  mukod gueg guge epalg xjkctt
  hub hbu hbzul buh sqfl
  xyrly lvpitr xfzn jjcl uvcnz dnpdyzq ifaiwe zlvzcx
  wxzsf tgd khvwp cmd mzv bsvjvjm wvhpk ublnqyz mvbjvjs peishcb
  zunmk hxpney nphxey znmku
  bfxlgur wftcw xfkf fsik xkff ffxk
  sxyjzr ugcscx uiovqx ldzhord xgxbfph ldzhord prdhg rhdhzd ugcscx
  udg drb apyjq dgyevo fuxjhg
  qshbe aigfdp wyvz xfcos wve dgfrufw dwimmb jfh wfrjbzk nwgrigr sbrpbb
  ahpn xnzeof wxbv chxpcu jmso age ojsm bqonfki hqhrkw
  mfupm vvig ndqbbm jlw
  ejqh ebcrytj zpiqtpp ogznd
  wkwkae odq rsrnqk nslczz hiyyhur kuw mjbuwll vduvod ryhuhiy swo tsos
  znkufyx jejrdno knr wkx ikrlly tnxtj
  iizdiw iizdiw hukep rwj eaq ptm klta rwj onaz
  znb egqy qqnc igqr ikza ojgzkr xaen kurb pyckxvt wqx
  pbohpw bphowp dajwdpp kotevs
  hmuvxu zdnguk jhcmj gccyxiu cxgiycu uyxcgic akxi demeff
  zjr lupzwcy puva rzj
  cdn wee iqkbhl jwxo nhl cqd mqgqf ltdfg
  phwco ggcj cggj ergpqmc kcz
  aryjl wqwmkc aklktpz kptnroc mckqww
  jadydt atjdyd tajdyd owswsgm
  dshqt kacoge sdqth ylobih
  kdnik knkdi dinkk xwvqa gvii
  cifbkpt zye xhwnrhm fctibpk sbn pdqry emkye kzyjpa plzkc btkfcip gcchi
  kekfr fufp dfy eqebgko obbn nuh
  zixfbus skuf bea gimbqq caubhto eba uvkovz xisfzub peukmyn
  okihcgh gazrc slee vlnwyg geviex eesl nmnvk rcbv ycupyw
  tcvlgqs wxe lusvwzy unr yzluwvs wsylvzu zkwth qdykv
  hyenkj ugao vlwgb putcepr lyeccs fqdotx burf aqew fje rfbu
  uhmnc zgnkarz gylqawm abl zimcz qbs zzmic
  pxkbpn zuxlwtt rlbhegv zlxuwtt ooxpr pgjx
  leg wavgps fcplfpc xvxih ueskmi dvu wbiq nvtia pwjojw usiemk ojwwjp
  zmrpknx xrfpq avque tvoyqp
  lyposyj mckyoub sqbl olpsjyy hjafzi wmojb nvezofd
  yflxrg egi aij qvc yflxrg typbs yflxrg kliexy eqnj jqrr
  gggt sht kdajvz sht gkqwaot sht vout
  ahl aucpih feig man umtchcv ceqabr tfptb
  ftlywun voaorf kde ilwt hlpoe pksqxyh vpg cxo xgq fdkkl sgxhnq
  zzekhfi akb lupta sgtd qapznzf lgidsx lidsgx akgmq ettuwjq xyumf
  dxhpku lwoxpim gwb lhjmoh gxqapd ntmvc rvwwszg pvin lwoxpim coubc
  qia bxmremo rjf vaeio eqexwz wuoz sguf bsbusof xqeewz
  iczzz krf hbq tsgrih mzjabrt sfnwrm djignf zwac cwaz dgc nsrfmw
  yvarsva zzpbp yai und kkbinr zlyj nyxxof ggrgu vyk eib
  nepzm yrrgr vrlhbv hykmiog natrqx jvpq nbyhe zuo grx nwl
  cfboeev hcn yfobyx cqvlo obctww xxaux ofybxy wouguq avuztl xmgqq xyofby
  tikv uvzp oxopqy reh uzvp wefka vli kied gngks vbz thfsxyt
  exxvknp pucbdyl dboto tzat qze xyinygz mhzl ubahr ekxbtk
  jcz ufszbi pknsfgb ivok ijau okxolj etecn aurun zsa gbxs uryx
  ypnb ousd osg mvset ipffzdn dfinfpz ltescx
  taeoct aoetct aocett ttda fcdqnxv
  bimtlk ssax bmrifkr vfxdmq hglp rgzr zpvk zhxtq rndwy mmr arkr
  bwvdb axxbhzk nwfmbbu kzuc sahv cvas wdac acsv
  xavkwou yvx ouwkxva otbe uzr mmw atq yiy ghavd qta pqlhv
  omzht vsdsc zhtmo hmotz
  eqt wtveez syift shtfnc hmckjxa apwbvn yme okdl hbihdtv hxahns eetvwz
  rokdg ndjw hprxjc viys mbcctod dbvd
  lhzb fyxf xaslmi sjd vqp grxhmfe snetfv mgivd uaknj givkdi
  gxkxl kqcdnl rna jhcuepd npiedg djcpheu huce njryw bjluhq bvedvl kqxu
  sogh uym atpzuwx vjgbe xgrvkg thgbyn mptcebt rkro
  tnpxw uxrqxd lajmsmr tnnlt vrvbf deret hkmvrs eubvkn kks hjq
  rcdoa zfja vod supip dvo
  zbxdo xglqv how mgoq jqrdou pwrminc lidi nfs xglqv lidi
  ldmnp dnqn ahhr tha mvsbsfh rpm rgus faf tjash
  ewrdol jqhfpf rckj mrxlwj redjg bmxprx grla
  rhr jksebwa vtu skwaejb vut
  wrx iqvrjh atrt xrw vtqo tkcasd xedjh zkqrh vvhj
  owc qlzygar uajwwe obzl inxawur
  crbtrf phvy nzipo rctbfr trrcbf
  vwuun wcfhhzo vxxjdt fbf bqtmmhs bffqcna
  wkxfxmv zmrkyh sggw whwcw zukynw
  lsdiy lnbn kblxi qfyib irfl mymrr zqbl
  gwdkeu ghn afry zxoz fary uzntlnk kee xtnop ptnox zngoran
  lgs lsg sgeseiz gsl
  erpoqpi svtnv vsogl uym amzxbs
  jtmodqx yjcsfcl zosugm jrekfdw xxbdqnx fcha
  vio tlfxokx xaoq pydeiq glxsuzm honifvf maiuzsy uizsyam eco
  ophcui saka qyt ltti syw
  qff qff sde ryv
  eiii jazx nlehtx tnhvxl rzvsjo qkupif feypppe tefxr wdjmlc
  pdrr mwuy wccd rxla drpr enbbap
  pclml ubwdbz hfudj gdpujfm ovabv
  uink ffebi wdvhqzs qiympf lqxihty vnsp wdvhqzs hutxkcs lxfuos hutxkcs
  fycoaw palkpz yrkg kappzl ncjym mergg kryg
  eqy npvgh ghafkro piqnogb polacs qye hnvpg
  dxyy udhmz jij tqsuic qxz erctv
  urum nmbr cgek eetmhj gxr oxgukf wzdmvi oibzt fxkoug rcrywcr rglx
  jkp ofej waibl opqhmww ifnczcg jdtkbc lil isc ill mylvuv
  vqbcosk yhhsy gasmj bspx peakt cjtekw hvzo ywe qcvbosk ohzv qddt
  edq llbvsx vedyvlm gou wkecank rkgf ziyrr belgo tbz
  wbome vhzf ztk zaxiu ywjb supuf beq sxq spuuf pufus
  femu ymkdoew kjynct aia
  yjymr orovqj aremii licw bdtnc
  uyade fbx duaye ujtvpn
  yzvp pvzgjp yofcvya gvkkoh cafyvoy mhsm okhkvg
  xuh qkaf dmi imd tzmlce mqkxj qilrc dim cadotvy
  azpqgb kyc aflgyaf laagffy kesmk jzyzaer taf bpkbzdg
  ogd dbdlh dqt zaaloh
  exal vgnfx omu omepvwf szcwq snz bptite bzqyxl khmblyc sse emg
  yqcbwsn aihhf tqck tcqk wqwqy cfez xahpn
  qqbuf lil ies tqu pyxhqp mnfuk azj
  vwma rzdtgl mxbasw nwgjav mwav
  itpjfq rrgyt hralwm fqrig btwcod
  ydjd kmk fvwr wrfv yvhw mkk
  xbsxub yhsj xzbuf ace xubbsx fzuxb vxk
  ttsist vubpf mhwkmtx vlj hdsva kmmhtwx ukxr upfvb tbma fxsrnxl hzwufho
  wckjvz unmtev egxts ihw topvw ptowv rnihhmq
  gpdtl kcric nwg ssbs qah aarp ydsdty ngw
  lzhxbbq oktvcw xbasqe owtmwgp koa gumjie sodwrp hqsw aqh dtgsbb
  xjbyy mxfxa ogvk nqiy qyni ldqwryj niyq jjixc
  uhbul daccgva xtiz dim uhbul yjmakv yjmakv
  huo esajup ouj oju ujo
  eeeu hwvsk jfkmds okhi pogskfm itdlbll
  lpyubo dylpfb iehwug decj ntidy cuygyg lalkb iutu oxgm imn")

(def day5
  "2 1 2 -2 0 0 -5 0 -3 -5 -8 -2 -1 -2 -1 -9 -10 1 -11 -5 -9 -7 -13 -19 -22 1
  0 -3 2 -9 -4 -5 -15 -13 -30 -21 -4 0 -34 0 -31 0 -29 -42 -1 2 -24 -16 -16
  -12 -22 -37 -16 -34 -46 -12 -53 -12 -23 -44 -1 -29 -9 -52 -17 -30 -60 -5 -29
  -26 -48 -55 -10 0 -50 -1 -8 2 -37 -74 -63 -39 -7 -81 -33 -62 -59 -20 -58 -54
  -23 -19 -80 -39 0 0 -92 -75 -24 0 -73 -36 -14 1 -102 -97 -30 -105 -99 -84
  -46 -67 -88 -86 -94 -53 -88 0 -100 -86 -11 -93 -99 -21 -2 -108 -6 0 -113
  -116 -127 -42 -131 -124 -24 -56 -63 -130 -118 -52 -139 -43 -90 -123 -7 -93
  -117 -34 -59 -140 -103 -52 -115 -83 -42 -92 -48 -82 -104 -38 -2 -28 -150 -39
  -30 -71 -146 -55 -114 -141 -158 -55 -21 -121 -142 -137 -119 -99 -113 -99 -33
  -99 -20 -129 -83 -64 -179 -182 -43 -86 -50 -135 -186 -68 -100 -181 -22 -106
  -178 -157 -46 -41 -80 -166 -77 -81 -144 -132 -81 -11 -38 -57 -69 -13 -79
  -146 -1 -165 -52 -134 -86 -160 -97 -220 -92 -200 -145 -175 -138 -205 -127
  -165 -155 -211 -134 -31 -118 -190 -40 -182 -96 -134 -93 -84 -76 -34 -33 -203
  -16 -245 -167 -102 -5 -44 -239 -127 -255 -116 -61 -140 -238 -69 -254 -203
  -178 -229 -250 -120 -109 -153 -108 -137 -247 2 -151 -270 -164 -62 -186 -272
  -190 -180 -70 -179 -38 -208 -215 -151 -156 -62 -57 -275 -182 -169 -264 -70
  -279 -55 -287 -57 -3 -67 -155 -213 -17 2 -200 -291 -179 -175 -73 -257 -47
  -118 -206 -93 -293 -199 -102 -118 -188 -66 -288 -21 -204 -80 -237 -175 -297
  -235 -168 -262 2 -162 -95 1 -286 -318 -9 -213 -159 -127 -175 -266 -240 -268
  -245 -196 -281 -86 -202 -127 -144 -157 -333 -122 -230 -182 -38 -296 -12 -224
  -123 -40 -6 -324 -135 -289 -85 -179 -37 -58 -125 -228 -124 -250 -73 -35 -286
  -267 -257 -348 -83 -3 -98 -99 -273 -118 -310 -23 -299 -96 -51 -273 -79 -112
  -355 -48 -219 -10 -103 -18 -201 -108 -34 -362 -165 -359 -347 -157 -148 -20
  -344 -66 -337 -387 -62 -125 -4 -355 -322 -263 -381 -108 -25 -262 -425 -100
  -54 -315 -221 -268 -211 -321 -89 -124 -297 -22 -162 -117 -430 -152 -373 -256
  -37 -61 -59 -436 -377 -346 -245 -167 -451 -392 -382 -248 -254 -382 -249 -267
  -216 -205 -310 -326 -144 -107 -65 -382 -79 -401 -370 -221 -283 -269 -64 -207
  -262 -181 -146 -52 -169 -147 -225 -179 -215 -116 -115 -37 -227 -250 -228
  -132 -414 -425 -230 -224 -319 -42 -353 -285 -38 -145 -263 -25 -142 -296 -267
  -43 -315 -352 -105 -275 -354 -66 -414 -464 -215 -107 -267 -394 -10 -27 -315
  -286 -113 -454 -400 -468 -245 -18 -427 -479 -281 -43 -29 -15 -371 -127 -371
  -251 -343 -267 -355 -271 -68 -454 -532 -264 -513 -170 -484 -85 -329 -389
  -317 -382 -535 -169 -395 -53 -429 -394 -465 -250 -419 -434 -84 -130 -229
  -496 -336 -388 -412 -123 -502 -205 -367 -224 -40 -551 -99 -394 -321 -515
  -260 -410 -518 -22 -23 -259 -397 -306 -199 -157 -49 -298 -176 -564 -271 -6
  -297 -514 -432 -455 -192 -95 -447 -237 -571 -543 -229 -405 -282 -235 -380
  -25 -603 -335 -94 -533 -463 -396 -421 -393 -588 -376 -152 -328 -460 -90 -315
  -533 -207 -590 -100 -588 -574 -259 -183 -522 -424 -272 -341 -443 -217 -143
  -26 -196 -632 -520 -606 -277 -176 -547 -564 -444 -228 -223 -115 -200 -616
  -576 -398 -157 -78 -586 -12 -650 -239 -152 -20 -366 -100 -478 -666 -247 -105
  -230 -218 -48 -238 0 -387 -660 -542 -189 -339 -577 -527 -273 -565 -230 -578
  -147 -106 -373 -513 -8 -465 -66 -408 -351 -357 -119 -251 -626 -81 -575 -542
  -193 -219 -189 -635 -77 -517 -608 -309 -716 -712 -287 -67 -312 -334 -584
  -687 -488 -612 -42 -180 -726 -235 -606 -538 -470 -477 -504 -278 -24 -435
  -610 -540 -646 -503 -151 -350 -43 -699 -459 -516 -424 -343 -297 -460 -592
  -30 -614 -125 -425 -180 -73 -550 -361 -390 -380 -518 -418 -305 -326 -84 -675
  -320 -557 -486 -457 -414 -69 -228 -683 -610 -188 -608 -480 -225 -186 -374
  -256 -672 -145 -323 -453 -252 -214 -600 -49 -652 -593 -93 -42 -101 -600 -422
  -146 -191 -474 -725 -568 -572 -498 -506 -702 -120 -210 -340 -482 -210 -666
  -520 -647 -219 -435 -455 -814 -304 -610 -224 -95 -425 -456 -761 -339 -256
  -793 -49 -317 -274 -374 -620 -730 -130 -128 -420 -315 -47 -92 -467 -269 -563
  -495 -501 -32 -755 -774 -154 1 -685 -657 -38 -727 -428 -293 -68 -203 -850
  -775 -545 -740 -683 -728 -502 -520 -44 -53 -826 -555 -539 -291 -435 -673
  -865 -114 -467 -679 -598 -611 -566 -606 -320 -124 -430 -240 -85 -549 -847
  -481 -444 -792 -695 -405 -427 -292 -533 -91 -5 -546 -181 -156 -488 -29 -17
  -572 -510 -663 -321 -177 -516 -85 -829 -109 -236 -876 -141 -427 -180 -576
  -45 -178 -6 -236 -381 -638 -144 -391 -739 -43 -898 -896 -395 -280 -712 -127
  -823 -130 -783 -324 -29 -136 -941 -816 -712 -120 -639 -209 -522 -618 -205
  -557 -153 -451 -280 -214 -683 -134 -329 -403 -156 -645 -194 -811 -377 -161
  -620 -920 -225 -632 -543 -658 -864 -137 -928 -616 -728 -145 -182 -879 -595
  -598 -409 -934 -23 -58 -301 -427 -599 -562 -373 -656 -360 -783 -68 -228 -712
  -912 -260 -490 -588 -481 -610 -615 -180 -914 -960 -462 -522 -782 -617 -687
  -477 -934 -54 -201 -279 -101 -27 -759 -407 -187 -202 -715 -488 -206 -802
  -737 -18 -364 -325 -155 -573 -536 -769 -747 -669 -856 -521 -24 -921 -394
  -726 -251 -5 -533 -923 -752 -28 -775 -100 -801 -22 -723 -383 -952 -355 -1058
  -975 -975 -706 -843 -75 -124 -150 -98 -1019 -195 -342 -915")

(def day6
  "14	0	15	12	11	11	3	5	1	6	8	4	9	1	8	4")

(def day7
  "yvpwz (50)
vfosh (261) -> aziwd, tubze, dhjrv
xtvawvt (19)
nspsk (24)
sgtfap (19) -> bohjocj, bqvzg
oyuteie (52)
irrpz (226) -> cibfe, hemjsj, upbldz
vtvku (426)
vbsfwqh (6055) -> govhrck, pglpu, rwuflbi, ppgaoz
nupmnv (47) -> cngdg, olgsb, lmvmb
ulndqey (71)
fujgzbt (198) -> fbesgp, hewtnrw
nsbvvsi (39)
ajvtdl (36)
xrgca (85)
mksrqb (45)
ozfsktz (56) -> xzwjii, uhxjy
peretma (15) -> suzsw, ycvvjgc, cdyhvr, jixggay
boplau (77) -> boggvxt, cnyasj
rzqffr (84) -> tbppaj, htyeqf, pgnyu, ruhqn
shkkwp (5)
bpptuqh (68) -> ugxls, zlshut, ltqljtw
bngres (944) -> ktwby, tblhhg, jzgpty, skbprng
tjnyo (55)
ujfeg (13) -> vldxkw, kfbsjv, rlqwaz, dvwsrc, ymxlwf, zvmhx, wfdxfxq
mxplky (35)
ezkqh (91)
kozgi (48)
pgnyu (94)
yhunajf (25)
pvpmt (77)
cvitrpt (88)
zjwih (46) -> cvitrpt, chtamif
mhfnxk (6) -> jddyhz, gvrhj, ctpdk, oprvyxy, luzjys
dbrnqc (77)
iorlbdv (43)
twwzhwf (118) -> ndgupvi, aumbaa
mvngvtg (67)
kwkprrw (187) -> uobxcai, aklrjg
wtvffu (120) -> vzxvex, mtsbrje, slfile
vjwivnq (52)
cbwbdsv (157) -> przuau, atjtgjx
rwuflbi (8) -> lapoix, dkdovw, spgcwtn
upbldz (27)
xgnqi (64)
ksotbs (48) -> cqbvru, tqbvdu
romrjs (55) -> vhilqk, gzxuw
ftmwqqj (76)
kqpvqie (63) -> qomhp, fwvsi
xxrsnzv (39)
trqfblp (85)
rmjktk (28) -> wtkfiw, mlrdsj, pnicdos, woujz, psrzr
lwygknn (96)
ctgbkrb (76) -> iavuhwe, onvkja
iqbrs (67) -> tghoyv, hoheog, kjpzwp, wwvad, ezmjqnt
efuqu (136) -> umljkr, qblnh
egwjjg (85)
dgtvwiy (27)
ndgupvi (28)
zdvai (85)
auhio (82)
dywjt (40)
hhboca (18)
pptbkz (96)
iqsvbk (73)
nwjibjk (31)
veclzy (308) -> dohjma, hbiwkt
repak (139) -> bcjxhps, eyckbl
xguqlvl (74)
rkjmp (59)
eqviuw (171) -> mqlsvih, lrnjjfq, njgkkxo
zogylog (80)
kroqr (38)
onvkja (49)
tedehrz (94) -> ipqjff, dmiqf
lhwyt (287) -> jbwbw, htyyatf, ppvwkei, vtbzq, pynomi, uswsg
mrbqk (64)
aoxtau (64)
gvrhj (59) -> qurrva, xrgca
lfksu (72)
tnwle (49)
dgnpr (93)
ucsxzhl (21) -> tpefei, ntkeax, jvayv, yuobv
wbibf (183) -> pvtat, ygnww, akngdu, swpkkpi
ljaktj (21939) -> seeqikh, mqrfbqc, ihnus
agqkf (69)
tubze (48)
fcapw (105) -> nkhzrd, shkkwp
sdied (47)
zfmeblt (56)
wqyzjzi (41)
zxjeza (185) -> ioumv, pjersb
vecxd (56) -> civozek, qzsypj
dkxebg (73) -> atrmuc, mhfnxk, zwwbw, ryaotx, yqhmb, yztqpc
oqiye (35)
edyqn (60)
fujwzoc (404)
llcbiqi (79)
yejkld (41)
utbev (25) -> hsxru, jfuxeo, zyshq, eznabc, lgsda, uobxonm
ojxyv (79)
vbpde (20)
fyjowg (26)
yfzjc (31)
obwzec (41)
zcanxq (85)
woujz (125) -> qntvn, kwzrzn
adhwzp (305) -> agqkf, yallk
zvcfznz (184) -> rjuhrpu, yzwnh, hikik
dxbrl (66)
appays (423) -> nupmnv, cyppxr, rqoczb
rgndu (81) -> tsbbzu, dtxrjg
jncex (74) -> rrdczem, bhhgpe, gtvnow, vndrx
cfvbuip (53)
vsqfx (29)
zocaag (38)
ppymmsf (19)
amwxj (69) -> pptbkz, kweqtf, jwmbs
svadhfo (99)
xdsbnr (8)
duvrzrl (22)
dfaro (8)
vgugdtt (220) -> nxpakq, fhsecp
fwddmmv (43)
igaww (28)
bbxbrsh (31)
swpkkpi (13)
bkqexxz (77)
ovfduq (16)
rtzce (39)
gmwto (119) -> wbeizj, refgrip
rmdhj (24)
yeref (77)
ifstrgw (23) -> qpixldb, tukirqp, bwcyqb, bzbmt
untdpje (45) -> owimy, yejkld
esiwujq (41)
emvxet (131) -> vecdh, ftgvx
mztuzb (764) -> jgzvrf, eaqiocy, fsagr
bwnabqh (83)
gypsfc (43)
bhemin (44)
imydd (164) -> iabqb, vdpnsak
qajpehv (49)
oxnfnsv (76) -> twwzhwf, uiyyc, gfaamu, umhcrc, bcjpzpk, vrqeade
qfdldyu (231)
dmsmgs (29)
oqady (7) -> mhtzh, cpreh
zbhwrc (759) -> esrilo, dcngdo
cyppxr (272) -> pitmcu, nvlqab
jtzjrke (45)
przuau (59)
eexkce (44)
xsewlv (124) -> fwfpmti, rszjhxn
fbesgp (28)
sxegjbz (297) -> yhycc, gxmry, favzt
nxpakq (17)
hbvvse (98)
ygzwut (83)
cngdg (93)
tfzum (69)
ccghjn (58)
ylbpwah (190) -> htlpx, eduzave, peqgox, fkqkfdu
mobbhp (141) -> clgbo, jtzjrke
icdlpb (62)
wkrtcw (1663) -> qqzhz, ybewhak, mazbb
uswsg (312)
zdvlgn (63) -> rmpryj, qrtdtt
hfcifm (107) -> tpfuxx, wdivd
cltqhc (25)
gnrnwtz (513) -> efxakc, jxlgwb, ksotbs, gvayfu, wqvtdc
rdufpid (25)
kszmj (137) -> eemfgbx, amvjukd
ykuto (662) -> pvuoltd, kcyiwqs, kbjnzw, kszmj
ckswvj (264) -> cleanrs, ntixh
czhsx (51)
luayjsp (47) -> alhjba, qjghc
djnsvq (295) -> nbnqidn, ijjfn
qxzfv (57)
qyzja (94)
rmpryj (96)
eyreht (45)
vzkii (216)
rwheyic (68)
rjmdre (21)
qntvn (71)
lescv (32)
qomhp (32)
oegwh (70)
tukirqp (85)
pumxr (103) -> wvpwez, kiuiq
fjrpc (89)
gmoliax (1327) -> jniisu, cfafhwl
wsrwgvc (80)
zhgzg (62) -> aonfu, vqiqglo
fxcuah (208) -> uofigyp, kfxyzqc
fnjnhmk (76)
zlshut (62)
hsnbry (26)
pjnyxo (30)
luzjys (115) -> kisjb, owsosl
vecdh (14)
vfrqc (13)
udjcwm (18)
twpmx (30)
oiysq (64)
lcazsyd (78)
ryaotx (399) -> lmctbp, imydd, wlkatbq, sikzsc
rbnfwkf (51)
kdtpg (4260) -> lymwlyg, xgqzb, appays
djvps (30)
vjqjwk (76)
pbrdt (66)
rciilr (33)
heumdl (75)
whqvs (146) -> ttxrd, qxuakjr
zvtxa (77)
cpkkq (18) -> romrjs, xmpdevf, nnofr, qgqou, nmkjx, nbvwta
sxada (7) -> dgrwv, irlfj, rmjpia, ciafn
frbmr (67) -> tnwle, nnyqes
tgksxw (53) -> dkffgot, yvpwz
ijvux (77)
tblhhg (44)
xxsivgz (17) -> kkhfzgn, rbqcbvl
ylxxmlt (83) -> qxzfv, dacbtyf, aqhcy
vndrx (248) -> xecqd, tznkdt
jwezuh (57) -> kxrxtq, cijfsu
ztqvzwj (86)
dluyef (1987) -> zcicmp, qkhhg, eivrwbt, iqzdu, rmvry
ybzvav (873) -> agwyte, kawloxz, bmdxyf
pvuoltd (115) -> glgrdm, gfizf, bnehbkl, lkyeyn
ceefghr (21) -> mksrqb, saqjax, llild, qwvvha
khphs (101) -> qwlepd, acgrw
yxvgj (13)
zifogt (85)
wgwlbz (83) -> uwaij, qklota
xlyvge (235) -> zgnoqml, trqfblp
qaxvec (41)
ylkfgx (6)
pzgvdt (37)
vtzdfjg (77)
tyiekxm (24)
jaiqusy (18)
avhkram (73)
akluijb (77)
bohjocj (73)
ofoto (1287) -> sgrcmj, jxykl
qgqou (227)
xtoaq (18)
peqgox (25)
kobcmy (64) -> haeymgy, egwjjg, zcanxq, zifogt
namzy (79)
vldxkw (463) -> gmwto, aflmt, boplau, khphs, qlthpj, epqsj
chtamif (88)
vqsuh (92)
iofsrh (35)
njgkkxo (17)
zsqqhe (94)
xupdam (64)
bqvzg (73)
hmetm (889) -> aklagd, ceefghr, whcxfwf, guztl
hoheog (296)
llsvd (18)
giupn (30)
dfzfwun (64)
nmfvmam (125) -> qxwlhxh, obwzec
ioumv (86)
gxmry (143) -> cyvplfd, wmcusr
ywgvsdb (131) -> fzfxs, yvdkbo, igaww
pwsye (49)
mywezi (407) -> gimkyms, bnxvusb
fzfxs (28)
kurgrkq (76)
txeucf (61)
gjwflt (84) -> xmfzu, gcbjhu, xxnannn, bwnabqh
vrgxe (1226) -> zbfyg, rocbcw, zouml, wbibf
izwze (16)
jwmbs (96)
euvev (46)
dgzvf (30)
iezdimc (82)
zkhll (992) -> bbqtwv, xupdam
jhxxfq (71)
hemjsj (27)
ryxkvxj (124) -> bhemin, apghde
qxuakjr (54)
aonfu (95)
dvopaww (98)
uobwwa (69)
dojvyuv (96)
ezcmm (886) -> ntjmwf, qfdldyu, vokxlx
yzwnh (15)
jfqanu (302) -> dcgarog, tcpqh
gfizf (14)
kvbdsv (46)
qzsypj (98)
vyffcrm (24)
iuoxs (13)
zitfb (23) -> ctagl, ddpgq
yueoma (46)
bbqtwv (64)
hgsng (45)
wgehfu (77) -> lescv, hnjbsg, bxvwe, evmuxia
arwqmho (66)
amvjukd (17)
ntixh (10)
zfzblty (47)
cxzuprb (592) -> texfyi, dvgstr, irhxzg
ekqux (76)
abxalr (697) -> zjwih, gjwuf, eqviuw
choiui (65)
tgjdwlh (24)
glhsr (60)
rtymngs (66)
rocbcw (169) -> djizg, dtnaqds
bjwws (46) -> evgtp, ztqvzwj
gowbiey (78)
yqhmb (497) -> njgrpbo, tedehrz, bjwws
lapoix (240) -> xkizl, dyhate, disan
vckqsrm (30)
sxdljm (66)
frpmd (13)
uwhwsv (86)
wknic (47)
pnicdos (165) -> czhsx, rbnfwkf
wrsik (80)
wbogdd (66) -> eibtll, ppmvpn
fyrqw (28) -> ekqux, zxoqwjv, ipogzy, dhzuvup
uttjzap (22)
gqjkk (41)
gcbjhu (83)
bnxvusb (21)
bxvwe (32)
difjqqd (47)
xxnannn (83)
lxzucvk (94)
ehynj (98) -> ijvuozb, wutdi
vwkbje (25)
ciafn (62)
pyyuj (34)
zofljd (685) -> hfcifm, emvxet, ahrdgu
zyshq (140) -> dpcmm, eosuf
zgslahq (101) -> ximrxj, auvxopo
xszci (72)
fomxzjj (47)
mcduoz (6)
rcltx (73) -> djnsvq, hqmum, ifstrgw
pemna (98)
fgnsyw (39)
tfppg (77) -> uwhwsv, fmapm
tohlady (84)
gzxuw (86)
mkdiuil (96)
qfhkgt (52)
csxwyly (47)
intuzzk (82) -> jqqxv, dlnyi, pbides, qtcqtv
bfxhl (78) -> qvmorke, pazbvum
eduzave (25)
kwbni (74) -> abxalr, bbsuv, gnrnwtz, rmjktk, eqcnl, pdwesa, sunvhgy
hhnvuv (83)
ismgnve (68)
alhjba (84)
mspkmg (94)
rszjhxn (34)
frleu (60)
mewfm (48) -> gowbiey, yevks
mpulnxr (42) -> eziuhsd, xsewlv, tckcxhb, bfxhl
mzksj (68)
shnqfh (1649) -> wnnov, mdtniv, fwpuy
bxpfop (153)
zucsr (26)
gmpqmio (93)
wzjbp (174)
zcicmp (301)
ppvwkei (93) -> eqjfyj, lghygm, grrycy
tcsoaw (239) -> kzfzp, oypzs
nnsial (78)
mazbb (98) -> dsxhhub, fqoqkzk
lclbeiq (81) -> bwxicc, uttjzap
jddyhz (91) -> vmmww, mvalh
umqrwk (132) -> itvhd, jbuxnpr, mxopf
qxfan (34)
cdafdq (26)
drrjnr (80)
kxrxtq (68)
cnyasj (56)
kpdbki (38)
dgzoyx (72)
zwwbw (155) -> eqbvn, qcghv, tfppg, xaanoft
zsxbg (22)
fwvsi (32)
hefgter (6) -> pirieql, hikocbe
mwelezd (81)
eptcqns (16)
jmbiky (96)
gnozwfo (242) -> vqsuh, cxlfgl
wtkfiw (48) -> owresw, grqjew, okbzzwd
ltqljtw (62)
lymwlyg (72) -> snkox, adhwzp, cvdjts
mgeizij (60) -> sbcuc, rqrfl
zghvta (57)
uqcqc (85)
tozwp (19)
nqjrds (81)
cfufhn (11)
yznhjg (60) -> choiui, yhecio, lprpgxe
qtbhnxr (27)
bwdfws (27)
kiuiq (89)
zjfrc (141) -> zghvta, lkqiqa
ooiyeep (80)
xiawjb (38)
wgzdt (216) -> vfrqc, frpmd
lugkrb (36)
mvalh (69)
cruxrmy (52)
azwmkej (33)
tqbvdu (61)
szvlte (21)
bzbmt (85)
gcczhgd (43) -> eecoekq, kwpnxyh
jxlgwb (24) -> pzahn, yadtxl
gfaamu (8) -> ygzwut, hhnvuv
ntjmwf (211) -> owqdo, umvvwsq
rqhrb (19)
jixggay (28)
gavjzk (91)
mbydswv (69)
unqbwiz (10) -> rixge, lkwapf, phvbwg
veycd (62)
lmctbp (106) -> wqyzjzi, gqjkk
jwpjy (94) -> ywlub, rtymngs
febjzqn (46461) -> mwtojre, iqmqju, dkxebg
yvdkbo (28)
iayzdx (7181) -> fakyd, fjwmd, uzicvdh
uobxonm (118) -> wrsik, wsrwgvc
sehxgj (41) -> wpezjoi, dcnuouj, vrpqev, kjwvqdz, vzkii
eziuhsd (192)
gcichoe (79)
jwzmsv (45)
fmapm (86)
rbtvx (53)
sgkksme (94)
zmces (86)
rmjpia (62)
feiki (376) -> oqady, fcymcb, wlleabp
sgrcmj (54)
dohjma (76)
kgzqclo (71)
eqjfyj (73)
zcjrk (1009) -> zdvlgn, sxada, yznhjg, pnoxhe
zhudrmb (78) -> mvngvtg, kvags
iylwxgn (98) -> edebwk, gjwflt, rlapq
beraoor (10) -> pumxr, gasnep, ulznkvm
gxnoomf (185) -> ihrhkj, cfufhn
fhoprd (71)
pbides (59)
xqgjvz (192) -> twpmx, zwofu
mexuge (52)
rbfbf (47)
oprvyxy (125) -> oyuteie, yibxqhs
eqcnl (637) -> kgjcj, wgzdt, rrgffal
kikfs (40)
ntvul (77)
bwxicc (22)
rttwea (55) -> fnjnhmk, kurgrkq
kfxyzqc (9)
ddmrgzp (69)
clgbo (45)
qoapjhr (173)
kkhfzgn (88)
njgrpbo (74) -> dgzoyx, rlywif
bwcyqb (85)
esrilo (47)
blutjw (717) -> fxcuah, efuqu, jwpjy
owimy (41)
oeeqrsd (62)
akoqk (16)
owqdo (10)
fwpuy (26) -> eqnpvwk, lbhxdbr, lqqkjjr, tlvpv
tzqzk (52)
gxobd (91)
npqroe (36)
pstaxvr (761) -> peretma, kqpvqie, untdpje
boggvxt (56)
uobxcai (10)
vmyda (63912) -> rcltx, ajwxaa, yvqasth
jznrci (51) -> mclgebd, emdsyso
ktgfsm (50)
kweqtf (96)
mtsbrje (66)
rjuhrpu (15)
zgnoqml (85)
qlthpj (57) -> arwqmho, yydyjr
cdudjbj (128) -> iofsrh, nuuwbui, mxplky
estwket (94)
adjlg (67)
jbwbw (114) -> dxbrl, lhdhko, fdjhpq
ezfky (71)
rrdczem (71) -> dgrfhvi, adjlg, ajyqxh
xqrmgdk (6543) -> unqbwiz, cxzuprb, rmkhel
nmkjx (213) -> wycesn, gpfipvk
qklota (16)
jbuxnpr (30)
xtzqooh (27)
vbcfe (49938) -> yuepckc, ybzvav, sxapjc, cfvlt, dluyef
drzrtpi (96) -> nuuqqqy, scqutwb
xkfps (7)
nvxlr (49)
hazbs (31)
ajyqxh (67)
guhyk (66)
ipwxws (190) -> difjqqd, wknic
akngdu (13)
evgtp (86)
eznabc (150) -> dvhbwl, xnuoisk
limjm (52)
bykjeka (71) -> sdied, zfzblty, csxwyly, fomxzjj
gpfipvk (7)
cpreh (86)
qurrva (85)
sikzsc (46) -> dpetvw, pridma
vokxlx (69) -> gadnfmt, nqjrds
scbycx (193)
qjghc (84)
ctpjjsh (964) -> thlbs, wgehfu, fetns
uzicvdh (42) -> altsh, xhhhix, calvq, bykjeka, iucuw
tpefei (109) -> qtznzyx, myznok
sizfqfs (116) -> vsqfx, dmsmgs
qumahjn (71)
ctagl (97)
hewtnrw (28)
ocpzltp (67)
ihnus (40) -> vrgxe, shnqfh, auzded, hkhsc, jwddn, mcxki, lhwyt
iuydxn (253) -> fhoaub, kmnjo
evubadn (49)
wpezjoi (112) -> zpxqtrg, tzqzk
qtznzyx (57)
dcnuouj (76) -> oegwh, ztzfled
ddpgq (97)
ktrkhe (67)
podcsx (71)
univhdj (26)
puxaz (60)
jadome (50)
uplbokf (5475) -> vbxqhoy, cpkkq, iqozz
llild (45)
lylaoh (170) -> lomhz, tzjzmmv, cefbp
owsosl (57)
iqozz (681) -> syykbn, mbmzme, cdudjbj
nvdon (345) -> dgzvf, djvps
iigqfh (327)
ctatrbo (279) -> tyiekxm, rmdhj
yevks (78)
cipxwb (85)
edmljb (134) -> ligev, glhsr
yallk (69)
owtxsq (71) -> zxjeza, pzgvsxu, amwxj
pejmc (151) -> fjvoff, esgqip
pridma (71)
vdpnsak (12)
hlfbt (200) -> univhdj, xnxdmdb
wyuwk (315) -> ibcbvy, ztaff
phvbwg (86) -> yachdf, utncpu, tohlady
nbnqidn (34)
tnyjkok (93)
kwpnxyh (41)
yachdf (84)
ximrxj (42)
appok (73)
rrgffal (78) -> trxgpy, zmjfa
uiyyc (126) -> nspsk, plxdglq
rcxtjte (210) -> drrvse, szvlte
ruild (9) -> zqdezgy, cxpoqyu
mqlsvih (17)
xgqzb (451) -> mgeizij, hefgter, ajtjnwp, ljhezx, pwwqvhw
gmdcbyg (214) -> mexuge, limjm
mxopf (30)
wnnov (170)
qgwcfll (95)
smtbsys (49)
jiyuw (40)
sunvhgy (218) -> fmlhgs, clmyzsu, zvcfznz, xrkca, jznrci
wycesn (7)
sbgwln (66)
dkffgot (50)
ximrdyg (217) -> tpbgp, ibmcytl
mvqfvit (38)
spgcwtn (63) -> yeref, itxaax, ntvul
apmpzd (46)
njkeu (67) -> vecxd, rpcoxd, emqoxss, elusirt, hlfbt, xqgjvz
irlfj (62)
zxoqwjv (76)
vjwlck (28)
ezmjqnt (146) -> wdssrl, gwcig
plzfjy (41)
beeeaye (71)
nqkwmyd (254)
hbiwkt (76)
hspol (20)
ntjawca (63)
yuobv (91) -> vtdpkof, basjwp, pfvpb
lqqkjjr (36)
zzqwzy (48) -> cnbzj, vftqnj, mywezi
goyzt (22)
apghde (44)
ahrdgu (23) -> ismgnve, rwheyic
vqiqglo (95)
ruclz (25)
wuqbe (98)
nedbkp (49)
fpmipcr (40)
gadnfmt (81)
kxrfley (55)
thlbs (59) -> iqsvbk, tbyihc
wqvtdc (126) -> uyjre, dvkrlzc
omfvqhv (29) -> iorlbdv, fwddmmv
lihivo (81)
ligev (60)
iabqb (12)
zwjqar (71)
ddsqd (61)
wwvad (23) -> owzybm, gavjzk, gxobd
plhybj (48)
rgyamx (79)
trxgpy (82)
vzxvex (66)
favzt (69) -> qxfan, pyyuj, lhahl
atrmuc (1081) -> qrsgca, bhthbfj
mqrfbqc (9769) -> vbqtd, ykuto, mgktiii, iylwxgn
ihkcni (66)
ttxrd (54)
dvhbwl (64)
nlhxju (31)
kjwvqdz (140) -> zocaag, gszqlv
gulset (80)
vubst (114) -> giupn, vckqsrm
pvejq (52)
lhahl (34)
rqoczb (310) -> ropzx, xdsbnr
xhhhix (259)
ictdnyf (1551) -> ezfky, ckxblyq
jexvtb (15)
fmlhgs (103) -> ntjawca, jwpwt
fcymcb (17) -> mwelezd, jaoiiae
cepddei (195) -> ppymmsf, mcdmpq, tozwp
suzsw (28)
dlnyi (59)
nzrfhb (198) -> xwpwer, vjwlck
kmnjo (35)
itvhd (30)
tpbgp (11)
syykbn (127) -> teastj, ntazqjk
vhilqk (86)
ctpdk (31) -> yzciq, ljlotlp
putpp (92) -> pbrdt, ihkcni, sxdljm
peydhei (737) -> dojvyuv, nnogow, lwygknn, ndajgou
yzciq (99)
xmfzu (83)
plzgfx (98)
qckem (28)
kuakyv (77)
qpixldb (85)
nnjbjm (23) -> cgwnn, veclzy, rzqffr
nrkdm (200) -> dgtvwiy, qtbhnxr
xwkski (59)
mgktiii (827) -> ggbxd, gjfoe, qoapjhr
elusirt (106) -> appok, avhkram
ttkboa (24)
ckxblyq (71)
disan (18)
owresw (73)
pyuuxw (39)
hwsrjah (176) -> pvejq, cruxrmy, vjwivnq
skbprng (44)
lkyeyn (14)
mdtniv (82) -> eexkce, oiiagw
ljhezx (130) -> jpxogsl, pjnyxo
upsiumf (18)
glgrdm (14)
rvcmif (288) -> jdmjou, ddmrgzp
wvsspk (25) -> lqnwffm, edgplu, qyzja
umljkr (45)
ggbxd (67) -> jikpfbk, siqksje
kuujprz (67) -> ctatrbo, xibdlh, wyuwk, siflauk, iigqfh, jhwltnv
aklagd (201)
lprpgxe (65)
auvxopo (42)
uorika (69)
avbgwvm (81)
nnogow (96)
iybvphk (85)
sxapjc (129) -> peydhei, sehxgj, lgtffok
utrdhz (75)
nbvwta (153) -> bbviexo, pzgvdt
jzgpty (44)
qqzhz (32) -> eyreht, xxswgx
ropzx (8)
xaanoft (42) -> cwgrjoh, uorika, uobwwa
ybnpc (307) -> dibqua, ozfsktz, txrxyqh
cfvlt (753) -> rusayd, feiki, ucsxzhl
ruxuw (43)
ywpjusg (64) -> joxmxc, mzksj
qtcqtv (59)
qwddgc (82)
eyckbl (58)
lgsda (44) -> lcazsyd, mwdfwe, nnsial
tlvpv (36)
rlqwaz (901) -> sizfqfs, ehynj, wzjbp, vubst
fgbnlu (66)
zvmhx (105) -> tcsoaw, rtnpvf, lbkvgme, xtttnfp
oiiagw (44)
jpxogsl (30)
lomhz (78)
calvq (85) -> ccghjn, lpascsz, zkecjv
ljlotlp (99)
jqqxv (59)
iqzdu (187) -> itygd, cdvmqs
ugkey (86)
tjjrys (94)
dtjhnzr (85)
tqeeva (214) -> xwkski, rkjmp
tznkdt (12)
daiah (26)
cwgrjoh (69)
dibqua (24) -> rgyamx, gcichoe
scqutwb (94)
yeifw (71)
dyhate (18)
acgrw (44)
fndpz (18)
pynomi (170) -> kgzqclo, ygvkw
kcyiwqs (35) -> givljl, yntnjp
lbkvgme (223) -> jadome, dtwbpl, ktgfsm
ygvkw (71)
dgsutv (25)
rlywif (72)
nevpm (49)
xtttnfp (73) -> heumdl, axgpyq, utrdhz, paeea
ptmqo (284)
ibmcytl (11)
ejwno (80)
mcdmpq (19)
azvke (170) -> wscktm, jexvtb
govhrck (176) -> vuqriq, crfse, eoditdi
tzjzmmv (78)
dhjrv (48)
dlifqat (49)
ruhqn (94)
mhtzh (86)
bhhgpe (124) -> bywwy, xguqlvl
ucicsdu (199) -> itvizjy, wsnqied
agwyte (498) -> gcczhgd, lclbeiq, rgndu
gapjnb (200)
pitmcu (27)
vtdpkof (44)
joxmxc (68)
mclgebd (89)
refgrip (35)
hsxru (30) -> oeeqrsd, icdlpb, veycd, dlrpnot
jikpfbk (53)
usztpox (160) -> goyzt, eagggd
aqhcy (57)
umhcrc (174)
qwvvha (45)
crxnz (10)
mbmzme (37) -> qajpehv, dlifqat, bponwxc, hjzehyw
uyjre (22)
tsbbzu (22)
tenzzu (67)
ijvuozb (38)
oypzs (67)
ymxlwf (327) -> nzrfhb, wbogdd, fujgzbt, nqkwmyd, vgugdtt
dvgstr (87) -> rqhrb, xtvawvt, receu
yzuygum (57) -> oiysq, jibdphs
vuqriq (238)
lsysd (68)
meayff (35)
guztl (73) -> mrbqk, aoxtau
rtnpvf (177) -> pwsye, nevpm, nvxlr, smtbsys
zbqpk (112) -> apmpzd, yueoma
bbviexo (37)
gimkyms (21)
fjwmd (692) -> luayjsp, njqzaxg, ywgvsdb
igfhu (31)
qkhhg (105) -> yknwap, qqqsv
ybewhak (122)
xkizl (18)
siqksje (53)
vtbzq (178) -> tenzzu, ocpzltp
giblwzi (7)
atjtgjx (59)
bjlwfmo (49)
jibdphs (64)
cxlfgl (92)
wltlu (1495) -> swumt, qeosq, guhyk
lkqiqa (57)
qwlepd (44)
rmvry (145) -> ejzfwt, pknjpc, qfhkgt
xmpdevf (185) -> rjmdre, abcyotz
utwzg (10)
lpascsz (58)
abcyotz (21)
grqjew (73)
vindom (25)
tzmes (28)
ktwby (44)
gfsnsdu (38)
ywlub (66)
iryrfjs (31)
fwfpmti (34)
givljl (68)
edebwk (161) -> dtjhnzr, cipxwb, uqcqc
aziwd (48)
ajtjnwp (46) -> clfrs, qdwkxia
nvlqab (27)
dvwsrc (1517) -> jiyuw, dvtdje
basjwp (44)
rhchuf (67) -> sedysm, nedbkp
cdvmqs (57)
dkdovw (262) -> afglbpz, ovfduq
seeqikh (10944) -> mztuzb, zovbvcr, nnjbjm
bgcrdqu (174)
huqpiix (121) -> eptcqns, aiget
bywwy (74)
aujxmm (89)
xecqd (12)
tbppaj (94)
zkecjv (58)
utncpu (84)
grrycy (73)
htlpx (25)
pvtat (13)
dfdlzzt (71)
uwaij (16)
edgplu (94)
aflmt (153) -> puyiail, jaiqusy
eoditdi (10) -> boqtohm, odsrv, iblrer
imshim (39)
nioeugr (38)
eagggd (22)
ulznkvm (211) -> meayff, oqiye
psrzr (213) -> bwdfws, sdrhiyo
vmmww (69)
blofdav (18)
zbfyg (49) -> xoqxrbl, dgnpr
qzhyumq (25)
dpcmm (69)
qvmorke (57)
exuwhfs (89) -> qumahjn, kkvkz
rdqvi (56)
axlgwj (38)
vbxqhoy (744) -> nquonb, zhudrmb, ryxkvxj
rqlaf (67)
mlrdsj (107) -> tzdtm, etmvojh
tnzqj (1057) -> nbpic, ctgbkrb, bgcrdqu
lghygm (73)
wvjjht (60)
wmvht (36)
xibdlh (255) -> tgjdwlh, vyffcrm, ahridq
pjersb (86)
pdwesa (407) -> ximrdyg, xaebyo, qqpsrts, ucicsdu
tflulu (3299) -> ezcmm, tnzqj, njkeu, ctpjjsh
rmkhel (55) -> olkcm, yquqk, iuydxn
odsrv (76)
kfbsjv (73) -> ylxxmlt, whqvs, edmljb, bpptuqh, jwjggy, nrkdm
vywqra (18)
bnehbkl (14)
lkwapf (290) -> ttkboa, xvcikqe
kawloxz (45) -> rttwea, nmfvmam, kwkprrw, gxnoomf
drrvse (21)
tklfj (465) -> fcapw, omfvqhv, wgwlbz
receu (19)
zqdezgy (72)
ntkeax (91) -> sbgwln, fgbnlu
boqtohm (76)
rpcoxd (252)
hiqqg (82)
cdyhvr (28)
snkox (67) -> mspkmg, lxzucvk, zsqqhe, sgkksme
zpxqtrg (52)
olgsb (93)
hikocbe (92)
xwpwer (28)
kzfzp (67)
dfyyjta (33822) -> hvhjh, ujfeg, iayzdx
hvhjh (6551) -> uveybq, iqbrs, zrnnyet
kjpzwp (160) -> fmvyjwj, lsysd
htyyatf (240) -> fndpz, xtoaq, hhboca, llsvd
uveybq (947) -> ywpjusg, gapjnb, azvke
cqbvru (61)
saqjax (45)
emlqbo (18)
jvayv (171) -> daiah, fyjowg
dtnaqds (33)
rxuwet (92)
xnuoisk (64)
etmvojh (80)
eqnpvwk (36)
puyiail (18)
ijrzzr (77)
jfuxeo (150) -> xgnqi, dfzfwun
pzgvsxu (282) -> cltqhc, ruclz, vindom
tqpgc (1018) -> bbxbrsh, uwxbv, nlhxju, hazbs
nadbbgx (258) -> akoqk, izwze
qdwkxia (72)
rbqcbvl (88)
wutdi (38)
mlrjfy (38)
efxakc (154) -> dfaro, lcsqgx
dacbtyf (57)
mevea (303) -> qaxvec, esiwujq, plzfjy
dsxhhub (12)
wjzyxt (252)
pnoxhe (27) -> vjqjwk, enpprra, ftmwqqj
kkvkz (71)
clfrs (72)
zovbvcr (383) -> diyvlug, xhodnx, repak, zjfrc
jwjggy (210) -> duvrzrl, zsxbg
xjfkrjs (33) -> ibzalz, rxuwet
gtvnow (180) -> euvev, kvbdsv
etfuhz (123) -> zoicqbj, ajvtdl, wmvht
afglbpz (16)
nquonb (50) -> avbgwvm, lihivo
nbpic (148) -> iuoxs, yxvgj
eivrwbt (17) -> yeifw, podcsx, fhoprd, beeeaye
gszqlv (38)
yhycc (121) -> vwkbje, qzhyumq
gjzddf (38)
cxpoqyu (72)
emdsyso (89)
qqpsrts (85) -> doudbv, mmzdk
gjwuf (124) -> bjlwfmo, evubadn
hkhsc (171) -> ipwxws, yrdgv, ckswvj, yjlrwzu, ptmqo, ojishwt, drzrtpi
mwtojre (207) -> hmetm, utbev, wltlu, ictdnyf
fqoqkzk (12)
nkhzrd (5)
enpprra (76)
gwcig (75)
uqcpah (91)
koued (75) -> kxrfley, tjnyo
dtxrjg (22)
pnkag (93) -> ofoto, zzqwzy, zobjfxe, blutjw, aisfhs, gmoliax
iucuw (99) -> gulset, drrjnr
fhsecp (17)
dgrwv (62)
ibzalz (92)
tbyihc (73)
cleanrs (10)
cibfe (27)
ycvvjgc (28)
dnphyfm (7185) -> mpulnxr, sxegjbz, tklfj
xnxdmdb (26)
jniisu (34)
eaqiocy (93) -> dywjt, kikfs, fpmipcr
bugtxto (26)
pprkvq (26)
myznok (57)
iblrer (76)
bhmzx (64) -> tgoeoh, ojxyv
yibxqhs (52)
xoqxrbl (93)
hoeqoas (99)
oxvfpna (222)
okbzzwd (73)
yhecio (65)
esgqip (62)
zwofu (30)
nnyqes (49)
yydyjr (66)
ztzfled (70)
yvqasth (1018) -> lfksu, xszci
eqbvn (153) -> plhybj, kozgi
hqmum (79) -> dfdlzzt, ulndqey, zwjqar, jhxxfq
gvayfu (64) -> cfvbuip, rbtvx
fsagr (141) -> lugkrb, npqroe
pfvpb (44)
cvdjts (271) -> zmces, ugkey
bcjpzpk (174)
jxykl (54)
mwdfwe (78)
diyvlug (133) -> ddsqd, txeucf
wbeizj (35)
xxswgx (45)
wfdxfxq (676) -> irrpz, wvsspk, dqtthnn
lcsqgx (8)
lmvmb (93)
cyvplfd (14)
olkcm (131) -> mkdiuil, msbsjpj
kloumi (39)
xzwjii (63)
zmjfa (82)
fakyd (842) -> sgtfap, frbmr, rhchuf
jgzvrf (173) -> hspol, vbpde
slfile (66)
jwpwt (63)
xhodnx (67) -> estwket, tjjrys
ihrhkj (11)
wlkatbq (188)
cijfsu (68)
ipqjff (62)
ztaff (6)
eecoekq (41)
oaxskk (266) -> azwmkej, rciilr
sedysm (49)
jhwltnv (19) -> akluijb, zvtxa, pvpmt, bkqexxz
xhqgsro (6)
bmdxyf (318) -> zgslahq, koued, yzuygum
dmyzfr (18)
rqrfl (65)
clmyzsu (115) -> mlrjfy, xiawjb, kroqr
rusayd (901) -> ylkfgx, lxsurts
tzdtm (80)
dpetvw (71)
eccwi (6)
wdssrl (75)
uofigyp (9)
aisfhs (183) -> lylaoh, fujwzoc, kobcmy
dcgarog (15)
eosuf (69)
qeosq (66)
itxaax (77)
mmzdk (77)
dvtdje (40)
aumbaa (28)
wdivd (26)
eemfgbx (17)
dlrpnot (62)
ugxls (62)
qqqsv (98)
lfhaqdn (27)
qblnh (45)
yknwap (98)
ipogzy (76)
irhxzg (124) -> utwzg, crxnz
dtwbpl (50)
lqnwffm (94)
ygnww (13)
xrkca (217) -> xhqgsro, avymjz
qrsgca (35)
ftgvx (14)
fhoaub (35)
nuuqqqy (94)
ppmvpn (94)
kisjb (57)
owzybm (91)
jwddn (134) -> vfosh, nvdon, kopmae, inhqoxd, xlyvge
cnbzj (141) -> kuakyv, vnsrv, ijrzzr, dbrnqc
wlleabp (179)
lhdhko (66)
bbsuv (439) -> ionnpk, etfuhz, mobbhp, exuwhfs
umvvwsq (10)
eibtll (94)
otuwt (35) -> ejwno, zogylog, ooiyeep
epqsj (137) -> bugtxto, hsnbry
yuepckc (66) -> tqpgc, pstaxvr, owtxsq
dhzuvup (76)
crfse (238)
ibcbvy (6)
cefbp (78)
uhxjy (63)
bponwxc (49)
mcxki (29) -> rvcmif, gnozwfo, mevea, vtvku, dmtzxui
emqoxss (66) -> tnyjkok, gmpqmio
ionnpk (97) -> rqlaf, ktrkhe
gasnep (91) -> qgwcfll, nkcet
avymjz (6)
fetns (37) -> zfmeblt, jtsyj, rdqvi
altsh (89) -> zdvai, iybvphk
kgjcj (242)
bltmlm (93) -> xdlvys, tflulu, xqrmgdk, dnphyfm, kwbni, uplbokf, vbsfwqh
hikik (15)
nnofr (71) -> xxrsnzv, pucsbv, rtzce, kloumi
siflauk (271) -> qckem, tzmes
nkcet (95)
ijjfn (34)
itvizjy (20)
numadl (98)
jkjsi (98)
xaebyo (41) -> svadhfo, hoeqoas
yntnjp (68)
axgpyq (75)
civozek (98)
djizg (33)
qrtdtt (96)
kopmae (13) -> pemna, vuzsg, numadl, wuqbe
yztqpc (572) -> xxsivgz, scbycx, jwezuh
msbsjpj (96)
yjlrwzu (77) -> mbydswv, tfzum, kwhwph
tcpqh (15)
fkqkfdu (25)
vbqtd (86) -> cepddei, rcxtjte, zkibug, wjzyxt, zhgzg
wmcusr (14)
zrnnyet (731) -> zbqpk, tayyz, usztpox, mewfm
yadtxl (73)
eqctwan (511) -> zitfb, xjfkrjs, xddom
evmuxia (32)
lbhxdbr (36)
fjvoff (62)
wsnqied (20)
vftqnj (57) -> dvopaww, hbvvse, plzgfx, jkjsi
pwwqvhw (34) -> fgnsyw, pyuuxw, nsbvvsi, imshim
hnjbsg (32)
pglpu (224) -> oxvfpna, umqrwk, bhmzx
etcces (4977) -> eqctwan, jncex, zofljd
fmvyjwj (68)
lgtffok (509) -> ruild, bxpfop, huqpiix, tgksxw
iqmqju (3619) -> oxnfnsv, bngres, zkhll
xvcikqe (24)
wvpwez (89)
saijc (96)
aiget (16)
fdjhpq (66)
adcbil (47)
lrnjjfq (17)
teastj (53)
csybv (33546) -> kdtpg, wseyo, etcces, pnkag
zkibug (238) -> xkfps, giblwzi
pzahn (73)
auzded (499) -> tqeeva, fyrqw, oaxskk, hwsrjah, jfqanu
bhthbfj (35)
dvkrlzc (22)
dgrfhvi (67)
qcghv (67) -> ezkqh, uqcpah
sdrhiyo (27)
kwhwph (69)
ejzfwt (52)
vuzsg (98)
tgoeoh (79)
vrpqev (64) -> axlgwj, gfsnsdu, kpdbki, mvqfvit
vrydu (43)
vnsrv (77)
gjfoe (161) -> mcduoz, eccwi
ppgaoz (65) -> pejmc, cbwbdsv, otuwt
xddom (39) -> aujxmm, fjrpc
lxsurts (6)
tayyz (50) -> vtzdfjg, ijvux
kwzrzn (71)
doudbv (77)
ahridq (24)
dqtthnn (217) -> hgsng, jwzmsv
haeymgy (85)
zoicqbj (36)
bcjxhps (58)
swumt (66)
iywwyn (43)
pirieql (92)
ntazqjk (53)
ajwxaa (208) -> wtvffu, gmdcbyg, intuzzk
wscktm (15)
itygd (57)
hjzehyw (49)
jdmjou (69)
iavuhwe (49)
tpfuxx (26)
wseyo (5904) -> beraoor, zbhwrc, ybnpc
rlapq (322) -> adcbil, rbfbf
jtsyj (56)
zouml (199) -> dmyzfr, udjcwm
txrxyqh (107) -> rdufpid, yhunajf, dgsutv
zobjfxe (525) -> ylbpwah, nadbbgx, putpp
inhqoxd (327) -> cdafdq, zucsr, pprkvq
ndajgou (96)
paeea (75)
yrdgv (284)
sbcuc (65)
aklrjg (10)
kbjnzw (117) -> xtzqooh, lfhaqdn
cfafhwl (34)
htyeqf (94)
dmiqf (62)
tghoyv (210) -> iywwyn, gypsfc
plxdglq (24)
qxwlhxh (41)
pknjpc (52)
njqzaxg (23) -> saijc, jmbiky
tckcxhb (68) -> yfzjc, nwjibjk, igfhu, iryrfjs
xdlvys (3528) -> zcjrk, wkrtcw, kuujprz
dcngdo (47)
pazbvum (57)
kvags (67)
nuuwbui (35)
whcxfwf (115) -> vrydu, ruxuw
ojishwt (126) -> llcbiqi, namzy
uwxbv (31)
vrqeade (102) -> emlqbo, vywqra, upsiumf, blofdav
rixge (338)
dmtzxui (98) -> auhio, qwddgc, hiqqg, iezdimc
texfyi (144)
cgwnn (384) -> nioeugr, gjzddf
mwzaxaj (59) -> csybv, febjzqn, bltmlm, dfyyjta, ljaktj, vmyda, vbcfe
yquqk (83) -> wvjjht, edyqn, frleu, puxaz
jaoiiae (81)
pucsbv (39)")

(def day8
  "ioe dec 890 if qk > -10
gif inc -533 if qt <= 7
itw dec 894 if t != 0
nwe inc 486 if hfh < -2
xly inc 616 if js >= -3
j inc 396 if b != -5
nwe dec -637 if uoc > 0
b inc 869 if yg >= -3
gif dec -221 if iyu < 0
tc dec -508 if gy >= -7
x dec 637 if gif < -526
nwe dec -185 if nwe != -8
x inc 638 if b != 869
ih dec -722 if itw > 9
xly inc -38 if ih >= 8
hm dec 910 if t == 0
uoc dec -585 if qt == 0
js dec -325 if hm == -910
yr dec -922 if cp != 0
qt dec 316 if itw != 2
bi dec -422 if iyu <= -1
uoc inc -862 if itw <= 3
itw dec -301 if x < -632
gif inc -492 if fi != 5
uoc inc -745 if x < -631
xly inc 21 if js > 331
hm inc 44 if js > 334
js dec 503 if tc > 503
t inc -216 if j == 396
yg inc 559 if nwe > 189
bhp dec -214 if x >= -646
hm dec 366 if fi == 0
t dec -658 if nwe == 185
hm inc -432 if qt <= -307
xly dec 695 if uoc >= -1031
cp inc -438 if x != -647
yg dec 211 if x >= -628
bi inc 829 if ih > -8
yg dec 540 if tc >= 503
hm dec -913 if qt > -310
qk inc -406 if itw < 309
uoc dec -716 if iyu >= -1
ih inc -655 if qt != -316
ih inc 6 if xly > -80
cp inc 795 if xly > -88
bhp dec 59 if yr < 1
yr dec 952 if x >= -628
xly dec -867 if j > 393
fi inc 720 if ioe >= -892
gif inc 454 if ioe > -886
j dec 547 if fi != 720
qk inc 665 if bi > 819
hm dec -174 if cp != 357
hm dec -795 if uoc <= -314
uoc inc 273 if itw <= 307
gy dec 212 if xly >= 783
tc inc 918 if ih != 9
tc inc -43 if js >= -186
gif inc -615 if b == 869
bhp inc -335 if fi > 724
ih inc 747 if hm >= -1711
ih inc -515 if ioe != -881
yg dec 967 if cp == 357
yr inc -23 if qt < -309
gif dec -16 if cp == 357
itw inc 353 if uoc <= -41
cp dec -788 if b <= 869
bi dec 510 if itw < 306
yg inc 321 if qk > 265
itw inc -194 if gif == -1624
yr dec 484 if b == 869
yr dec 828 if yg != -1515
cp dec -700 if gy != -212
ioe dec -238 if iyu >= -5
xly inc -334 if bi != 316
js dec 642 if uoc < -27
cp inc 131 if x >= -633
cp dec 693 if iyu >= -2
bi inc 671 if hm >= -1712
fi dec 781 if nwe >= 176
ioe inc 770 if qk > 253
nwe dec 381 if j < 406
qt inc -599 if hm != -1715
yg inc 277 if qk >= 268
hm dec -656 if ioe < 117
uoc dec -875 if ih < 243
js dec 297 if yg >= -1514
hfh dec 821 if iyu >= 8
ioe inc -133 if iyu > -9
x dec -623 if iyu != 0
gy inc 240 if cp >= 451
gy inc 937 if hfh != -10
tc dec 476 if tc != 1376
iyu dec -35 if hm == -1706
nwe dec 86 if yr >= -1344
cp dec 96 if qk != 259
x dec 864 if b >= 865
hm dec -965 if bhp != 161
bhp dec 402 if b == 859
b inc -19 if hm <= -746
xly dec 24 if uoc >= 840
ih inc 816 if js >= -1117
xly dec 511 if iyu != 5
t dec -214 if iyu != -8
yr inc -81 if tc <= 898
js inc -187 if yr < -1331
x inc -970 if cp < 462
hm dec 668 if tc > 904
uoc dec -488 if yg <= -1502
uoc inc -974 if j != 394
qk inc -58 if qt > -913
j dec -741 if js == -1304
j dec 58 if nwe != -276
fi dec 772 if qt != -910
yg dec -271 if bi == 990
uoc dec 983 if gy != 966
t inc -355 if uoc >= -636
hfh dec -486 if yr == -1335
itw inc -102 if tc < 905
gif inc 663 if b > 862
fi dec -546 if js < -1294
bhp dec -955 if qt > -914
qt dec -927 if ih == 1054
gy inc -272 if uoc <= -620
yg inc -30 if nwe != -291
nwe dec 393 if ih != 1048
ih dec 904 if tc != 915
nwe inc -211 if j != 1084
qt dec 249 if tc <= 913
ioe dec -703 if gy <= 695
hfh inc -836 if j <= 1079
tc inc -153 if qk <= 255
bhp dec -697 if uoc == -627
hfh dec -762 if qk <= 263
xly inc -369 if qk >= 269
xly inc -135 if xly > -80
j dec 773 if x <= -2468
iyu inc -541 if iyu < 9
itw inc 870 if xly < -77
x inc 646 if x >= -2474
qk dec 447 if fi >= -287
qk dec -37 if yr >= -1344
nwe inc 406 if gy >= 703
j inc 72 if j > 299
qt inc 946 if t < 306
hm dec -406 if cp == 451
ioe inc -906 if bhp < 849
xly inc -924 if b <= 871
xly inc -645 if qt == 709
b dec -578 if bi <= 999
js inc 237 if t <= 304
gif inc 425 if bi <= 989
uoc dec 318 if x < -1821
fi inc 455 if bhp < 859
fi inc -282 if iyu <= -537
bhp dec 980 if cp > 451
t dec 204 if gif > -967
ioe inc -273 if fi == -114
iyu inc 940 if yr != -1335
t inc -593 if uoc == -945
tc inc -34 if yr >= -1335
yg inc 204 if gif < -969
nwe dec -128 if iyu == -541
qk dec 949 if yr >= -1335
nwe dec -582 if js >= -1063
tc dec -550 if cp <= 452
js inc 427 if ioe <= 407
bhp inc -672 if gif == -961
b dec 890 if hm > -1419
qk dec -801 if iyu > -538
t inc 789 if tc != 1433
tc dec 134 if hm >= -1413
cp inc -287 if b > 552
b inc -131 if bhp == -800
iyu inc 991 if b <= 429
itw inc 3 if hm > -1411
ih inc 263 if b < 428
j dec 848 if t <= 294
xly inc 94 if ioe != 406
j inc 116 if qk < -1094
t inc -779 if yr >= -1338
nwe dec 910 if tc >= 1281
uoc inc -945 if itw <= 975
xly inc -675 if hm <= -1410
xly inc -58 if x == -1825
hfh inc -701 if bhp < -798
nwe inc -586 if uoc > -952
x inc -164 if b < 435
hfh inc -641 if j != -354
yg inc 827 if qt > 699
iyu dec -512 if t < -478
j inc 651 if itw < 987
yg dec 537 if hfh >= -289
ih inc -289 if qk <= -1094
itw dec 158 if nwe > -2260
xly dec 704 if hfh == -289
itw dec -564 if j <= 305
nwe inc -820 if hfh >= -290
bhp dec -252 if nwe < -3065
bhp dec 680 if itw <= 1385
cp inc 811 if t == -486
x inc 423 if b > 419
itw inc 131 if gy <= 693
qk inc 500 if nwe == -3065
gif dec 583 if yg <= -981
x dec -261 if fi > -115
itw dec 314 if cp >= 977
itw inc 197 if b < 429
ioe inc -666 if x <= -1314
hfh inc 345 if hm < -1402
b inc 653 if itw >= 1719
qk dec 298 if itw != 1701
t dec 943 if qt > 703
gif inc 430 if qt > 715
bi inc 60 if gy > 687
j inc -816 if gy > 685
xly inc 334 if js != -1075
qk inc -388 if xly != -2656
t inc 475 if fi <= -113
t dec -842 if nwe > -3078
fi inc 284 if cp == 976
xly inc 265 if ih >= 127
qt dec -481 if hfh < 58
xly dec -484 if ioe <= 415
ioe inc 525 if xly >= -2183
t inc 658 if tc >= 1281
nwe dec -680 if t != 555
qk inc 395 if tc <= 1293
bi dec 422 if gy > 683
itw inc 713 if t == 546
bhp dec 285 if iyu < 967
gif inc -186 if tc <= 1297
iyu inc -858 if iyu <= 954
gy inc -320 if nwe < -2384
yg inc 691 if qt < 1199
hm inc -19 if j <= -528
bhp dec 403 if xly == -2175
gy inc -886 if qk >= -1399
yg inc 404 if js < -1065
gy inc -255 if fi > 160
bhp inc 740 if bi <= 620
b inc -548 if nwe != -2398
t dec 255 if nwe >= -2397
t inc -678 if js >= -1064
js inc -497 if js >= -1071
iyu dec -463 if hfh >= 52
js inc -537 if b != -122
t inc -518 if yg != 129
ih dec 208 if yr >= -1341
qt dec -566 if yr > -1340
itw inc 113 if nwe == -2395
ioe dec -471 if itw != 2424
cp inc 222 if tc > 1283
ih inc 28 if bhp != -1915
yg inc -923 if xly <= -2169
bi inc 893 if yr == -1335
cp inc -459 if gy > -771
b inc 958 if bhp != -1922
yg dec 896 if tc < 1298
ih inc 385 if hfh != 48
j dec -663 if iyu <= 1419
fi dec 89 if x != -1297
tc inc -294 if gif != -1147
uoc inc -232 if qk < -1381
bi inc 743 if hfh >= 48
gif inc 317 if yr == -1335
hm dec 938 if xly <= -2172
iyu dec 961 if xly > -2173
qk dec -69 if qk == -1390
ioe dec -450 if hfh != 52
j inc 944 if qt != 1748
x dec 48 if gy <= -764
yr dec 505 if iyu <= 1422
x dec 316 if yg < -1695
bi inc -386 if nwe != -2392
xly inc 363 if fi < 87
uoc dec 629 if nwe < -2384
ioe dec 816 if bhp < -1909
uoc inc 196 if fi == 81
gif dec -196 if iyu > 1419
bi dec -652 if nwe != -2402
gy dec -416 if yg > -1701
bhp dec 78 if qk < -1384
bi inc -660 if ih != 328
ih dec 352 if fi >= 79
ioe inc 288 if itw == 2414
b dec -461 if xly < -1814
t inc -291 if js > -1557
tc dec 502 if j == 432
gif inc -800 if qk >= -1384
yr dec -59 if gif > -641
yg dec 360 if yr <= -1275
nwe dec 790 if gy >= -358
b dec 544 if yr == -1268
tc inc 743 if ih <= -22
bhp inc 314 if cp <= 744
x dec -708 if tc <= 2039
iyu inc 968 if yr != -1274
x dec 454 if hfh <= 61
qk dec -41 if bhp >= -1685
ih inc 302 if tc != 2032
cp inc -167 if yr == -1281
ih inc 349 if qt != 1760
gif inc -367 if uoc <= -1603
xly inc 229 if gy <= -344
gif dec 459 if fi >= 76
hm dec -297 if gy <= -347
j dec -320 if iyu <= 2397
qt dec 634 if b <= 837
cp dec 342 if ih > 324
ioe dec -119 if qt > 1120
x dec 564 if hfh != 56
tc dec 779 if qt < 1130
hm dec 586 if ioe > 695
yg dec 373 if ioe > 683
xly inc 344 if iyu <= 2397
hm inc 91 if qt < 1121
ih inc -219 if ioe <= 699
ih inc -937 if ioe <= 702
hfh dec -805 if t != -221
itw dec -385 if ioe == 693
nwe inc 946 if js != -1564
qt dec 685 if hm == -2052
itw dec -625 if bhp <= -1674
ih inc 933 if qt < 442
js inc 971 if itw != 3437
hfh dec 444 if uoc >= -1617
itw inc 182 if itw >= 3433
hfh inc 509 if gy > -353
ioe dec 952 if bhp > -1688
b inc 210 if uoc > -1615
itw inc -579 if hm < -2044
hm inc -427 if j <= 739
uoc inc 90 if nwe > -3190
ih inc -678 if x <= -1407
js dec -238 if x >= -1419
ih inc -263 if tc > 1244
ioe dec 30 if gy == -352
t inc -76 if ih < -837
yr dec -442 if cp < 400
iyu inc -425 if j < 751
qk inc -189 if js <= -350
yg inc 282 if t >= -311
b inc 11 if j > 737
qt dec -839 if bi == 1863
tc inc -40 if bhp >= -1688
b inc 439 if yg >= -2142
bhp dec 144 if b == 1057
gy dec 460 if b <= 1065
qk inc -681 if itw >= 3035
iyu dec 0 if j > 740
ih inc -189 if uoc <= -1520
bi inc 360 if hm > -2058
yr inc 603 if xly <= -1232
j dec 68 if fi < 90
yg dec -425 if gy <= -804
x dec 295 if x != -1405
nwe inc -355 if j <= 679
ioe dec -506 if xly < -1229
bhp inc -489 if j >= 671
hm inc 730 if ioe > 209
gy inc -464 if iyu < 1968
bi dec 191 if yg > -1731
j dec 707 if gy < -804
ioe dec -180 if qt == 437
ioe inc 909 if yr != -231
yr dec -627 if ih == -1027
itw dec -145 if gy == -818
hm inc -156 if gif == -1460
hm inc 301 if yg < -1735
gif dec 137 if bi == 2039
cp dec 777 if yg <= -1717
fi dec -90 if yg != -1726
cp dec 256 if tc <= 1217
gy dec -401 if ioe >= 393
js inc 850 if tc <= 1219
iyu inc 124 if hm >= -1478
tc dec 341 if nwe > -3534
yr dec -13 if tc <= 1218
x inc -44 if cp != -634
t inc 591 if qk >= -2229
t dec -38 if itw <= 3045
xly inc -996 if ioe >= 396
cp inc -21 if x > -1757
t dec -989 if itw >= 3033
uoc inc 193 if fi >= 74
x inc 234 if uoc > -1333
hm dec -353 if gif > -1597
cp inc 684 if t == 1315
bhp dec -437 if iyu < 2095
xly inc 59 if j == -30
nwe inc 176 if fi >= 81
b inc 573 if iyu > 2086
gif inc 526 if gy > -418
ioe dec 261 if b >= 1625
qk inc -976 if bi != 2040
qt inc -648 if cp >= 31
x dec -490 if iyu <= 2099
bi dec 450 if x <= -1021
hm dec -317 if hm >= -1485
x inc 462 if ih != -1032
qt dec 436 if yr == 409
uoc inc 289 if b <= 1638
qk inc -998 if uoc > -1045
j dec 387 if b < 1622
gy dec 13 if cp >= 24
bi dec -822 if js != 489
ioe dec -140 if xly < -2170
uoc dec -15 if ih < -1021
x dec -906 if hfh < 923
itw dec -986 if yr > 402
cp inc 474 if gy < -421
gy dec 530 if fi == 72
fi inc -277 if tc < 1214
ioe inc -26 if gif < -1066
itw dec -900 if ih < -1025
fi dec 443 if yg < -1719
hfh inc 602 if uoc == -1023
yg inc 409 if ih == -1027
t dec -366 if t != 1323
gif inc 158 if yr == 409
iyu inc 464 if hfh >= 1525
xly inc -71 if yg > -1319
cp dec -247 if qk > -4201
cp dec -278 if cp > 745
tc inc -903 if ioe < 257
itw inc 221 if bhp < -1870
yg dec -352 if j >= -31
ih dec -77 if qk < -4188
itw inc -921 if gy < -414
tc dec 565 if iyu != 2561
uoc inc -564 if cp <= 1035
ih dec -962 if qt == 1
hfh dec 646 if itw < 4222
js dec 604 if qk != -4186
hfh dec 389 if itw != 4214
cp inc -819 if bhp == -1876
tc inc 121 if qk <= -4185
cp dec 330 if t == 1677
nwe inc -864 if gy >= -417
uoc dec -459 if hm == -1155
x inc 886 if tc >= -124
tc inc 954 if yg != -973
qk inc 408 if x <= -563
hfh inc 37 if uoc == -1587
bhp dec -426 if cp == 197
nwe inc -46 if itw < 4233
j dec -546 if fi <= -637
bi dec 421 if xly > -2253
cp inc -991 if nwe >= -3417
gif inc -9 if hfh >= 1169
yg dec 397 if tc >= 816
gif inc 258 if js > -115
bhp dec -594 if tc != 822
t inc -975 if ioe != 253
tc inc 999 if ioe >= 242
gif inc 746 if yr < 419
gy inc -980 if yr > 407
qt inc 892 if cp > -789
itw inc 806 if yr != 402
hm dec 977 if iyu <= 2559
gy inc 848 if gy >= -1405
bi dec -310 if qt == 893
hfh inc -40 if yg == -1353
js inc -626 if j == 516
iyu dec 863 if yr < 408
bi inc 321 if b < 1633
qt inc 895 if hfh <= 1178
hfh dec -782 if js == -735
cp inc -443 if yr < 410
yg inc -446 if bi >= 2626
uoc inc -251 if tc < 1824
ioe dec 549 if yg < -1360
x dec -961 if yg <= -1362
gif inc -184 if js > -743
js dec 929 if x <= 397
yg inc 994 if yg == -1362
js dec -246 if bi < 2629
yg dec 875 if j <= 522
ih inc 287 if yg >= -1240
itw inc 615 if hfh == 1958
ih dec -665 if qt <= 1790
itw inc -747 if qt == 1788
iyu inc -631 if tc > 1810
j inc 178 if bhp >= -1273
nwe dec 85 if itw >= 4897
tc inc 530 if cp > -1222
fi dec 329 if tc >= 1819
uoc inc 1 if iyu != 1932
t dec 905 if nwe > -3497
j inc -836 if ioe >= -299
xly dec 185 if nwe > -3499
b inc 41 if gif >= -92
iyu dec 315 if t > -196
nwe dec 676 if uoc != -1839
bi dec 380 if tc != 1819
ih dec -397 if gif != -102
yr dec 449 if js > -1420
hfh inc 584 if bhp <= -1282
ih inc -524 if ioe >= -304
gif dec 688 if hfh == 2542
hm dec -370 if bi == 2621
js dec 575 if hm != -1768
qk dec -85 if itw != 4893
xly inc 961 if tc == 1819
itw inc 913 if qk <= -3699
bi inc -858 if js != -1420
t dec 622 if bi != 1764
qt inc -980 if fi >= -964
fi dec -380 if js == -1418
qt dec -446 if bi != 1772
gy inc -414 if yr != -50
qt dec 628 if itw < 5813
bi inc -649 if fi >= -588
uoc dec 821 if iyu >= 1916
gy dec 19 if xly > -1472
qt dec 137 if qt <= 1608
bhp dec -850 if t >= -830
bhp inc 19 if iyu != 1928
yr dec 272 if ih == 153
js inc 628 if hm > -1775
qk inc 893 if b != 1630
fi dec -959 if gif < -783
x inc 848 if bhp <= -405
bhp dec -555 if j != -318
gif dec -906 if hm > -1772
itw dec -722 if hfh == 2542
bhp dec 960 if j != -327
js dec 670 if gif >= 123
itw inc 153 if yr > -313
t dec -712 if ioe == -299
ioe inc 345 if itw == 6693
hfh dec -411 if bi <= 1123
hfh dec 368 if qt == 1469
ioe dec -688 if itw != 6684
cp inc 888 if bhp != -818
iyu dec 184 if fi != 378
xly inc 143 if fi == 371
cp dec 691 if ih <= 151
bi dec -712 if xly != -1333
nwe inc 231 if bhp < -808
qt dec -736 if fi != 361
hm inc 536 if gy < -988
yr inc -539 if j < -311
xly inc -311 if bi <= 1833
qk inc 271 if yg != -1237
uoc dec -40 if gif < 119
nwe inc 223 if yg >= -1239
qt inc 501 if itw <= 6685
xly inc 11 if t >= -109
hm dec 147 if j != -328
js inc -15 if hm == -1379
bhp inc -841 if fi > 362
fi inc -26 if ioe != 397
fi dec 283 if tc > 1817
uoc inc 105 if j < -314
ioe inc -885 if gif < 113
xly dec -849 if iyu == 1746
j inc -614 if yg != -1243
yg inc 115 if fi <= 62
itw inc -405 if cp != -1222
hm inc 854 if gy >= -988
tc dec 143 if cp >= -1232
gy dec -111 if b < 1635
ioe inc -939 if hfh == 2585
gif dec 43 if bi < 1831
gif dec 519 if itw < 6284
nwe inc 197 if gif != -449
bi dec -71 if bi != 1835
ih inc 479 if hfh >= 2595
ih inc -979 if qk <= -3438
iyu dec 653 if uoc == -2513
yr dec -346 if hfh <= 2585
gif inc 572 if xly != -1638
qk dec -891 if j > -322
qk dec -911 if ih == 150
tc inc 995 if iyu <= 1094
hfh inc -129 if x == 1241
xly dec -533 if gy > -884
gif dec 806 if b == 1630
hfh dec -805 if hfh >= 2453
iyu inc 242 if qt < 2714
fi dec 878 if bhp <= -1661
t dec 166 if hm == -1379
ioe inc -456 if hm == -1379
hm dec 225 if cp >= -1228
xly dec 820 if qt <= 2707
tc dec -786 if bi != 1898
uoc inc 751 if qk != -2547
b dec 922 if x >= 1238
j dec -179 if b <= 715
hfh dec -590 if qk == -2539
x dec 187 if itw == 6280
bi dec -967 if js < -804
tc inc 133 if qt <= 2713
t dec -411 if tc <= 3596
uoc inc 264 if fi < 61
gy inc 223 if ioe <= -1000
gif dec -942 if ioe >= -1008
hfh inc 999 if gy > -662
fi inc -485 if ih <= 153
nwe dec 733 if ioe > -1015
x inc -753 if bhp <= -1653
js dec 784 if gif < 263
bhp dec -307 if b != 703
itw dec -757 if ih == 153
iyu dec 785 if bi >= 2874
nwe inc -450 if t == 136
bi dec -77 if yg > -1132
js dec 990 if yr > -515
iyu inc 337 if bi > 2933
uoc dec 241 if ih != 153
bi dec 459 if hm == -1604
iyu inc -85 if bhp > -1353
qk inc -437 if hm != -1597
x inc -852 if qt <= 2708
gy dec 311 if hm != -1604
iyu dec 876 if t <= 143
x dec 779 if yr <= -513
hfh inc -843 if qt < 2707
bi inc 536 if yg < -1120
itw inc -886 if hfh != 4003
js inc 107 if hfh <= 4009
x dec 156 if yr >= -504
hfh dec 971 if j < -140
tc inc -480 if hm != -1595
x inc 993 if j > -142
tc inc 910 if yg != -1120
tc dec 571 if x != 432
x dec -194 if nwe > -4935
yr inc 288 if b <= 717
fi inc 970 if tc >= 3454
xly dec -554 if fi <= -431
ih inc -380 if iyu <= 706
qk dec 162 if nwe == -4925
fi dec 396 if gif != 261
xly inc 698 if yr >= -224
qt dec 57 if hm >= -1607
tc inc 555 if x <= 644
x inc -20 if xly == -1217
ih dec -766 if ioe <= -1005
tc dec -101 if gy != -660
hm dec 989 if ioe == -1006
yr inc 995 if gy != -651
cp inc -70 if cp < -1232
gy inc -408 if hfh > 3027
yg dec 360 if bhp >= -1352
hm inc 263 if qk < -3137
qt dec -603 if yg > -1495
xly inc 516 if js <= -2466
ioe inc -442 if uoc >= -1764
ih inc 898 if gy < -1053
gif dec 863 if tc >= 4114
hfh dec 478 if hfh != 3030
js dec -623 if hm > -2328
itw dec 792 if qt > 3244
yg inc -367 if tc == 4105
gy inc -330 if b <= 709
x inc 821 if t == 136
bi inc -979 if cp >= -1235
nwe dec -608 if qt == 3252
cp inc 885 if yr == 778
js dec 197 if b > 717
ih inc -737 if qt != 3248
iyu dec -570 if tc < 4107
x dec 63 if bi == 2039
t dec 323 if j < -133
tc inc -962 if tc > 4098
ih inc 663 if xly == -701
ih inc 489 if ih >= 1363
uoc inc -393 if js >= -2480
j inc -62 if cp == -342
gy dec -29 if bhp < -1348
yg inc -202 if nwe >= -4322
fi dec -628 if bi == 2039
itw inc -338 if itw > 5350
qt dec 24 if qt > 3242
tc inc 877 if t == -188
nwe inc 521 if ioe != -1448
gif inc -328 if gif > 261
ih inc -277 if bhp != -1351
tc inc -61 if qk > -3148
nwe inc 588 if xly != -702
qk dec -762 if qk >= -3141
gif inc 937 if b <= 708
t inc -749 if fi == -191
js inc 814 if qk == -2376
j inc -479 if bhp < -1346
iyu inc -941 if gif == 876
js dec 411 if hm > -2326
itw inc -832 if gy == -1364
iyu dec 537 if xly != -706
yg inc 687 if js == -1658
b inc 741 if tc > 3073
x dec 86 if ih < 1584
fi inc 851 if t > -940
x dec 680 if qk >= -2373
gy inc 882 if itw != 4183
tc dec 711 if fi < 658
yg dec -166 if qt == 3228
yg dec 684 if fi > 661
bi inc -715 if gif < 875
gy dec 168 if nwe != -3719
gif inc 301 if yg != -1204
ioe inc 293 if cp >= -344
yr dec -395 if bhp <= -1351
qk inc 97 if js > -1659
hm inc 371 if gif > 865
gif inc 443 if uoc >= -2151
nwe dec -677 if cp > -351
qk dec -845 if yg != -1201
ih dec -890 if cp != -337
nwe dec -932 if hfh > 2549
xly dec -281 if ih <= 2473
cp dec 76 if j == -682
hfh dec 236 if gy == -650
uoc inc 804 if b != 1448
iyu dec -791 if yg <= -1205
yr inc -720 if bi == 1324
b inc -234 if yg == -1204
bhp dec 728 if t != -938
qk dec 703 if itw == 4194
hfh dec 944 if uoc <= -1360
yg dec -384 if hfh < 2324
hm inc -765 if fi == 660
b inc -769 if hfh == 2322
ioe dec 681 if b >= 455
iyu inc -795 if itw > 4180
bhp dec -533 if t < -933
j inc 861 if ih < 2475
ioe inc 136 if itw < 4189
b dec 400 if fi >= 663
fi inc -483 if gif < 868
bhp dec 815 if qt == 3228
x inc -236 if yr > 456
xly inc 516 if b > 443
bhp inc -95 if x != 1295
uoc inc 174 if js > -1668
bi inc 126 if b <= 454
uoc inc -742 if gy >= -655
ih dec -381 if qt > 3235
itw inc -493 if bi <= 1449
fi inc -673 if fi <= 653
ih inc 55 if x >= 1284
tc inc 55 if fi == 657
ioe inc -715 if yg <= -820
nwe inc -665 if qk != -1432
qt inc -129 if b <= 443
fi inc 984 if yg <= -819
cp dec 112 if bi < 1454
j dec -268 if hm <= -2729
fi inc -620 if itw <= 4191
nwe dec 330 if tc != 3077
xly dec -490 if bi == 1450
cp dec 922 if yg >= -825
itw inc -752 if uoc < -1914
yg inc 502 if tc == 3082
hm inc 987 if qt > 3226
qk inc -234 if bi <= 1451
ioe inc 201 if iyu > -64
fi dec 730 if xly <= 591
js inc 506 if qk <= -1661
x dec 453 if yr <= 455
t inc -941 if uoc > -1923
hm inc 30 if bi < 1458
gy inc 947 if fi > 292
hm dec 976 if iyu == -56
tc inc -420 if qk <= -1667
tc dec -779 if gy < 302
bi inc -814 if x < 845
qt dec 484 if bi > 643
yr inc 363 if fi >= 290
ioe inc -314 if gif > 866
ioe inc 84 if yg < -312
bhp dec -941 if itw >= 3447
gif inc -791 if ioe >= -1898
yg dec -440 if ioe != -1904
b dec -673 if x != 835
tc dec -547 if qt <= 3233
t inc 641 if yg < 120
xly dec 548 if fi < 293
iyu dec -947 if hm >= -2687
gif inc 643 if yg > 119
iyu dec 681 if j >= 174
ioe dec 326 if cp == -1452
cp dec 489 if js == -1151
bi dec -814 if js != -1152
tc inc -803 if yr >= 812
qk inc 155 if itw >= 3429
bhp inc -445 if tc < 3188
itw inc -280 if js > -1147
js inc -921 if ioe > -2227
gy inc 200 if b == 446
js inc -943 if nwe > -3125
gif inc -412 if hfh == 2322
t inc 240 if ioe == -2225
iyu inc 774 if b != 440
yr dec -305 if ih != 2522
ih dec -24 if uoc > -1928
yr inc 770 if gif != 1102
iyu inc 440 if yg != 115
gy inc 148 if qt >= 3226
ih inc 534 if tc != 3182
yr inc -140 if js > -3019
ih dec 369 if bhp < -2894
x dec -155 if uoc <= -1915
x inc -627 if nwe < -3114
yr dec -446 if tc != 3180
cp dec -638 if xly <= 593
gy inc -97 if x <= 370
qk inc -769 if fi == 298
hfh dec 365 if bhp < -2906
j dec -533 if xly != 586
x dec 24 if uoc < -1910
fi inc 391 if tc <= 3182
yg inc -329 if b >= 440
ih dec -722 if t >= -1633
qk dec -517 if x >= 340
gif inc 404 if qt != 3225
x inc -52 if ih >= 2716
hm dec 759 if js < -3008
nwe dec -356 if yg >= -212
ioe inc -337 if x == 332
xly inc 610 if cp < -811
j dec 778 if j >= 178
qk dec -477 if ioe != -2231
gy inc 61 if b < 451
hfh inc -726 if gy <= 615
iyu inc -899 if itw < 3441
ih dec 980 if qt >= 3232
js inc 165 if yr != 1425
js dec -668 if yg == -207
b dec 584 if x < 339
qk dec -606 if cp < -806
gif inc 269 if hm <= -3437
js dec -472 if t > -1638
fi inc -853 if xly < 1197
gif inc -716 if iyu != 522
cp inc -536 if itw < 3440
hfh dec -848 if hfh <= 1594
hfh inc -135 if iyu <= 525
fi dec -789 if hfh > 1459
bhp dec -689 if uoc == -1919
hfh inc -507 if ioe <= -2221
yg dec 440 if ih > 2714
qk inc 99 if bhp != -2213
qk dec -330 if hfh >= 958
j inc -946 if ih != 2717
bhp dec -107 if gy < 619
cp dec -797 if t >= -1641
cp dec 615 if fi <= 228
nwe dec 502 if t != -1641
hm dec -58 if hm < -3433
t inc -324 if nwe < -3257
hfh dec 754 if gif > 1067
hm dec 2 if ih == 2709
bi dec -992 if fi == 230
t dec 646 if ih == 2709
qk dec -549 if nwe == -3261
tc dec -772 if nwe <= -3260
fi dec 640 if fi == 230
ioe dec 921 if tc >= 3957
tc dec 79 if tc <= 3964
tc inc 541 if yr == 1427
qk dec 331 if cp >= -546
ih dec 561 if itw >= 3438
itw inc 558 if qk != 119
hfh inc 809 if hfh >= 948
itw dec 981 if xly < 1197
ih dec -431 if js > -1708
yr dec -737 if hfh != 1754
tc inc 873 if ih != 2709
bhp dec -53 if hm != -3395
fi inc -207 if j >= -1541
bi inc -484 if itw >= 2449
js inc 548 if j > -1549
tc inc 336 if nwe != -3257
gy dec 39 if hm == -3386
gif inc 186 if itw > 2450
hm dec 671 if fi > -404
bi inc 950 if qt > 3227
gif inc 667 if fi > -414
x inc 380 if qk != 111
t dec -194 if js > -1172
yr inc -954 if itw < 2449
bhp inc 61 if j <= -1539
x inc -893 if bhp > -1997
ih dec -169 if iyu > 522
b dec 211 if cp >= -548
gif inc -278 if fi < -406
js dec 22 if t < -2404
ih dec -350 if nwe <= -3255
yg dec 198 if qk != 116
fi inc -466 if bi != 2091
yr inc -28 if yg == -405
cp dec 165 if xly < 1201
hm inc 875 if bhp >= -1995
qk inc 856 if gy > 565
hm inc -20 if iyu == 525
hfh dec -771 if itw > 2448
ioe dec 162 if fi == -874
yg inc -662 if ih < 3233
yr inc -214 if t == -2413
xly dec -251 if yg != -1065
itw inc 361 if ih < 3234
qk inc 206 if yg <= -1060
t inc 398 if ioe == -3146
qk dec 35 if b > 437
itw inc -841 if xly != 1445
hfh inc -461 if yr == 1922
hm dec -698 if itw != 1968
fi dec -98 if bhp < -1986
t inc 224 if hfh < 2080
j inc -767 if hm == -1833
uoc dec -817 if j > -2320
qt inc -801 if tc < 4761
hfh dec 597 if iyu == 525
itw dec 670 if cp != -709
ih inc 173 if bhp <= -1983
js inc 904 if hm == -1833
ih dec 913 if bi >= 2091
x inc 841 if itw < 1297
t dec 74 if gif == 1634
j dec 302 if yr != 1916
xly dec -815 if bi > 2092
qk inc -349 if x <= -168
xly inc 974 if fi >= -783
ih inc -251 if t == -1865
ioe dec 314 if nwe != -3253
fi dec -245 if ih < 2239
js dec -69 if js <= -272
yg inc -33 if j != -2623
xly dec -146 if ioe < -3451
bhp dec 757 if itw < 1309
qt inc -472 if yr >= 1918
xly dec 662 if hfh < 1477
nwe dec 66 if iyu <= 528
gy inc 47 if j > -2623
bhp dec -567 if bhp < -2746
gy inc 968 if nwe >= -3333
t dec -731 if iyu >= 519
gif dec -522 if j > -2621
qk inc -855 if tc > 4754
gif inc 125 if gif <= 2159
iyu dec -400 if hfh >= 1476
nwe dec -751 if qk >= -67
hm inc -645 if ioe != -3466
bi inc -235 if nwe >= -2571
b dec 981 if yr >= 1922
gy dec -452 if hm <= -2475
fi dec -532 if qt <= 1959
cp inc 612 if hm >= -2487
fi dec -455 if bi < 2097
t dec 80 if iyu <= 934
xly inc -694 if hm == -2478
uoc dec -759 if qk >= -59
ih dec 244 if yr >= 1918
tc inc 638 if ih > 1998
cp inc 1 if fi < 458
cp inc 454 if nwe > -2573
gy inc -134 if hm < -2471
yg dec 1 if iyu >= 919
js inc 91 if yg < -1108
yg inc 173 if ioe <= -3458
gy dec -760 if t != -1214
qk dec 816 if iyu < 920
gy dec 226 if itw < 1301
bi dec 704 if tc == 4755
iyu inc -661 if nwe != -2572
nwe inc -425 if qt < 1963
gy inc 197 if qk == -58
t inc 853 if nwe == -3001
js dec 603 if yg >= -928
j dec 708 if xly == 2026
hm dec 836 if b == -535
x dec -132 if itw > 1299
cp inc 587 if gif > 2278
t dec -715 if nwe <= -2994
b inc 63 if yr == 1922
tc inc 147 if bi > 1383
yg dec -24 if cp <= 476
gy dec -11 if j == -3322
bhp inc -488 if tc >= 4903
iyu inc 890 if hfh >= 1476
qk inc 224 if yr <= 1928
bhp dec 845 if itw > 1303
uoc inc 965 if qk != 166
fi dec -186 if cp == 482
t dec 388 if js >= -814
hm dec -157 if t < 357
hfh inc -353 if nwe == -3008
x dec 846 if cp >= 491
fi inc -436 if iyu <= 1154
qk dec 465 if bhp == -3027
yg inc -489 if t <= 354
js inc 809 if gif >= 2284
iyu inc 757 if hfh != 1479
uoc inc -765 if yg != -1415
bi inc 637 if nwe == -3005
ih dec 369 if ih == 1993")

(def day9
  "{{{{},{{{<a!!!>aa!!!>,,,}!>!>!!eo!!a>},<'!!!>,<!>}!!!>u!'!>,<!!!>!>o!!!>,<'!!!>!!!!<{{i>},{{}},{{<i!!!>u{!!eu!!>}}},{{{{{<\"!>},<!o!>},<<!!!!!!,!!i>},{<{!>},<ii'>},{{<,o!!<'!!\"!!!>>,{}}}}},{}},{{}}}},{{{{<e!e,!a'!!!>}e\"}>,{<o!>,<{<!>},<<!<'!!ia>}},{{},<'!!!>!!e<\"{u!>},<<!!!!!}>}},{{{{<!!!>!!!!iu!!!>!!!>{<eu}i,!!!!!!>}},{{<>,<!!!!o!a\"{!>},<i!>,<!!u!!a!>},<\"!i!>},<}au!>!!o>}},{<!>},<e}iaae'}!>!,!>o{,e!!{!>>}},{<\"\"!>!>a>,{<!{'u!!}{u,!>},<!>,<'}a>}},{{},{<}!!!>'o>}}},{{{<oi'<}!!!!>},<'!>},<u!>>},{<!!!!!!!>},<e!!!eu!>!o{'o,!!>,<{,oui'\"!!\"i!!!>},<<u}!!\"aa!>,<>},{{{{<,'{'!>},<o}!>!>},<\"!u!,!a!>},<\"{>},<!>,<!{!>!!<oo}>}}}}},{{},{<<>},{}},{{{<!!!>},<,!'!!'u'!>},<!!ii!!!>i>},<!i!!}<'>},{{{<!!!!a!!!!a>}},{<!!!>aie!>>}},{{<}!!!>a!!i!>!!!!!>o!>,<>,{{<!<>}}}}},{{{{<{a!>,<!!!>!!u{!\"o!>,<>,{<e!!o>}}}},{{{{<{e!!!!!!o{''!>,<>},{{{{},{<u!>},<!>,<!>>}},{}},{<>}},{{{<!!!>{!!o{}!!!>,,!>'u\"'!!}!><!!!>>},{<!!!>ou!>},<!>ui,!!}{!!!>!>,<\"{!!!>},<>}},{{<oa!!!>u!{u!''ae<e>},{{<uu!!!><!!!>>}}}}}},{{{{<!!!!{<}''\">,<}!>},<!>},<!>},<!!!>!!!!!!}!o!>},<}a>},{{{<e'\"iau!>},<>,{<!!!>ie!!!>!!!>,<!!i<!!!>o!eu<'{>}},{<,a!!!>!>},<!!!!!>,a!>!!e!!!>},<ui!}>,{<!>},<aao'!>},<ea!!!>,<>}}},{{},{}}},{}},{{{<!'}!ei>}},{<!!!>,<!!u'}>,{<!>},<o}iu<!>>}}},{{{{}},<\"{\"!>},<!>!>},<i!>},<{}u>},{{{{{<e'!!u<!!!>o\"<<!!u!>,<{\"!oe!>,<!u>}},<ae!!!!!>!>},<!>,<!!!>{>}}}},{{{},{<\"{o\"}\"<!!!>}>,{<!>},<!!!!!>ee{!!!!!>iu>}},{{<,!>a!!u!!u<\"{!>},<!>},<,!\",i!!e{>},{<ue}e!!,oa!>},<\"!>},<!!o>}}},{{{{<!>{!>,<!e'i!!<!!!!!e>},{{{}}}},{{<\"!!'!!!>oo}}''!!!!!>!!!>},<ii''>},{{<!!!>u!!a!>!!'o!!!>,<i!>!!!!<',o}>}}}},{{{{},{{<}}'i\"u!!!>,<}!!!!{!e}!!u\"!!!>>},{<,i!>},<!>{i!i{!o<\"oe!!u!>i>}}}},{{{<<a!><>,<{e!,!><!!iaei!!e,<o!>},<!o>}},{{<}!o}!!i!>!!o}o!!!>oeia'!\">,{}},{<!>,<!>,<{o}ia!>},<!>!>},<\"\"!i'>},{<i!!e!!!>!!!>o!e>}},{{{{<{!!!>,<!>u''>},{}}},{{{{<}!{!>,<!>,<!!!>a!>},<}!>!!!>>},{{{<a\"o!>!a!!!!<!!i<i'\"!!!uu>},{<i!>,<oii\",!>!o>}}}},{{{}},{<e!>!!!>uau!>,<}!!u!!!>!!!>!>,<eo>,<}!!!''a'a!!!>'!!!>!>},<e,,!>},<'<!>,<<>},{{{<u!>},<!u{!>,<!>},<!!\"!>>},<e!{!!!!>}}},{{<\"!!i!,'<',u,!!\"'aa}!>>,<'a!>},<!>},<{aa,!<!!!!u!>!>,<!>,<!>!>,<<>},{{<>},{<a'!>!>},<'!uu,o!>,<<\"!>,<!>},<!,!!!!>}},{{<i!ie!>},<!>!>,<!!e\",oi,!>'<<!!>,{{<>},{}}}}},{}},{{},{<!\"o>,{<u!!,!!!>'i!!{ea!>\"!>,<!!!>!a{>}},{<!>,<!!<'!!!!e!!'!>,<!!,!>,<<!>,<!!!>ua{>}},{{{},{{}}},{{<!>oia!>!>},<!!!>},<i\"!!}>},{<a!!!>,<>}}},{{{{{<!!!>!!\"!!!!,,a!!!>i}e{!>},<!!!!au!>,<o!>,<!!,>,<!!}!>,<!!\"<!!}i!!!>u!>},<i!>,<!>>},{{{{<e<<{!>!\"!>},<\"}',}<e<!,>},<a!><}ua>}},<ea!!!>!>},<'o'<!u\"u\">}},<!e!!!!!!\"}>}},{<!>},<!>{a>},{}}},{{{},<u<!!u!>>}},{{{{{<!,\"'!}o!!!e!>!!e\"ee>}},<o!u<{!!u<!{!>!>\",{a\"{}>},{{<}}!!!!!>!!!>,<\"!>},<uuou,{,>},{<!>},<!!!!o!>},<!a!!!><e>}},{<!{!!u!ee!>,<!>,<!!,{{ia}a\"!!,!>>,<>}},{{{{<!>,<!!!>e>}},{<a!!'iiaa<!a!!'o!>!>!!!>,<{>},{{{},{{<!!!!!!oa}<!>!e'ia!>!>},<{e}!!a!\"!>,<o<>},{<'!}\"e>}}}}},{{<!>},<}!>},<i!>!a!!!>,!!!>,<,!>},<e}>},{},{<>}},{{{{<e!>>,<!!!!!>},<!>,<ee{e'}>},{<!!e!!!><!>,<,!<{!>{>}}},{{<!!\"!>},<!!!>,<,!!\"uu>}}},{{<!'o\"!!!>!>,<e!>,<!!o{!!!!!!!>!!!!!!!>{!>},<>,<uo\"!>},<!oe!'!!a>},{{<,!>!!>,<!!e<,}i,!!>},{{{<'e,<<}!!{!!io!>},<>}}},{{}}},{}}},{{{{{}}},{<}!!e!!}!>!>},<\"!,'{!!!>>,{<!o>}}},{{{{{{{<,,{,ei!!!>},<'!>,<\"!>,<u}!!u>},{<}!\"!!!>,<ooe!!e!!ai}!!\"i>}}}},<!>!>},<!>},<!!'>},{<!>},<!>!>,<<i!!!>!>\">},{{<!>,<{i!>},<!}!>'!!i{\"!<>,<'!!!!!>,<a{eo'a{<>},{{},{}}}},{{{},{{{<oe!>,<!!!>e'!!!!!>}}{!ia!u>}},{{<!>,<,,!><!'}\"a!!!>e!!!>>,<<!>,<!!!!'}!>},<a!!!>o\">}},{{{<!!!!{i!!\"!!!>!!}!!!!!>e>},<!!!!!>{i!!!>e!'o!!e!!!>\"'!!{!!u>},{{<!\"!>\">},<}!!!>u!!!!!>\"ie>},{<,!o'!>,<\"!>,<'>,{<!!!>!>>}}}},{<}<!!!uu>,{<\"o}<!!!>,<}o!!!e!>au>}}},{},{{{<!!!>!>!!!!!>'!!!!!>!,a,o!uo,!>,<!>},<!>},<o>},{}},{<!!!>},<!>>,<au!!!>},<!u,!!!!!<{!>},<'!>,<!o<<i!!!!o>},{<>,{<ei!aa!'{!e!>},<{>}}},{{{<!!!e,u!>}!>},<!!!>,<!!!>,<!!!!a!>!!!!!>},<!!!>>}},{{<i}'{!!o,u{\"!>},<\"{iauu{>},{}},{{{{<o!!!>!!!\"!>},<!\",!!!>}o\"e>,{<!>},<!!,>}},{}},{}}}}},{{{<'!>},<!!!>!>!!'}>}},{{<!!u!>,<'!!!!!!!>,<oe{\"!>',!!!>u!>i<!o,>},{},{<}},!,'>,{{{<,a!{{!!u!!!>!!!>>}},<o>}}},{{<\"!>},<e!>},<u\"!!u<eui>}}},{{{{},{{{},<!>!!ie,ie}ao!>,<u>},{<,!!,aoa!>},<!!!><!!!>!!!>!>!o!>},<i!!!>i>}}},{<!>},<>,<e!e!!},}!!!>aa>},{<,i}!!!!,!>a!u'i!!!!!>u!>e}!>},<<!!!>},<>}}}},{{{<,!>,<e\"{<e'!!{!!e!!!!!!!>!!o!>},<u\"i,!!,>,<!!i!>},<e!!!!u!\"!!!>!>u!o!!!>},<<<>},{{{<,!,>}},{<!!!!!!!!a{!>,<u,e>}}},{<!>},<!>!>,<!!!>!a!!!!!<>,{<,'<e!!!!}o}{!!!>,<!!i<}e!!e>}},{<!!!>!!,!!i\"{<\"'>,{<}{!!'<{!>,<!>!>,<i!>},<!><!!!>>}}},{{<!a!>'!!!>!u!!!>,>}}},{}}},{{{}},{{{},<a!!!>{\"!!,!!e'!>>},{}},{{<{<i\"o!!!!\"i!!u!>,<\"!>},<e>},{}}}},{{{<}!!,u',oa!!!><\"!!,!>},<!!>},{<!>o!!o!>},<!>,<}!!!>>}},{<!!!!u}u!!!>>},{{},<!!!!!>},<'u!<!<!!<!>},<!!!<ae!>>}}},{{{<\"oea!,!!!>\"!>,<u!>a'{\"o!!!>>,<i,!<iu!a<{,>},{<!>},<<i!>!!!>,e,}\"!>},<!}!>!!ou!>,<u!!!!i>,{<!>,<!>},<oo},oo,!>\"!!'<e>}}},{<o>}},{{{<i!!!aoo!>,<!e!>},<e,e!>},<!!!!!!!>o!>},<>},{<o!!i!!!!e!!{,!!!!!>},<,oi,!!u!!!>!!a>}},{<o{!>!!!>},<'}u!!!!!><!!!>!!!>>,<!>!!{!e<e!!!>!!!>uu>}}}}},{{{<o!>},<eaa!!!!!>i!!}e'}!!!!!>!!!>>},<o{>},{{}},{}},{{<!>,<!>},<a}>,<!>!!o\"!>,<!!!!<,e>},{{{<!>},<a'iu'}u>},{{}}},{<oaa,,!>,<!!!!!>!>},<e!!a!!\"}oe!!!>,>},{<!!!>},<!>},<',!}'}!>'!>,<!>},<\"}u>}}}},{{},{},{<!>,<!>,<!>,<}i{!!!>,<<!!!>i>}},{{{{<{{!}iu!!{!!!!!!!>!!!>},<!!}u>},{{},{<!!a'u!>,<,!>!>},<,!!\"!>},<!>!>},<ae'{>,<o!!{>}}},{{{{<!!!>ei!>,<o{'!!!>}!>},<!!!!!>},<!'>},{{<!!!>!{e,<>}}},{<ei,!!u<!!au>,{<\"ui!>!{!!!>},<oo\"}u!>},<!!!>},<!>},<>}},{<<!>},<!>,<\"<!>,<!>,<!>},<a>,{<!!!u!ai}!e!>o>}}}},{{<!>,<!!!!!u,'!>'!uoo>}}},{{<'e!i!>!>{u,!>!!{!>,<!!!>>}},{<}'}'a!>}!!!e!!i!!{<!>,<>,{<!>!>!>,<o!!!!ueo!!i>}}}},{{{<'>,{<!!!>u!<!>},<!u!!a!>i!!,!>o\"u,!!\"a>}},{<\"e<!>!!o!!ia!!!>!!!>>},{}},{{{<e!>{i{!!!!!!!!!!}!>,<!>,<o<{}>}},<!'e{i!>},<eea}<'!!!>!>'!,!ua}!!}>}},{}}},{{{{<<\"!!!!{a!!i,a!!!>,,i!!!!!>o!a!!!>>}},{{}}},{{{<!!!><!>,<{,>}},{<{!>},<!!!>!!'!>},<a'e!'e}}{u',>,{{}}},{{<!!!!!!i!>a!>},<!!{!!i!!!>}o>,{<<e!!!>o!>},<!!!!!>!!a}\"}!!u,!!!>!>,<>}}}},{{{},{{{<!>},<a>},{<,!!!>'u!!!>u!>},<<!>},<>}},{{<!>},<\"\"\"<!{ea!>,<\"!!!><,o!!!e!!<!!!>!>},<>}}}}}}},{{{{{{},<>}},{{<\"{!>!>,<u!o!!}!>!a\"!>!>},<}<,i!!<>}}}},{{{{<{!>!!!>>,{}}},{}},{{{},{}},{{<!>,<!>!>\"!au<\"!!u!!!a}!!<>},{<uao\"!!\"!!!>{'e}!{!>},<}!>},<!!!!ou>}}}},{{{{{{{{<!>},<{!!!>,<i'{}!>,<uu!!!><!!{\"o!!<!!iu>}},{{{<!>,<!,!>,<!>!!!>e\"o>},{<u!!!>{i!!!!!>}!>},<'!!}e''<\">,{<\"!,!>},<!!o!!i!!!>!>},<e!!!>,{'!o!{!>},<!a!!!>,<>,<>}}},<{<e'auuae\"''!>,<!>},<!>,<>}},<i{o!>},<i>},{{},<!>},<!>!>},<!!ie!>},<!e!>,<}'!>},<!>},<<\"!>>}},{{<!>,<!>!!iie!>},<!>,<}!!aia!!{>}},{{<!>!>,<o!!!>a!,!>!!,a!!!>!>,<}ie\">},<,!!!>!!!>!!\"{i'!{u!!u,}!!!!!!',}!!,>}}},{{<i<a!,,!>},<!>},<!!a!>\">,{<\"<!!uae<>}},{{<,!>},<}!u!!ea!>}>},{{<!!\"a!}i!>,<,!i!!!>},<!>,<!!\",!!\">}}}},{{{<\"!>},<!!!>>,{<!>>}},{{}},{<!!<!!!>uo!!o!aoe!>,<>}},{{{<!>,<!>!!ia'e>}},{{{{<o!>,<!!!>},<!!>},<!>,<!!}\",!!!>},<<!!o{aoiu!!a!!!>{>},{{},{}},{{},<}!'!!!!!!}!o!>,<u!>>}},{{<\"i!>,<!>o\"i!!\"!!!>'!!u!>,<>,{{},<!>{!!!!o,!>,<e!>},<i!!a\"!!!>>}}}}},{{{{<!>,<!!a!!!>!!!!!>\"!<!i>},{{<!!i',<!!u>}}}},{{{{{{<}i'!>!>},<,>}},{<<<a},ii!!u>}},{<}!>,<}!,!!o!>,<,eo<ea\"<,!!!!!>,<{>}},{<>,<!!!>a>},{<u!>!!!>i!!i'!>,<e\"!i>}}},{{},{}}}},{{{<!>,<!!<e>},<<!!<!!a{!!!>ii{,'!!iue>},{{{{{<!>,{u!!!>,<!a!\"!>'!>,<u>},{{<o!!!!{!!!>ei!!,!!'!!a>},{{{},{}},{{<{i\"a!!!!>},{{},<!!!>!>ii,!!e<!!!>},<>}}}},{<!>{}e\"!!a}!>!o!>},<>}},{<}!>},<',!!!>o<u!>},<<{'!!!>a!!!>!!{!!!!{>}},{{{<!>!!!>\"!!>},{<!>{!{,!>,<!!!>'>,<!>,<!!!!!!'!!eo\"!!!>!!oo'>}}}},{{{},<!a!!oe>}}},{{<}!>},<<'!>,<<!!!>o{!uo>}}}}},{{{{{<!!!>!!},!!!>a\"i!\"!>\",eo!>!}o>},<!!!>,<!>},<!>},<!!!!!!!>auu}!!!>},<!!!>},<!>},<''>},{{},{<{}!>,'!!!!!>},<!u{!!!>!!{!!!>\"!!!>!!!>{!!!>},<>}},{{{{<!>,<\"!>>,<,'e!!!>'oo,!!!i!>},<!<!>!!!>!!}\"'>},<o<}{!>!!!!!>u<u!>,<i}!>,<>},{<}o{!>,<{<,!!{>,{}}},{{<!>>,{<!>},<!>},<ue!!!!<!e!!!><e}\"!!!>}!>ie!>,<>}},{<<!>,<o!>},<\"o<ea,>}}}},{{<<}!!!!!>!>},<\"}!!ii<\"'!eio!,>,<'!>!!!{'!!'\"}i>}},{{{<',>},{}},{{}},{<!>},<<!a>}},{{<!!i!>},<'u!{{!>},<a!>,!!!>>,{{{<e!<\"!!!>},<!\",,!!!!o!>u!!',>}}}},{{{<e!,!!!!!>'!!!>!!!!{,!!\"u!!!>!>\"ee<!>,<\"!!!>>,<{!!!!!>!!<!,'o\"!>,<!!o!!'!>,<!!>},{<!!oe}eu!>{!>a!>,<!>,<e>}},{<!!u!!e!!}\"i!>,<!!!>},<<!!!>!>,<uu!i}'!>},<>,{<!!ee{{u>,{{{{<!>},<}i{!>!>,!!ao!!!>!!}>}}},{{{{},{}},<!!!>{!!!>!>},<<{a\"!!!!!>,<>},<}<!!}>}}}}}}},{{{{{{<a'iu!!o<<\"'u!!e{'!>},<>},<{!u!!!!!>!!u\"ao\"{!!!>},<{>},{{{<o!>},<}>,<!>,<{!>,<!!!>},<!!!>i!!ai'!>!>},<!>\"u!'>},{<!!!>\"!>u!>},<!>!!{}{eu!!{e>}},{{},{<!>!>}u!\"!!!>!e}!!!>,au\"\"i!!!>!!>}}},{{{<eii!!,>}}}},{<!!!{>,{}},{<!>},<'eo<u'\"'{!!!>!!!>u<'>}},{{{{},{{{<\"!!}\">,{<!!!>!i!!e>}},{{},{<!>},<>}}},{<ou\",!a!>,<!>!>,<!>,<,!!!i>,<!!e!,!!!!{e!!e,a!>},<!{{o'!>,<\"\"!>>},{{<<'i<!!!>}>},{}}},{{<!!{!!!>!i!>eea!ia!>}a!!''>},<!>},<'!>},<ao>}},{{{{{},{{<}!>,<u!!!!!!<!}!!!>>},{}}},{{<o!>},<o!>},<!e!!!>>,<e<'!!!!!>!!!!!!,\"!>>}}},{<ie!!\"}!>!>!\"!>,<e'!>},<!>,\"u\"!o,>}},{<!<}!>\"!>},<<i!{u!>},<,!!!>}e{a!!!!<>},{{<}!>,<ea{!!!o!>,<!>,<!!!>eu!!<!!!e!>},<i\">}}},{{{<!>},<ai!!!>ii>}},{{{<{>},{<\"!>},<!>,<'!>,<'!>,<!!o}!!!!!!a!>u\"\"!!e'>}},{<!!!a!!\"!!!>>}},{<i!>},<!!!>eu<{u>}},{{<!!<!!!!<>,<uo!>,<>},{<!>},<e!!!!!!u!!!>>,<!!!{!!!>>},{<!!!>}i!>},<!'a{!>},<!'a}u!!{}!!!>,<>,{{<i!>!>,<u!>},<u!<e>}}}}},{{{{{<!>ua!>},<'u!!o!!!>!>,<>},<>},{},{{<au!!!>},<!!!>!!o!>>}}},{},{{<!!!>e}\"!!!!!!!!!u!!'o'!>},<a>},<e}o!!!>!>},<!>{aue'ee<!>{a>}}},{{{<i{<o''!e\">,{{},{{<o>}}}}},{{{<}!'o!>},<iui>}}}},{{{{<''!!!!\"}!!!>{!!o!!'!}!>i!!!!e<!{!!i>},<o>},{{{},<ie}!!!{!!!>!!>},{{{<!>},<!!}i>},{<,{!!!!!><e!!{!u<e!>!!!>},<!!a!>,<\"\">}},<!aa>},{{<!!!>},<a!>,<!>},<!a!>},<!>,<>},{<a!>>}}},{<e\">}}}},{{<!!!>>},{<!e!!<<,!!!>\"ia!>},<e!!,>,{<!!!!!!!>u!!!o,<,!>a!!u!>a!>},<\"!!!>,!>>,{{},<'!!!>'>}}},{{<>,{{<{,a!!i!!!>!!!>!>,!!u<!!!>!>>}}},{<ei<!><'ue!>i>,<!!ea!!{\"\",>},{{<!>{!>,<!!o}ii!>},<!o'!>},<!>},<\",!!!{!>!>,<,>}}}}},{{{<!>},<i>},{<'<,a}!!!><o<!>},<!!ue}\"!>},<!>{>}},{{{<ai!!{}!>},<,!!!>e!!!>},<!>!>!ei>,{<u!!e,<!!{o!>},<!!!!!>{\"!!!},!!u!!>}},{<uou<a!!!>!!o!>u!>,<!!>},{{<!>},<i}{o!\"a>},{<!<ie!!!>}'<!!{u\"uu!!!>!>>}}},{{{<ao'i{a<}e\"{!!!>oa!!!!!!!>,{!>},<>},<!!>}},{}},{<!>},<i>,<!>,<ie!>o},!!u{!!i>}}},{{{{{},{}}},{{{{<i!>,<!}!>'}>}},{<!!!>!>,<o<!!!>ei!>},<oe\"!>\"}>,{{},{<!,!!o!!!>,!!e!u!!!>!!{>}}}},{{{},{<oe!!e!>},<!!!a'i!!}{!>\"!u!!ea!,{oi>}}}}},{{{<!>,<!>},<!>,<>,{<aoe<!>},<\"!>},<!\"}!!a!>},<>}}},{{{{{{<}u,!>,<},!>!>!eu!!!>u<!>},<!!}o!!>},<\"!>,<!>,<ea}!!}ae}>}}},{{<!>},<a!>},<<\"!>\"e!,{u!!!>!!!>au!!!>>},<a!>},<\"!!o!>},<a!>}>}},{{<,,e!!!>!>!!!>!!!>!}>},{<!>,<\"}!>!>!>},<{!>},<!>},<!!!>!>!!\"{>}},{{{{{}}},{<!a!!!>e!!!>!'a!{!e}i>}}}},{{},{<i,!!ao!>,<u<oi!>},<'!>,<>,{}},{<'i!!,\"!>},<!!!>,<u}!!!!e{!>},<!>,}>}}},{{<oo!>,<a}!>,<!!e'a>},{{{{<o!\"a}{<!!!>,e'a>},{<i!>,<{!!o}e!>},<!><>}}}}},{{{<}!>},<,!!o!!o<!>},<!!!>!!a!<}>,<a!!'{i!>},<i!>},<'i!>,!!i!>!>},<!><\">},{<,i!<au!>},<!!!>},<o!!!>!!!>e}!>,<,i>}},{{<o!>},<\"'!!!>!!}!><\"!>},<!<oo}{!>,<>},{<!!!!{!!\"}}}{a',{au>,{}},{}},{{{{<ai!!!>,<!>},<,!!,<!},{<!>},<}!!!>'!!!>>},<!!!!!><!>},<!!!!!e!!i>},{{<!o,!>,<o!!!!!!a>},{<!>!>},<o!>},<!>ae<>,{<e,o}!!'{{!>!e<io!!!>!!!i{'!>!>,<>}}}},{{<ea'!!!!o}'a>},{{}},{}},{{{{<e!!!!!!!>!>>}},{<!!!>>}},{<'!>},<u!>'}>,<}>},{{{<!!}iu<!>,<!>,<!>},<!>,<>},{<'o{!!>}},{<i<}!>o\"!>,<!>!!!>\"!!!>!!uoa>}}},{{{{{{<e!o\"!!!>!!,o!!!>>,{}},{<'!>{!!!>!!!>!>},<\"<!>u!!u!!!!o!>>}},{{<e!!ao!<a!>},<!e!{>}}},<!!',iia}!!!><u<}>},{{<!!!>},<}}!>,<,!!!>{!!!''!!i!!\"!>,<'>}},{}}}}}},{{{{<{{!!'!>!>,<<!!!>!>,<!!!>!<!>{!>},<!!{!>,<i\">},{<>}},{{<}<i!!}!!e!!<o!!!!u!!!>},<<!,a<>}},{{}}},{{{<,!!!><}!>!!!!!>!!oi{>},<{ueuio!!!>>},{{{},{}},{{<a!>},<!,!>!>,<!>!}!>},<aee'!>,!'oe!!!>>},{<!!!{''}i}o!!>}}},{{},<!>,<!\"\"o<!<!>},<!!\">}}},{{{{},<,>},{}},{{{}}},{{{{{}},{{<!>},<!>,<\"{!!>}}}},{{}},{{}}},{{<!!o!>},<!!!>},<!!!>!>e!>,>,{<o!!a!>o!>!!!!!>!>,<a!>\"}!{!>,<!\"!!!'>}},{{}}}}},{{{{{}},{<!{'\"<!!!>}>,<u!'!>!>,<a!!!}!>a!>!>eee<!>,<>}},{{{{{},<!>,<{a!!{!>!>},<,!ueao'>},{{{<!!!!!>},<!!aa!>a<!>!,oo!!!>!!!>>}}},{{<>},<!!!>},<!>,<>}},{{{<e!!!>,<'!a,!!ae{!>,<'{a!>},<!uoeee>},{{{<oa!>},<!>,<!}!!!>>},{<\"!{,'a!>},<!!,}oa'>}},{<<!!!!!>e}>},{<{\"!<e{!!!!!{!>{'<!>},<!!oiua!!!>},<>,{<!!!><!>,<oo!!!>!!!>i!!!>''!!e'!!!>,<>}}}}},{{{<<!!'!>},<!!!>i\"!>},<\"!!\"'!>>},{<o,{>}},{{{<'>},{<!!!>},<>}},{<a!>,<\"\"e'a'<!>o!!!>,<>},{{<!>},<i!!!!<o!!!>},<!\">},{<>}}}},{{{{<\"!>!>!>,<!,,!!<\"!!!!!>e,!>{}!>,<!>,<!}>},<<!>},<{u<!!\"!>,<!a\"\"!!!!e!!!!<\"eou>},{{<o\">},{<}>}}},{{<!!!>i{>},<!>\"uue{>}}},{{{<!'a!,!!\"!!!>!>,<<o,<!>ae,!!!>!e!!u,>},{}}},{{{}},{{<!>},<!>},<!>,<>,<,,o!>},<!!'},{!!a!>,>},{{{<<>},{<\"!>,<!!!!!>!>!\"u!!!>!>,<!>,<!!!>u{au!>},<\"!>},<>}},{<!u!>'!!!>\"uu>}}}},{{{{},{<a>}},{{},{{{<}oe>},{<u{}!!<}a!>,<}!!'!}!\"!>\"<>}}}},{{}}},{{<i>,<{o>},{<u!!!!i!!!>!>!!!>i\"!!,{!!!>,<'\"!}>},{{<oa!!!>!>},<!>},<!!>},{{},<u<u<!!!!!!!!!>},<'!!}!!!>e!>},<{!>,<>}}},{{{<\"a!!!>'!>},<!{!>,<aa!!!!!>'>},{{}}},{{<o!!!!\"{!>,<,\"u!!!><!!a,>},{}}}}}},{{{{{{<,a<o\"!!!>'!!!>},<}!io>},{<>}}},{{<!!!>!!!o}!!!>!}\"!>o!>}!>,<'a!!!!!'}!>},<!a>,{{<!a!!!>\",u!>,<>},{<!!!>},<!>,eooo!>},<>}}},{{{<!'i!!u!>!!!!!!!>aae!!!>o!!!>!i>}}}},{{{{{<!!!>o>,{<o,!>,<{u!\"\"!!}\",!!{!>,\"}!!!>{>}},<!!eu!>!>},<,ei,e!>},<!!}e\"!>>},{<e<uie,!>!!>,{{<{!!oa!>!!,u!!!>},<!!!!!!'!>},<!>,<,!!}\"!>>},{<!>},<ue!!!>!!!!!>!!}>,<e!!}o}o!>},<oi{<!>,<e!'\"<!!u>}}},{<u!!<u{!>},<!!!>!!!>{!io!!i>,<,{\"!!!>},<a!,!!!><e!>o!!!',i!!o{!!!>},<!>>}},{{{<!!!!!!a!>!!'!!!!!>},<e{!!!>\"!!!>!>},<!!!>!'}<>},{}}}}},{{{<!!!!u!>,!!!>,<oe,{ua{!>!>,<\"!!!>a!>>},{},{{{{<}e}!>,<a\"i!!ii,>,{<!!!!!!!>!>},<!!\">}}},<!!'!!!>o>}}}}},{{{},{{},<i!>,<!>},<i}ua'<!>},<{>}},{<>,{}},{}},{{<u{!!!>},<,>},{<!!\"}a,'!!!>!>i!!!e!>,<aia!!}!!!!!!!!{!>!!u!e>,{<,o!!!!!!!!\"!''!e!><!!!!!>,<i!!a,!<!!{>}}},{{{{<!!!>!!,!!!>{}!!!>>},<!,e!o!!}\"o!>,<>},{{},{<ao!!a!!!!!e<'\"{!o\"''{!!!>'>,{<o!><!>i!>!>,<'!>},<<!!<!u!iao!{,>}}}},{{{}},{<!!ae<>},{{<}ae>},{}}},{}}},{{{{<!{,i!!e!>!!!!!>!!i!!{\"o!>},<,a{o>,{<!a!!e!>},<{e'u!>,<}!>},<'\"!!u>}}},{{},<!!<!>>},{{{<'!>,<a\"o!!a!>e>},{<!>},<\"i!!>}},{<\"!<!!i!>!!<'!>,<'!!,<u!>!>},<a!}!>,<a>,{}}}},{{{<a,o<\"!!!>}'!!!!}>},{<!>!!!>>}},{{{}},{<e'!>},<a{u!>},<!>i'<!!i!ui,!>},<\"}>}},{<,!!!<o>}},{{{{<ai>},{}}}}}},{{{{<!!!!>}},{{<!!!>!!<u!',!>},<>},{<!!{!{>}}}}}}")

(def day10
  "88,88,211,106,141,1,78,254,2,111,77,255,90,0,54,205")

(def day11
  "se,ne,se,ne,n,n,n,n,ne,nw,nw,n,nw,nw,sw,sw,sw,sw,sw,sw,sw,sw,s,sw,sw,sw,ne,sw,sw,s,n,s,s,s,s,nw,s,s,s,s,s,se,s,s,n,s,n,se,se,s,se,s,s,s,se,nw,se,se,se,se,se,ne,se,se,se,se,se,n,se,se,se,ne,se,se,se,se,se,se,n,se,se,ne,ne,ne,n,ne,ne,ne,nw,ne,ne,ne,ne,ne,nw,ne,ne,nw,ne,ne,ne,se,ne,ne,ne,sw,se,sw,ne,n,s,sw,ne,n,n,ne,ne,n,ne,ne,n,n,n,n,n,n,se,sw,ne,n,n,n,ne,n,nw,n,ne,ne,ne,n,n,n,n,n,ne,n,ne,sw,nw,ne,sw,nw,n,n,s,nw,n,n,n,n,n,n,n,nw,s,sw,nw,n,se,se,nw,s,nw,nw,nw,nw,nw,se,n,nw,n,s,sw,n,nw,sw,n,nw,se,nw,n,nw,n,nw,sw,n,nw,ne,se,n,nw,ne,nw,nw,se,n,n,nw,nw,se,n,nw,sw,nw,n,n,nw,nw,s,nw,ne,nw,nw,nw,ne,nw,se,n,nw,nw,sw,nw,sw,nw,nw,se,sw,sw,nw,s,nw,nw,nw,nw,sw,n,ne,nw,s,nw,nw,sw,nw,nw,sw,s,nw,nw,nw,se,nw,nw,sw,sw,nw,se,ne,nw,nw,sw,nw,sw,sw,nw,sw,s,sw,sw,sw,sw,nw,n,nw,nw,sw,sw,nw,nw,nw,nw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,nw,sw,sw,se,sw,sw,sw,nw,sw,sw,sw,sw,sw,sw,nw,sw,sw,s,sw,se,sw,sw,sw,ne,n,sw,sw,sw,sw,sw,sw,se,sw,sw,n,sw,sw,s,sw,n,se,s,sw,s,sw,sw,sw,nw,sw,n,s,sw,sw,s,sw,sw,sw,sw,sw,s,sw,sw,s,sw,sw,sw,ne,sw,s,sw,nw,sw,sw,s,s,s,nw,s,s,nw,sw,se,s,s,sw,sw,sw,s,n,s,n,sw,s,sw,s,s,sw,sw,s,s,sw,nw,nw,s,sw,s,sw,se,s,nw,s,se,se,s,sw,s,sw,s,s,sw,sw,s,sw,s,s,sw,s,ne,s,s,n,s,sw,s,s,s,s,s,ne,sw,s,s,s,s,sw,se,se,s,s,s,s,s,nw,s,sw,s,s,se,s,s,se,n,s,s,s,sw,s,s,s,ne,nw,se,ne,s,s,se,nw,s,s,sw,s,ne,s,se,s,sw,s,sw,s,s,s,s,s,se,s,nw,s,s,s,s,s,nw,sw,s,sw,s,se,ne,s,sw,nw,se,se,sw,s,se,se,s,s,s,se,s,s,sw,se,se,se,se,se,se,sw,s,s,sw,s,s,s,se,s,se,se,se,s,se,ne,se,s,s,se,se,se,ne,s,se,s,s,se,se,nw,s,s,se,s,s,s,nw,se,s,se,s,se,se,se,s,s,s,s,s,s,s,s,se,ne,s,s,nw,se,se,se,s,s,se,se,se,s,s,se,s,s,s,se,se,se,se,se,s,se,se,se,n,se,s,nw,se,s,se,n,se,nw,se,se,se,se,s,nw,se,se,se,nw,se,se,se,se,n,se,se,se,s,se,se,se,sw,se,se,se,n,se,se,se,n,n,se,n,se,se,se,se,n,se,se,se,se,sw,nw,se,sw,nw,sw,se,se,se,se,se,s,se,n,se,se,s,se,se,se,ne,se,se,se,se,se,se,ne,se,se,ne,s,se,se,se,se,se,se,se,se,ne,n,se,se,se,ne,nw,se,n,ne,se,nw,sw,s,se,ne,se,se,ne,se,se,se,ne,se,se,ne,se,sw,ne,s,se,se,se,se,se,se,se,ne,s,sw,nw,ne,se,se,ne,se,se,se,ne,ne,s,se,se,ne,n,ne,se,se,se,se,se,ne,se,se,s,se,se,ne,sw,se,se,se,se,se,se,se,se,se,se,se,s,ne,ne,ne,se,nw,ne,ne,se,se,s,se,se,ne,se,se,ne,se,se,se,se,se,ne,se,sw,se,ne,n,nw,nw,se,se,se,sw,n,ne,se,s,s,nw,ne,s,ne,se,ne,ne,ne,ne,ne,se,nw,ne,se,ne,se,ne,ne,ne,se,ne,se,se,ne,se,ne,ne,ne,se,se,ne,ne,se,se,se,ne,se,se,s,ne,se,ne,ne,nw,ne,ne,ne,se,ne,ne,se,ne,sw,ne,ne,nw,ne,sw,se,ne,ne,se,s,s,ne,ne,ne,se,sw,ne,nw,se,ne,se,ne,ne,ne,s,se,se,ne,ne,ne,ne,ne,s,ne,ne,sw,ne,ne,n,ne,nw,se,ne,ne,ne,se,se,se,ne,ne,ne,ne,s,ne,ne,se,se,ne,ne,ne,ne,n,ne,ne,s,ne,ne,ne,sw,ne,se,ne,ne,ne,nw,n,ne,ne,ne,nw,se,ne,n,ne,ne,n,s,ne,ne,ne,se,ne,ne,ne,ne,sw,ne,ne,ne,ne,ne,ne,ne,ne,s,ne,nw,ne,ne,ne,ne,s,ne,ne,ne,n,ne,ne,se,sw,se,ne,ne,ne,ne,se,n,ne,ne,sw,n,ne,n,ne,ne,ne,ne,ne,ne,ne,ne,se,ne,ne,ne,ne,n,ne,se,s,se,ne,nw,ne,ne,ne,ne,s,ne,ne,n,ne,n,ne,ne,ne,ne,ne,ne,ne,n,ne,ne,n,ne,ne,ne,nw,n,ne,n,ne,ne,ne,sw,ne,ne,ne,n,sw,nw,n,sw,ne,n,ne,se,ne,s,ne,nw,ne,nw,ne,ne,ne,ne,n,n,ne,nw,ne,nw,ne,s,n,ne,ne,se,n,s,ne,ne,n,ne,s,s,nw,s,s,ne,n,ne,ne,sw,ne,n,ne,n,n,ne,se,ne,ne,n,n,ne,n,sw,ne,nw,n,ne,n,ne,n,ne,n,n,s,ne,ne,se,nw,ne,ne,ne,ne,se,s,n,ne,ne,n,n,n,ne,ne,n,n,ne,se,n,ne,n,n,n,n,ne,ne,nw,ne,ne,n,ne,se,n,se,s,nw,n,n,n,s,n,n,n,ne,n,ne,ne,n,n,s,sw,ne,ne,ne,n,s,n,n,n,ne,se,ne,n,n,n,n,n,n,ne,ne,n,n,n,ne,ne,n,ne,n,n,s,ne,n,n,n,n,n,nw,n,n,nw,se,n,n,n,ne,n,ne,sw,n,ne,s,n,se,n,n,ne,se,n,nw,ne,ne,n,ne,n,n,n,n,n,ne,n,n,sw,s,n,ne,ne,n,n,n,n,ne,n,n,n,n,n,n,n,n,n,n,n,n,n,n,nw,n,n,ne,s,n,s,n,n,s,ne,n,sw,n,s,ne,ne,n,n,n,n,n,se,ne,n,n,n,n,n,n,n,s,s,n,s,nw,n,s,n,n,n,n,se,ne,se,n,se,n,n,n,s,n,n,n,n,n,n,n,ne,n,n,ne,n,n,ne,sw,nw,n,ne,n,n,n,nw,n,n,n,n,n,n,sw,nw,n,se,n,n,n,n,nw,n,s,nw,n,n,n,se,n,n,n,n,s,n,n,n,n,n,n,n,n,n,nw,n,n,nw,n,n,n,n,n,n,sw,s,n,sw,n,n,n,n,sw,se,ne,n,n,n,n,n,s,n,n,n,n,n,n,ne,n,ne,n,n,n,n,n,n,n,n,n,n,nw,s,n,n,n,nw,n,n,n,n,n,nw,se,n,n,se,n,n,n,s,n,nw,se,n,nw,se,n,n,n,n,n,nw,n,n,ne,n,sw,n,nw,n,n,n,n,n,n,n,n,ne,n,n,n,n,nw,n,n,s,n,n,n,n,n,n,n,nw,nw,ne,n,n,n,n,n,s,n,nw,n,n,nw,ne,n,nw,n,n,nw,n,nw,n,nw,n,sw,sw,n,nw,n,nw,se,sw,n,nw,nw,n,nw,n,n,n,ne,n,nw,n,nw,n,nw,se,n,nw,n,n,n,n,n,ne,n,n,n,s,n,nw,n,n,nw,nw,n,n,n,n,n,n,n,n,n,ne,n,n,n,sw,nw,n,n,n,n,n,nw,sw,s,n,nw,nw,n,n,n,n,se,n,n,n,ne,n,nw,n,n,n,n,n,n,n,n,nw,n,ne,n,nw,n,nw,ne,nw,s,n,nw,n,nw,n,nw,sw,nw,nw,n,n,sw,n,se,nw,ne,n,n,nw,nw,sw,nw,n,sw,se,n,sw,n,n,n,nw,n,se,sw,nw,n,nw,nw,n,nw,n,n,n,nw,nw,s,n,nw,s,sw,n,nw,nw,nw,ne,ne,nw,n,n,n,nw,n,nw,n,nw,nw,nw,nw,nw,nw,nw,nw,n,nw,nw,n,n,nw,n,n,n,ne,sw,nw,nw,nw,n,nw,sw,nw,ne,n,nw,nw,n,n,ne,n,n,nw,nw,nw,n,nw,s,se,nw,n,n,n,nw,s,n,nw,ne,nw,n,n,n,nw,nw,se,n,nw,ne,n,nw,nw,nw,nw,n,nw,nw,nw,nw,nw,n,n,nw,n,n,nw,se,s,n,se,se,nw,nw,nw,nw,se,nw,sw,ne,se,nw,s,nw,nw,nw,nw,n,se,nw,nw,nw,nw,nw,nw,nw,n,ne,nw,s,sw,nw,nw,nw,nw,nw,nw,nw,n,nw,nw,nw,nw,n,nw,nw,sw,nw,nw,se,n,nw,nw,nw,s,nw,nw,nw,n,sw,nw,s,n,nw,nw,nw,nw,s,s,nw,nw,nw,nw,nw,ne,s,n,n,nw,nw,nw,n,nw,n,nw,nw,nw,nw,ne,ne,n,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,n,s,n,nw,nw,nw,nw,nw,nw,nw,ne,nw,nw,nw,nw,sw,nw,nw,nw,nw,ne,n,nw,n,se,nw,nw,n,nw,nw,nw,nw,nw,s,nw,n,nw,n,ne,nw,nw,nw,sw,nw,nw,sw,se,nw,nw,ne,nw,nw,nw,nw,se,nw,nw,nw,nw,nw,nw,nw,nw,n,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,n,nw,nw,n,nw,n,nw,sw,ne,nw,nw,sw,nw,nw,nw,nw,nw,nw,nw,se,s,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,sw,nw,nw,nw,s,nw,nw,se,sw,nw,nw,nw,n,nw,nw,sw,nw,s,nw,nw,n,s,s,se,n,nw,nw,nw,nw,nw,nw,n,sw,nw,sw,nw,nw,nw,nw,sw,ne,nw,nw,ne,se,nw,nw,n,nw,nw,nw,nw,se,nw,nw,sw,ne,nw,nw,nw,nw,sw,ne,nw,nw,sw,s,sw,sw,n,nw,nw,nw,n,nw,nw,nw,nw,nw,nw,nw,nw,sw,ne,sw,nw,sw,nw,nw,nw,sw,nw,se,ne,s,nw,nw,ne,nw,s,nw,sw,sw,sw,nw,nw,nw,nw,n,nw,nw,nw,nw,nw,nw,nw,nw,n,nw,n,nw,nw,ne,s,nw,sw,nw,nw,ne,nw,nw,n,nw,se,sw,nw,sw,nw,nw,nw,nw,nw,nw,nw,sw,n,sw,n,nw,nw,se,nw,nw,nw,nw,nw,nw,nw,ne,sw,n,nw,se,nw,s,nw,n,nw,nw,nw,sw,se,sw,nw,nw,nw,nw,n,nw,sw,n,nw,nw,se,nw,s,nw,sw,nw,nw,sw,ne,nw,se,sw,nw,s,sw,ne,nw,nw,nw,nw,sw,se,nw,sw,nw,sw,ne,sw,nw,sw,nw,sw,sw,nw,nw,nw,nw,nw,sw,nw,nw,n,nw,nw,n,ne,sw,sw,s,sw,nw,s,nw,s,se,n,nw,nw,nw,nw,sw,sw,nw,ne,nw,sw,nw,nw,s,nw,nw,sw,nw,sw,nw,n,se,nw,nw,nw,nw,se,nw,se,nw,sw,nw,nw,nw,nw,sw,nw,s,nw,n,ne,se,sw,n,sw,sw,se,sw,nw,ne,nw,nw,nw,sw,nw,ne,nw,se,nw,sw,nw,sw,nw,nw,se,nw,sw,sw,nw,sw,nw,nw,sw,se,nw,se,sw,nw,sw,sw,sw,nw,sw,nw,sw,sw,sw,nw,sw,sw,sw,se,nw,nw,sw,nw,se,sw,nw,n,sw,nw,nw,sw,nw,nw,nw,sw,sw,ne,sw,nw,sw,se,sw,sw,sw,ne,nw,nw,ne,sw,sw,sw,ne,sw,s,sw,n,sw,sw,nw,sw,se,sw,sw,nw,sw,sw,sw,nw,sw,nw,nw,nw,se,nw,sw,sw,sw,nw,sw,nw,nw,sw,se,sw,n,nw,nw,sw,nw,sw,sw,sw,sw,sw,s,sw,s,s,sw,nw,nw,sw,sw,nw,nw,nw,sw,nw,sw,nw,ne,sw,nw,n,nw,sw,se,nw,sw,se,sw,sw,nw,sw,s,sw,nw,nw,ne,nw,sw,nw,sw,n,nw,sw,sw,sw,nw,ne,nw,sw,se,nw,nw,sw,nw,n,sw,sw,ne,sw,sw,sw,sw,s,nw,nw,sw,nw,sw,s,nw,nw,nw,se,sw,nw,sw,n,sw,nw,nw,ne,sw,sw,nw,nw,sw,se,sw,nw,nw,nw,sw,nw,nw,sw,s,nw,sw,s,sw,sw,sw,n,sw,sw,sw,sw,sw,nw,nw,sw,sw,nw,sw,sw,nw,nw,s,sw,sw,sw,ne,sw,nw,n,sw,sw,sw,sw,se,sw,sw,sw,sw,nw,nw,sw,sw,sw,n,sw,n,sw,nw,s,sw,sw,sw,nw,ne,sw,sw,ne,sw,sw,n,ne,ne,nw,sw,sw,sw,nw,n,nw,ne,sw,nw,sw,nw,sw,n,sw,s,sw,sw,nw,sw,sw,nw,se,sw,sw,n,sw,nw,s,nw,sw,nw,nw,nw,sw,nw,sw,nw,nw,nw,sw,sw,sw,sw,sw,sw,sw,sw,nw,ne,sw,nw,sw,sw,sw,n,sw,sw,sw,sw,ne,nw,sw,ne,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,s,nw,se,se,sw,sw,sw,sw,nw,sw,nw,ne,s,sw,sw,nw,sw,nw,nw,se,n,sw,nw,sw,sw,sw,sw,sw,ne,sw,sw,s,sw,sw,sw,s,nw,se,nw,sw,s,n,sw,s,nw,ne,sw,sw,sw,ne,sw,nw,nw,n,se,sw,nw,n,sw,sw,sw,sw,se,se,sw,sw,sw,n,sw,sw,sw,sw,sw,sw,sw,se,sw,sw,sw,sw,sw,sw,nw,sw,sw,se,s,nw,nw,nw,nw,sw,sw,n,sw,sw,s,ne,sw,nw,sw,sw,sw,sw,sw,n,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,nw,sw,nw,s,n,sw,s,sw,sw,sw,sw,sw,sw,s,nw,ne,sw,sw,sw,sw,sw,sw,nw,s,sw,nw,sw,sw,sw,sw,sw,ne,se,sw,ne,sw,sw,se,s,sw,sw,sw,sw,ne,n,sw,sw,nw,sw,sw,sw,s,n,sw,sw,sw,sw,sw,sw,sw,sw,sw,nw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,nw,sw,sw,nw,s,sw,sw,sw,sw,n,sw,sw,se,sw,sw,sw,ne,sw,sw,sw,sw,sw,sw,sw,sw,nw,ne,sw,n,n,sw,sw,sw,sw,sw,sw,sw,sw,ne,sw,sw,sw,sw,sw,nw,sw,sw,se,sw,nw,sw,ne,sw,s,sw,sw,s,sw,sw,s,sw,s,sw,s,sw,sw,sw,s,sw,sw,sw,sw,ne,sw,sw,n,sw,sw,s,sw,nw,sw,ne,sw,sw,sw,s,sw,se,sw,sw,sw,n,sw,sw,sw,sw,sw,sw,s,s,s,n,n,sw,nw,sw,s,sw,sw,sw,s,sw,sw,sw,s,sw,nw,s,sw,sw,sw,se,sw,sw,sw,sw,sw,sw,sw,s,sw,s,se,ne,sw,sw,sw,sw,sw,sw,s,sw,s,se,nw,n,nw,sw,sw,sw,sw,sw,sw,n,sw,sw,sw,sw,sw,sw,s,sw,sw,sw,se,sw,nw,sw,nw,s,n,s,s,sw,sw,sw,s,se,sw,s,sw,sw,sw,s,se,s,sw,sw,sw,sw,sw,sw,sw,sw,sw,n,sw,nw,sw,se,sw,sw,sw,n,sw,sw,sw,s,sw,sw,sw,sw,sw,sw,s,sw,sw,sw,se,s,sw,ne,ne,sw,sw,sw,sw,s,s,sw,sw,sw,sw,s,se,sw,sw,sw,nw,s,s,se,sw,n,sw,s,sw,n,sw,sw,sw,sw,s,sw,sw,sw,ne,sw,sw,se,sw,se,sw,s,sw,sw,s,n,sw,sw,sw,sw,s,s,s,sw,sw,sw,s,sw,ne,nw,n,sw,nw,sw,sw,sw,sw,s,s,sw,sw,sw,s,sw,sw,s,nw,se,s,sw,sw,sw,sw,sw,sw,se,s,sw,ne,s,se,s,sw,sw,sw,s,sw,sw,sw,sw,s,s,s,s,nw,s,ne,s,se,sw,sw,sw,nw,s,sw,nw,s,sw,s,sw,s,sw,nw,sw,sw,sw,sw,sw,se,sw,sw,s,s,nw,sw,sw,sw,sw,sw,sw,n,sw,nw,n,sw,n,ne,n,n,sw,s,s,sw,s,sw,s,sw,s,sw,sw,s,s,s,s,n,sw,s,sw,ne,n,sw,sw,s,s,sw,sw,sw,s,sw,s,sw,sw,s,s,nw,sw,sw,sw,sw,sw,sw,s,s,sw,s,sw,sw,sw,sw,n,s,s,ne,n,sw,s,sw,s,nw,sw,s,sw,sw,s,s,sw,s,sw,sw,sw,s,sw,nw,s,sw,sw,nw,s,sw,s,sw,sw,sw,s,nw,sw,nw,sw,sw,s,sw,sw,s,sw,sw,s,ne,sw,n,sw,sw,sw,s,sw,sw,sw,sw,ne,sw,n,n,sw,n,sw,s,s,s,sw,sw,s,s,sw,sw,nw,sw,s,s,sw,sw,se,nw,sw,sw,sw,sw,n,s,sw,s,nw,s,s,sw,s,s,n,ne,sw,s,s,s,s,nw,s,s,sw,sw,sw,sw,s,s,s,s,sw,s,s,n,se,s,s,n,s,s,sw,nw,s,s,ne,s,s,s,se,sw,sw,sw,s,sw,sw,se,s,sw,s,n,sw,se,s,n,ne,se,s,se,sw,sw,sw,s,sw,nw,sw,s,sw,sw,sw,s,sw,s,sw,s,se,sw,s,s,s,sw,sw,s,s,sw,s,ne,se,s,sw,s,s,n,sw,s,sw,sw,s,s,nw,s,se,sw,s,se,n,sw,sw,ne,sw,s,sw,sw,sw,se,s,nw,n,sw,ne,se,sw,s,s,sw,s,ne,sw,s,s,sw,s,s,ne,s,s,nw,sw,se,sw,se,sw,s,s,s,sw,sw,s,sw,s,s,sw,ne,s,n,se,sw,s,n,s,sw,s,s,sw,sw,s,se,s,s,s,s,se,sw,sw,s,s,s,s,s,s,se,s,sw,s,s,s,s,se,s,n,s,s,s,sw,n,n,nw,ne,se,s,sw,s,s,sw,sw,s,s,sw,n,s,sw,sw,ne,s,nw,ne,s,s,s,s,s,s,se,s,s,s,sw,s,n,nw,s,s,sw,sw,s,s,s,s,s,ne,s,sw,sw,s,s,sw,s,sw,s,sw,nw,sw,s,s,sw,sw,sw,sw,s,se,s,s,s,sw,s,n,sw,nw,sw,s,s,nw,sw,s,s,sw,s,s,sw,sw,sw,s,s,s,se,s,sw,s,s,sw,s,s,s,s,s,s,s,s,s,nw,s,s,sw,s,s,s,s,ne,s,ne,sw,sw,s,n,sw,s,s,s,sw,s,sw,sw,s,sw,s,sw,sw,sw,ne,s,s,s,s,s,s,s,n,sw,s,ne,s,s,s,sw,s,s,sw,s,s,sw,s,n,s,se,s,se,s,n,s,sw,s,s,s,s,sw,sw,se,sw,ne,sw,sw,s,s,sw,s,s,s,s,s,s,s,sw,ne,s,s,s,s,s,sw,se,s,s,s,sw,s,s,s,s,sw,s,sw,sw,s,s,nw,ne,n,sw,sw,s,nw,s,s,ne,ne,s,s,sw,s,s,s,s,s,ne,s,s,s,s,s,s,s,se,s,s,s,s,s,s,s,n,s,s,sw,s,s,s,s,s,s,s,n,s,s,s,s,s,s,sw,s,s,s,s,n,s,n,s,s,s,s,s,s,s,s,n,s,s,ne,s,s,s,s,s,s,s,n,s,s,s,s,s,s,sw,s,s,s,s,s,s,s,nw,nw,s,s,s,s,se,s,s,ne,s,s,sw,s,sw,s,s,sw,sw,n,s,n,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,n,s,s,ne,s,s,s,sw,s,s,s,s,s,s,s,s,s,s,s,s,n,s,s,s,se,nw,s,s,s,s,s,nw,s,s,s,s,sw,s,s,s,ne,s,s,s,s,s,s,s,s,n,s,s,nw,s,s,se,s,s,s,se,s,s,s,sw,s,s,s,s,s,nw,s,s,se,s,s,s,s,s,sw,sw,s,s,s,s,s,s,s,se,s,s,s,s,se,s,s,s,s,s,s,s,s,s,s,s,s,sw,s,s,s,ne,s,nw,s,s,se,s,se,s,s,s,s,s,s,s,ne,se,sw,sw,se,se,s,s,s,s,s,s,s,s,s,ne,ne,s,s,s,s,nw,s,s,s,s,s,n,sw,s,s,se,se,sw,nw,nw,s,s,s,s,s,s,n,s,s,s,s,s,s,se,s,ne,s,se,s,s,s,s,s,sw,n,ne,n,s,ne,se,se,s,s,s,s,nw,s,s,ne,s,s,s,n,s,s,s,s,s,s,s,s,s,s,s,s,nw,s,s,ne,s,s,n,s,s,se,sw,s,nw,s,sw,ne,s,s,s,ne,s,s,nw,s,s,s,s,s,s,s,s,s,se,nw,s,s,s,se,se,se,s,nw,s,s,s,se,s,s,se,sw,ne,s,se,s,se,s,se,nw,n,se,s,se,ne,sw,s,s,s,s,s,s,s,se,s,s,s,sw,s,s,s,s,nw,s,se,s,s,n,ne,s,s,s,s,se,s,s,s,s,s,se,s,sw,s,s,se,se,s,s,s,s,s,n,ne,nw,s,s,s,se,ne,s,se,se,nw,s,s,s,s,s,s,s,s,s,s,s,s,s,n,s,s,s,n,s,s,se,s,s,se,s,s,s,nw,s,s,s,se,s,sw,sw,s,se,s,se,s,s,s,se,se,s,sw,se,s,s,s,s,s,s,se,s,s,s,se,se,s,s,s,s,s,s,s,s,ne,s,nw,s,s,s,se,se,se,s,se,se,s,s,s,s,nw,s,se,s,se,s,se,se,s,se,sw,s,s,se,se,s,ne,s,ne,nw,s,s,s,s,se,se,se,se,s,se,ne,se,se,s,s,se,ne,nw,s,se,s,s,s,nw,s,s,s,s,nw,s,s,s,s,se,sw,s,sw,s,s,s,se,nw,s,se,sw,s,ne,s,s,s,s,n,se,se,s,nw,s,s,s,se,se,s,s,s,s,se,se,s,s,s,se,s,s,ne,s,s,se,sw,se,n,ne,s,s,s,s,s,s,s,nw,se,se,s,s,s,se,s,se,n,s,sw,s,se,s,se,s,s,s,se,s,n,se,s,se,se,s,se,s,se,s,se,sw,s,se,s,se,s,s,s,s,s,s,s,se,nw,s,s,se,s,se,s,s,s,se,se,se,s,se,sw,s,ne,s,s,se,s,s,s,s,se,n,se,n,sw,s,se,ne,se,s,sw,s,se,sw,s,ne,se,n,se,nw,ne,s,s,se,s,sw,se,se,se,s,s,s,se,s,s,s,s,se,se,se,se,se,s,s,n,s,s,s,s,s,se,s,s,s,s,s,nw,sw,ne,se,se,s,s,se,se,se,se,se,s,s,ne,s,se,se,se,se,s,nw,se,s,s,s,s,s,ne,s,se,se,se,se,s,s,s,se,n,s,se,s,se,se,se,se,s,s,se,se,se,se,se,s,s,se,s,s,sw,se,sw,sw,se,sw,n,s,s,s,n,s,s,se,sw,ne,n,se,s,se,sw,se,s,se,se,s,se,s,s,s,s,se,se,s,s,nw,se,se,se,se,s,s,s,sw,s,se,se,s,se,nw,se,se,se,s,sw,s,s,se,s,s,s,s,se,s,se,s,se,se,s,s,se,se,s,se,ne,se,s,se,se,se,s,s,n,s,se,n,s,s,s,se,se,se,s,s,se,s,n,s,sw,se,se,s,se,se,se,s,s,se,se,n,n,nw,se,n,ne,se,s,se,se,s,se,s,s,n,s,nw,se,s,s,ne,n,ne,se,s,se,sw,se,se,s,s,se,s,se,s,se,n,s,se,se,sw,se,s,s,se,se,sw,s,s,nw,se,s,se,se,ne,se,se,se,se,se,se,s,se,s,n,s,s,se,s,s,se,se,s,nw,s,se,se,se,s,se,s,s,se,se,se,se,se,se,s,se,nw,se,se,s,s,se,s,sw,se,se,s,s,n,n,n,s,s,se,s,nw,se,s,se,s,se,nw,se,s,s,ne,se,n,s,s,s,se,s,n,se,s,s,se,se,se,s,se,se,se,se,n,nw,se,se,se,se,se,s,se,se,s,se,se,se,s,se,s,se,n,s,se,s,se,se,se,s,se,se,se,se,se,se,s,se,se,s,s,se,s,s,sw,se,n,se,s,se,s,se,se,se,se,s,s,se,se,se,sw,se,s,se,s,s,se,se,s,se,se,se,se,n,se,sw,se,se,sw,sw,s,se,sw,s,se,se,s,se,se,sw,s,s,s,se,nw,se,s,s,se,se,se,s,se,se,se,s,n,se,se,se,s,s,se,se,se,sw,se,s,sw,s,se,se,se,s,n,se,s,s,s,s,se,ne,se,sw,s,sw,se,se,se,nw,sw,s,se,se,s,se,se,sw,se,s,s,n,se,se,se,ne,s,s,se,s,s,se,nw,s,se,s,se,se,se,se,se,se,se,s,s,s,s,n,se,se,s,ne,se,se,se,sw,se,s,ne,se,s,se,se,se,se,s,s,se,se,se,s,ne,s,s,se,se,s,se,se,se,se,se,s,se,se,se,se,sw,se,s,se,ne,s,se,se,se,se,s,se,n,n,s,se,se,s,se,s,s,se,se,se,se,ne,sw,se,sw,nw,se,se,se,se,se,se,se,s,se,s,se,s,se,s,s,n,se,se,se,s,se,se,se,se,se,s,se,se,se,nw,se,ne,se,se,se,se,nw,n,se,n,se,s,se,se,s,s,se,n,s,s,nw,se,s,n,sw,se,n,se,se,s,nw,se,se,se,se,se,nw,se,se,se,se,n,se,se,se,se,se,se,s,se,se,se,ne,se,se,se,nw,s,se,s,se,se,ne,se,s,ne,s,se,ne,se,se,se,se,ne,n,se,se,s,se,se,sw,se,s,se,sw,se,se,se,n,se,se,se,se,ne,se,s,sw,se,se,se,s,s,se,se,se,se,se,se,se,se,s,se,se,n,se,se,n,n,se,se,se,s,se,se,se,se,nw,se,se,se,sw,se,se,se,s,se,n,se,se,se,s,se,s,se,s,se,se,se,n,sw,se,se,se,n,se,se,nw,se,se,se,se,se,n,se,se,se,nw,se,se,se,se,se,se,se,se,s,se,ne,se,se,se,se,se,sw,se,nw,se,se,s,nw,se,se,se,se,s,ne,se,se,se,se,s,s,se,n,s,se,nw,s,se,se,se,se,s,ne,ne,se,s,se,s,se,se,se,se,se,nw,se,s,se,se,se,se,se,se,se,se,se,s,se,se,se,ne,nw,se,se,se,se,se,se,se,se,nw,se,se,se,se,se,se,se,se,se,se,nw,se,se,n,se,ne,se,se,se,se,s,s,se,se,se,se,se,se,se,se,se,se,se,se,se,se,nw,n,se,se,se,s,se,se,sw,s,s,se,se,nw,se,se,se,se,ne,sw,se,s,se,se,se,se,s,se,se,se,se,se,s,se,se,se,se,se,se,se,se,sw,se,se,se,se,n,se,se,n,s,se,n,se,se,se,se,se,se,se,se,s,se,se,se,se,se,se,se,se,ne,sw,n,se,se,s,se,se,se,se,se,sw,nw,sw,nw,sw,s,se,nw,n,sw,nw,n,n,n,ne,ne,ne,n,ne,n,ne,sw,se,ne,ne,ne,ne,se,ne,ne,ne,n,se,ne,se,se,n,se,se,se,ne,se,se,n,se,sw,se,ne,se,se,s,se,se,s,s,n,nw,s,sw,n,s,s,ne,s,se,sw,se,sw,sw,sw,sw,ne,sw,se,s,s,sw,nw,s,sw,s,s,s,sw,sw,s,nw,sw,sw,sw,sw,sw,s,sw,sw,sw,sw,sw,sw,nw,sw,nw,nw,s,sw,s,sw,sw,sw,sw,sw,nw,sw,sw,se,sw,sw,sw,sw,ne,nw,nw,nw,nw,sw,nw,se,nw,ne,ne,ne,sw,nw,sw,nw,sw,nw,sw,nw,nw,s,nw,s,nw,n,nw,nw,nw,nw,nw,n,n,s,nw,se,ne,nw,nw,nw,nw,nw,nw,se,sw,n,nw,nw,nw,n,nw,n,nw,sw,se,nw,ne,nw,nw,n,ne,ne,n,nw,ne,nw,n,n,nw,n,nw,nw,n,n,nw,n,s,n,nw,ne,nw,n,nw,n,sw,n,n,n,n,n,nw,n,sw,s,n,n,n,n,n,n,n,n,n,nw,n,se,n,n,s,ne,n,sw,se,n,ne,s,s,n,n,n,n,n,ne,n,n,ne,n,se,s,n,se,sw,ne,sw,s,s,n,ne,n,se,ne,ne,n,ne,ne,n,ne,n,n,sw,nw,ne,n,n,se,ne,ne,s,s,ne,n,s,sw,ne,ne,ne,n,ne,ne,n,ne,ne,n,se,ne,ne,se,ne,ne,ne,ne,nw,ne,sw,n,n,ne,ne,ne,n,se,ne,n,ne,ne,ne,sw,ne,ne,sw,nw,se,ne,ne,ne,ne,ne,n,ne,se,ne,sw,sw,ne,ne,ne,ne,ne,ne,ne,n,ne,ne,se,ne,n,n,se,ne,se,ne,ne,ne,ne,ne,ne,se,ne,ne,ne,ne,ne,s,sw,se,ne,se,ne,se,ne,ne,sw,ne,ne,se,ne,ne,se,ne,se,ne,se,ne,ne,ne,nw,ne,se,se,ne,se,se,se,sw,se,se,ne,se,se,se,ne,se,ne,se,ne,se,se,nw,ne,sw,se,se,se,sw,ne,se,ne,se,se,se,se,sw,se,nw,nw,se,se,se,se,s,nw,se,sw,n,ne,nw,ne,se,se,se,se,ne,s,s,se,se,se,ne,nw,ne,se,se,ne,se,se,se,se,se,se,se,s,se,se,se,se,se,se,s,se,se,se,se,se,se,se,se,se,nw,se,se,se,se,se,se,se,se,se,se,se,se,se,s,ne,se,s,se,se,ne,s,se,ne,se,sw,sw,se,se,s,se,se,se,s,sw,s,s,s,sw,n,s,ne,se,se,s,se,se,se,s,se,se,se,se,se,se,se,s,s,ne,se,se,se,se,nw,n,se,se,se,n,s,se,se,se,se,se,se,s,se,s,ne,s,nw,se,s,se,s,s,n,s,se,se,n,se,s,sw,ne,s,se,s,s,se,s,n,se,s,s,s,se,nw,s,sw,sw,s,s,nw,s,se,s,se,s,se,sw,se,se,s,se,s,se,s,s,s,se,s,nw,nw,sw,se,s,nw,s,s,sw,s,s,s,se,s,se,s,s,se,s,sw,s,n,sw,se,s,s,s,s,nw,se,s,s,s,se,s,s,ne,se,s,se,se,s,s,se,s,s,se,s,n,s,se,s,sw,s,s,s,s,s,s,ne,s,s,ne,s,s,s,s,s,s,s,s,se,sw,s,sw,s,s,sw,s,s,s,s,s,sw,s,ne,nw,s,sw,s,s,s,s,s,s,s,sw,sw,s,nw,s,s,s,se,se,s,sw,s,nw,s,sw,s,s,sw,se,s,se,s,nw,s,s,s,n,s,s,s,n,s,s,ne,nw,n,s,s,s,s,s,s,s,n,s,sw,sw,sw,s,s,s,s,s,s,sw,s,s,s,s,s,s,ne,se,sw,se,s,sw,sw,se,s,s,s,sw,n,sw,s,s,sw,s,s,s,s,sw,nw,s,ne,s,s,s,s,s,s,s,sw,s,sw,sw,s,s,sw,s,sw,s,s,s,s,sw,sw,s,s,s,s,s,sw,sw,s,s,sw,s,nw,s,s,sw,sw,s,nw,ne,sw,s,sw,s,sw,s,s,ne,s,ne,s,sw,s,sw,sw,s,s,s,sw,sw,sw,sw,sw,s,sw,ne,s,s,s,sw,s,n,sw,sw,sw,sw,sw,sw,sw,n,s,sw,nw,sw,sw,sw,n,se,sw,sw,s,sw,n,sw,s,s,s,s,s,s,s,sw,s,sw,sw,se,sw,sw,sw,n,sw,nw,sw,se,s,sw,nw,se,sw,sw,sw,n,sw,se,sw,sw,s,sw,sw,sw,s,s,n,sw,n,sw,sw,sw,sw,sw,s,sw,sw,sw,ne,s,n,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,sw,s,ne,sw,ne,sw,sw,sw,ne,sw,sw,s,se,n,sw,sw,s,n,sw,sw,sw,sw,sw,sw,nw,sw,sw,sw,sw,sw,sw,n,sw,ne,se,sw,nw,s,sw,sw,sw,n,sw,sw,sw,sw,n,sw,sw,sw,ne,sw,se,ne,sw,se,ne,nw,sw,sw,sw,sw,nw,ne,sw,sw,nw,nw,sw,nw,sw,sw,nw,sw,s,n,se,sw,nw,sw,ne,sw,nw,sw,sw,sw,sw,sw,sw,se,se,nw,sw,ne,sw,nw,sw,sw,sw,sw,s,sw,sw,ne,sw,sw,sw,sw,sw,sw,sw,nw,sw,se,sw,sw,nw,n,s,sw,s,sw,sw,sw,ne,sw,nw,se,nw,nw,sw,nw,nw,sw,nw,nw,se,sw,sw,sw,sw,nw,nw,sw,nw,se,sw,sw,nw,ne,sw,sw,sw,se,sw,sw,sw,sw,ne,ne,nw,ne,nw,sw,sw,sw,n,s,nw,n,sw,nw,sw,nw,se,sw,nw,nw,nw,sw,nw,nw,sw,sw,nw,nw,sw,sw,sw,nw,sw,nw,nw,ne,sw,sw,nw,nw,sw,n,se,sw,nw,nw,sw,n,sw,s,sw,sw,nw,nw,sw,nw,se,n,sw,s,sw,sw,nw,nw,sw,nw,nw,sw,sw,sw,sw,nw,nw,sw,nw,nw,sw,nw,sw,sw,nw,se,nw,sw,sw,sw,sw,se,s,sw,nw,n,n,ne,nw,sw,nw,sw,s,sw,sw,sw,nw,s,nw,ne,s,sw,ne,nw,nw,nw,nw,sw,nw,nw,sw,nw,sw,sw,sw,sw,nw,nw,nw,sw,s,sw,nw,nw,sw,nw,sw,nw,nw,s,nw,nw,sw,sw,sw,sw,sw,sw,sw,sw,nw,nw,nw,nw,nw,nw,nw,nw,sw,sw,nw,sw,nw,nw,sw,nw,se,s,sw,nw,nw,nw,nw,nw,nw,nw,sw,nw,nw,sw,sw,s,nw,nw,nw,nw,nw,nw,nw,sw,sw,nw,nw,nw,n,ne,s,sw,s,se,nw,nw,s,nw,nw,nw,sw,s,ne,nw,nw,nw,nw,s,nw,nw,nw,ne,nw,sw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,ne,ne,sw,nw,ne,nw,nw,nw,nw,nw,sw,nw,nw,nw,nw,nw,nw,nw,s,nw,nw,nw,nw,nw,nw,se,sw,nw,n,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,s,s,nw,nw,ne,nw,sw,nw,s,nw,nw,nw,nw,n,nw,sw,nw,nw,nw,nw,nw,nw,ne,nw,nw,nw,nw,nw,nw,nw,ne,nw,sw,nw,nw,nw,nw,nw,nw,nw,nw,n,n,nw,nw,nw,se,s,s,nw,nw,nw,nw,n,nw,nw,nw,sw,se,nw,nw,n,nw,n,nw,nw,se,nw,nw,nw,nw,nw,nw,nw,se,nw,nw,n,nw,nw,nw,se,nw,nw,nw,nw,nw,nw,nw,nw,nw,nw,ne,nw,nw,n,nw,nw,ne,nw,nw,n,nw,nw,n,n,nw,n,ne,nw,nw,nw,nw,nw,nw,s,nw,nw,nw,nw,ne,nw,nw,n,nw,ne,n,nw,nw,nw,nw,nw,nw,s,nw,nw,nw,nw,ne,sw,nw,nw,nw,n,nw,nw,nw,nw,n,nw,n,nw,nw,nw,nw,nw,nw,sw,nw,ne,nw,nw,sw,nw,nw,n,nw,nw,nw,s,s,s,nw,nw,nw,nw,n,nw,se,sw,s,nw,nw,nw,sw,n,n,nw,nw,ne,nw,n,se,n,nw,n,nw,nw,nw,n,nw,nw,nw,nw,n,nw,s,n,nw,n,nw,nw,nw,nw,nw,n,ne,nw,sw,nw,n,nw,nw,nw,n,s,nw,n,se,nw,ne,n,nw,nw,nw,n,nw,ne,nw,n,n,nw,n,s,nw,ne,ne,n,n,nw,se,nw,n,s,n,nw,n,n,n,n,n,nw,nw,s,nw,n,nw,nw,n,n,n,se,n,nw,nw,s,n,nw,n,nw,n,nw,nw,n,n,nw,n,n,se,sw,n,n,n,nw,nw,n,nw,nw,n,n,nw,n,se,n,n,n,n,nw,n,sw,n,nw,nw,nw,nw,ne,nw,n,n,n,n,n,n,nw,nw,se,nw,nw,n,n,n,nw,sw,ne,n,nw,n,n,nw,n,se,se,n,n,n,n,n,nw,n,n,n,s,sw,n,n,s,n,sw,n,se,n,nw,nw,se,nw,n,n,nw,sw,nw,n,n,n,ne,n,n,sw,nw,nw,se,nw,n,nw,sw,ne,sw,nw,n,n,nw,nw,nw,nw,nw,se,nw,n,nw,n,n,nw,sw,n,n,nw,nw,se,sw,n,n,n,n,n,n,nw,nw,nw,n,n,ne,se,n,nw,nw,ne,n,nw,n,n,n,sw,se,n,n,nw,nw,nw,n,n,n,n,se,n,ne,n,s,nw,n,n,n,nw,s,se,n,nw,nw,nw,n,n,nw,n,n,n,n,se,nw,n,n,nw,n,n,nw,n,n,n,n,n,n,n,sw,n,n,n,n,n,se,s,n,n,n,n,s,n,n,n,sw,n,n,nw,n,se,sw,n,sw,n,n,n,n,n,n,ne,n,n,nw,n,n,n,n,n,n,n,se,n,n,n,ne,n,n,n,n,n,n,s,n,nw,n,n,n,n,n,n,n,n,ne,n,n,n,n,sw,n,ne,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,sw,n,n,n,n,n,s,n,n,n,n,n,n,sw,se,n,n,n,n,n,n,s,s,n,nw,n,ne,n,n,n,n,ne,sw,n,n,sw,s,n,n,n,n,n,n,sw,n,nw,n,s,n,n,n,ne,sw,n,n,n,n,n,n,n,n,n,n,n,n,n,n,sw,n,sw,n,n,n,s,sw,n,n,n,s,n,ne,n,sw,nw,n,ne,nw,sw,n,n,n,n,n,n,se,nw,n,nw,sw,n,n,n,n,ne,s,n,ne,s,n,n,n,ne,n,n,nw,n,n,n,n,n,n,s,n,n,n,n,n,n,ne,n,n,n,ne,n,n,n,n,nw,nw,n,n,ne,ne,sw,ne,s,n,n,s,s,ne,nw,n,ne,ne,sw,ne,n,n,se,n,n,ne,s,ne,n,n,n,n,n,n,n,n,n,n,ne,n,nw,ne,n,n,se,ne,ne,ne,n,n,s,ne,n,ne,ne,n,s,ne,n,n,ne,n,sw,s,ne,se,n,n,n,nw,n,n,n,ne,ne,n,nw,n,ne,n,n,ne,n,nw,ne,nw,n,n,n,ne,ne,n,ne,sw,n,ne,n,nw,n,n,n,se,n,n,se,n,ne,n,n,ne,nw,ne,n,n,n,ne,n,nw,ne,n,n,sw,ne,n,s,ne,ne,ne,n,sw,n,n,ne,ne,n,ne,ne,n,se,ne,n,ne,n,n,n,nw,ne,n,n,n,ne,se,n,ne,ne,n,n,n,n,n,se,nw,n,nw,n,se,n,s,n,n,ne,ne,ne,ne,ne,ne,n,se,ne,n,s,s,ne,ne,ne,s,nw,n,n,s,s,n,ne,sw,ne,ne,n,n,ne,sw,ne,ne,sw,n,ne,ne,n,n,s,ne,ne,n,s,ne,ne,ne,n,ne,n,n,ne,ne,n,n,n,ne,n,ne,sw,ne,ne,ne,n,ne,sw,ne,s,se,sw,n,sw,ne,ne,ne,s,nw,nw,s,ne,n,sw,ne,se,ne,n,ne,n,s,se,ne,ne,ne,nw,ne,ne,n,ne,sw,ne,ne,ne,ne,sw,n,ne,se,se,sw,n,ne,n,n,ne,ne,ne,ne,ne,n,n,sw,n,se,n,n,n,n,ne,n,se,n,ne,ne,n,n,n,n,n,ne,ne,n,ne,s,s,ne,n,nw,n,se,ne,ne,n,n,ne,n,ne,s,ne,ne,n,n,n,ne,n,ne,n,n,ne,n,n,n,se,sw,n,ne,ne,n,ne,ne,ne,n,ne,ne,ne,se,ne,ne,ne,n,n,s,ne,nw,ne,n,n,ne,ne,ne,ne,ne,nw,ne,n,ne,n,n,ne,ne,ne,ne,ne,ne,ne,ne,ne,n,ne,ne,ne,ne,ne,s,n,ne,n,ne,ne,ne,ne,ne,ne,n,n,ne,ne,ne,ne,ne,ne,n,s,ne,n,ne,n,n,n,ne,ne,ne,ne,se,n,ne,se,ne,n,ne,n,ne,ne,ne,n,ne,ne,ne,ne,ne,ne,s,nw,se,ne,n,ne,ne,ne,n,ne,ne,ne,se,ne,se,n,ne,ne,ne,ne,sw,ne,n,ne,ne,s,ne,sw,n,n,nw,sw,ne,ne,n,ne,ne,ne,ne,ne,ne,nw,ne,se,ne,ne,ne,n,ne,ne,ne,n,s,ne,ne,ne,n,ne,ne,ne,s,ne,n,ne,n,sw,nw,ne,ne,sw,n,n,s,nw,n,se,ne,nw,ne,n,ne,ne,s,s,ne,sw,ne,n,s,n,ne,ne,ne,ne,n,n,ne,ne,ne,s,ne,ne,ne,ne,ne,ne,s,ne,ne,ne,ne,ne,s,ne,ne,ne,ne,ne,ne,ne,ne,n,ne,n,ne,ne,ne,ne,ne,nw,sw,sw,ne,n,n,ne,ne,nw,s,nw,ne,ne,ne,ne,sw,n,ne,n,ne,sw,s,se,ne,ne,ne,se,ne,ne,se,n,ne,ne,ne,n,n,ne,ne,n,ne,n,se,ne,nw,ne,s,ne,ne,ne,n,nw,n,se,ne,ne,ne,nw,n,sw,ne,ne,se,ne,ne,ne,nw,ne,ne,ne,s,n,ne,se,ne,ne,se,n,ne,ne,ne,ne,s,ne,ne,ne,sw,ne,ne,sw,ne,ne,ne,ne,se,n,ne,ne,se,ne,ne")

(def day12
  "0 <-> 1543
1 <-> 66, 1682
2 <-> 1525
3 <-> 958
4 <-> 593, 1542
5 <-> 484
6 <-> 297
7 <-> 372, 743, 1965
8 <-> 934
9 <-> 1224, 1489
10 <-> 10, 129, 147, 1394
11 <-> 1244
12 <-> 12, 994, 1954
13 <-> 1027
14 <-> 875, 1211
15 <-> 405
16 <-> 1437, 1476
17 <-> 996
18 <-> 216, 777
19 <-> 404, 1524, 1539, 1941
20 <-> 1365
21 <-> 21, 460, 1431, 1624
22 <-> 313, 530
23 <-> 942, 1125
24 <-> 180, 338
25 <-> 771, 1547, 1561
26 <-> 463, 1012, 1276, 1760
27 <-> 738
28 <-> 311, 1023, 1461, 1739, 1853, 1900
29 <-> 1075
30 <-> 355, 467, 1605
31 <-> 1137, 1740
32 <-> 1125, 1756
33 <-> 658, 1149, 1790
34 <-> 344, 899
35 <-> 337, 716, 1416, 1780
36 <-> 1641
37 <-> 812
38 <-> 86, 1195
39 <-> 1967
40 <-> 1125, 1550
41 <-> 1881
42 <-> 151
43 <-> 222
44 <-> 73, 1075
45 <-> 1865
46 <-> 634, 837, 1143
47 <-> 647
48 <-> 695, 901
49 <-> 49
50 <-> 1598, 1619
51 <-> 125, 550, 1161
52 <-> 241, 387, 1951
53 <-> 286, 1414
54 <-> 1231, 1926
55 <-> 437, 1292, 1919
56 <-> 1108, 1597
57 <-> 1073, 1205, 1565
58 <-> 555
59 <-> 59, 1410
60 <-> 1735
61 <-> 61
62 <-> 149, 525, 1341
63 <-> 356, 1251
64 <-> 146, 1187
65 <-> 1582
66 <-> 1
67 <-> 126, 303, 1942
68 <-> 1135, 1742
69 <-> 1904
70 <-> 70, 371
71 <-> 513
72 <-> 1877
73 <-> 44
74 <-> 1617
75 <-> 1495
76 <-> 326, 867
77 <-> 420, 961, 1425
78 <-> 1517
79 <-> 1233
80 <-> 415, 1075, 1354
81 <-> 958, 1089
82 <-> 1677
83 <-> 403
84 <-> 85, 1877
85 <-> 84, 260
86 <-> 38, 191, 442
87 <-> 969, 1993
88 <-> 610, 1507
89 <-> 151, 758, 1081, 1521, 1596
90 <-> 474, 1958
91 <-> 950, 1554, 1949
92 <-> 117, 169
93 <-> 1899
94 <-> 819
95 <-> 1886
96 <-> 360, 1626
97 <-> 916, 1146
98 <-> 675, 1502, 1566
99 <-> 384
100 <-> 1905
101 <-> 403, 1387, 1717
102 <-> 196, 1281
103 <-> 947
104 <-> 104
105 <-> 975
106 <-> 303, 546, 750
107 <-> 380, 1929
108 <-> 108
109 <-> 999, 1059
110 <-> 1617
111 <-> 340
112 <-> 112
113 <-> 1063, 1281, 1758
114 <-> 114
115 <-> 631, 1067, 1904
116 <-> 1036
117 <-> 92, 318
118 <-> 1269
119 <-> 898
120 <-> 1052
121 <-> 121
122 <-> 1046
123 <-> 1127
124 <-> 717, 1018
125 <-> 51, 535, 639, 709
126 <-> 67, 557, 1060, 1098, 1640
127 <-> 1286
128 <-> 128
129 <-> 10, 1467
130 <-> 492, 1961
131 <-> 1103, 1433
132 <-> 1191
133 <-> 309, 1049
134 <-> 1361, 1390
135 <-> 683, 1575
136 <-> 702, 1188
137 <-> 290, 302, 1527
138 <-> 656, 975, 1279
139 <-> 882, 1537, 1542, 1930
140 <-> 1200
141 <-> 562, 680, 1865
142 <-> 1233
143 <-> 653, 1076
144 <-> 1334
145 <-> 145, 1797
146 <-> 64
147 <-> 10, 863, 1090
148 <-> 1138
149 <-> 62, 463
150 <-> 1223, 1228, 1239
151 <-> 42, 89, 404, 694
152 <-> 1389, 1672
153 <-> 1596
154 <-> 1193
155 <-> 980
156 <-> 164
157 <-> 157, 1270
158 <-> 272, 1461
159 <-> 777, 1153
160 <-> 595, 1209
161 <-> 365, 686
162 <-> 516, 987
163 <-> 546, 1004, 1056
164 <-> 156, 164
165 <-> 165, 1696
166 <-> 310, 370, 397
167 <-> 1446
168 <-> 1900
169 <-> 92, 1680
170 <-> 240, 619, 1088, 1509
171 <-> 186, 1610
172 <-> 1622, 1698
173 <-> 447, 542, 912
174 <-> 669, 1687
175 <-> 689
176 <-> 176, 1816
177 <-> 518, 1422, 1493
178 <-> 479, 731, 1615, 1718
179 <-> 483
180 <-> 24, 180
181 <-> 329, 1941
182 <-> 405, 1175, 1685
183 <-> 291, 466, 558, 891
184 <-> 527
185 <-> 185, 868, 1136
186 <-> 171, 867
187 <-> 1622, 1634
188 <-> 211
189 <-> 797
190 <-> 1307, 1504
191 <-> 86, 996
192 <-> 1810
193 <-> 315
194 <-> 194, 1198
195 <-> 1401, 1581, 1904
196 <-> 102
197 <-> 539
198 <-> 1996
199 <-> 1601
200 <-> 1617, 1776
201 <-> 294, 390
202 <-> 839, 986
203 <-> 1683
204 <-> 546
205 <-> 1673, 1894
206 <-> 1825
207 <-> 207, 222
208 <-> 210, 1679
209 <-> 384, 421, 1249
210 <-> 208, 210, 1721
211 <-> 188, 211, 388, 394, 440, 1205
212 <-> 834, 1857
213 <-> 1102
214 <-> 1803
215 <-> 1033, 1831
216 <-> 18, 1039
217 <-> 1168, 1983
218 <-> 1273, 1944
219 <-> 845, 1271
220 <-> 321, 640
221 <-> 629
222 <-> 43, 207, 285, 1486, 1508
223 <-> 809, 1371
224 <-> 480
225 <-> 904, 1190, 1378
226 <-> 226
227 <-> 1044, 1294
228 <-> 793, 911
229 <-> 1450, 1940
230 <-> 822
231 <-> 321, 352
232 <-> 232
233 <-> 491, 543
234 <-> 1880
235 <-> 861, 1349
236 <-> 1738, 1977
237 <-> 590, 1246, 1805
238 <-> 238, 665
239 <-> 577, 818, 877
240 <-> 170
241 <-> 52, 241, 946, 1439, 1441
242 <-> 262, 1226, 1647, 1661
243 <-> 316, 1117, 1831
244 <-> 345
245 <-> 527, 1392, 1526
246 <-> 1335
247 <-> 1754, 1842, 1905
248 <-> 1572
249 <-> 1066, 1185, 1593
250 <-> 934, 1775, 1821
251 <-> 726, 749
252 <-> 252
253 <-> 253
254 <-> 1765
255 <-> 753, 1337
256 <-> 411, 1190
257 <-> 1664, 1731
258 <-> 846
259 <-> 450, 1062, 1975
260 <-> 85, 1097, 1620
261 <-> 662, 1667
262 <-> 242, 1285, 1641
263 <-> 263
264 <-> 1481
265 <-> 470, 1671
266 <-> 971
267 <-> 1002, 1893
268 <-> 393, 1435
269 <-> 413, 806, 1287, 1525
270 <-> 1182
271 <-> 1377
272 <-> 158, 867
273 <-> 273, 499, 568, 845, 1293
274 <-> 800
275 <-> 936
276 <-> 925
277 <-> 1295
278 <-> 1085, 1140
279 <-> 873, 883, 989
280 <-> 280
281 <-> 1211
282 <-> 445, 674, 1234
283 <-> 771
284 <-> 1874
285 <-> 222, 647, 1092
286 <-> 53, 1191, 1678
287 <-> 595, 1928
288 <-> 947
289 <-> 822, 1836, 1962
290 <-> 137, 1034, 1735
291 <-> 183
292 <-> 331, 1038
293 <-> 1025
294 <-> 201, 630, 1421
295 <-> 1083, 1366
296 <-> 701, 1187, 1618
297 <-> 6, 990
298 <-> 1093
299 <-> 299
300 <-> 846, 1990
301 <-> 1306
302 <-> 137, 1011
303 <-> 67, 106, 1779
304 <-> 1202
305 <-> 439
306 <-> 622, 1858
307 <-> 1379
308 <-> 688, 1631, 1700
309 <-> 133
310 <-> 166, 362
311 <-> 28, 933
312 <-> 881
313 <-> 22, 549, 678, 1145
314 <-> 1439
315 <-> 193, 650, 1572
316 <-> 243
317 <-> 317, 990, 1638
318 <-> 117, 499
319 <-> 1300, 1309, 1614
320 <-> 633, 1693
321 <-> 220, 231
322 <-> 977, 1722
323 <-> 730, 1372, 1996
324 <-> 755, 1184
325 <-> 1155, 1857
326 <-> 76
327 <-> 1072, 1814, 1985
328 <-> 1125, 1279
329 <-> 181
330 <-> 936, 1241
331 <-> 292, 1172
332 <-> 1859, 1882
333 <-> 940
334 <-> 468
335 <-> 697, 802
336 <-> 437
337 <-> 35, 737
338 <-> 24, 1540
339 <-> 493
340 <-> 111, 422, 525
341 <-> 1790
342 <-> 496, 1007
343 <-> 343, 1264
344 <-> 34, 344
345 <-> 244, 709
346 <-> 553, 1616
347 <-> 909
348 <-> 521, 1660
349 <-> 363, 1294
350 <-> 719, 1782, 1974
351 <-> 405, 915
352 <-> 231, 1694
353 <-> 1140
354 <-> 363, 1339
355 <-> 30
356 <-> 63, 771, 1110
357 <-> 1299, 1347
358 <-> 635
359 <-> 1541
360 <-> 96, 360, 1741
361 <-> 361
362 <-> 310
363 <-> 349, 354
364 <-> 1827
365 <-> 161, 1734
366 <-> 900
367 <-> 1139, 1545
368 <-> 535
369 <-> 1622
370 <-> 166, 1463
371 <-> 70
372 <-> 7, 452, 810, 1283
373 <-> 997, 1658
374 <-> 467, 1774
375 <-> 716, 1841
376 <-> 638, 1079, 1262
377 <-> 606
378 <-> 993
379 <-> 379, 791
380 <-> 107
381 <-> 475, 1510
382 <-> 780
383 <-> 383
384 <-> 99, 209, 1590
385 <-> 1388
386 <-> 1829
387 <-> 52, 1532, 1874
388 <-> 211, 576, 1281
389 <-> 1309
390 <-> 201, 1183
391 <-> 959, 1944
392 <-> 688, 1062, 1299
393 <-> 268
394 <-> 211, 828, 1701
395 <-> 1587
396 <-> 1082
397 <-> 166
398 <-> 1454, 1508
399 <-> 1007
400 <-> 1531
401 <-> 511, 1140
402 <-> 652, 1065
403 <-> 83, 101, 1585
404 <-> 19, 151
405 <-> 15, 182, 351
406 <-> 769
407 <-> 1275, 1578, 1752
408 <-> 1173
409 <-> 409, 432
410 <-> 487, 1334
411 <-> 256
412 <-> 412, 590
413 <-> 269
414 <-> 1371
415 <-> 80, 1832
416 <-> 939, 1644
417 <-> 562, 1510
418 <-> 536
419 <-> 473
420 <-> 77, 1059, 1535, 1863
421 <-> 209
422 <-> 340, 913, 989
423 <-> 854, 951
424 <-> 512
425 <-> 1087
426 <-> 773
427 <-> 1121, 1574
428 <-> 745
429 <-> 1669
430 <-> 1018
431 <-> 1377
432 <-> 409
433 <-> 1641, 1999
434 <-> 1605
435 <-> 1412, 1500
436 <-> 879, 1704
437 <-> 55, 336, 548, 1839, 1987
438 <-> 980, 1399
439 <-> 305, 439, 627, 628, 884, 1241
440 <-> 211
441 <-> 677
442 <-> 86
443 <-> 1213
444 <-> 592, 845, 1282
445 <-> 282, 1366
446 <-> 710, 797, 1017, 1974
447 <-> 173, 447, 614
448 <-> 1187, 1511
449 <-> 598, 1000, 1257
450 <-> 259
451 <-> 451, 519
452 <-> 372
453 <-> 1549
454 <-> 940
455 <-> 926, 1543, 1706
456 <-> 682, 1253
457 <-> 1694, 1755
458 <-> 549
459 <-> 1192, 1456, 1613
460 <-> 21, 490
461 <-> 1920
462 <-> 788, 1254, 1695
463 <-> 26, 149
464 <-> 945
465 <-> 1654
466 <-> 183
467 <-> 30, 374
468 <-> 334, 468
469 <-> 1514
470 <-> 265
471 <-> 1190
472 <-> 1911
473 <-> 419, 1230, 1611, 1711
474 <-> 90, 1253
475 <-> 381, 825
476 <-> 476, 503
477 <-> 1337, 1362
478 <-> 1605
479 <-> 178
480 <-> 224, 1810, 1858
481 <-> 871, 1525
482 <-> 1137, 1457, 1553
483 <-> 179, 1336
484 <-> 5, 1788
485 <-> 1874
486 <-> 986, 1409
487 <-> 410
488 <-> 1911
489 <-> 885, 1867
490 <-> 460
491 <-> 233, 1046, 1880
492 <-> 130, 492
493 <-> 339, 810, 1218
494 <-> 1275
495 <-> 1218, 1396
496 <-> 342, 1445
497 <-> 548
498 <-> 729, 1208, 1590
499 <-> 273, 318, 774
500 <-> 521, 673, 1035
501 <-> 504, 978
502 <-> 641
503 <-> 476, 615, 1230, 1374
504 <-> 501
505 <-> 505, 1336
506 <-> 1810
507 <-> 1396
508 <-> 603, 1067, 1794
509 <-> 1714
510 <-> 1441
511 <-> 401, 1497
512 <-> 424, 530
513 <-> 71, 513
514 <-> 822, 925, 1603
515 <-> 1575, 1856
516 <-> 162, 1116, 1634
517 <-> 936
518 <-> 177, 1418, 1862
519 <-> 451
520 <-> 1318, 1621
521 <-> 348, 500
522 <-> 569, 674
523 <-> 1438
524 <-> 1181, 1552
525 <-> 62, 340, 1968
526 <-> 981
527 <-> 184, 245, 527, 574, 1767
528 <-> 873
529 <-> 945, 1139
530 <-> 22, 512, 1957
531 <-> 531
532 <-> 921, 1071, 1087, 1989
533 <-> 1845
534 <-> 534, 1929
535 <-> 125, 368
536 <-> 418, 702
537 <-> 1442, 1545
538 <-> 552, 1167
539 <-> 197, 1643
540 <-> 1983
541 <-> 1574
542 <-> 173
543 <-> 233
544 <-> 1128
545 <-> 903, 989
546 <-> 106, 163, 204
547 <-> 1636, 1916
548 <-> 437, 497, 926
549 <-> 313, 458, 1793
550 <-> 51
551 <-> 1756
552 <-> 538, 1061
553 <-> 346, 1564
554 <-> 554
555 <-> 58, 792, 1221
556 <-> 1133, 1881
557 <-> 126, 1222
558 <-> 183
559 <-> 1927
560 <-> 797, 1008, 1048, 1328, 1984
561 <-> 1294, 1761
562 <-> 141, 417
563 <-> 676, 1763
564 <-> 675, 1220, 1503
565 <-> 565, 1360
566 <-> 1364
567 <-> 618, 1419
568 <-> 273, 1830
569 <-> 522, 1642
570 <-> 778
571 <-> 881, 1353
572 <-> 902, 1699
573 <-> 1679
574 <-> 527, 656
575 <-> 718, 1232
576 <-> 388
577 <-> 239
578 <-> 1312
579 <-> 1504, 1970
580 <-> 580
581 <-> 1548, 1893, 1986, 1993
582 <-> 1228, 1516, 1729
583 <-> 583, 677, 1178
584 <-> 1231
585 <-> 585
586 <-> 1109, 1646, 1681
587 <-> 1124, 1237
588 <-> 1004
589 <-> 1756
590 <-> 237, 412
591 <-> 591, 692, 1078
592 <-> 444
593 <-> 4, 944
594 <-> 1780, 1911
595 <-> 160, 287
596 <-> 969, 1256
597 <-> 830
598 <-> 449
599 <-> 1863
600 <-> 1928
601 <-> 1079
602 <-> 1514
603 <-> 508, 1869
604 <-> 848, 1234
605 <-> 605, 952, 1310
606 <-> 377, 638, 880
607 <-> 607, 853, 947, 1453
608 <-> 608, 812
609 <-> 1091, 1430
610 <-> 88
611 <-> 1661
612 <-> 675
613 <-> 1409, 1946
614 <-> 447, 1825, 1992
615 <-> 503
616 <-> 616
617 <-> 758, 1813
618 <-> 567, 618, 1068
619 <-> 170
620 <-> 1937
621 <-> 734, 1122
622 <-> 306
623 <-> 1340
624 <-> 1644
625 <-> 1926
626 <-> 1362, 1528
627 <-> 439
628 <-> 439
629 <-> 221, 1313
630 <-> 294, 1650
631 <-> 115, 1770, 1849
632 <-> 1958
633 <-> 320, 845, 1445
634 <-> 46, 1024
635 <-> 358, 1355, 1778
636 <-> 1000, 1120
637 <-> 798, 1009
638 <-> 376, 606, 1244, 1590
639 <-> 125, 849
640 <-> 220
641 <-> 502, 779, 1675
642 <-> 740, 1405, 1473, 1702
643 <-> 930, 1446
644 <-> 801, 1531, 1886
645 <-> 1062, 1171, 1595, 1973
646 <-> 1677, 1751
647 <-> 47, 285
648 <-> 1024, 1691
649 <-> 1743
650 <-> 315, 819, 876
651 <-> 1715, 1909
652 <-> 402, 1777
653 <-> 143
654 <-> 868, 1820
655 <-> 875
656 <-> 138, 574
657 <-> 725
658 <-> 33, 1038
659 <-> 997
660 <-> 1438, 1449
661 <-> 1359, 1736
662 <-> 261, 1541
663 <-> 896, 981, 1681
664 <-> 1538, 1639
665 <-> 238, 1105
666 <-> 1036, 1313
667 <-> 920, 1123
668 <-> 668, 1808
669 <-> 174
670 <-> 670
671 <-> 767, 1923
672 <-> 1549, 1564
673 <-> 500
674 <-> 282, 522, 1921, 1953
675 <-> 98, 564, 612, 1260
676 <-> 563, 1838
677 <-> 441, 583
678 <-> 313
679 <-> 679
680 <-> 141
681 <-> 1850
682 <-> 456
683 <-> 135, 1880
684 <-> 1315
685 <-> 1819, 1966
686 <-> 161, 786, 796, 1801
687 <-> 687, 1557
688 <-> 308, 392
689 <-> 175, 1899
690 <-> 952
691 <-> 691
692 <-> 591, 1457, 1662
693 <-> 693
694 <-> 151
695 <-> 48, 1436, 1539, 1702, 1903
696 <-> 696
697 <-> 335, 1130, 1812, 1823
698 <-> 1064
699 <-> 1269, 1807
700 <-> 755
701 <-> 296
702 <-> 136, 536, 702
703 <-> 834, 1333
704 <-> 1583, 1731
705 <-> 1322
706 <-> 991
707 <-> 1083
708 <-> 1382
709 <-> 125, 345, 948
710 <-> 446, 1207, 1770
711 <-> 1831
712 <-> 1663, 1935
713 <-> 1410
714 <-> 968
715 <-> 788
716 <-> 35, 375
717 <-> 124, 1265, 1495, 1639
718 <-> 575
719 <-> 350
720 <-> 811
721 <-> 1827
722 <-> 722, 1492, 1650
723 <-> 1967
724 <-> 1059, 1726
725 <-> 657, 725
726 <-> 251
727 <-> 1129
728 <-> 1459
729 <-> 498
730 <-> 323, 1617
731 <-> 178
732 <-> 1375
733 <-> 953, 1384
734 <-> 621, 1312, 1911, 1955
735 <-> 1100, 1117
736 <-> 1180
737 <-> 337
738 <-> 27, 738
739 <-> 888
740 <-> 642
741 <-> 1132, 1304, 1680
742 <-> 1615
743 <-> 7, 1766
744 <-> 744
745 <-> 428, 857, 969, 1179, 1180
746 <-> 751
747 <-> 1256
748 <-> 1528
749 <-> 251, 912, 1686
750 <-> 106
751 <-> 746, 751
752 <-> 1354, 1498, 1571
753 <-> 255
754 <-> 908, 1066, 1589
755 <-> 324, 700
756 <-> 1013
757 <-> 814
758 <-> 89, 617, 1602
759 <-> 1550
760 <-> 1047
761 <-> 970, 1129
762 <-> 1508
763 <-> 1329
764 <-> 765, 1415
765 <-> 764, 1908
766 <-> 1197, 1595
767 <-> 671
768 <-> 1457
769 <-> 406, 852
770 <-> 770, 1427
771 <-> 25, 283, 356
772 <-> 1606, 1823
773 <-> 426, 1836
774 <-> 499
775 <-> 1031, 1733
776 <-> 1035, 1567
777 <-> 18, 159
778 <-> 570, 1994
779 <-> 641
780 <-> 382, 1703, 1928
781 <-> 1647
782 <-> 1367
783 <-> 1454, 1609, 1785
784 <-> 1243, 1380
785 <-> 1236
786 <-> 686, 1050, 1413
787 <-> 1026, 1613
788 <-> 462, 715, 1855
789 <-> 789, 1021, 1186, 1573
790 <-> 1744
791 <-> 379
792 <-> 555, 1041, 1107
793 <-> 228, 1194
794 <-> 864, 1591, 1612
795 <-> 795, 1478
796 <-> 686, 1652
797 <-> 189, 446, 560
798 <-> 637, 1342
799 <-> 799, 1890
800 <-> 274, 800, 1578
801 <-> 644, 1618, 1732
802 <-> 335
803 <-> 803, 1497
804 <-> 804
805 <-> 805
806 <-> 269
807 <-> 817, 987
808 <-> 1445
809 <-> 223, 1489
810 <-> 372, 493, 1397
811 <-> 720, 1786
812 <-> 37, 608
813 <-> 1410
814 <-> 757, 928, 1201, 1305
815 <-> 1079, 1443
816 <-> 1563
817 <-> 807, 1614
818 <-> 239, 1135
819 <-> 94, 650
820 <-> 820, 1747
821 <-> 827
822 <-> 230, 289, 514
823 <-> 1397
824 <-> 899
825 <-> 475
826 <-> 1468, 1696
827 <-> 821, 1238
828 <-> 394, 1459
829 <-> 1372, 1448
830 <-> 597, 1112
831 <-> 831
832 <-> 832
833 <-> 833
834 <-> 212, 703, 1913
835 <-> 835
836 <-> 1904
837 <-> 46
838 <-> 1512
839 <-> 202, 1510, 1690
840 <-> 842
841 <-> 1430
842 <-> 840, 1397
843 <-> 955, 1303, 1500, 1845
844 <-> 1221
845 <-> 219, 273, 444, 633, 1434
846 <-> 258, 300
847 <-> 847
848 <-> 604, 1960
849 <-> 639, 1126, 1601
850 <-> 1027, 1818, 1899
851 <-> 1124, 1536
852 <-> 769, 852, 1551, 1727
853 <-> 607, 1885
854 <-> 423, 1544
855 <-> 1889
856 <-> 981, 1683
857 <-> 745
858 <-> 858, 1041
859 <-> 859, 1465
860 <-> 860, 1288
861 <-> 235, 1133
862 <-> 979, 1255, 1484
863 <-> 147
864 <-> 794
865 <-> 865, 902
866 <-> 1033, 1367
867 <-> 76, 186, 272, 1205
868 <-> 185, 654
869 <-> 895
870 <-> 1082
871 <-> 481, 1151
872 <-> 872
873 <-> 279, 528
874 <-> 1358, 1787
875 <-> 14, 655
876 <-> 650, 1227
877 <-> 239, 1674
878 <-> 878
879 <-> 436, 1786, 1844
880 <-> 606, 1181
881 <-> 312, 571
882 <-> 139
883 <-> 279, 1498, 1746
884 <-> 439
885 <-> 489
886 <-> 1592
887 <-> 919
888 <-> 739, 1958
889 <-> 1930
890 <-> 1026, 1326
891 <-> 183, 891
892 <-> 1984
893 <-> 1078
894 <-> 1178, 1329
895 <-> 869, 1863
896 <-> 663
897 <-> 1693
898 <-> 119, 1316
899 <-> 34, 824
900 <-> 366, 900
901 <-> 48, 1030
902 <-> 572, 865, 973
903 <-> 545
904 <-> 225, 1083
905 <-> 1172, 1274, 1507
906 <-> 1878
907 <-> 1372
908 <-> 754, 1485, 1923
909 <-> 347, 909
910 <-> 1335, 1621
911 <-> 228, 1077, 1736
912 <-> 173, 749
913 <-> 422, 1688
914 <-> 1232, 1615
915 <-> 351, 1889
916 <-> 97, 1593
917 <-> 1574
918 <-> 1254, 1829, 1940
919 <-> 887, 1284, 1392
920 <-> 667, 1480
921 <-> 532, 1296, 1712
922 <-> 922
923 <-> 1194
924 <-> 1931
925 <-> 276, 514
926 <-> 455, 548
927 <-> 1241
928 <-> 814
929 <-> 1276, 1645
930 <-> 643
931 <-> 1868
932 <-> 1823
933 <-> 311
934 <-> 8, 250, 1416, 1886
935 <-> 1927
936 <-> 275, 330, 517
937 <-> 1692, 1867
938 <-> 1050
939 <-> 416, 1430
940 <-> 333, 454, 1094, 1980
941 <-> 1698
942 <-> 23, 1642
943 <-> 1102
944 <-> 593, 1365, 1577
945 <-> 464, 529, 945
946 <-> 241, 1037
947 <-> 103, 288, 607, 1095
948 <-> 709, 1217
949 <-> 1960
950 <-> 91, 1862
951 <-> 423
952 <-> 605, 690
953 <-> 733, 1894
954 <-> 1113, 1978, 1988
955 <-> 843
956 <-> 956
957 <-> 1122
958 <-> 3, 81
959 <-> 391, 1383, 1711
960 <-> 1280, 1796
961 <-> 77
962 <-> 1509
963 <-> 963, 1788, 1897
964 <-> 1810
965 <-> 1147
966 <-> 1741
967 <-> 1979
968 <-> 714, 968
969 <-> 87, 596, 745, 1082
970 <-> 761, 1495
971 <-> 266, 971
972 <-> 972
973 <-> 902, 1398
974 <-> 1862
975 <-> 105, 138, 1118
976 <-> 1369
977 <-> 322, 1970
978 <-> 501, 1270
979 <-> 862, 1244
980 <-> 155, 438, 1072, 1176
981 <-> 526, 663, 856, 1355
982 <-> 1344
983 <-> 1947
984 <-> 1221
985 <-> 1042
986 <-> 202, 486, 1635
987 <-> 162, 807, 1267
988 <-> 988
989 <-> 279, 422, 545, 1128
990 <-> 297, 317, 1891
991 <-> 706, 1229, 1712
992 <-> 1269
993 <-> 378, 1441
994 <-> 12
995 <-> 1693
996 <-> 17, 191
997 <-> 373, 659, 1678
998 <-> 1278
999 <-> 109
1000 <-> 449, 636
1001 <-> 1922
1002 <-> 267
1003 <-> 1989
1004 <-> 163, 588
1005 <-> 1159, 1247, 1261
1006 <-> 1218
1007 <-> 342, 399, 1148
1008 <-> 560
1009 <-> 637, 1129
1010 <-> 1257
1011 <-> 302
1012 <-> 26, 1184, 1866, 1937
1013 <-> 756, 1150
1014 <-> 1014
1015 <-> 1281
1016 <-> 1720
1017 <-> 446
1018 <-> 124, 430
1019 <-> 1341
1020 <-> 1369, 1666
1021 <-> 789
1022 <-> 1022, 1102, 1675
1023 <-> 28, 1630
1024 <-> 634, 648
1025 <-> 293, 1263, 1317
1026 <-> 787, 890
1027 <-> 13, 850
1028 <-> 1334
1029 <-> 1029, 1147
1030 <-> 901
1031 <-> 775, 1674
1032 <-> 1720, 1757, 1764, 1892
1033 <-> 215, 866, 1189, 1930
1034 <-> 290, 1988
1035 <-> 500, 776
1036 <-> 116, 666, 1036, 1707
1037 <-> 946
1038 <-> 292, 658, 1932
1039 <-> 216
1040 <-> 1867
1041 <-> 792, 858
1042 <-> 985, 1969, 1998
1043 <-> 1069
1044 <-> 227
1045 <-> 1045, 1404
1046 <-> 122, 491, 1046
1047 <-> 760, 1309
1048 <-> 560, 1316, 1370
1049 <-> 133, 1936
1050 <-> 786, 938, 1050
1051 <-> 1371, 1533
1052 <-> 120, 1106, 1901
1053 <-> 1489, 1675
1054 <-> 1075, 1905
1055 <-> 1543
1056 <-> 163, 1056
1057 <-> 1525, 1635, 1870
1058 <-> 1545
1059 <-> 109, 420, 724, 1781
1060 <-> 126, 1308
1061 <-> 552, 1065, 1543, 1594
1062 <-> 259, 392, 645, 1414
1063 <-> 113, 1520, 1976
1064 <-> 698, 1254
1065 <-> 402, 1061, 1496
1066 <-> 249, 754, 1546
1067 <-> 115, 508
1068 <-> 618
1069 <-> 1043, 1240, 1461, 1632
1070 <-> 1266, 1904
1071 <-> 532, 1514
1072 <-> 327, 980, 1985
1073 <-> 57
1074 <-> 1833
1075 <-> 29, 44, 80, 1054
1076 <-> 143, 1778, 1940
1077 <-> 911
1078 <-> 591, 893, 1629
1079 <-> 376, 601, 815, 1840
1080 <-> 1144, 1295, 1330, 1765
1081 <-> 89
1082 <-> 396, 870, 969, 1498
1083 <-> 295, 707, 904
1084 <-> 1084
1085 <-> 278
1086 <-> 1659, 1661
1087 <-> 425, 532
1088 <-> 170
1089 <-> 81, 1089, 1219, 1913
1090 <-> 147, 1099
1091 <-> 609, 1579, 1779
1092 <-> 285
1093 <-> 298, 1520
1094 <-> 940
1095 <-> 947
1096 <-> 1096
1097 <-> 260
1098 <-> 126, 1682
1099 <-> 1090
1100 <-> 735
1101 <-> 1848
1102 <-> 213, 943, 1022
1103 <-> 131
1104 <-> 1783
1105 <-> 665
1106 <-> 1052, 1106
1107 <-> 792
1108 <-> 56
1109 <-> 586
1110 <-> 356, 1259
1111 <-> 1133
1112 <-> 830, 1820
1113 <-> 954
1114 <-> 1114, 1475
1115 <-> 1115
1116 <-> 516
1117 <-> 243, 735
1118 <-> 975, 1689
1119 <-> 1235, 1337, 1474, 1741, 1994
1120 <-> 636
1121 <-> 427
1122 <-> 621, 957
1123 <-> 667, 1123
1124 <-> 587, 851, 1250, 1953
1125 <-> 23, 32, 40, 328
1126 <-> 849, 1429, 1676
1127 <-> 123, 1998
1128 <-> 544, 989
1129 <-> 727, 761, 1009, 1691
1130 <-> 697
1131 <-> 1436, 1719
1132 <-> 741, 1278
1133 <-> 556, 861, 1111
1134 <-> 1795, 1956
1135 <-> 68, 818
1136 <-> 185
1137 <-> 31, 482, 1466
1138 <-> 148, 1953
1139 <-> 367, 529, 1154
1140 <-> 278, 353, 401, 1608
1141 <-> 1141
1142 <-> 1690, 1809
1143 <-> 46, 1495
1144 <-> 1080, 1580
1145 <-> 313, 1772
1146 <-> 97
1147 <-> 965, 1029, 1648
1148 <-> 1007
1149 <-> 33
1150 <-> 1013, 1417, 1864
1151 <-> 871, 1990
1152 <-> 1229, 1572
1153 <-> 159, 1228
1154 <-> 1139
1155 <-> 325, 1829
1156 <-> 1650
1157 <-> 1489
1158 <-> 1472
1159 <-> 1005
1160 <-> 1467
1161 <-> 51
1162 <-> 1254
1163 <-> 1331
1164 <-> 1895
1165 <-> 1165, 1214, 1345
1166 <-> 1254
1167 <-> 538
1168 <-> 217, 1223, 1818
1169 <-> 1169, 1514, 1761
1170 <-> 1948
1171 <-> 645
1172 <-> 331, 905
1173 <-> 408, 1535
1174 <-> 1203, 1238, 1472
1175 <-> 182, 1817
1176 <-> 980
1177 <-> 1307, 1463
1178 <-> 583, 894, 1800, 1971
1179 <-> 745, 1259
1180 <-> 736, 745
1181 <-> 524, 880
1182 <-> 270, 1216, 1878
1183 <-> 390
1184 <-> 324, 1012, 1356
1185 <-> 249
1186 <-> 789
1187 <-> 64, 296, 448, 1377, 1488
1188 <-> 136, 1320
1189 <-> 1033, 1823
1190 <-> 225, 256, 471
1191 <-> 132, 286, 1802
1192 <-> 459
1193 <-> 154, 1193
1194 <-> 793, 923, 1479
1195 <-> 38, 1766
1196 <-> 1196, 1470
1197 <-> 766
1198 <-> 194, 1252
1199 <-> 1199
1200 <-> 140, 1200
1201 <-> 814, 1802
1202 <-> 304, 1865
1203 <-> 1174
1204 <-> 1583
1205 <-> 57, 211, 867, 1331, 1784, 1819
1206 <-> 1286
1207 <-> 710
1208 <-> 498
1209 <-> 160, 1209
1210 <-> 1210, 1599
1211 <-> 14, 281, 1278
1212 <-> 1289, 1644
1213 <-> 443, 1714, 1863
1214 <-> 1165
1215 <-> 1215
1216 <-> 1182, 1436
1217 <-> 948, 1377
1218 <-> 493, 495, 1006
1219 <-> 1089
1220 <-> 564
1221 <-> 555, 844, 984
1222 <-> 557
1223 <-> 150, 1168
1224 <-> 9
1225 <-> 1323
1226 <-> 242
1227 <-> 876, 1314
1228 <-> 150, 582, 1153, 1918
1229 <-> 991, 1152
1230 <-> 473, 503, 1444, 1945
1231 <-> 54, 584, 1713
1232 <-> 575, 914, 1232
1233 <-> 79, 142, 1776
1234 <-> 282, 604
1235 <-> 1119
1236 <-> 785, 1236
1237 <-> 587, 1751
1238 <-> 827, 1174, 1417
1239 <-> 150, 1597, 1852
1240 <-> 1069, 1769
1241 <-> 330, 439, 927
1242 <-> 1242
1243 <-> 784, 1541
1244 <-> 11, 638, 979
1245 <-> 1245
1246 <-> 237, 1330
1247 <-> 1005, 1598
1248 <-> 1573
1249 <-> 209, 1522
1250 <-> 1124
1251 <-> 63
1252 <-> 1198
1253 <-> 456, 474
1254 <-> 462, 918, 1064, 1162, 1166
1255 <-> 862
1256 <-> 596, 747
1257 <-> 449, 1010, 1568, 1837
1258 <-> 1803
1259 <-> 1110, 1179
1260 <-> 675
1261 <-> 1005, 1753
1262 <-> 376, 1318
1263 <-> 1025, 1887
1264 <-> 343
1265 <-> 717
1266 <-> 1070
1267 <-> 987, 1368
1268 <-> 1268
1269 <-> 118, 699, 992, 1939
1270 <-> 157, 978
1271 <-> 219
1272 <-> 1858
1273 <-> 218
1274 <-> 905
1275 <-> 407, 494
1276 <-> 26, 929
1277 <-> 1919, 1982
1278 <-> 998, 1132, 1211
1279 <-> 138, 328, 1852, 1912
1280 <-> 960
1281 <-> 102, 113, 388, 1015, 1877
1282 <-> 444
1283 <-> 372, 1562
1284 <-> 919
1285 <-> 262
1286 <-> 127, 1206, 1286, 1875
1287 <-> 269
1288 <-> 860, 1736
1289 <-> 1212, 1321, 1424
1290 <-> 1319, 1884, 1959, 1978
1291 <-> 1291, 1998
1292 <-> 55
1293 <-> 273, 1995
1294 <-> 227, 349, 561
1295 <-> 277, 1080, 1570
1296 <-> 921, 1714
1297 <-> 1297
1298 <-> 1518, 1733
1299 <-> 357, 392
1300 <-> 319
1301 <-> 1301
1302 <-> 1912
1303 <-> 843
1304 <-> 741
1305 <-> 814, 1828
1306 <-> 301, 1610
1307 <-> 190, 1177
1308 <-> 1060
1309 <-> 319, 389, 1047
1310 <-> 605
1311 <-> 1463
1312 <-> 578, 734
1313 <-> 629, 666
1314 <-> 1227, 1980
1315 <-> 684, 1472
1316 <-> 898, 1048
1317 <-> 1025
1318 <-> 520, 1262
1319 <-> 1290
1320 <-> 1188
1321 <-> 1289, 1737
1322 <-> 705, 1501
1323 <-> 1225, 1323
1324 <-> 1720
1325 <-> 1837
1326 <-> 890
1327 <-> 1470, 1914
1328 <-> 560
1329 <-> 763, 894
1330 <-> 1080, 1246
1331 <-> 1163, 1205
1332 <-> 1332
1333 <-> 703
1334 <-> 144, 410, 1028, 1854, 1871, 1925
1335 <-> 246, 910, 1807
1336 <-> 483, 505
1337 <-> 255, 477, 1119
1338 <-> 1455, 1810
1339 <-> 354, 1845
1340 <-> 623, 1627
1341 <-> 62, 1019, 1841
1342 <-> 798
1343 <-> 1343
1344 <-> 982, 1344
1345 <-> 1165, 1369
1346 <-> 1410, 1643
1347 <-> 357
1348 <-> 1623
1349 <-> 235, 1991
1350 <-> 1698
1351 <-> 1669
1352 <-> 1352
1353 <-> 571, 1519, 1657
1354 <-> 80, 752
1355 <-> 635, 981
1356 <-> 1184
1357 <-> 1679
1358 <-> 874, 1838
1359 <-> 661
1360 <-> 565, 1555
1361 <-> 134
1362 <-> 477, 626
1363 <-> 1459
1364 <-> 566, 1791
1365 <-> 20, 944, 1465
1366 <-> 295, 445
1367 <-> 782, 866
1368 <-> 1267
1369 <-> 976, 1020, 1345
1370 <-> 1048
1371 <-> 223, 414, 1051
1372 <-> 323, 829, 907
1373 <-> 1373
1374 <-> 503
1375 <-> 732, 1375
1376 <-> 1376
1377 <-> 271, 431, 1187, 1217
1378 <-> 225
1379 <-> 307, 1379, 1507
1380 <-> 784, 1870
1381 <-> 1381, 1569
1382 <-> 708, 1839
1383 <-> 959, 1586
1384 <-> 733, 1716
1385 <-> 1385
1386 <-> 1386
1387 <-> 101
1388 <-> 385, 1777
1389 <-> 152
1390 <-> 134, 1669
1391 <-> 1924
1392 <-> 245, 919
1393 <-> 1904
1394 <-> 10
1395 <-> 1582
1396 <-> 495, 507, 1622, 1902
1397 <-> 810, 823, 842
1398 <-> 973
1399 <-> 438, 1670, 1671
1400 <-> 1400
1401 <-> 195, 1649
1402 <-> 1402
1403 <-> 1603
1404 <-> 1045
1405 <-> 642
1406 <-> 1562, 1668
1407 <-> 1606
1408 <-> 1408
1409 <-> 486, 613
1410 <-> 59, 713, 813, 1346
1411 <-> 1666
1412 <-> 435
1413 <-> 786, 1709
1414 <-> 53, 1062, 1600
1415 <-> 764, 1558
1416 <-> 35, 934
1417 <-> 1150, 1238
1418 <-> 518
1419 <-> 567
1420 <-> 1858
1421 <-> 294
1422 <-> 177
1423 <-> 1668
1424 <-> 1289
1425 <-> 77
1426 <-> 1870, 1943
1427 <-> 770
1428 <-> 1428
1429 <-> 1126
1430 <-> 609, 841, 939, 1826
1431 <-> 21
1432 <-> 1975
1433 <-> 131
1434 <-> 845, 1487
1435 <-> 268, 1597
1436 <-> 695, 1131, 1216
1437 <-> 16, 1910
1438 <-> 523, 660, 1438
1439 <-> 241, 314
1440 <-> 1714
1441 <-> 241, 510, 993
1442 <-> 537
1443 <-> 815
1444 <-> 1230
1445 <-> 496, 633, 808
1446 <-> 167, 643, 1526
1447 <-> 1848
1448 <-> 829, 1709
1449 <-> 660
1450 <-> 229
1451 <-> 1481, 1527
1452 <-> 1452
1453 <-> 607
1454 <-> 398, 783
1455 <-> 1338, 1618
1456 <-> 459
1457 <-> 482, 692, 768, 1512
1458 <-> 1752
1459 <-> 728, 828, 1363
1460 <-> 1625
1461 <-> 28, 158, 1069, 1926
1462 <-> 1462
1463 <-> 370, 1177, 1311
1464 <-> 1935
1465 <-> 859, 1365
1466 <-> 1137
1467 <-> 129, 1160
1468 <-> 826
1469 <-> 1524
1470 <-> 1196, 1327
1471 <-> 1799, 1993
1472 <-> 1158, 1174, 1315, 1472, 1502, 1981
1473 <-> 642
1474 <-> 1119
1475 <-> 1114, 1513
1476 <-> 16, 1598
1477 <-> 1477
1478 <-> 795
1479 <-> 1194
1480 <-> 920
1481 <-> 264, 1451, 1504, 1952
1482 <-> 1795
1483 <-> 1984
1484 <-> 862, 1629
1485 <-> 908
1486 <-> 222
1487 <-> 1434
1488 <-> 1187
1489 <-> 9, 809, 1053, 1157, 1490
1490 <-> 1489
1491 <-> 1872
1492 <-> 722
1493 <-> 177
1494 <-> 1824
1495 <-> 75, 717, 970, 1143
1496 <-> 1065
1497 <-> 511, 803
1498 <-> 752, 883, 1082
1499 <-> 1646
1500 <-> 435, 843
1501 <-> 1322, 1501, 1753
1502 <-> 98, 1472
1503 <-> 564
1504 <-> 190, 579, 1481
1505 <-> 1527
1506 <-> 1939
1507 <-> 88, 905, 1379
1508 <-> 222, 398, 762
1509 <-> 170, 962, 1509
1510 <-> 381, 417, 839
1511 <-> 448
1512 <-> 838, 1457
1513 <-> 1475
1514 <-> 469, 602, 1071, 1169
1515 <-> 1766
1516 <-> 582
1517 <-> 78, 1555
1518 <-> 1298
1519 <-> 1353, 1918
1520 <-> 1063, 1093
1521 <-> 89
1522 <-> 1249
1523 <-> 1566
1524 <-> 19, 1469
1525 <-> 2, 269, 481, 1057
1526 <-> 245, 1446
1527 <-> 137, 1451, 1505, 1948
1528 <-> 626, 748, 1771
1529 <-> 1618
1530 <-> 1530
1531 <-> 400, 644
1532 <-> 387
1533 <-> 1051, 1725
1534 <-> 1658, 1696
1535 <-> 420, 1173, 1867
1536 <-> 851
1537 <-> 139
1538 <-> 664
1539 <-> 19, 695
1540 <-> 338
1541 <-> 359, 662, 1243
1542 <-> 4, 139
1543 <-> 0, 455, 1055, 1061, 1604
1544 <-> 854, 1619
1545 <-> 367, 537, 1058
1546 <-> 1066
1547 <-> 25
1548 <-> 581, 1917
1549 <-> 453, 672
1550 <-> 40, 759
1551 <-> 852
1552 <-> 524
1553 <-> 482
1554 <-> 91
1555 <-> 1360, 1517, 1833
1556 <-> 1869
1557 <-> 687
1558 <-> 1415
1559 <-> 1559, 1773
1560 <-> 1693
1561 <-> 25
1562 <-> 1283, 1406
1563 <-> 816, 1563, 1768
1564 <-> 553, 672, 1892
1565 <-> 57, 1665
1566 <-> 98, 1523, 1907
1567 <-> 776
1568 <-> 1257, 1998
1569 <-> 1381
1570 <-> 1295
1571 <-> 752, 1827
1572 <-> 248, 315, 1152
1573 <-> 789, 1248, 1772
1574 <-> 427, 541, 917, 1796, 1882
1575 <-> 135, 515
1576 <-> 1957
1577 <-> 944
1578 <-> 407, 800
1579 <-> 1091
1580 <-> 1144
1581 <-> 195
1582 <-> 65, 1395, 1582
1583 <-> 704, 1204
1584 <-> 1584
1585 <-> 403, 1889
1586 <-> 1383
1587 <-> 395, 1587
1588 <-> 1983
1589 <-> 754
1590 <-> 384, 498, 638
1591 <-> 794
1592 <-> 886, 1608
1593 <-> 249, 916
1594 <-> 1061
1595 <-> 645, 766
1596 <-> 89, 153
1597 <-> 56, 1239, 1435
1598 <-> 50, 1247, 1476
1599 <-> 1210
1600 <-> 1414
1601 <-> 199, 849
1602 <-> 758
1603 <-> 514, 1403, 1792
1604 <-> 1543, 1979
1605 <-> 30, 434, 478, 1961
1606 <-> 772, 1407
1607 <-> 1607, 1653
1608 <-> 1140, 1592
1609 <-> 783
1610 <-> 171, 1306, 1744
1611 <-> 473
1612 <-> 794, 1859
1613 <-> 459, 787, 1613
1614 <-> 319, 817, 1616, 1924
1615 <-> 178, 742, 914
1616 <-> 346, 1614
1617 <-> 74, 110, 200, 730
1618 <-> 296, 801, 1455, 1529
1619 <-> 50, 1544
1620 <-> 260
1621 <-> 520, 910, 1748
1622 <-> 172, 187, 369, 1396
1623 <-> 1348, 1623
1624 <-> 21
1625 <-> 1460, 1625, 1936, 1963
1626 <-> 96
1627 <-> 1340, 1677
1628 <-> 1628, 1728
1629 <-> 1078, 1484
1630 <-> 1023
1631 <-> 308
1632 <-> 1069
1633 <-> 1669, 1935
1634 <-> 187, 516
1635 <-> 986, 1057
1636 <-> 547, 1762
1637 <-> 1637
1638 <-> 317
1639 <-> 664, 717
1640 <-> 126
1641 <-> 36, 262, 433
1642 <-> 569, 942
1643 <-> 539, 1346, 1745
1644 <-> 416, 624, 1212, 1931
1645 <-> 929
1646 <-> 586, 1499, 1687
1647 <-> 242, 781, 1723
1648 <-> 1147, 1947
1649 <-> 1401
1650 <-> 630, 722, 1156, 1869
1651 <-> 1943
1652 <-> 796
1653 <-> 1607
1654 <-> 465, 1970
1655 <-> 1987
1656 <-> 1656
1657 <-> 1353
1658 <-> 373, 1534
1659 <-> 1086
1660 <-> 348, 1660
1661 <-> 242, 611, 1086
1662 <-> 692
1663 <-> 712
1664 <-> 257
1665 <-> 1565
1666 <-> 1020, 1411, 1887, 1967
1667 <-> 261
1668 <-> 1406, 1423
1669 <-> 429, 1351, 1390, 1633, 1938
1670 <-> 1399
1671 <-> 265, 1399
1672 <-> 152, 1684, 1707
1673 <-> 205
1674 <-> 877, 1031, 1991
1675 <-> 641, 1022, 1053, 1920
1676 <-> 1126
1677 <-> 82, 646, 1627
1678 <-> 286, 997
1679 <-> 208, 573, 1357
1680 <-> 169, 741
1681 <-> 586, 663, 1811
1682 <-> 1, 1098
1683 <-> 203, 856, 1817
1684 <-> 1672, 1927
1685 <-> 182, 1950
1686 <-> 749
1687 <-> 174, 1646
1688 <-> 913
1689 <-> 1118
1690 <-> 839, 1142
1691 <-> 648, 1129
1692 <-> 937
1693 <-> 320, 897, 995, 1560, 1716
1694 <-> 352, 457
1695 <-> 462
1696 <-> 165, 826, 1534
1697 <-> 1877
1698 <-> 172, 941, 1350
1699 <-> 572
1700 <-> 308
1701 <-> 394, 1843
1702 <-> 642, 695, 1861
1703 <-> 780, 1909
1704 <-> 436
1705 <-> 1906
1706 <-> 455
1707 <-> 1036, 1672, 1854
1708 <-> 1708
1709 <-> 1413, 1448
1710 <-> 1822
1711 <-> 473, 959
1712 <-> 921, 991
1713 <-> 1231
1714 <-> 509, 1213, 1296, 1440, 1999
1715 <-> 651
1716 <-> 1384, 1693
1717 <-> 101
1718 <-> 178
1719 <-> 1131
1720 <-> 1016, 1032, 1324
1721 <-> 210
1722 <-> 322
1723 <-> 1647
1724 <-> 1724, 1738
1725 <-> 1533, 1743
1726 <-> 724
1727 <-> 852
1728 <-> 1628
1729 <-> 582
1730 <-> 1733, 1935
1731 <-> 257, 704, 1989
1732 <-> 801
1733 <-> 775, 1298, 1730, 1762
1734 <-> 365
1735 <-> 60, 290
1736 <-> 661, 911, 1288, 1749
1737 <-> 1321
1738 <-> 236, 1724
1739 <-> 28
1740 <-> 31
1741 <-> 360, 966, 1119
1742 <-> 68
1743 <-> 649, 1725
1744 <-> 790, 1610
1745 <-> 1643
1746 <-> 883
1747 <-> 820
1748 <-> 1621
1749 <-> 1736
1750 <-> 1750
1751 <-> 646, 1237
1752 <-> 407, 1458
1753 <-> 1261, 1501
1754 <-> 247
1755 <-> 457, 1855
1756 <-> 32, 551, 589
1757 <-> 1032
1758 <-> 113
1759 <-> 1848
1760 <-> 26
1761 <-> 561, 1169
1762 <-> 1636, 1733
1763 <-> 563
1764 <-> 1032
1765 <-> 254, 1080
1766 <-> 743, 1195, 1515
1767 <-> 527
1768 <-> 1563
1769 <-> 1240
1770 <-> 631, 710
1771 <-> 1528
1772 <-> 1145, 1573
1773 <-> 1559
1774 <-> 374
1775 <-> 250
1776 <-> 200, 1233
1777 <-> 652, 1388
1778 <-> 635, 1076
1779 <-> 303, 1091
1780 <-> 35, 594
1781 <-> 1059
1782 <-> 350, 1964
1783 <-> 1104, 1837, 1879
1784 <-> 1205
1785 <-> 783
1786 <-> 811, 879, 1786, 1847
1787 <-> 874, 1829
1788 <-> 484, 963
1789 <-> 1858
1790 <-> 33, 341
1791 <-> 1364, 1791
1792 <-> 1603, 1792, 1868
1793 <-> 549
1794 <-> 508
1795 <-> 1134, 1482
1796 <-> 960, 1574
1797 <-> 145
1798 <-> 1798
1799 <-> 1471
1800 <-> 1178
1801 <-> 686
1802 <-> 1191, 1201
1803 <-> 214, 1258, 1819
1804 <-> 1819
1805 <-> 237
1806 <-> 1973
1807 <-> 699, 1335
1808 <-> 668, 1898
1809 <-> 1142, 1987
1810 <-> 192, 480, 506, 964, 1338
1811 <-> 1681
1812 <-> 697
1813 <-> 617, 1910
1814 <-> 327
1815 <-> 1953
1816 <-> 176
1817 <-> 1175, 1683
1818 <-> 850, 1168
1819 <-> 685, 1205, 1803, 1804
1820 <-> 654, 1112
1821 <-> 250
1822 <-> 1710, 1957
1823 <-> 697, 772, 932, 1189
1824 <-> 1494, 1848
1825 <-> 206, 614
1826 <-> 1430
1827 <-> 364, 721, 1571
1828 <-> 1305
1829 <-> 386, 918, 1155, 1787
1830 <-> 568
1831 <-> 215, 243, 711
1832 <-> 415, 1832
1833 <-> 1074, 1555
1834 <-> 1834
1835 <-> 1835
1836 <-> 289, 773
1837 <-> 1257, 1325, 1783
1838 <-> 676, 1358
1839 <-> 437, 1382, 1872
1840 <-> 1079
1841 <-> 375, 1341
1842 <-> 247, 1850
1843 <-> 1701
1844 <-> 879
1845 <-> 533, 843, 1339
1846 <-> 1870
1847 <-> 1786, 1972
1848 <-> 1101, 1447, 1759, 1824, 1848, 1873
1849 <-> 631
1850 <-> 681, 1842
1851 <-> 1851
1852 <-> 1239, 1279
1853 <-> 28, 1997
1854 <-> 1334, 1707
1855 <-> 788, 1755
1856 <-> 515
1857 <-> 212, 325
1858 <-> 306, 480, 1272, 1420, 1789
1859 <-> 332, 1612
1860 <-> 1977
1861 <-> 1702
1862 <-> 518, 950, 974, 1862
1863 <-> 420, 599, 895, 1213
1864 <-> 1150
1865 <-> 45, 141, 1202
1866 <-> 1012
1867 <-> 489, 937, 1040, 1535
1868 <-> 931, 1792
1869 <-> 603, 1556, 1650
1870 <-> 1057, 1380, 1426, 1846
1871 <-> 1334
1872 <-> 1491, 1839, 1959
1873 <-> 1848
1874 <-> 284, 387, 485
1875 <-> 1286
1876 <-> 1876, 1908
1877 <-> 72, 84, 1281, 1697
1878 <-> 906, 1182
1879 <-> 1783
1880 <-> 234, 491, 683
1881 <-> 41, 556
1882 <-> 332, 1574, 1882
1883 <-> 1883
1884 <-> 1290
1885 <-> 853
1886 <-> 95, 644, 934
1887 <-> 1263, 1666
1888 <-> 1888
1889 <-> 855, 915, 1585
1890 <-> 799
1891 <-> 990, 1896
1892 <-> 1032, 1564
1893 <-> 267, 581
1894 <-> 205, 953
1895 <-> 1164, 1895
1896 <-> 1891
1897 <-> 963
1898 <-> 1808, 1915
1899 <-> 93, 689, 850
1900 <-> 28, 168
1901 <-> 1052
1902 <-> 1396, 1902
1903 <-> 695
1904 <-> 69, 115, 195, 836, 1070, 1393
1905 <-> 100, 247, 1054
1906 <-> 1705, 1971
1907 <-> 1566, 1934
1908 <-> 765, 1876
1909 <-> 651, 1703
1910 <-> 1437, 1813
1911 <-> 472, 488, 594, 734
1912 <-> 1279, 1302
1913 <-> 834, 1089
1914 <-> 1327
1915 <-> 1898
1916 <-> 547
1917 <-> 1548
1918 <-> 1228, 1519
1919 <-> 55, 1277
1920 <-> 461, 1675
1921 <-> 674
1922 <-> 1001, 1922
1923 <-> 671, 908, 1923
1924 <-> 1391, 1614, 1958
1925 <-> 1334
1926 <-> 54, 625, 1461
1927 <-> 559, 935, 1684
1928 <-> 287, 600, 780
1929 <-> 107, 534
1930 <-> 139, 889, 1033
1931 <-> 924, 1644
1932 <-> 1038
1933 <-> 1933
1934 <-> 1907
1935 <-> 712, 1464, 1633, 1730
1936 <-> 1049, 1625
1937 <-> 620, 1012
1938 <-> 1669, 1938
1939 <-> 1269, 1506
1940 <-> 229, 918, 1076
1941 <-> 19, 181
1942 <-> 67
1943 <-> 1426, 1651
1944 <-> 218, 391
1945 <-> 1230
1946 <-> 613
1947 <-> 983, 1648
1948 <-> 1170, 1527
1949 <-> 91
1950 <-> 1685
1951 <-> 52
1952 <-> 1481, 1952
1953 <-> 674, 1124, 1138, 1815
1954 <-> 12
1955 <-> 734
1956 <-> 1134, 1956
1957 <-> 530, 1576, 1822
1958 <-> 90, 632, 888, 1924
1959 <-> 1290, 1872
1960 <-> 848, 949
1961 <-> 130, 1605
1962 <-> 289
1963 <-> 1625
1964 <-> 1782
1965 <-> 7
1966 <-> 685
1967 <-> 39, 723, 1666
1968 <-> 525
1969 <-> 1042
1970 <-> 579, 977, 1654
1971 <-> 1178, 1906
1972 <-> 1847
1973 <-> 645, 1806
1974 <-> 350, 446
1975 <-> 259, 1432
1976 <-> 1063
1977 <-> 236, 1860
1978 <-> 954, 1290
1979 <-> 967, 1604
1980 <-> 940, 1314
1981 <-> 1472
1982 <-> 1277
1983 <-> 217, 540, 1588
1984 <-> 560, 892, 1483
1985 <-> 327, 1072
1986 <-> 581
1987 <-> 437, 1655, 1809
1988 <-> 954, 1034
1989 <-> 532, 1003, 1731
1990 <-> 300, 1151
1991 <-> 1349, 1674
1992 <-> 614
1993 <-> 87, 581, 1471
1994 <-> 778, 1119
1995 <-> 1293
1996 <-> 198, 323
1997 <-> 1853
1998 <-> 1042, 1127, 1291, 1568
1999 <-> 433, 1714")

(def day13
  "0: 3
1: 2
2: 4
4: 6
6: 5
8: 6
10: 6
12: 4
14: 8
16: 8
18: 9
20: 8
22: 6
24: 14
26: 12
28: 10
30: 12
32: 8
34: 10
36: 8
38: 8
40: 12
42: 12
44: 12
46: 12
48: 14
52: 14
54: 12
56: 12
58: 12
60: 12
62: 14
64: 14
66: 14
68: 14
70: 14
72: 14
80: 18
82: 14
84: 20
86: 14
90: 17
96: 20
98: 24")

(def day16
  "x5/11,pj/i,x0/4,pa/f,x9/14,pk/h,x11/1,s13,x15/14,pa/o,x2/9,s4,x1/7,s5,x4/15,s1,x14/0,ph/e,x6/11,s7,pj/k,x4/14,s1,x15/7,s7,x4/9,s13,x11/2,pi/e,x13/8,s7,x10/4,pp/g,x7/11,pd/n,x14/0,pk/g,x5/15,s14,x8/6,s8,x7/0,s13,x8/10,s14,x5/2,pb/j,s3,x3/12,s4,x0/1,s8,x3/12,s15,x15/13,pg/f,s1,x7/0,s6,x5/12,pa/h,s12,x9/2,s12,x10/15,s13,x4/5,pi/k,x8/10,s12,x12/7,pp/l,x14/9,s6,x2/1,s12,pc/d,x7/5,s9,pg/b,s14,x4/8,pe/l,x5/12,pa/i,x11/13,s9,x1/6,pj/k,x5/7,s4,x0/4,s2,x10/2,pe/h,x4/11,pn/m,x8/9,s1,x14/13,s15,x4/7,s3,x14/5,pe/j,x13/12,s7,pm/d,x5/2,s9,x14/10,s12,x4/7,pn/b,x13/10,s7,x12/9,s9,x3/7,s11,x6/1,s1,x10/15,s3,x6/14,s7,x2/9,s4,x11/4,s13,x0/6,s15,x10/1,pd/g,x2/4,pf/c,x15/9,s12,x8/2,ph/a,x0/14,s8,x11/4,pk/b,x3/2,s2,x0/8,pf/e,s4,x2/11,pa/g,x6/8,s11,x0/7,s4,x4/13,po/h,x1/11,pm/c,x9/13,s9,x4/14,s6,x10/0,s8,x1/14,s4,x2/15,s12,x14/3,s13,pb/a,s12,x13/11,s7,x15/6,pc/j,x2/8,pk/g,x1/6,pi/b,x11/14,ph/p,x6/12,s13,x15/11,pn/g,x8/4,pi/o,x7/11,s10,x5/9,s15,x3/4,s8,x15/0,s4,x6/11,pl/e,x15/10,s10,x2/8,s1,x15/0,s12,pa/o,x9/8,pi/f,s1,x6/5,s1,x3/9,pl/n,x4/8,pg/a,x10/12,s8,x7/1,pn/d,s8,x4/14,s2,x7/6,s10,x11/8,s9,x4/12,s7,x1/9,po/m,x11/13,s9,pc/d,x0/1,s4,x8/11,pi/e,x2/0,pg/a,x9/5,s7,x0/3,s9,x5/9,s5,x15/14,pb/c,x8/12,s11,x10/6,pl/h,x5/3,s9,x15/0,s5,x3/5,s9,x0/12,s9,x2/6,s5,x8/10,pj/c,s13,x12/11,pf/a,s7,pn/m,x9/1,s15,x4/6,pe/j,x15/3,s6,x0/12,s7,x2/7,pn/c,x9/11,s9,x15/1,pp/l,x0/14,s14,x5/8,s10,x3/14,s13,x9/7,s7,x6/11,s14,x8/12,pn/m,x7/4,ph/o,x14/11,s4,pc/k,x4/12,s7,x5/15,pd/e,x2/9,s11,x14/4,s2,x7/12,pm/f,x11/4,s1,x10/5,s11,x4/15,pp/l,x8/13,pj/n,x9/15,po/k,s2,x12/14,pm/a,x9/10,s3,x13/5,s13,x0/9,s3,x5/8,s13,pg/k,x13/2,s11,x0/6,s7,x9/14,s8,pj/i,x3/1,s15,x2/11,po/c,x7/5,pg/l,x13/9,pn/m,x2/7,s5,x11/1,s13,x8/2,pd/l,x14/13,s9,x2/4,s10,x12/6,pm/b,x14/13,s4,x6/3,s10,x10/8,pd/h,x0/5,s6,x6/13,pi/e,x11/14,pg/c,x3/1,s9,x14/2,s1,x0/6,ph/j,s14,x15/9,s11,pg/a,x4/7,s13,pl/i,x9/3,s4,pj/h,x7/12,s5,x3/4,pg/c,x15/14,s10,x12/2,s13,pk/j,x13/1,pf/d,s14,x2/10,pi/j,s9,x11/8,s3,x10/4,s7,pc/f,x11/0,pp/d,x9/7,s15,pm/g,x6/12,s13,pj/f,s11,pi/c,x8/7,s10,pf/l,x0/13,pn/k,x15/9,s2,x11/14,po/m,x15/2,pf/c,x4/0,pn/e,s9,pg/j,x15/3,s12,x9/13,s3,x7/4,s15,x11/3,s15,x14/1,s8,x5/6,s14,x8/2,s6,pb/m,x7/10,s7,x14/9,s8,pn/j,x7/15,pl/d,x4/5,s9,x10/6,s4,x5/2,pg/j,x7/4,s11,x8/14,s7,x15/5,s1,x13/9,s15,x1/6,s3,x5/12,s12,x0/11,s3,x8/4,pl/m,x2/12,pf/j,x7/1,s11,x8/5,pp/g,x7/12,s4,x1/5,s10,x10/0,s3,x13/9,pk/h,x1/8,s5,x13/3,pg/d,x6/0,pb/m,x10/1,pk/j,x11/14,s7,x12/1,s10,x2/4,s12,x7/3,pn/b,x1/2,pj/h,x5/13,pi/g,x10/9,s15,x11/3,s7,x15/5,s5,x9/4,s8,pb/l,x8/5,s9,x6/0,s2,x9/11,s7,x3/12,s1,x1/11,pe/m,x4/12,s11,x15/13,pg/p,x4/8,s7,x1/7,pb/j,x14/10,s3,x15/6,pi/l,x0/4,pm/o,s7,x13/2,pl/n,x1/9,s4,x11/3,po/d,x13/4,s14,x8/12,pb/j,x10/11,s11,x1/3,s15,pd/f,x15/7,s10,x13/9,s7,x10/0,pc/l,x15/4,s8,po/d,x9/12,s14,x14/13,s11,x0/9,s7,x7/15,s5,pm/e,x1/12,s14,x8/15,s6,x6/12,s5,x15/0,pi/g,x3/5,s7,x1/13,ph/c,x5/0,pp/g,x10/3,pe/j,x0/5,s5,x9/14,pm/g,x11/7,s13,x3/13,s11,x15/5,s5,x13/12,s10,x8/1,s8,x5/14,s15,pk/b,x12/6,pi/p,x2/7,s6,x4/1,pm/e,x8/5,s2,x10/11,pb/p,x9/8,s15,x11/2,pg/h,x3/12,pm/d,x4/5,s13,x9/13,pa/i,x3/15,s7,x8/11,pb/p,x5/10,s10,ph/d,x13/12,pa/o,x11/6,s13,x0/10,s8,x1/11,s8,x7/2,s3,pl/h,x1/15,s11,x3/5,s8,x6/4,pn/a,x7/1,pc/g,s8,x9/11,s11,x13/0,pk/a,x4/10,s15,x3/11,pb/m,x12/8,s1,x4/3,s6,pi/e,x14/13,pl/n,x8/11,s9,x14/0,pm/o,x6/15,pc/l,x8/0,s4,x10/3,s10,x7/0,s14,x11/10,pb/n,x8/2,pp/d,x13/14,s7,x2/1,s7,x13/8,s8,pj/a,s12,x5/0,s11,pf/o,s6,x4/8,s9,x6/13,s7,pe/l,x4/1,pb/m,x5/9,s1,x4/15,s7,x11/9,pk/i,s8,x7/13,s12,x5/3,pn/o,x13/4,pj/e,x8/0,s6,x7/4,s13,x0/11,pm/p,x2/4,s12,x9/11,s8,x4/6,s4,x0/9,s7,x8/14,pf/k,s6,x10/12,pi/n,x5/0,pa/f,x15/11,pd/l,x2/1,pn/p,x4/9,s3,x6/15,s9,x14/10,pl/a,x2/12,s15,x13/3,s6,x6/4,pp/g,x1/7,po/b,x0/6,pk/l,x3/2,pb/j,x5/7,s6,x10/0,s8,x13/12,pk/e,x3/2,s11,x9/8,s8,x15/10,s5,x3/1,s9,pa/i,x6/13,s9,x15/0,pg/e,x7/12,pi/a,x10/4,s13,pg/e,s13,x13/3,s14,x4/0,s4,x7/12,pk/o,x14/4,pb/c,x2/13,pe/i,s10,x9/11,pp/b,x5/1,s11,x11/15,pf/h,x8/10,po/a,s3,x7/15,s15,x4/6,pp/m,x11/15,ph/f,x7/8,pi/a,s10,x5/3,pb/e,x12/9,po/j,x8/14,s1,x7/9,s10,x3/8,s14,x5/6,s1,x7/10,s1,x11/9,pb/k,s4,x10/12,pj/c,x15/5,s1,x11/3,ph/k,s10,pp/c,x15/5,pb/m,s12,x14/3,pi/l,x11/9,s12,x3/2,s9,x13/5,s6,x0/3,s3,x15/5,pd/c,x4/3,s2,x5/9,pp/i,s5,x4/6,pg/j,x8/10,pp/a,x14/11,s6,x1/13,pl/c,x12/10,pj/o,s1,x15/1,s9,x9/7,pl/f,x1/13,s9,x12/15,s13,x11/0,s6,x15/1,pd/k,s15,x0/14,s2,x15/5,s9,x2/6,pg/a,s11,ph/k,x15/13,s4,x1/10,pi/p,x2/3,s2,x7/15,s14,x11/12,pd/m,x13/1,pc/o,x14/0,pl/g,s6,x3/2,s14,x11/12,pp/f,x4/10,s10,x0/12,s15,x6/11,s12,x2/13,pi/l,x4/12,s6,x1/3,s2,x12/0,s2,x9/3,pe/p,x0/5,pb/i,x14/9,pj/o,x7/13,s4,x3/14,pa/n,x2/9,pm/c,s14,x0/4,pg/i,x3/15,pd/m,s1,x6/11,pp/h,x15/9,s1,x10/4,s2,x3/12,pg/n,s12,x4/13,pj/o,x5/2,s9,x10/9,s7,x8/6,s15,x15/2,pf/g,x5/14,s14,x0/15,s10,x10/13,s4,x6/0,pb/j,x1/3,pl/e,x15/12,s9,x8/2,pg/n,s12,x5/10,pf/e,x14/15,s12,x9/11,pp/o,x15/2,s12,x11/5,s2,x14/8,s9,x15/1,pb/a,s15,x7/14,s2,x4/15,pm/e,x14/10,s14,x13/3,s15,x14/6,pg/o,x5/10,s13,x8/7,s7,x9/2,s10,x11/5,s3,x8/13,s3,x9/14,pf/n,x3/10,s5,x15/9,s13,x14/0,pj/p,x13/10,s13,x6/5,s4,x0/7,s7,x9/11,pd/o,x13/1,pp/a,x15/12,s13,x2/10,s6,x13/12,s14,x2/6,s1,x4/8,s3,x0/11,s11,x10/6,ph/e,s4,x4/5,s3,pg/p,x9/11,po/k,s4,x4/5,s10,x1/14,s13,x4/13,pa/e,x6/5,s2,x0/14,s11,x3/1,s12,ph/b,x2/9,s6,x12/4,pi/l,x9/6,s2,x7/14,s7,x5/10,pa/m,x7/1,s5,x9/10,pk/l,x11/0,pd/j,x8/4,pi/a,x1/0,s9,pb/l,x5/3,pc/h,x6/7,s12,x3/11,pe/l,x6/10,pp/i,x12/14,s14,x13/0,pk/a,x11/14,s3,x9/1,s5,pm/j,x12/14,s11,x3/4,po/c,x5/0,s8,x1/13,s15,pi/e,x5/14,s13,x9/3,s13,x1/6,s15,x0/15,s7,x6/8,s9,x5/3,pf/h,x14/8,pm/c,x6/9,pf/b,x2/7,s2,x5/15,pc/m,x12/11,s2,x9/15,ph/p,x1/4,s5,x7/0,s12,x8/12,pc/a,s5,x6/4,pl/g,x1/13,pm/c,x10/14,s12,x6/15,s2,x5/9,s2,x3/6,s5,x5/4,s14,x14/12,ph/n,x1/0,s4,x8/5,s7,x15/0,pd/b,x13/3,s5,x0/10,s9,x2/9,s9,x11/3,s13,x15/12,pl/n,x11/9,pa/d,x7/12,pp/h,x9/3,pf/c,x1/0,s14,x8/13,pa/m,x10/11,pe/g,x9/3,s12,x14/6,s1,x5/7,pm/k,x10/3,po/f,x15/9,pj/p,s6,x1/13,ph/b,x12/0,po/c,x15/5,s10,x0/11,s5,x2/5,s5,x6/4,s2,x10/9,s13,ph/a,x11/6,s13,x10/12,pb/f,x4/8,s10,x6/13,s11,x9/4,s1,x14/11,pk/n,x6/15,s9,x9/1,pg/l,x6/12,s9,x3/13,s15,x7/11,pb/o,s3,x6/2,s1,x14/1,s14,x7/10,s2,x14/13,pn/h,x8/15,pi/p,x2/3,s3,x4/5,pl/f,x1/8,s3,x14/12,s3,x9/0,pi/m,s4,x12/6,s9,x14/11,s10,x5/10,s15,x8/7,pf/p,x5/2,s12,x12/7,s2,x15/0,pd/o,x6/4,pn/i,x9/7,s3,x14/4,s7,x15/1,s9,ph/e,x10/11,pg/o,s10,x4/9,pk/d,x6/12,s10,pb/p,x15/1,s11,x12/3,pf/e,x1/8,s10,x15/11,s6,x4/12,s8,x3/1,s11,x4/9,s8,pg/b,x1/14,s7,x12/9,s8,x13/11,ph/k,s7,x14/3,pg/a,x15/9,s5,x4/8,pm/h,s7,x10/5,pe/o,x12/11,s1,x5/14,pi/l,s4,x3/4,pb/m,x10/11,pf/g,x3/4,pc/l,x5/13,s4,x8/12,s12,x14/4,s13,x7/2,s3,x0/5,s10,x7/4,pe/o,x14/11,pm/h,s2,x6/12,s9,x0/7,pj/i,s6,x9/2,s11,x5/7,s14,pn/l,x4/10,pp/a,x11/13,pf/h,x12/4,s14,x13/2,s11,x3/5,s2,x6/4,pi/j,x8/15,s7,x0/12,pd/k,x3/1,pi/l,x8/0,s10,x11/3,s7,x9/7,s2,x14/1,s11,pk/o,x8/9,s2,x1/10,s7,x6/8,pg/n,x9/7,pd/b,x13/15,pk/m,x9/1,pc/h,x12/13,s6,x8/2,s6,x1/10,s11,x14/12,s2,x1/8,s7,x5/13,s14,x9/11,s14,x6/4,pk/b,x5/12,pn/j,x2/4,s5,x13/7,s10,x0/14,s6,pg/o,x13/15,s1,x5/9,s5,x0/12,s4,x5/14,s10,x11/2,pa/c,s11,x15/7,ph/f,s1,x4/1,s7,x2/7,s9,pc/k,s9,x9/1,pl/g,x12/4,pk/h,x1/13,s8,x7/5,pa/g,x12/6,s12,x2/10,s15,x12/3,s3,x4/6,pb/p,x2/8,s13,pl/n,s9,x15/6,s10,x7/2,s11,x6/9,s1,x1/0,pd/f,x12/8,pi/j,x5/0,s15,x13/9,pn/l,x2/10,pb/g,x1/9,pl/m,x8/6,s11,x3/13,s5,x2/9,s11,x0/5,s14,pi/g,x9/3,s9,x2/15,s10,x12/4,pe/j,x9/3,ph/i,x11/14,s7,x4/13,s13,x2/5,s6,x13/9,pj/l,x4/12,s10,x6/14,s11,x15/12,pn/b,x11/3,s9,x10/9,pj/c,x4/15,s4,pd/g,x8/13,po/c,x2/14,s5,x10/0,pk/b,x6/11,po/c,s11,x0/8,pp/f,x9/11,pg/o,x10/7,s2,x2/3,s1,x5/4,s3,x14/13,s13,x2/7,s1,x3/5,s3,x1/2,s13,pj/f,x15/6,pn/c,x3/14,pp/e,x5/13,s4,x8/3,s5,x13/0,pf/j,x6/9,s15,x4/1,s8,x5/13,s8,x6/8,s12,x9/13,pm/e,x3/11,s12,x4/10,pa/c,x8/0,s5,x3/13,pl/i,x9/15,pj/b,x10/5,s1,x8/2,pe/o,x4/7,s5,x8/12,pd/c,s3,x11/4,s1,x7/3,pg/f,x5/11,ph/l,x1/14,s1,x13/6,s2,x1/4,pi/j,x0/7,pe/n,s6,x12/3,pc/i,x9/8,s6,x7/3,s12,x10/2,pa/e,x9/14,s11,x12/3,s1,pd/o,x11/2,s9,x10/5,pc/f,x15/3,s15,x11/10,s13,pj/i,x1/2,s1,x3/0,po/d,x2/15,s5,pj/e,x14/13,s13,x6/11,pb/m,s14,x15/7,s9,x10/8,s11,x15/4,s4,x0/9,s13,x2/6,pi/g,x15/5,pa/n,x2/13,s9,x10/14,s6,pj/l,x2/6,s8,x9/7,s5,x11/14,pg/d,x9/8,pe/o,x4/10,s9,x6/0,pf/d,s10,x2/4,ph/a,x13/3,pp/l,x4/14,pj/f,x12/1,pb/i,x3/6,pn/a,x11/8,pg/m,x2/10,s3,x15/5,pi/a,x3/14,s6,x5/7,pb/f,x14/13,s6,x5/9,s6,x12/0,s13,x6/1,s10,x2/5,pe/n,x10/15,pa/g,x3/2,s12,x13/6,s5,x0/14,s2,x12/3,s13,pm/d,x9/7,s5,x3/6,s3,x5/11,s7,x15/6,pk/p,x10/13,pl/m,x2/14,s9,x1/13,s14,x6/3,s12,x13/15,s3,x2/3,s12,x9/7,pn/g,s13,x11/12,po/k,x5/9,pb/i,s11,x12/4,s2,x15/0,s3,x14/13,s11,x8/6,s2,x10/7,s7,x12/14,s11,x3/11,s11,x6/0,pj/a,x9/3,s1,x4/10,s7,x7/15,pb/m,x0/2,s8,x14/13,s5,x8/4,pd/g,x13/2,s6,x15/7,pp/i,x2/4,s3,x14/8,po/k,x7/6,s3,x9/5,s10,x7/10,pp/g,x13/5,s4,x12/2,s4,x5/3,s3,x8/4,s1,x13/11,s10,ph/c,x9/15,s9,x0/3,s9,x15/9,pn/b,x6/7,pm/p,x13/8,pg/a,x11/10,pj/e,x12/9,pd/l,x11/3,s14,pa/p,x2/7,s7,x13/8,pl/e,x2/1,pd/g,x3/15,pc/l,s5,x13/7,s10,x14/4,s13,x0/12,s15,x14/15,s11,x2/9,pb/d,x4/10,pj/h,x5/8,pe/c,x2/6,pa/h,x5/7,s8,x0/11,s4,x10/14,pj/d,x8/6,pi/f,s15,x12/10,s13,x8/4,s14,x11/5,s11,pb/g,x8/1,s3,x10/13,s13,x7/14,pe/p,x0/8,pk/j,x7/10,s13,x12/5,s11,pn/e,x0/10,s1,x4/6,s13,x10/14,s8,x6/8,s2,x9/0,pi/c,x3/12,s15,x14/5,s5,x4/3,s7,x2/12,s7,x14/15,s6,x0/1,s7,x11/3,pl/n,x8/7,s7,x12/5,s13,x13/6,s1,x8/4,s4,x15/6,s2,x2/4,s13,x0/3,pg/h,x7/5,s8,x13/2,pi/n,x12/3,s7,pe/p,x8/10,pn/l,s2,x2/15,pc/i,s5,pk/d,s8,x5/0,po/m,x8/4,pp/e,x1/7,s13,x9/2,pj/a,x6/7,pi/d,x10/2,s5,x9/1,pl/a,x7/11,pd/m,s13,x1/10,s6,x15/12,s6,x14/2,pl/k,x6/8,s3,x4/14,pc/o,x15/3,s6,x9/7,s1,x14/3,s1,pd/e,x11/5,pk/c,x3/1,pf/n,x9/11,s6,x10/6,s14,x1/8,pi/d,x9/15,pe/f,x7/6,s6,x9/4,s4,x7/13,s1,pd/l,x8/0,s3,x2/11,pa/c,x1/13,pg/k,s3,x7/5,s14,x11/6,s7,x1/2,pj/l,x15/14,s8,x4/9,s15,x8/10,s1,x7/0,pn/m,x8/15,s8,x14/5,s1,pf/e,x9/12,pl/m,x2/10,s12,x13/12,s3,x2/3,ph/d,x15/14,pp/b,x3/8,s4,x10/7,pi/n,s15,pg/p,x12/8,pi/h,x5/7,s7,pf/p,x12/3,s14,x4/14,s1,x13/15,po/m,x7/5,s15,x12/15,pb/j,s4,x9/6,ph/f,s3,x13/5,s1,x12/6,s4,x5/8,pj/k,x1/0,pl/c,x13/8,s13,x11/14,pk/f,x9/12,s3,x13/11,s11,x7/6,s3,x8/14,pg/d,x5/11,s9,x7/3,s4,pc/n,x12/14,pl/f,x13/15,s13,x9/3,ph/b,x10/0,pk/g,x15/2,pi/a,x14/0,s9,x1/6,pb/g,x2/4,s4,x5/12,pi/m,x7/1,s14,x10/11,pn/a,x8/3,s13,x1/0,pc/f,x11/3,pl/h,x2/13,s9,x5/11,pj/d,x7/14,s15,x10/6,pe/a,x13/1,pb/k,x4/3,s2,x1/13,pe/h,s9,x0/12,s15,x10/7,pf/i,x4/2,s1,x11/1,po/p,x13/9,s5,x15/10,s4,x1/4,s9,x13/12,pg/k,x8/14,s13,x1/9,s3,x6/5,s7,x3/2,s10,x5/8,pi/d,x14/7,s5,x1/9,s13,x11/10,s3,x6/13,s12,x9/7,s6,x4/12,s8,pb/m,x14/13,s2,x9/6,s5,x10/5,s13,x7/6,s1,x0/1,s12,pk/p,s14,x15/10,s15,x12/7,pc/n,x11/6,pb/o,s8,x1/13,s9,x3/2,s8,x13/8,pp/e,s11,x5/11,pi/g,x1/6,s1,x9/11,pl/f,x5/12,s4,x4/0,pe/c,s3,x12/5,s10,x2/1,pp/n,x10/13,s9,x6/15,s5,x11/4,s11,x13/7,s9,x14/4,s15,x11/7,s4,x1/5,pd/i,x13/14,s2,x15/10,s3,x11/9,s13,x2/10,ph/o,x7/1,s15,x15/4,pa/i,x8/3,s4,x10/1,pm/l,x7/15,pe/h,x12/14,s3,x13/8,pi/k,x10/3,s8,pb/n,x13/7,s9,x15/5,s12,x0/2,pk/p,x14/13,po/j,x6/1,s15,x4/0,s14,x9/13,s14,x1/0,s8,x4/15,s14,x10/6,pd/h,x5/1,pa/g,x7/12,ph/k,x11/3,s1,x2/5,s12,x9/12,s15,x11/7,s13,x2/13,pf/d,x10/1,s4,pg/m,x4/2,pb/i,x6/10,s10,x9/11,ph/e,x1/7,pf/i,x4/0,s12,x6/9,pb/o,x5/4,s2,x9/15,pn/l,x1/11,s15,x15/8,s7,x3/4,s1,x0/5,s10,x2/13,s14,x0/7,pi/m,x14/15,s9,x12/7,pc/f,s2,po/a,s1,x5/6,s9,x2/7,s2,x3/5,pc/f,x9/1,s13,x8/13,pe/g,x3/0,s3,x1/10,s6,x15/13,pm/f,x10/1,pp/i,x6/7,s15,x9/10,pk/o,x7/15,pm/g,x1/9,s8,x2/14,s8,x5/6,s2,x11/13,pf/n,x7/12,s15,x1/9,pg/a,x4/2,s1,pb/m,x3/7,pe/a,x1/5,pg/l,x15/4,pn/p,x7/1,pm/l,s9,x3/9,pn/a,x6/2,po/f,x12/13,s10,x14/15,s10,x13/5,s6,x8/3,s6,pb/i,x5/9,s8,x4/7,s14,x8/10,s1,x12/5,pd/c,s1,pn/k,x6/0,s4,x10/1,ph/i,x3/7,s11,x15/6,pe/d,x12/8,s12,x6/14,pj/a,x4/0,s9,x14/12,s5,x5/13,pg/c,x1/4,s3,x0/3,s11,pi/p,x14/9,s15,x1/15,s9,x2/9,s8,x5/14,s5,x9/11,pl/m,x6/5,pe/j,s5,x0/9,s13,x4/15,s6,x10/13,pl/g,s7,x11/4,s4,pj/f,s7,pa/g,x6/8,s12,x7/5,s12,x2/8,pj/m,x9/4,pf/o,s4,x15/10,s6,x12/1,s4,x3/15,s1,pj/h,x14/5,pf/a,s12,x11/2,s7,x12/6,pb/k,x0/10,s6,x7/9,s10,x2/1,s10,x12/5,s14,x11/14,s3,x1/5,pd/h,x11/3,s1,x5/12,s15,x1/9,s6,x5/13,s10,x6/1,pe/l,x12/0,s10,pb/j,x8/3,s10,x14/15,s5,x1/4,s14,x11/9,pi/n,x0/13,s7,x8/2,s14,x15/5,pc/j,x4/10,s14,x6/8,pp/l,x1/15,s2,pd/g,s12,pi/l,x13/11,pf/h,x15/1,pd/j,s10,x8/9,pk/o,x2/6,pa/l,x13/8,po/d,s12,x0/12,pa/c,x4/7,pb/n,x9/6,s1,pk/e,x14/7,s6,pa/p,s13,x6/8,s10,x1/9,pk/l,x5/2,s7,x6/12,pa/d,s2,pl/f,x7/4,pg/n,s1,x13/15,s9,x8/0,s1,x4/10,pb/l,x12/1,s3,x8/15,s9,pg/d,x7/0,po/h,x4/12,pe/a,x0/3,s8,x14/1,s15,x5/6,pl/g,x11/9,s5,x7/12,pj/m,x15/2,pc/i,x1/7,po/g,x0/5,s14,x3/8,s14,x15/4,pm/e,x5/10,pk/d,x6/15,s1,x2/9,s15,x10/5,s4,x8/13,s3,x14/3,pi/o,x6/15,pk/h,x3/10,s2,x5/4,s13,x12/14,pd/l,s2,x11/9,pn/h,x14/7,pl/b,x2/4,s12,x1/5,s11,x12/6,s11,x15/7,s1,x12/0,s9,x4/10,s6,x6/15,s13,x1/14,s9,x11/5,s8,pg/a,s7,x10/6,s8,x0/11,s8,x15/12,pm/h,x10/13,s15,x1/0,pj/b,x13/10,s1,x1/3,s10,x11/15,s11,x14/13,s15,x9/7,pa/e,s15,x4/5,s3,x9/15,s9,x12/8,po/j,s2,x9/3,s10,x4/7,s5,pf/a,x13/15,pk/p,x5/8,pa/i,x14/11,pl/j,x12/7,pf/n,x11/9,s12,x7/2,s13,x4/12,ph/a,x2/13,s8,x5/10,s14,x13/9,pk/b,x3/10,s10,x1/13,s9,x4/12,pd/m,x1/6,pc/k,x8/9,pj/a,x13/14,s15,x9/0,pc/f,x12/4,s13,x13/11,pg/e,x7/6,pc/i,x5/0,pj/p,x6/7,pb/a,s2,x4/15,s4,x6/13,pg/h,x9/2,pa/m,x1/10,s9,x8/0,s13,x15/4,s13,x2/8,pk/j,x3/5,s11,pi/f,x15/14,s11,x11/12,po/a,s8,x4/2,s13,x7/6,s10,x3/5,s14,x4/9,s8,x11/10,s7,x4/7,pn/l,x0/3,po/h,x10/8,s1,x12/14,s9,x7/4,s12,x10/8,s6,pf/i,x14/1,s1,x12/2,s12,x9/14,pc/d,s10,x15/1,s8,x14/0,s2,x15/3,s4,po/j,x4/9,pm/k,x6/7,pc/e,x9/8,s10,x3/12,s8,x6/11,s11,x15/4,s1,x5/0,pn/k,x7/6,s3,x1/11,pi/f,x5/4,pa/e,x2/12,pp/j,x10/3,s7,x8/11,s14,x4/0,s10,x11/15,s1,x1/5,pb/e,x7/6,s5,x3/12,s2,x2/9,s13,x8/0,pa/p,x5/12,s4,x9/7,s2,x6/0,s1,x9/11,s13,x15/13,s12,x5/6,s15,x3/13,pd/j,x15/11,pf/p,x0/10,s11,x8/9,s1,x14/11,pj/m,x4/5,pe/k,x6/12,pg/j,s5,po/m,x10/0,s11,x1/12,ph/p,s3,x7/0,pn/m,x5/15,pk/f,x9/11,s2,x10/14,s15,x11/5,s7,x14/0,s2,x8/11,pd/h,x14/1,s4,x4/11,s13,x7/3,pp/g,x0/15,s7,x5/8,pm/n,x10/9,s2,x12/6,ph/a,x9/14,pd/b,x2/10,s12,x7/5,s11,x14/0,pn/o,x1/2,pc/h,x0/13,pd/n,x4/8,s10,pa/k,x12/0,s5,x15/2,s14,x7/1,s9,x15/2,pg/l,x14/13,s8,x0/1,pj/h,x7/2,s6,x4/10,pb/k,x13/11,s15,x2/15,s11,x1/14,s14,x12/6,s11,x9/15,s11,x7/8,s12,pi/n,s2,x12/11,s7,x3/10,s5,x5/11,s8,x7/10,po/f,x14/9,pd/b,x3/10,s9,pn/h,x14/8,s2,x6/12,s3,x5/2,po/c,x15/6,s5,x14/11,s7,x0/13,s10,pp/k,x14/15,s2,x0/7,pg/b,x5/9,s1,x15/0,s4,x3/8,pf/p,x9/7,pi/d,x10/5,s14,x13/8,pm/l,x2/1,s6,ph/o,x15/0,s8,x8/4,pc/k,x3/15,s11,x8/1,s2,x13/6,s2,x2/4,pa/n,x7/14,s8,x4/12,pp/k,x13/0,s7,x8/10,s11,x12/5,s3,x8/7,pa/b,x12/10,pg/k,s13,x14/3,s5,pd/l,x7/4,pj/p,x5/3,s14,x14/12,pn/k,x11/6,pm/i,s15,pa/p,x3/10,pc/j,x7/0,s9,x8/4,s9,x10/1,pf/o,x4/3,pl/g,x12/10,s6,x1/9,s6,x15/10,s4,pp/d,x9/4,s3,x14/15,po/b,x2/7,s4,x0/9,s14,x8/5,s6,x9/2,s14,x7/3,s15,x5/11,pd/j,s7,x2/3,s4,x7/6,po/k,x12/14,pl/b,x0/7,s6,x12/1,s1,x14/7,pm/a,x12/13,s14,pi/p,s5,x4/15,s1,x8/13,pm/e,x12/1,s13,x11/7,ph/f,x14/1,pg/c,x6/15,s8,pa/f,x3/13,pc/k,x1/5,pj/l,x10/15,s6,x14/9,pc/o,x8/13,s9,x15/6,s9,x5/12,s2,pk/m,x8/13,s11,x15/14,s5,x12/0,pe/j,s12,x11/2,pl/i,x13/12,s5,x4/9,pj/f,x10/0,ph/l,s1,x7/9,s9,x8/3,pi/a,s12,x15/10,pm/k,x1/11,s6,x12/13,pn/l,x0/8,pk/b,x1/13,pl/i,x2/3,pf/c,x6/7,pb/l,s15,x0/12,s11,x9/5,s10,x12/4,ph/p,x13/1,s11,x15/6,s5,x3/1,pg/f,s13,x14/11,pp/l,x15/9,pi/h,x1/4,s14,pl/o,x14/3,pp/f,x13/4,ph/k,s6,x1/0,pn/e,x12/4,pm/a,x9/10,pi/n,x12/1,s8,x2/15,pf/l,x4/10,s5,x8/13,pc/g,x10/15,s6,x11/7,pi/n,x6/8,pm/h,x14/12,pb/d,x6/13,s1,x0/5,pe/m,x10/15,pi/n,x12/13,s3,x11/8,pj/e,x3/5,s2,x1/7,pb/k,x11/4,ph/p,x13/8,s3,pi/c,x3/10,s10,x12/1,s13,x4/9,s14,x6/11,s9,x10/3,pn/l,x12/5,s2,x7/9,s14,x13/5,s4,x15/0,ph/e,x1/13,pa/m,x14/11,s1,pe/c,s10,x12/7,s13,x11/4,s9,pp/l,x9/3,s8,pj/d,x14/8,pa/f,x7/9,pi/l,x1/13,s4,po/f,x9/10,ph/d,x7/13,s6,x2/6,s2,x14/8,s9,x12/13,s1,x8/5,s3,x0/11,pk/a,x13/3,pf/m,x15/11,pj/p,x3/6,s2,x0/11,s5,x9/7,s14,x1/15,pi/e,s11,x4/13,pd/j,x5/2,pn/k,x15/6,pf/m,x1/4,s3,pi/h,x6/12,s14,x1/7,s2,x14/15,pb/c,x11/3,s13,x14/7,pi/h,x8/11,s6,pb/j,x13/10,pi/e,x1/4,s10,pg/n,x2/6,s6,x11/15,pi/a,x5/6,s3,x10/0,pk/e,x13/8,pi/f,s6,x1/9,pm/a,s4,pb/i,x14/0,s12,x3/8,s12,x15/0,pj/n,x1/2,s6,pi/g,x9/8,ph/m,x12/11,s12,x6/3,pe/g,x2/13,s9,x5/1,s10,x14/12,ph/m,x1/3,pk/a,x13/14,pn/i,x10/3,s5,pc/f,x9/12,s1,x13/0,s2,x4/12,pn/p,x8/10,s13,x4/1,pl/c,x0/12,s1,pi/b,x14/7,pg/e,x13/5,pn/i,x11/4,pm/a,x9/10,s2,x3/0,pb/j,s7,x15/11,s7,x7/6,s4,x1/0,s3,x8/7,s8,x6/9,s4,x7/1,pm/c,x3/14,s1,x13/2,pn/o,x3/6,s1,x0/15,s9,x10/9,pd/j,x13/11,s9,x4/5,s11,x14/12,s10,pi/e,x5/2,pp/b,x8/13,s13,x15/11,s3,x8/6,pl/n,x0/9,ph/a,x12/4,s5,x9/1,pd/b,x8/15,pn/e,x12/5,s15,x3/15,pl/p,x1/2,s14,x11/6,pe/o,x3/14,pp/n,x15/9,ph/k,s14,x12/1,pa/b,x4/3,s7,x7/1,s2,x8/12,s11,x1/10,pm/o,x5/0,pf/n,x13/7,s3,pg/e,x5/14,s13,x8/11,pa/c,x5/6,s3,x14/11,pe/d,x7/0,s14,x3/10,ph/o,x12/5,s9,x7/15,pl/g,x3/8,s12,x10/12,ph/n,s15,x9/8,s8,x7/2,s1,x15/8,s2,x5/4,pf/i,x13/10,pc/m,x0/1,pl/n,x7/12,s8,x5/13,s1,x14/1,pa/i,x0/2,ph/c,x14/4,s4,x12/8,s14,x14/13,po/f,x11/12,s1,pe/j,x5/3,s13,x7/13,pa/m,x8/0,s14,x1/9,s11,x10/6,pb/f,x4/2,pi/o,x12/5,s3,x10/13,ph/j,x5/14,s12,x11/3,pc/e,x8/5,po/n,x1/9,pd/f,x15/4,s6,x1/0,s8,x3/2,ph/e,x15/8,s9,x0/5,s7,x4/8,s12,x5/10,s12,x14/12,s7,x9/11,pm/c,s1,x13/1,s5,x7/0,ph/i,x3/12,s1,x14/0,s11,x6/3,pn/l,x7/15,s3,x10/6,pb/j,s10,x13/12,s3,x7/9,pf/p,x13/1,s11,x10/4,s2,x15/1,s1,x14/0,s11,x15/12,pc/k,x9/13,pm/j,x10/3,s12,pf/e,x14/8,s13,x13/6,s3,x11/8,po/c,x0/5,s8,x10/3,s13,ph/g,x6/0,pe/b,s14,x2/1,pm/p,x10/11,s15,x13/9,s14,x7/8,pn/b,x0/2,s14,x8/4,pp/e,x10/15,s8,x5/9,pk/j,x11/10,s5,x7/2,s2,x12/5,pp/b,x14/7,s6,x1/11,s14,x4/7,s7,x15/6,pc/e,x11/10,s12,x4/9,s11,x0/13,s1,x6/10,s13,x13/15,pm/f,x7/5,s14,x3/15,pb/e,x1/10,s3,x12/15,s4,x3/13,s9,x2/11,pn/d,x13/4,s11,x6/15,s14,x1/9,pe/c,x10/11,s10,x6/3,s14,x15/5,s8,x11/6,po/p,x3/9,s2,x0/8,s8,x10/13,s1,x2/3,s10,x15/5,s9,x1/8,ph/a,x13/12,pe/l,x9/0,s1,x12/3,pm/j,x0/7,pi/k,x6/3,pa/d,x4/2,s8,x1/9,s14,x7/13,pk/m,x1/11,pn/g,x9/2,s7,x7/15,s7,x0/2,pj/l,x1/7,pp/k,x11/13,ph/g,x2/10,s8,x5/8,s3,x9/0,s7,x13/2,pn/c,x12/10,s4,x8/3,s2,pd/o,s4,x1/11,pj/a,x3/14,s15,x11/15,pl/p,x4/14,pc/j,x15/9,pb/o,x12/10,s13,x7/9,pe/i,x15/0,s12,x1/3,pl/p,x5/4,s12,x2/9,pa/f,s3,x4/5,po/p,x9/7,pa/b,x15/5,s8,x2/4,s12,x3/11,s7,x6/12,s1,x3/5,s4,x6/1,s11,x0/14,s12,x13/6,s11,pd/i,x8/7,pf/k,s1,x6/5,s3,pm/a,x3/9,pg/j,s13,x8/15,s12,pf/h,x14/7,s1,x10/1,pm/b,x6/9,pn/i,x8/0,pj/g,x12/11,s12,x1/0,s5,x12/11,s8,x6/7,s12,x4/8,pi/f,x5/7,s2,x13/3,pm/b,x2/5,pp/f,s7,x11/3,s10,x2/14,s13,pb/c,x1/11,pe/k,x7/5,pg/b,x14/11,s11,x2/10,s9,x11/7,s5,x5/14,pn/f,x6/7,pa/d,x2/15,s15,x10/6,s10,x9/7,s9,x1/6,ph/g,x9/0,pj/c,x12/1,s3,x8/3,s14,x10/7,s3,x6/14,po/h,x3/12,s9,x13/4,s6,x6/1,s9,x9/7,s2,x2/4,pm/a,x14/10,po/k,x1/8,pm/b,x5/4,pf/i,x14/3,po/n,x15/13,s4,pd/j,x14/12,po/f,x0/6,s5,x2/7,s7,x5/13,s4,x9/8,s2,x12/0,s9,x9/10,pa/l,x5/0,pe/j,x15/11,s9,x14/10,pl/k,x3/6,s11,pa/n,x0/2,s15,pb/f,s2,x7/1,pn/j,x11/10,s6,x5/14,s10,x11/12,s10,x4/15,s3,x0/12,s1,x3/1,s13,x8/13,pi/l,s8,x14/3,pp/j,x9/11,pi/a,x15/12,pl/h,x0/4,s1,x13/6,pm/k,x10/7,pj/e,x0/11,pp/m,x12/15,s4,x9/7,po/i,x8/6,s4,x0/15,pd/g,x2/7,pf/p,x3/5,pj/n,x9/7,s3,x4/5,s12,x1/10,pk/m,x8/5,s9,x4/13,s10,x15/11,pc/i,x3/8,s2,x12/1,pp/m,x10/8,s13,x5/0,pk/o,x9/6,s8,x3/4,s13,x5/0,s7,pf/d,x2/12,pm/e,s13,x0/9,pb/k,x8/6,pd/l,s12,x10/7,pe/k,x15/8,s6,x9/7,ph/f,x6/3,pe/m,x7/15,pg/j,x5/14,pa/k,x0/11,s1,x10/8,pd/h,x4/5,s2,x10/8,pp/j,x9/4,s6,x7/0,s11,x4/8,s3,x11/14,pm/i,x3/10,pp/j,x0/2,ph/c,x8/3,pg/m,x6/7,s1,x2/10,s12,x9/7,s6,x11/2,s5,x0/5,s5,x14/6,pp/b,x8/2,pl/g,x7/3,pj/b,x1/11,s3,x12/14,pg/o,x1/9,pn/i,x3/4,pp/o,x10/1,s13,x15/5,pa/d,x4/11,po/p,x5/3,s11,x15/10,s12,x5/11,pe/l,x8/3,pg/n,x0/2,s12,x7/4,s14,x14/3,pc/o,x0/15,pb/a,x13/10,ph/g,x11/15,pm/b,x7/4,pn/d,s6,x12/11,s14,x1/4,pb/a,x5/3,pj/l,x2/9,s7,x1/15,s6,x5/6,pm/d,x3/7,s15,x6/10,pf/e,s7,x15/4,s12,x5/2,s12,x12/10,ph/b,x4/13,pk/j,x7/8,pm/a,x13/1,s10,pn/d,x3/12,ph/g,x4/1,po/j,x0/13,s10,x10/2,pe/m,x7/13,pi/d,x12/14,pa/g,s12,x15/4,s8,x5/6,s15,x14/3,pc/j,x0/4,s11,x5/14,pb/n,x12/15,po/h,x2/4,s11,x8/0,pk/p,s7,pa/l,x5/2,po/b,x13/12,s4,x15/1,s7,x6/9,s15,x10/2,pp/f,x0/13,pg/l,x12/6,s11,x0/7,s3,x6/12,pb/n,x3/4,s7,x10/13,s1,x2/11,s1,x7/14,pa/k,x10/3,s3,x7/0,pi/e,x9/13,pa/n,x4/1,s14,x2/13,s14,x1/4,pg/b,x6/3,s14,x5/12,s13,x4/1,s7,pj/h,x9/12,s3,x11/6,s11,x15/12,s3,x11/4,pi/g,x7/6,s14,x8/15,ph/b,x6/12,pp/n,x15/2,pm/c,x4/3,pd/i,x13/7,s5,x11/8,pk/c,s1,x9/10,pd/g,x7/0,s10,x15/3,s3,x14/11,pi/j,x9/3,po/g,x5/6,pk/l,x15/10,pn/b,x0/4,s2,x10/3,s4,x2/9,pf/h,x3/15,pp/e,x13/10,pl/f,x3/7,s7,x8/1,pe/p,x12/2,pn/h,x11/8,pd/j,x4/2,pp/h,x0/6,pk/b,x9/13,pn/g,x12/3,pj/a,x13/15,pb/m,x12/6,pl/a,x7/0,s8,x13/6,s6,x14/9,s13,x7/6,pi/g,x13/9,s10,x12/2,pb/d,x4/7,pf/h,x5/10,pd/a,s1,x13/8,s12,x6/11,s12,x4/1,pb/e,x11/9,pc/d,x8/13,s2,x4/9,s10,x0/13,s2,x5/14,pn/o,x9/13,s12,x5/3,s15,x12/1,s5,pc/k,x0/3,s12,x4/10,s7,pb/n,s2,x6/3,s9,x5/15,pc/l,x9/12,pk/o,x11/8,pl/h,x7/6,s6,x2/12,s9,x8/11,pb/m,x9/12,s11,x11/2,pf/d,x10/6,s4,x8/0,s11,x10/6,s7,x14/2,pc/m,x13/9,pk/l,x4/8,pi/f,x0/5,s12,pa/e,x7/15,s6,x9/2,s9,x14/4,pp/i,x8/12,pf/l,x4/3,pe/k,s13,x15/1,s13,x0/11,s7,x14/6,pi/j,x15/10,s4,x11/1,s15,x12/15,s14,pl/p,x7/8,s8,x13/1,s12,x15/4,s5,pa/n,x1/13,s12,x6/2,s4,x10/14,pj/f,x12/3,pb/h,s8,x5/9,pd/i,x1/10,s6,x14/2,pg/k,x15/5,s1,x12/11,pj/e,x6/0,s6,x11/2,ph/m,x5/13,s5,x0/10,s13,x8/6,pd/e,x9/1,s15,x2/5,s14,pc/g,x7/6,s7,pk/e,x14/13,pi/n,x2/9,pb/a,x13/0,s1,x15/14,pf/m,x2/6,pd/c,x12/9,pk/m,s3,pi/f,x11/14,pg/m,x6/2,pj/c,x13/3,s1,x1/0,s9,x10/12,s7,x8/2,pi/k,s13,x12/5,s4,x2/1,s12,x3/9,po/h,x6/5,s8,x1/8,pk/e,x12/2,pi/o,x9/11,pp/e,x12/1,s15,x7/3,po/k,x1/10,s15,x5/9,pl/h,x11/7,pe/f,x4/0,s5,x8/6,pg/n,x3/15,pl/d,x0/5,pf/c,x9/11,s13,x4/15,pd/a,x1/0,s8,x10/11,s12,x14/7,s11,x4/15,s4,x6/5,s5,x8/15,s5,x9/1,pk/i,x12/14,pp/h,x13/5,s9,x6/10,s7,x1/0,pj/d,s8,x4/6,s1,x2/8,pf/b,s2,x4/6,s14,x3/10,pn/c,x13/12,s14,x0/5,s6,x1/9,pm/l,x3/2,pc/n,x5/1,s7,x3/12,s10,po/d,s1,x11/2,s1,x4/3,s15,x0/9,s5,x4/7,pj/k,s8,x6/2,s5,x3/4,s8,x0/11,pi/p,x6/10,pe/h,s5,x12/3,s15,x1/9,s11,x7/5,s14,x2/9,s5,x1/12,s1,x7/14,s14,x8/12,pd/k,x15/9,s11,x8/10,pn/e,x14/1,s10,x2/5,pg/i,x0/11,s1,x7/4,pb/m,x3/14,pe/o,x7/6,pi/p,x8/10,s5,x5/15,pd/m,x7/12,s11,x14/8,s5,x11/2,pg/h,x12/10,pc/m,x9/14,s6,x10/6,pp/b,x0/2,s5,x4/8,pj/i,x9/10,pg/p,s15,x7/2,s8,x11/14,s4,x8/4,pm/b,x3/14,s9,x9/6,s12,x4/7,s4,x3/2,pg/o,x9/11,pm/h,x15/0,pi/j,s6,x4/2,s5,x0/10,s11,x6/15,pg/m,s15,x11/5,s4,x4/3,s2,x0/11,s1,x15/6,pj/b,x12/8,pg/f,x15/11,s11,x2/0,pl/c,x9/5,s5,x7/2,s5,x0/4,pn/f,s1,x9/5,s10,x3/15,pm/l,s6,x2/14,pj/p,x11/3,s1,x13/8,s5,x14/15,s5,x3/13,pg/m,s7,x9/8,pa/f,x2/13,s11,x9/15,s3,x4/10,s10,x11/0,pg/m,x6/4,s8,x0/2,s1,x9/3,s5,x7/12,po/b,x2/5,pk/p,x15/11,pb/e,s11,x2/13,pd/h,x0/6,pn/f,x3/15,s7,x10/7,pg/l,x15/9,pn/j,s9,x2/7,s8,x11/3,s7,x13/5,pc/h,x8/12,s13,x6/7,pm/b,x0/5,s9,x1/8,s2,x5/6,s5,x0/9,s11,x12/13,s5,x7/0,pi/c,x8/15,s6,x11/3,s5,x9/15,s6,x5/6,po/d,x9/0,pn/j,x12/10,s7,x1/11,pm/d,x9/6,s6,x15/11,po/a,x9/1,pf/b,x2/12,s10,x5/6,pa/k,x1/13,s2,x7/6,pc/i,x10/8,s2,x0/1,s7,x5/11,s7,x0/9,s10,x3/12,s8,x13/6,s7,x0/1,s11,x2/15,s10,x9/4,pe/h,s3,x3/6,pk/n,x1/5,s15,x15/8,s13,x4/9,s15,x0/12,s6,x8/13,s2,x15/9,s12,x5/0,s14,x12/10,pj/p,x15/7,pg/o,x1/2,s15,x12/15,s6,x5/8,s2,x3/6,pj/f,s9,pl/d,x15/8,pm/o,s5,x10/4,s14,x0/12,ph/p,s5,x7/9,s10,x2/13,pi/l,x1/4,s6,x13/8,s9,pa/c,x9/14,pg/i,x2/8,pp/d,s12,x3/11,pa/i,x9/13,s15,x11/10,s6,x3/6,s14,x12/15,pg/h,x10/9,s1,x4/0,s9,pa/e,x1/3,s2,x12/2,po/j,x6/5,s9,x0/3,pd/b,x13/12,ph/e,x4/15,pa/n,x10/13,pf/g,x6/3,pj/o,x15/7,s15,x2/8,s3,x9/6,s1,x1/11,s3,x0/14,pf/d,x8/2,s15,x6/13,pe/c,x3/9,s11,x2/13,s10,pi/n,x7/6,pg/e,x9/10,s3,x4/7,s2,x3/5,ph/i,x11/14,s13,x15/6,s10,x2/1,s10,x4/11,s10,x0/9,pf/p,x5/3,pi/h,x10/9,s9,x13/12,s1,x1/3,s9,x4/15,s13,x5/8,pg/e,x4/7,s2,x10/8,s15,x14/12,s13,x10/2,pm/l,x9/5,s12,x2/6,s9,x9/1,s4,x15/7,s2,x9/8,pa/g,s11,x10/3,pc/e,s11,x13/14,s15,x9/7,s6,x10/0,pi/d,x4/12,pk/o,x15/2,pg/e,x6/0,s9,x10/2,s14,x12/14,s3,x6/7,s13,x12/15,s6,x3/13,s4,x8/5,pd/b,x10/4,pp/i,x12/11,s15,x5/14,ph/a,x4/9,s15,x6/15,s3,x13/3,pk/p,x9/7,pb/o,s6,x14/10,pc/g,x11/2,s15,pe/d,x4/15,s5,x12/10,pj/k,x8/2,s2,pa/m,x9/3,pf/e,x5/8,pg/i,x14/6,s9,x15/11,s3,x5/1,s9,x13/14,pp/a,x15/10,s4,x2/0,s7,x14/5,s14,x9/4,pj/m,x2/15,s13,x7/1,pc/e,x10/0,pm/h,x11/6,s13,x4/10,pp/d,s13,x7/9,s3,pe/c,x8/15,pl/j,x1/13,s7,x0/14,s5,x11/7,s1,x14/12,pp/c,x10/1,s9,x6/7,pf/h,x1/12,pj/e,x14/10,pl/b,x7/3,s6,x14/10,s13,x4/13,pn/d,x10/7,pk/o,x6/12,s6,x3/7,pn/m,x14/0,s12,x10/1,pp/i,x11/15,s5,x6/12,s3,x8/3,pn/c,s6,x10/4,pe/b,x6/0,pg/n,x13/12,s11,x5/7,pl/b,x3/4,s8,x6/9,s15,x2/12,s10,x14/8,pa/n,s4,pk/o,x15/6,pi/f,s10,x9/5,s11,x7/10,po/c,s8,x0/8,s3,x1/11,pd/h,x13/14,pe/a,s13,x1/11,s10,x0/7,s1,x9/15,s13,x3/2,s8,po/h,x11/10,s9,pk/n,x1/13,s11,pd/l,s13,x3/7,s4,x0/6,pn/m,s1,x2/1,pi/g,x7/0,pk/a,x10/2,po/c,x6/9,pi/d,x3/10,s2,x11/7,s10,x14/2,s4,x11/1,s15,x9/14,s3,x12/13,s6,x6/14,pb/n,x8/15,po/p,x10/13,s12,x1/14,s4,x9/3,pk/g,x6/13,pa/d,x3/4,pn/e,x6/7,s15,x1/10,s13,x9/5,s7,x11/14,s6,x0/13,s1,x3/15,pa/g,x13/4,s8,x6/3,s14,x13/8,s15,x9/4,s3,x13/15,s5,x7/5,pj/f,x13/11,pa/b,x12/4,po/j,x11/13,s11,x4/3,pp/h,x7/9,pa/b,s3,x14/10,s3,pk/d,x0/6,s11,x12/8,s15,x0/6,s14,x2/14,pf/h,s9,x6/5,pc/n,x11/15,pa/g,x9/5,po/b,x15/1,pj/k,x10/3,s5,x12/13,pb/e,x14/15,s10,x11/3,ph/j,s11,pc/l,x2/15,s4,x9/13,s14,x1/0,s1,x3/2,s14,x6/7,s8,x15/12,s5,ph/p,x11/5,pm/n,x14/0,pj/k,x2/7,s5,pp/l,x10/5,pf/g,x13/15,s5,x5/11,s3,x12/9,s5,x2/0,s9,x7/4,pj/k,s6,x2/12,s2,pm/n,x4/13,pd/f,x11/10,pi/k,x7/6,ph/o,x13/9,s2,x8/4,pp/c,s13,x11/14,s15,pj/n,x10/1,pb/h,s10,x0/13,s8,x15/1,s2,x4/9,s14,x11/12,s8,x6/4,pc/l,x12/3,s3,x7/9,s5,x10/6,pd/a,x4/9,s9,x14/15,pi/b,x8/10,s9,x0/13,pk/f,x12/11,pj/d,x5/15,s12,x9/0,pn/c,x7/2,s15,x10/15,pm/d,s5,x1/5,pn/e,x13/14,pb/l,x15/6,po/i,x9/11,pj/b,x6/14,pl/n,x0/8,s3,x5/13,s8,x8/0,s7,x10/3,s3,x5/0,s2,pe/c,x11/10,s2,x14/8,s1,x5/15,s7,x11/10,s8,x3/1,s10,x14/5,s5,x10/0,s8,x1/5,s8,x2/15,s14,x14/0,pn/g,x8/5,s3,x14/9,ph/l,x1/15,s8,pi/g,x7/14,pl/b,s7,x11/12,pn/k,x5/7,s8,x1/8,s2,x6/9,s7,x12/14,pj/e,x15/13,pl/i,x3/2,s10,x7/13,pe/h,s11,x8/11,pc/i,s4,x7/4,po/k,x6/11,ph/f,x15/1,s8,x4/6,s2,x12/3,s12,pj/g,x8/6,s2,x2/13,pc/i,x15/1,pl/n,x6/2,s13,x0/10,s12,x1/5,s8,ph/j,x7/12,pp/a,x6/15,s9,x1/2,s1,x10/9,s5,ph/c,x15/8,s4,x6/10,pe/a,x12/9,s12,x0/1,s8,x12/14,pn/o,s12,x9/1,s7,pf/p,x11/4,s15,x7/3,s2,x15/12,s6,x0/2,s3,x3/14,pb/i,x1/2,pd/c,x4/7,s1,x5/3,pk/o,s3,x11/1,s13,x6/13,s1,x2/10,s5,x5/4,pj/p,x1/0,pn/i,x5/15,s2,x12/6,s12,x9/0,s7,x3/5,s2,x10/9,s6,x4/7,pm/o,x14/3,s8,x15/0,pk/p,x3/5,pf/n,x4/1,s8,x7/9,s1,x13/3,s6,x7/6,ph/i,x1/0,s7,x3/13,pa/o,x9/0,pm/p,s13,pn/j,x14/2,s8,x7/13,s15,x10/1,s2,x3/8,pc/k,s5,x1/7,s15,x15/11,pl/h,x6/0,pi/g,x9/11,pa/l,x3/0,pf/b,x15/2,pe/i,x13/0,pd/k,s1,x1/12,s13,x9/0,s6,x11/7,s12,x2/6,ph/j,x12/3,pg/l,x1/13,s14,x8/10,pb/o,x3/6,s10,x2/10,s14,x6/1,pf/m,x9/13,pc/n,x1/3,s12,x6/12,pi/l,s13,x15/10,s10,pm/a,x14/13,s2,x15/0,s6,x1/4,po/k,x13/14,pl/a,x15/8,s6,pe/g,x1/6,s14,x12/13,ph/l,s12,pg/m,x1/15,po/i,s6,x8/5,s5,x15/13,pm/g,x4/8,s7,x9/7,pj/n,s8,pb/i,x12/4,ph/a,x14/7,pb/e,x4/3,s15,x6/13,pc/k,x0/8,s6,x9/13,pf/p,x1/7,s10,x10/6,s13,x2/1,pj/i,s14,x10/3,s14,x7/15,s11,x10/1,s14,x2/7,s2,x14/9,pf/b,x0/11,s12,x6/5,s8,x10/4,s4,x0/14,s3,pa/l,x8/10,ph/f,x6/5,pd/p,x7/11,pn/h,x9/10,s2,x15/1,pf/k,x13/11,ph/l,s2,x6/3,s4,x15/9,po/n,x0/8,s6,x4/1,pc/k,x8/2,s13,x11/12,s13,x0/14,pl/i,x4/5,s5,x9/6,s3,x7/15,s10,x12/11,s15,x15/5,s1,pj/a,x12/8,pl/f,x2/5,pe/g,x4/7,pk/o,x1/11,s1,x8/3,pc/f,x0/15,pj/i,x10/13,pn/d,x1/12,s4,x4/3,pf/a,x0/6,pj/i,x3/11,s10,x8/9,s5,pc/a,x3/0,s13,x5/12,s6,x13/14,s4,x7/15,pn/g,x4/5,pc/d,s6,x13/12,s7,x15/1,s11,x14/6,pf/l,x2/7,s10,pk/p,x4/14,pa/c,x5/2,s11,x1/10,s5,x2/7,s8,x10/1,s3,x12/6,pf/e,x8/10,pd/c,x13/3,pg/f,x10/11,pa/c,x0/15,s6,x11/2,s1,pm/j,x7/13,s2,x8/4,pg/a,x11/10,pe/d,x4/6,s3,x12/7,s11,x2/13,s10,x9/11,s8,x15/6,ph/j,x13/4,pm/i,x8/1,s13,pp/b,x4/7,s14,x12/15,s6,x2/13,pd/j,x0/11,s4,x7/5,pp/n,x6/2,s4,x5/1,s14,x4/11,s2,pg/e,x5/0,pm/c,s8,pj/i,x4/2,pb/l,x0/11,pp/f,x9/8,pb/h,x11/15,s4,x5/3,s15,x11/12,s12,x14/9,pn/p,x0/11,s7,x12/8,s7,pj/d,x4/6,s9,x10/8,s8,x7/4,po/i,x13/1,pg/p,x6/2,pk/n,x3/0,s7,x10/6,pd/b,x13/8,s15,x3/12,pf/p,x9/15,s5,x10/3,pc/o,x7/2,pn/p,x0/9,pa/j,x1/15,s5,x12/0,s9,x1/3,s10,pn/p,s15,x2/13,s4,pa/i,x3/7,pm/e,x5/14,s11,x0/3,s6,x11/8,s4,x1/13,s4,x2/12,s10,x15/6,s6,x10/11,s15,x12/5,pg/h,x7/6,pi/k,x10/2,pm/l,x7/5,s10,x13/2,s2,x6/7,pa/d,x0/12,s11,pm/e,x3/13,pc/j,x5/1,pe/b,s4,x6/14,s2,x10/8,s13,x6/4,s1,x0/7,ph/j,x5/10,s2,x1/3,s11,x15/9,pk/l,s6,x2/8,s15,x6/14,s12,x4/5,s13,x11/9,pc/f,x15/12,pj/e,x3/5,s14,x6/15,pg/p,x12/4,ph/e,x5/1,po/b,x8/9,pk/l,x6/14,pn/e,x11/4,pk/g,x5/2,s11,x9/11,s4,x3/7,s15,x13/14,pf/c,x4/8,s14,x13/10,s13,x1/2,pd/a,x0/3,s12,x5/2,pi/b,x0/13,pj/c,x9/10,s9,x6/15,ph/n,x7/4,s11,x8/3,s3,x2/0,s6,x10/14,s3,x0/6,s8,x12/7,s10,x14/8,s9,pp/c,x6/10,s3,x1/13,pm/e,x11/6,s9,x1/12,pd/f,s1,po/b,x15/8,s6,x7/9,pd/m,x4/13,s3,x10/2,s14,x13/3,s13,x14/1,s3,x15/13,s3,pn/p,x11/1,s12,x10/14,pl/k,x8/13,s12,pp/e,x9/4,pf/j,x1/14,s5,x10/15,s10,x9/12,s11,x5/1,pk/p,s5,x10/3,pn/i,x14/13,pk/h,x0/10,s11,x7/4,pp/b,x13/11,s4,x3/15,s6,ph/g,x7/0,pc/m,x8/10,s12,x13/0,s1,x9/11,pd/j,x15/5,s6,pp/b,x7/6,pk/e,x13/10,pc/o,x6/8,pe/g,s3,pc/l,x15/14,s2,x12/10,s15,x4/5,pn/b,x13/11,s15,x10/8,s4,x5/13,s4,x8/2,s4,x4/11,s11,x0/2,s7,x1/12,s7,x2/6,pi/f,s6,x8/13,s5,pg/m,x2/4,s1,x10/15,s6,x9/2,s7,pd/k,x11/14,s15,x15/9,pa/j,x11/0,s11,x13/14,s14,x0/9,s4,x10/12,s3,x4/7,s1,x14/5,s7,x8/15,s2,pd/h,x5/11,s13,x8/1,s12,x11/12,s11,pb/f,x13/4,s12,pa/d,x2/14,s8,x5/7,pf/b,s8,x2/1,s2,x15/12,s10,pi/g,s13,x9/14,s11,x2/0,pp/m,x10/4,pl/o,x5/0,s7,x3/6,pd/f,x7/8,s8,x15/14,s13,x9/2,s11,x8/13,s5,x5/10,s15,x1/8,s9,x3/10,s14,x9/4,pe/p,x8/0,s2,x5/2,pd/i,s15,x8/14,s13,x0/12,s12,x13/15,s15,x12/2,s14,x3/8,pe/c,x7/4,s13,x11/6,ph/k,x2/7,s4,x6/15,pp/e,x14/2,s14,x1/10,s13,x14/15,s1,x0/13,s13,x8/9,pl/g,x1/6,pp/a,x15/12,s3,x13/4,pf/o,x11/9,s7,x15/3,pa/p,x4/6,pb/k,x7/3,s1,x11/12,s11,x8/5,s2,x11/7,s13,x13/3,po/d,x10/15,pm/l,x7/8,pj/g,x2/15,s4,x13/7,po/k,x4/0,s1,x14/2,pe/j,x3/11,s14,x5/1,s1,x10/9,s15,x14/6,pa/d,x10/0,s10,x9/14,pl/k,x8/10,s14,x3/1,s5,x6/15,s7,x13/2,s1,x10/12,s12,x9/5,po/j,x1/10,ph/f,s15,x2/13,pi/b,x10/12,pa/e,x1/0,pn/m,x6/12,s15,x5/13,s8,x11/12,pl/f,x8/0,ph/a,x14/11,s2,x8/4,s6,x15/3,s13,x13/7,s9,x5/4,s10,x0/14,s10,x4/9,s3,x11/2,pj/d,x15/14,s3,x4/5,s2,x1/3,s2,x10/0,pp/i,x11/2,s13,x10/14,pm/f,x11/7,pl/o,x4/5,pm/c,x14/12,s4,x11/10,pg/o,x8/4,pe/i,x13/10,s15,x6/11,pk/h,s4,x8/13,s14,x4/0,s3,x11/5,s2,x4/8,s8,x2/3,s10,x12/11,s6,x13/7,pl/i,x0/2,pd/a,x15/11,s10,x10/12,s8,x5/1,pn/j,x12/2,s2,x5/6,pb/l,x2/12,pa/n,x15/9,ph/e,x0/6,s15,x15/2,pj/b,x10/12,s11,x5/13,s4,x12/1,s15,x8/13,s8,x6/15,s9,x0/2,s11,x9/8,pl/a,x15/14,pi/o,x2/13,pe/p,s1,x15/9,s14,x1/8,s3,x7/9,s14,x4/12,s2,x14/15,s4,x6/8,s10,x9/14,pl/a,x12/10,s7,x4/13,s10,pe/n,x1/14,s5,pj/o,x15/6,s11,x8/5,s4,x14/3,s3,x0/15,pg/p,x1/6,s1,x9/11,po/l,x8/14,pf/e,x4/11,s6,x5/14,pc/d,x7/12,s3,x1/8,po/p,x6/12,pi/b,x15/5,pl/f,x8/0,pe/c,s7,x15/1,s14,pf/m,x5/9,s11,x4/11,s8,x13/3,s9,x10/9,s14,x8/2,pd/e,x12/6,s3,x7/4,s6,pc/a,x14/10,s5,x2/6,s9,x13/11,s10,x8/4,s3,x15/2,s14,x12/6,ph/n,x10/11,s14,x1/14,pj/g,x8/0,pp/f,s2,x9/11,pd/e,s4,x6/10,s10,x5/9,s12,x7/6,pa/f,s8,x3/8,pm/k,x13/6,s9,x10/11,s3,x5/12,pc/h,x13/3,pb/p,x1/12,s15,x6/14,s15,x5/15,s1,x14/13,s10,x15/4,s1,x2/10,s7,x15/5,s2,po/d,x14/0,s14,x8/13,s15,x14/0,s14,x9/13,pi/l,s3,x12/5,po/a,x7/13,pp/m,x9/2,s10,x1/10,pk/b,x7/0,s6,x1/5,pi/l,x2/11,pm/p,x1/7,s7,x5/2,po/k,x7/0,pf/b,x8/4,pl/m,x14/13,pf/i,x7/2,s15,x13/5,s15,x9/12,s14,x10/1,s12,pj/l,x5/14,s2,x9/3,s6,pd/c,x0/5,s11,x14/12,pl/o,x11/13,s9,x15/0,pd/h,x1/6,po/j,x13/8,s4,x9/10,pk/e,x0/3,pa/l,s8,x13/14,s9,x2/5,pe/p,s1,x11/8,s5,x6/10,pf/l,s11,x12/5,s14,pc/n,x0/6,s1,x10/8,s14,x14/4,s5,pa/m,x12/0,pj/h,x15/2,s9,x8/1,s4,x2/3,po/b,s1,x13/6,ph/d,x10/7,s9,x12/9,pe/j,x5/15,pk/p,x14/3,pf/a,x6/4,pc/g,x0/9,s13,x8/12,s12,x10/9,s9,x14/7,s14,x2/8,pj/k,x4/6,pf/e,s13,pp/b,x14/3,s9,x2/5,pi/e,x9/10,s13,x8/2,s4,pp/m,x4/12,s4,x11/8,s2,x10/6,pg/a,x15/7,pl/j,x11/8,s11,x10/14,s1,x1/5,pf/o,x0/13,s1,x11/3,pm/b,x1/15,pg/o,x14/4,s13,x12/13,s3,x2/10,s15,x1/6,s5,x7/12,s2,x6/4,pd/j,x11/13,po/n,x0/7,s9,x15/3,s11,x0/6,pc/m,x7/15,s3,x6/2,s12,x9/3,pb/f,x5/6,s11,x10/13,pd/a,s7,x4/8,s11,pp/n,x6/12,pk/l,x7/9,s10,x6/1,pg/e,x5/7,s5,x8/14,s1,x3/10,s12,x13/15,pc/p,x7/8,pk/e,x6/13,pn/o,x15/14,s14,x4/6,s1,x7/1,pc/m,x8/5,pl/p,x1/10,pd/m,x14/5,s6,x9/10,s12,x14/15,pc/b,x3/9,pf/h,s15,x0/12,pa/l,x15/4,s11,x0/6,pb/i,x10/1,s11,x3/2,s14,x5/4,s4,x3/15,s5,x0/11,pp/m,x4/1,pi/c,x0/3,pa/n,x10/5,pp/o,x12/13,s6,pi/g,x8/9,pj/f,x6/11,s11,x9/14,s13,x13/4,s15,x8/6,s15,x12/9,s6,ph/d,x5/7,pa/e,x2/1,pc/j,x9/8,s1,x7/13,s14,x11/14,s2,ph/k,x12/9,pb/i,x13/14,s15,x12/0,s9,x15/11,pc/p,x14/4,pi/k,x1/6,s11,x3/0,pa/j,x2/8,s1,pe/k,x11/0,pc/f,s7,x1/9,s6,x3/0,s12,x4/6,s12,x2/13,s13,pm/l,x1/12,s13,x4/2,pd/k,x5/11,s15,pb/e,s4,x13/4,s12,x0/5,pf/k,x6/12,s13,x13/15,pj/a,x2/14,s6,x4/1,pi/c,s1,x14/13,pm/e,x4/2,pn/g,x6/5,s15,x13/1,pj/c,x5/10,s2,pf/o,x12/13,s9,x10/0,pp/h,x14/15,pf/e,x11/0,s2,x6/10,s13,x7/13,s5,x10/6,s1,x1/12,s1,x0/11,s4,x8/3,pi/a,s9,x12/15,s9,x8/4,s13,x7/2,s13,x11/10,s1,pe/j,s2,x12/13,s11,x3/5,s3,x6/9,s4,x10/7,s10,x4/5,s2,x12/15,s3,x5/4,po/d,x15/14,s2,x13/10,pf/i,s11,x15/4,s11,x1/11,s8,x9/4,s1,pn/a,x15/10,s8,x1/8,pg/c,x11/12,s12,x8/2,s9,pi/n,x10/5,s11,pd/a,x6/9,s4,x10/3,s13,x12/14,s7,x8/7,s15,x15/14,s15,x9/13,s2,x6/3,s9,x7/0,s12,x14/12,s13,x1/9,pl/m,s5,x7/3,pn/o,x13/10,pa/p,x14/4,s3,x7/13,s6,x12/6,s3,x7/5,s3,x13/2,s5,pb/n,x1/14,po/a,x11/9,s3,x7/8,s12,x15/14,s12,x13/7,pg/m,x3/5,s11,x9/13,s9,x1/8,pl/f,s11,x13/11,pp/m,x9/2,s6,x15/4,s6,x11/2,s11,x3/14,s1,x9/10,s13,x3/14,s12,pa/c,x7/0,pi/h,x2/11,s6,x3/4,pa/o,x11/8,s7,x7/9,s10,x3/0,pe/i,x10/5,po/p,x4/6,pc/m,x0/10,pk/g,x2/7,s1,x14/6,s8,x0/3,s6,x14/12,s14,x3/1,s3,x6/7,s7,x5/10,pl/f,x7/15,s15,x8/9,pe/h,x4/10,pb/o,x13/15,pe/i,x1/4,ph/d,s5,x11/10,s7,x1/7,s4,x11/14,s14,x1/4,s15,x10/6,po/j,x5/2,pg/m,x10/0,pn/h,x11/1,s3,x0/6,pk/c,s2,x3/7,s1,x8/14,s11,x12/3,s14,x2/11,s13,x14/12,pp/d,x4/10,s8,x3/6,s2,x5/0,s9,x8/14,s4,x15/9,pj/a,x2/6,ph/m,x5/15,s4,x8/0,s6,x5/3,s8,x11/13,pg/c,x14/12,s3,x8/13,s8,pb/f,x12/11,s2,x4/5,s14,x1/7,s11,x8/4,s8,x6/2,pg/d,x3/8,s14,pn/b,s10,x11/6,pi/c,s6,x10/15,s6,x6/2,s13,x7/11,pe/d,x15/2,pm/o,x5/9,s5,pk/b,x2/10,s13,x1/0,s5,x12/4,pm/i,x11/0,pn/o,x2/5,ph/d,x3/13,pl/a,x15/0,s2,x6/5,s11,x11/4,s5,po/f,x6/8,pj/e,x5/12,pc/l,x2/1,s8,x12/9,ph/o,x2/11,s13,pg/a,x9/4,pi/c,x1/14,s5,x6/15,pn/a,x7/10,pp/k,x11/12,s5,x15/0,s3,pn/j,x4/6,s4,x3/10,s2,x6/1,pb/h,s6,x3/7,pj/d,x10/0,s5,x11/15,s3,x8/14,pg/k,x10/3,pp/f,s8,x2/12,s9,x3/10,s7,x14/5,s15,x12/3,pa/l,x1/15,s13,x2/12,pk/f,x6/7,pe/l,x1/13,s3,pa/f,x0/7,s10,x14/9,s8,pm/h,x2/12,pp/o,x7/9,s14,x2/5,s10,x8/13,s15,x0/3,pa/n,s8,x13/7,s15,x4/5,s2,x9/15,s11,x8/10,pf/c,x1/12,s9,x0/3,pe/l,x7/14,pk/o,x3/9,s10,x4/12,pf/c,x15/10,s5,x8/14,s6,x4/7,s1,x15/10,s12,x0/1,s11,x8/3,pb/i,x0/12,s3,x7/5,s3,x4/2,pg/a,x9/3,s8,x1/15,s13,pi/n,x8/13,s6,pf/m,x4/15,s5,x1/3,pg/n,x2/5,s8,ph/p,s12,x8/6,pe/n,x2/15,pj/b,x0/14,pp/d,x6/3,s4,x8/2,pg/c,x0/12,pk/o,x14/7,s14,x10/9,s15,x7/8,s5,x5/9,pb/e,x15/3,s8,x7/2,s4,x6/14,s13,x8/11,pl/f,x6/2,s15,x14/7,s3,x1/15,s1,x9/3,pi/e,x7/6,s7,x0/15,s8,x11/14,s2,x15/7,ph/k,s2,x9/1,pi/d,x0/12,po/k,x9/4,pd/l,x8/15,s13,x2/3,s6,x7/9,s11,x0/10,s11,x11/12,s2,x14/5,s15,x12/8,s8,x3/13,pi/o,x2/6,s9,x10/8,s5,x12/5,s13,x2/4,s12,x15/8,pa/h,s11,x0/13,s5,x15/7,s1,x13/5,s10,x1/14,s5,x4/2,s12,x3/9,pm/c,x6/12,ph/o,x10/3,pc/m,x13/12,s11,x3/8,pi/p,x12/15,pe/l,x13/7,s10,x11/4,pm/n,x9/5,po/k,x15/7,s8,x12/3,s12,x13/6,s9,x11/14,pl/m,x13/1,s15,x11/15,s13,pk/o,x0/14,pn/e,x11/13,pp/m,x5/9,s8,x10/12,s15,pf/o,x8/11,s11,x2/7,s5,pd/i,x11/12,s2,x8/2,s3,x7/4,pj/o,x6/13,pb/k,x4/5,s3,x3/12,s4,x5/4,po/f,x0/1,pe/i,x6/15,pl/p,x14/13,pn/i,x5/8,s3,x7/4,pc/g,x1/12,pn/f,x13/9,pl/j,x5/7,s1,x8/2,s7,pp/b,x12/3,s10,pj/l,x9/15,s12,x3/10,pc/a,x14/6,pl/f,x3/0,s12,x9/15,s13,x0/6,s14,ph/o,x11/7,pl/g,x4/8,pd/h,x2/3,s1,x9/14,s6,x2/10,s6,x0/6,s15,x4/15,s2,x8/14,pe/m,x2/11,s9,pn/g,x7/6,po/c,x15/0,pd/e,x12/4,pk/n,x1/11,pf/i,s13,x8/13,pj/c,s9,x7/6,s10,x4/9,pa/l,x14/10,s4,x1/11,s6,x6/9,s8,x0/10,pg/d,x15/2,pf/l,s9,x8/9,s1,x6/11,s12,ph/k,x1/13,s2,x8/9,pj/l,x0/6,pb/p,x9/2,po/a,s9,x0/12,pk/l,x6/2,s3,x5/0,s5,x1/15,s12,x5/0,pn/o,x2/14,pk/f,x9/1,pg/n,x2/4,pe/a,x15/10,po/k,x3/13,pl/g,x4/10,s1,x1/0,s9,x3/9,pm/p,x1/13,s14,x5/14,pj/f,s10,x3/12,s13,pb/p,x10/8,s6,x15/4,s2,x3/10,s14,x13/4,s5,pe/o,x1/11,pj/b,x10/3,s7,x4/8,pn/e,x3/6,pj/h,x14/1,s8,x10/9,pb/o,x15/2,pa/i,x13/6,pl/e,x9/14,pn/o,x8/6,pl/k,x15/5,pi/p,x8/1,s14,x13/3,s8,x8/12,pc/n,x14/11,s4,x12/0,pa/o,x5/11,s5,x6/7,s3,x11/8,pf/e,x3/2,s10,x13/9,pp/j,x6/1,s10,x10/7,s13,x12/13,s6,x1/14,pk/g,x13/7,pf/c,s3,x14/4,pe/b,x6/1,pc/j,s4,x4/15,pi/l,x1/6,s1,x15/7,pn/d,x1/3,pf/a,x7/5,s10,pg/p,x8/9,pk/n,x3/13,ph/l,x14/9,pj/d,x7/13,s12,x15/12,s8,x11/7,pi/c,s1,x14/2,pp/a,x1/5,s3,x9/14,pe/l,x8/2,pp/b,x11/5,pm/c,x0/6,s10,x12/5,s9,pa/i,s9,x13/14,s1,x7/0,s14,x5/12,s8,x11/6,s13,x5/12,s11,pn/b,x9/13,pm/o,x2/6,pc/a,s8,pn/p,x5/15,pb/a,x14/10,s9,x9/5,s2,pd/e,x11/13,s8,x1/3,s3,x5/0,pp/b,x8/2,s12,x9/4,pd/f,x7/0,pm/c,x6/11,s14,x8/13,s2,x7/9,s10,x12/3,s11,x11/8,s6,x15/3,pa/h,x9/14,pd/f,x4/0,s5,x6/14,pg/m,s4,x12/0,pk/p,x15/14,s4,x9/11,pm/a,x10/4,s10,x0/3,s9,x10/14,s10,x13/0,s10,x5/3,ph/n,x11/0,s8,x13/15,s2,x10/7,s8,x4/5,pm/b,s4,x14/13,pk/g,x9/11,pf/o,s6,x7/13,pc/a,x4/3,pb/g,x14/15,pa/p,x13/7,s15,x2/9,s14,x1/15,pf/l,s13,x11/7,pj/o,x1/3,s11,x4/6,s8,x15/3,s4,x7/14,pp/l,x3/8,pf/n,x10/13,s4,x0/15,s4,x4/5,s5,x2/6,pe/j,x13/1,pm/g,x9/6,s6,x3/2,po/b,x8/10,s6,x3/12,pe/c,x5/8,pk/f,x2/13,s1,x15/0,s7,x10/7,s9,x3/8,pp/e,s9,x10/9,pm/h,x5/14,s5,x8/15,s7,x13/2,pk/c,x15/14,po/f,x4/8,pp/g,x11/10,s1,x15/9,s3,x10/7,s13,x9/8,pb/d,x14/11,s1,x2/1,s11,x13/0,s8,x11/15,s5,x14/12,s4,x7/1,s6,x5/10,s3,pj/f,x15/11,pn/h,x8/0,s13,x6/9,s1,x4/5,pj/a,x3/13,pp/i,x5/4,pn/l,s8,x13/3,s10,x12/7,s5,x6/10,s7,x15/0,s7,x14/11,s13,x12/15,s6,x5/6,s1,x13/1,s15,x3/12,s2,x0/7,pk/m,x11/10,s5,x2/3,pp/f,s10,x12/15,s8,x8/1,s2,x3/6,pm/j,s12,x10/1,s1,x2/8,pa/n,x4/10,pi/j,x15/5,s9,x1/14,pf/c,x3/12,s13,x8/13,pb/m,s9,x2/11,s7,x4/14,s2,x12/10,pj/a,s2,x0/14,s15,x13/8,pl/h,x12/15,pi/j,x2/4,s3,x5/7,s7,x6/0,pm/c,x9/12,pp/h,x10/5,pm/i,x15/11,s2,x14/10,s10,x5/6,s13,x1/4,po/h,x5/11,pm/e,x8/6,ph/i,x0/10,s11,x5/8,s14,x6/7,s6,x10/2,pk/m,s13,x3/14,pi/j,x8/2,s5,x3/0,s14,x12/15,pl/p,x6/1,pe/j,s12,x7/2,s8,x11/13,s7,x6/1,s15,pc/f,s8,x4/7,s12,x11/0,pl/n,x15/1,pm/e,x2/9,s14,x8/10,s15,x1/6,s3,x3/10,s14,x2/14,pd/g,x15/0,po/m,s5,x14/8,pa/e,x6/3,s7,x4/5,s13,pm/k,x3/14,s2,x15/12,pb/l,x6/14,s15,ph/e,x1/10,s14,x11/0,s5,x8/1,s3,x5/9,s3,x0/14,s13,x10/6,s12,x1/7,s14,x11/9,pi/f,x2/5,ph/m,x13/7,pn/c,x10/5,pb/h,x8/15,pl/a,x14/13,pn/k,s3,x3/12,s3,x9/15,s6,x13/12,s4,x8/5,s2,x2/10,pd/h,x1/11,po/f,s9,x2/13,pb/g,x7/9,s6,x10/2,s4,x4/9,s2,x12/15,s12,x8/4,po/h,x7/10,s1,x11/1,pf/l,x14/0,po/j,s3,x5/6,s9,x13/0,s8,x15/4,pi/f,x13/3,ph/a,x5/7,s2,x9/4,s11,x0/5,s14,x8/13,pg/p,x0/1,s4,x4/11,s2,x0/2,pd/e,x15/6,s2,x2/0,pa/i,x7/9,s6,x4/1,s1,x14/6,s8,pe/h,s11,x7/15,pi/c,x13/9,pl/a,x4/6,pi/n,x10/5,s10,x14/12,pg/p,x9/11,s2,x7/10,s9,x2/11,ph/c,x1/7,pk/p,x13/4,pm/i,s12,x6/0,s3,x3/8,s3,x1/12,s4,x15/9,s7,x12/1,s8,x7/10,pe/g,x5/12,s8,x7/13,pa/h,x2/15,s7,x4/0,s13,pl/g,x13/10,s2,x11/6,s2,pa/h,x5/2,s6,pp/j,x13/14,s1,x15/4,ph/n,s15,x11/8,s13,pi/f,x12/2,s1,x10/11,pn/j,x14/3,s4,x12/7,s10,x10/5,s8,x9/2,s8,x15/12,pp/e,x2/9,s3,x1/5,s10,x2/15,pf/j,s15,x14/1,s6,x5/15,pd/l,x9/7,s2,x6/5,s14,x0/11,pf/g,x7/10,s13,x6/1,s4,x7/5,s7,x1/9,s4,x15/13,s7,x6/9,pa/c,x1/7,pp/m,x9/15,s13,x11/1,pe/b,x13/4,s3,x1/5,pi/k,x14/3,s12,x0/5,pf/n,x11/2,pm/h,x5/9,pk/c,x3/12,s5,x9/11,ph/p,x6/3,s6,pm/i,s15,x4/7,s11,pn/h,s10,x15/6,pk/c,x4/5,s3,x6/0,pl/h,x4/3,s9,x0/12,s3,x3/7,pk/d,x10/9,s4,x8/7,s2,x13/4,s6,x11/5,s15,x9/2,s5,x1/8,ph/n,s5,x2/0,s10,x9/11,s7,x10/4,pe/c,s8,x9/13,s9,x10/11,pm/i,x1/14,s5,x3/10,s1,x14/15,po/h,x2/4,pg/a,s15,x8/1,pl/e,x7/11,po/d,x8/1,s12,x7/5,s4,x11/15,s11,x5/14,pf/g,x1/11,s5,x9/12,pj/e,x0/8,pk/f,x2/12,pp/h,x8/11,s8,x1/4,s1,x5/0,s11,x10/2,pm/i,x14/3,pn/e,x12/11,ph/f,x7/13,pe/o,x14/5,s1,x15/6,ph/b,x1/0,pa/d,x11/3,pn/h,x10/2,po/e,x12/1,s12,x13/6,pk/p,x12/5,pn/g,x8/10,s1,x6/3,s15,x14/8,s10,x4/10,pm/c,s3,x5/12,pi/o,x0/14,s11,pl/b,x7/4,ph/d,x6/2,s1,x7/10,s2,x0/1,s3,x3/14,pb/k,x4/1,pp/o,s4,x3/2,s14,x12/6,s2,x9/3,pj/l,x10/6,s10,x5/1,s10,x11/15,s3,x6/13,s15,x12/8,s10,x3/15,s15,x13/0,s11,x14/8,s6,x7/2,s11,x9/6,s7,x11/10,pi/d,x3/9,s10,x4/12,s12,x14/6,pg/e,x9/3,pm/n,x10/13,pi/l,x15/12,pa/f,x9/0,s7,x6/8,s3,x13/7,s4,x1/14,s6,x0/5,pn/b,x11/4,s12,x5/9,s13,x8/12,s6,x3/6,pg/l,x9/2,po/c,s11,x11/10,s14,x6/5,s8,x9/2,s14,x1/15,s6,x13/4,pe/d,x8/9,s1,x14/12,pl/b,x6/10,s10,x13/15,po/f,x1/0,pc/a,s7,x12/2,s8,x13/15,s10,x12/0,s14,x9/15,s2,x5/14,pj/i,x11/12,s14,x6/1,pe/n,x3/2,s1,x11/6,s7,x0/10,pa/j,x4/5,s14,x14/15,pl/m,x9/4,s3,x1/14,pn/c,x15/13,s11,x12/2,s11,x6/14,pg/p,x5/11,pj/f,x4/3,ph/m,x9/5,s10,x3/6,pf/l,x11/5,pi/h,s8,pc/k,x14/13,s1,x1/2,pl/e,x10/8,s14,x4/15,s6,x5/6,s11,x9/3,pp/f,x2/7,pd/o,x1/10,pb/c,x12/2,s13,x15/7,s4,x4/9,s9,x2/8,pk/d,x15/1,pn/i,x6/3,pp/c,x7/15,s14,x1/2,pe/b,x11/13,pl/m,x10/14,s13,x5/3,pf/p,x6/2,pk/i,x11/4,s15,x0/6,s11,x1/14,s13,x11/8,pd/o,x0/1,s2,x12/5,s14,x3/14,s12,pj/l,x15/13,s10,x4/9,pp/c,x11/2,s13,x12/9,s13,x14/3,pe/j,x11/8,pc/g,x14/10,s12,x13/15,s2,x3/7,s11,ph/i,x8/11,s15,x9/12,s4,x13/0,pc/a,x4/5,s1,x1/15,s3,x2/10,pl/n,s6,pd/o,x13/12,pj/f,x0/14,s2,x2/10,pk/a,x7/1,s10,x9/10,s8,pb/e,s3,x6/15,s6,x12/14,pm/f,x3/5,s12,x6/11,pn/k,s14,x7/3,s12,x9/4,s8,x10/1,s13,x14/9,s13,x6/5,pm/b,s14,x15/14,s8,x12/2,s9,x8/0,pf/i,x1/3,s2,x5/15,pg/e,x14/0,s15,x4/12,pf/d,x7/10,pk/o,x15/9,pp/l,x7/8,s14,x4/13,s12,x0/7,s10,x12/10,pi/c,s10,x11/9,ph/o,x6/2,pn/j,x4/7,pe/f,x10/11,s4,x14/0,s1,x4/13,s7,x5/11,s9,x1/15,pb/m,x14/7,pi/p,x4/11,pc/o,x1/12,s11,x7/3,s12,x5/0,s12,x15/6,pi/p,x1/9,pf/m,x4/7,s2,x5/12,s7,x1/0,pj/e,x9/8,pf/a,x15/6,s5,x5/4,s10,ph/l,x6/7,s8,x15/12,pc/e,x8/4,s15,x9/13,s5,x0/1,pk/h,s4,x12/6,pj/m,x4/15,s2,x10/12,ph/e,s7,x3/15,s4,x2/4,pn/c,x3/13,pb/p,s1,x0/1,s4,pl/n,x4/11,s5,x15/6,s13,x10/14,s3,x15/1,pj/i,x0/10,s3,pm/a,x9/6,po/f,x2/4,pd/p,s13,x15/7,po/c,x0/6,s8,pp/a,x4/7,s4,x0/12,s4,x2/8,s13,pn/i,s2,x14/0,pc/a,x13/2,s1,x4/3,s13,ph/b,s1,x8/6,s1,x12/7,s9,pa/l,x6/3,s4,x14/13,s14,pd/b,x6/2,pl/j,x0/1,s10,x4/8,s8,x6/9,ph/b,x8/11,s10,x7/14,pc/d,x3/5,s8,x7/9,s5,x8/2,pj/f,x14/0,s3,x5/6,pg/o,x4/1,s13,x9/12,pa/b,x1/10,pe/l,x12/5,pk/p,x2/3,s10,pd/e,x8/0,s15,x13/7,s9,x3/8,s3,x6/10,pp/c,x0/2,pd/n,x15/12,s1,x2/9,s9,x12/11,s15,pf/b,x14/4,pp/n,x0/8,s4,x4/11,ph/j,x3/12,pp/i,x1/7,s1,x9/3,s3,x8/7,s12,pk/m,s15,x14/1,pb/g,x6/2,s12,x0/9,s7,po/a,x4/3,s13,x9/7,s1,x3/11,s14,x10/5,s13,x1/6,s7,x0/7,pi/b,x13/14,po/d,x4/0,s3,x8/15,s14,x2/7,pl/m,x9/14,pd/c,s15,x5/1,s1,x10/2,s9,x3/11,s13,x1/14,pa/b,x6/0,ph/p,x4/9,s1,x7/11,s6,x12/8,pm/c,x4/9,pg/j,x1/12,pk/m,x8/4,s3,ph/a,x15/5,pb/n,x7/6,s5,x13/2,s14,x15/0,s2,x14/11,pf/e,x12/0,pn/j,x10/1,pd/a,x12/13,s12,x0/2,s14,x3/15,s4,pc/p,x7/8,pf/o,x9/13,s3,x1/15,s15,x7/0,s14,x13/1,s8,pk/a,x10/2,pd/m,x0/9,s13,x13/5,s2,pc/k,x9/15,pp/d,x1/10,s6,x3/15,s15,x11/1,s13,x3/4,pn/a,x5/9,pl/o,x11/14,pe/m,s15,x15/3,s7,x12/6,pi/h,x11/5,pd/j,x14/1,ph/l,x10/5,s4,x13/3,pb/m,s9,pk/c,s7,x15/11,pm/d,x13/3,s6,x15/0,s9,x3/10,s1,x4/15,pc/h,x5/14,s14,x8/0,s5,pn/a,x15/6,s7,x13/10,s1,x11/2,s1,x13/8,s15,x7/14,pl/m,x9/0,pb/a,x12/8,s5,x4/15,s14,x9/12,pl/f,s5,x2/13,s4,x0/10,s9,x15/12,s7,x2/7,pe/c,x0/13,pl/h,x3/4,s11,x1/15,s4,x0/3,s7,x13/4,s5,x5/7,pb/n,x4/1,pp/l,x3/2,pg/e,x6/14,s3,x9/8,s7,pd/c,x11/15,s15,x6/8,s2,x9/11,s9,pk/a,s1,x8/2,s9,x1/9,pj/g,x12/5,s10,x10/0,pn/i,x8/3,s7,x0/11,s7,x7/5,pg/b,s10,x14/0,s7,pn/e,x8/2,pj/m,x15/6,s13,x12/11,s8,x0/4,s9,x3/9,s2,x1/4,pk/a,x2/8,s14,x0/15,s5,pj/h,x10/1,s14,x8/2,s10,x4/6,pm/f,s13,pp/j,x3/2,s12,pa/f,s14,x13/12,s1,x15/1,pl/n,x0/12,s12,pa/e,x2/9,s11,x0/11,s5,x12/3,s13,x11/6,pk/b,x13/10,s3,x5/9,ph/g,x15/6,s13,x4/3,s7,x2/8,pl/c,x4/1,pn/m,x7/3,s8,x15/0,s9,x3/4,s10,x0/2,s7,x7/8,s7,pk/h,x0/12,pm/j,x15/7,s10,x9/14,pe/c,x0/15,pp/i,x11/7,s14,x5/3,s15,x2/15,s5,x5/7,s7,x1/12,s9,x8/4,pg/j,x9/2,s10,x12/1,s3,x7/6,ph/d,x13/3,pa/k,x14/7,s14,x13/9,pg/l,x11/12,pa/j,s3,x15/7,pm/o,x3/11,s12,x12/14,s12,x10/3,s7,x1/14,s11,x3/15,pn/e,s13,x7/1,s10,x12/11,s13,pb/j,x1/15,pd/c,s5,x9/14,s2,pp/e,x4/7,s5,x11/3,pn/k,x12/10,pd/o,x8/15,s7,pb/k,x0/9,pg/c,s9,pi/p,x4/5,pn/m,x0/8,s6,x1/4,ph/e,s9,x2/3,s5,x13/7,s8,x11/8,pm/i,x12/15,s2,x14/7,pb/p,x0/9,s6,x11/12,s13,x3/7,s8,x4/0,s8,x7/10,s4,x11/9,po/l,x10/4,s15,x12/3,s2,x10/7,pb/f,x15/3,s11,x0/11,pi/k,s11,x3/10,pf/e,x8/11,pl/a,x12/3,pn/i,x10/11,s10,x8/2,s2,x10/15,s12,x9/12,pb/c,s13,x6/0,pa/n,x14/5,ph/l,x7/8,s15,x11/0,pi/b,x3/10,pd/p,x5/1,pc/i,x15/0,s13,x7/8,s10,x11/3,s12,x12/1,pd/n,x4/14,s2,x7/12,s6,x10/15,s7,x12/2,pk/i,s14,x5/9,pp/f,x12/6,s4,x13/11,s4,x9/15,pg/b,x5/6,s5,x9/10,pm/p,x11/15,pa/b,x0/3,s14,x5/6,s8,x4/8,pk/h,x10/0,pl/m,s12,x3/1,s1,x11/7,pc/a,s1,x3/15,pn/h,x11/1,s8,x12/10,pb/j,x14/9,pi/d,x0/1,s4,x4/5,po/h,x6/11,s8,x8/7,s4,pe/m,s2,x13/12,pk/i,s12,pd/c,x4/8,s13,x0/14,s4,x9/11,s10,x2/12,s6,x10/11,s11,x4/7,pj/n,x8/13,s14,x12/15,s4,x1/0,s12,x4/11,pk/g,s14,x5/15,ph/p,x2/3,s9,x9/8,pc/k,x0/14,s15,x3/12,pj/m,x11/1,s11,x7/8,s9,x15/3,s4,x13/4,pk/l,x11/8,s6,x12/4,s3,x10/14,pp/g,s13,x11/12,s4,x2/5,s1,x7/6,s1,x15/1,s15,x12/14,s7,x6/1,pc/d,x3/14,s11,x7/13,s11,x0/12,pj/e,x7/3,s15,x12/1,pp/c,x15/2,s9,x9/14,pg/j,x3/13,pe/b,x14/5,pf/g,x11/8,s10,x12/14,s6,x0/4,s15,pb/o,s13,x11/7,pk/n,s5,x8/4,s1,x1/9,pe/c,x7/8,s10,x3/15,s9,pb/o,x12/6,s4,x4/10,s9,x12/11,s4,x3/1,pi/f,x4/12,s14,x3/10,s3,x8/14,pe/l,x1/4,s5,x2/13,ph/j,s6,x14/1,pf/e,x15/2,s3,x7/3,s13,pc/m,x10/1,ph/l,x13/4,pc/e,s5,x3/6,s7,x14/4,s6,pp/d,s2,x15/6,pf/a,x9/11,pm/o,s1,x12/6,pa/n,x1/10,s15,pk/l,x3/9,s15,x6/7,pg/p,s13,x14/4,pn/o,s11,x3/6,pl/k,x5/9,s4,x0/4,pa/o,x7/1,s15,x13/8,pc/h,x7/9,s8,x14/0,s14,x2/12,s6,x5/6,pa/n,x13/9,pp/h,x8/14,s2,x13/1,s6,x2/14,s15,x10/3,pm/o,x11/6,s11,x7/2,pl/g,s14,pp/f,x4/3,s7,x1/7,pc/j,x0/14,pm/i,x3/15,s15,x13/7,s5,x6/5,s11,x0/11,s4,pf/n,x12/5,s7,x10/2,pb/k,x0/8,pe/p,x11/6,s5,x3/5,s15,x6/15,s2,pg/f,x0/2,pa/p,x10/12,s4,x1/3,s11,x9/5,pn/m,x3/1,s11,x12/15,s7,x14/1,pd/f,x9/3,s2,x1/4,s15,x9/10,pn/i,s4,x0/14,pe/j,x6/15,pd/g,x0/7,pp/e,x6/4,po/m,s12,x9/0,s7,x10/14,s3,x0/4,s4,x1/3,s14,x9/2,pd/l,x3/1,pp/i,x13/15,s2,x5/4,pg/k,x12/1,pd/f,x3/9,s6,x2/11,s2,x13/7,pi/o,x0/11,s4,x2/8,s8,x12/10,pk/c,x8/6,pf/p,s3,x5/13,pn/b,x0/2,s13,x13/15,s4,x6/14,s9,x4/8,pm/p,x13/1,s5,x14/10,s15,x3/7,s11,x2/13,s6,x14/8,pf/c,s7,x10/7,pd/h,x6/5,s15,x4/0,s10,pa/k,x12/9,s11,x10/13,s15,x14/0,pm/d,x6/7,pa/b,x12/5,s6,x7/6,pj/i,s9,x12/9,pc/m,x6/4,pj/l,x15/10,s13,x1/0,s8,x6/5,pb/n,x11/4,pg/j,s4,x0/12,s8,x1/8,pa/n,x3/9,s3,x7/0,s7,pp/d,s2,pm/o,x12/9,s10,x4/11,pk/h,x6/1,s5,x5/11,s5,x6/7,s4,x9/10,pb/c,x11/5,s5,x13/15,s9,x6/9,pp/a,x4/12,s13,x0/2,s6,x12/1,pd/h,x7/14,s14,x1/15,s8,x0/8,s3,x10/9,s13,x4/13,s15,x5/1,pa/n,x11/7,s8,x5/15,pf/k,x2/12,pj/b,x10/0,s15,x12/9,pc/e,x15/5,s3,x13/4,s4,x2/12,pd/n,x3/4,s12,x9/10,s5,x15/8,s5,x4/5,s9,x8/11,s15,x12/5,pl/m,x7/4,s1,pj/n,x10/3,s12,x7/13,s4,x0/12,pk/c,x2/13,pn/b,x3/9,s3,x11/8,s4,x13/2,s3,x8/3,pi/d,x1/13,pk/f,x7/10,s12,x8/14,s2,x11/9,s2,x15/10,s1,x2/4,s4,x6/9,s8,x2/4,s11,x3/11,s12,pj/o,x9/12,s4,x7/8,s5,x10/11,pi/n,x8/9,ph/c,x5/10,s3,x13/4,s1,x1/8,s15,x4/15,s11,x10/8,s5,x9/11,s6,x3/5,s7,x15/1,pd/m,x12/10,po/c,x1/8,ph/f,s9,x11/12,pe/a,x15/5,s3,x1/2,ph/o,x11/9,pb/c,x5/8,s10,x7/2,s9,x14/9,pm/j,x10/3,s9,x15/12,pa/h,x10/0,s6,pb/e,x4/9,s12,x8/15,s12,x6/0,ph/m,x1/13,pa/c,x15/4,po/g,x7/1,pe/j,s5,x3/0,s7,x6/13,pp/c,x14/15,s14,x4/3,s9,x10/15,s13,x3/6,pg/o,x13/7,s7,x12/15,s15,x7/13,pe/d,x8/11,pg/k,x9/13,pm/l,s15,x5/10,s1,x12/8,pa/o,x4/11,s3,x10/1,s14,pg/n,x5/6,pm/h,x2/13,s5,x5/3,po/p,x1/4,pb/f,x2/10,s6,x7/15,s2,x0/8,s6,pm/d,x2/11,ph/g,x4/9,s2,x6/13,po/e,x9/4,ph/n,x0/3,pe/o,x4/6,s3,pf/h,x13/0,pk/j,x7/14,s6,pc/d,s8,x4/8,s10,x7/0,pa/l,x8/15,s14,x1/2,pi/c,x11/8,s9,pf/d,x7/4,s11,x10/15,pn/e,x13/11,pc/l,s14,x4/12,pf/k,s5,x6/7,pb/e,x5/14,s1,x11/13,pj/n,x10/12,pf/a,x7/4,pc/d,x12/13,s4,x8/9,s15,x15/3,po/e,x9/11,s3,x2/4,pp/l,x14/8,s3,x15/2,pe/j,x7/1,s11,pf/k,x14/6,s10,x8/4,s1,x7/11,pn/g,x14/8,s3,x5/12,ph/m,x3/9,s8,pe/k,x15/6,po/a,s9,x9/7,s13,x11/0,pl/p,x13/2,s13,x12/8,s13,x15/14,pf/o,s15,x4/9,pc/k,x11/5,pj/h,x13/0,pi/d,s9,x9/10,s5,pk/h,s2,x13/12,pc/a,x0/15,s4,x4/9,pi/k,x5/8,s2,x15/11,pe/o,x9/14,s1,x1/8,s5,x13/2,pa/k,x6/4,s1,x9/2,pg/p,s4,x6/7,s1,x13/1,pf/e,x14/12,pd/m,x11/10,s1,x12/6,pl/a,x0/10,s6,x14/2,s14,x7/6,s15,pe/n,x15/0,s12,x11/10,pb/h,x1/9,s2,x12/2,s15,x0/1,s7,x11/14,s3,x5/8,pg/l,x2/9,pj/p,s13,x14/10,s5,x9/2,pi/o,s11,x14/11,s8,x8/15,s14,x12/9,s6,x7/11,pn/d,x0/12,s13,x3/4,s9,x9/14,s9,x2/12,pe/a,x5/13,pk/b,x0/1,pd/e,s13,x8/9,s6,x10/7,s15,x2/8,pm/f,x3/5,s5,x4/2,pn/p,x8/3,s7,x12/7,s11,x0/6,pb/k,x4/7,pm/f,s8,x9/11,s13,x0/1,s4,x7/2,po/b,s9,x10/5,pd/a,x11/14,pj/l,x12/7,ph/c,x8/11,s12,x6/3,s11,x2/1,pi/d,s5,x13/9,pf/p,x1/6,s9,x15/10,pj/i,x7/4,s5,x13/5,s8,x10/14,s5,x12/5,s14,x2/4,s9,x5/12,s12,pb/m,x7/1,s4,pd/c,x9/4,s8,x2/7,s6,x11/1,pn/h,x15/3,s15,x1/12,s14,x5/4,pk/c,x1/7,s10,x15/2,pp/n,x0/3,s9,x5/1,pc/j,x3/7,s13,x10/1,pp/i,x15/13,pe/o,x3/1,s5,x2/4,pj/a,x12/15,pk/f,x4/13,s14,x12/1,pm/g,x13/15,pi/b,x5/2,s3,x11/8,s14,x15/6,s2,pp/k,x2/12,pl/n,x0/7,pk/e,x11/8,s10,x14/7,s10,x10/1,s13,x4/6,s10,x13/1,pf/b,x15/6,s4,x9/8,s11,x0/14,s15,x10/9,pg/k,x0/8,pl/p,x2/4,pg/o,x5/11,s14,x9/8,s2,x13/10,s13,x8/9,s3,x14/7,pk/c,s11,pn/e,x15/5,s4,x0/11,ph/a,x2/6,s10,pp/o,x4/14,s15,x3/1,s8,x7/0,s3,x10/12,pk/f,x14/7,s8,x15/4,pn/i,x9/11,pl/k,x3/5,pb/o,x7/6,s7,x5/2,s3,pi/h,x9/6,s13,po/n,x3/5,s6,x11/9,pm/j,x5/7,s1,x2/1,pc/a,x6/7,s15,x2/8,s8,pb/f,x13/12,pn/g,x3/5,pc/l,x13/4,pg/n,x8/0,pf/i,s3,x9/2,pc/d,x13/14,s3,x12/15,s13,x13/6,pk/b,x7/0,pc/g,s6,x11/4,s7,x3/8,s13,x14/12,pn/j,x8/0,s5,x9/13,s10,x10/7,s12,x8/3,pd/c,x2/10,pk/m,x8/7,pl/c,x9/15,s7,x13/6,pb/j,x2/15,pl/k,x13/7,s9,x12/10,ph/f,s8,x9/5,s2,x2/11,s11,x15/8,pk/o,x9/3,s8,pg/b,x4/7,s3,pm/c,x13/12,s3,x14/8,pf/n,x11/10,s10,x1/0")

(def day18
  "set i 31
set a 1
mul p 17
jgz p p
mul a 2
add i -1
jgz i -2
add a -1
set i 127
set p 826
mul p 8505
mod p a
mul p 129749
add p 12345
mod p a
set b p
mod b 10000
snd b
add i -1
jgz i -9
jgz a 3
rcv b
jgz b -1
set f 0
set i 126
rcv a
rcv b
set p a
mul p -1
add p b
jgz p 4
snd a
set a b
jgz 1 3
snd b
set f 1
add i -1
jgz i -11
snd a
jgz f -16
jgz a -19")

(def day19
  "                                                                                         |                                                                                                               
   +-------+                                         +-+ +-------------------------------------------------------------------------+   +-+   +-----------------------------------------+   +-+   +---+   
   |       |                                         | | |                               |                                         |   | |   |                                         |   | |   |   |   
   |       |                                         | +-|---------------+       +---------+   +---------------------------------------|---------------------------+                   |   | |   |   |   
   |       |                                         |   |               |       |       | |   |                                   |   | |   |                     |                   |   | |   |   |   
   |       |                                         |   +-------------------------------|-----------------------------------------|---|-|---|---------------------|-----------------+ |   | |   |   |   
   |       |                                         |                   |       |       | |   |                                   |   | |   |                     |                 | |   | |   |   |   
   |       |                                   +-------+   +-----------+ |       +-----+ | |   |                 +---------------+ |   | |   |     +---------------+       +---------+ |   | |   |   |   
   |       |                                   |     | |   |           | |             | | |   |                 |               | |   | |   |     |                       |           |   | |   |   |   
   |   +---------------------------------------------|-----------------|-|-----+       | | |   |                 +---------------|-|---------------|-------------------+   |           |   | |   |   |   
   |   |   |                                   |     | |   |           | |     |       | | |   |                                 | |   | |   |     |                   |   |           |   | |   |   |   
   +-------|-----+                             |     | |   |     +---------------------|---|---|-----------------------------------|-+ | |   |     |                   |   |           |   | |   |   |   
       |   |     |                             |     | |   |     |     | |     |       | | |   |                                 | | | | |   |     |                   |   |           |   | |   |   |   
   +-+ |   +-----------------------------------------------------|-----|-|-------------|-|-|---|---------------------------------|-|-|-|-|---|-----|-------------------|---|-----+     |   | |   | +---+ 
   | | |         |                             |     | |   |     |     | |     |       | | |   |                                 | | | | |   |     |                   |   |     |     |   | |   | | | | 
   | | |         |     +-----------------------|-----|-----------|-----|-|-----|---------|---------------------------+       +---+ +-|-|-----------|-----------+ +-----+   |     |     |   | |   +-|-|-+ 
   | | |         |     |                       |     | |   |     |     | |     |       | | |   |                     |       |       | | |   |     |           | |         |     |     |   | |     | |   
   | | +---------|-----------------------------------|-----|-----|-----|-----------------|-|-------------------------|-------|---------|-|-+ |     | +-----+   | |         |     |     |   | |     | |   
   | |           |     |                       |     | |   |     |     | |     |       | | |   |                     |       |       | | | | |     | |     |   | |         |     |     |   | |     | |   
   | |           |     |   +-------+           |     | |   |     +-----------------------|-----|-+                   |       |       | | | | |     | |     |   | |         |     |     |   | | +---+ |   
   | |           |     |   |       |           |     | |   |           | |     |       | | |   | |                   |       |       | | | | |     | |     |   | |         |     |     |   | | |     |   
   | |           |     |   |       |           |     | |   |           | |     |       | | |   | |                   |       |       | | | | |     | |     |   | |         |     |     |   | | |     |   
   | |           |     |   |       |           |     | |   |           | |     |       | | |   | |                   |       |       | | | | |     | |     |   | |         |     |     |   | | |     |   
   | |           |     |   |       |           |     | |   | +-----------|-----|-----------|---|-+                   |       |     +---|-------------|---------|-----------------------|---|---|-------+ 
   | |           |     |   |       |           |     | |   | |         | |     |       | | |   |                     |       |     | | | | | |     | |     |   | |         |     |     |   | | |     | | 
   | |           |     |   |       |           |     | |   | |         | |     |       +-|-----------------------------------------|-+ | | | |     | +-----|---|-----+     |     |     |   | | |     | | 
   | |           |     |   |       |           |     | |   | |         | |     |         | |   |                     |       |     |   | | | |     |       |   | |   |     |     |     |   | | |     | | 
   | |       +---------|-----------|-----------|-----|-----|-----------|---------------+ | |   |                     |       |     | +---|-|---------------|---|-|---+     |     +---+ |   | | |     | | 
   | |       |   |     |   |       |           |     | |   | |         | |     |       | | |   |                     |       |     | | | | | |     |       |   | |         |         | |   | | |     | | 
   | |       |   | +---|-------+   +-----------------|-----|-|---------+ |     |       | | |   |                     |       |     | | | | | |   +-------------|-|---------+         | |   | | |     | | 
   | |       |   | |   |   |   |               |     | |   | |           |     |       | | |   |                     |       |     | | | | | |   | |       |   | |                   | |   | | |     | | 
   | |       |   | |   |   |   |               |     | |   | |           |     |       | | |   |                     |       |     | | | | | |   | +---+ +-------|-+                 | |   | | |     | | 
   | |       |   | |   |   |   |               |     | |   | |           |     |       | | |   |                     |       |     | | | | | |   |     | | |   | | |                 | |   | | |     | | 
   | |       |   | |   |   |   |               |     | |   | |         +-------|-------|-|-|-------------+           |       |     | | | | | | +-|-+   | | |   | | +-+ +-------+   +-------|---|---+ | | 
   | |       |   | |   |   |   |               |     | |   | |         | |     |       | | |   |         |           |       |     | | | | | | | | |   | | |   | |   | |       |   | | |   | | |   | | | 
   | |       |   | |   |   |   |               |     | |   | |         | |     +-----+ | +---------------|-------------------|-----------|---|-|-|-|-------|---|---+ +---------------+ |   | | |   | | | 
   | |       |   | |   |   |   |               |     | |   | |         | |           | |   |   |         |           |       |     | | | | | | | | |   | | |   | | |   |       |   |   |   | | |   | | | 
   | |       | +-|-----|-------------------+   |     | |   | |         | |           | |   |   |         |   +-------|-------------|---|-|-|---+ | |   | | |   | | |   |       +-----------|---|-+ | | | 
   | |       | | | |   |   |   |           |   |     | |   | |         | |           | |   |   |         |   |       |       |     | | | | | |   | |   | | |   | | |   |           |   |   | | | | | | | 
   | |       | | | |   | +---------------------+     | |   | |       +-+ |         +-------|---|-----------------+   |       |     | | | | | |   | |   | | |   | | |   |           |   |   | | | | | | | 
   | |       | | | |   | | |   |           |         | |   | |       |   |         | | |   |   |         |   |   |   |       |     | | | | | |   | |   | | |   | | |   |           |   |   | | | | | | | 
   | |       | | | |   | | |   |     +-----|------L--|-------------------|-------------|---|---------------------|---|-------|---+ | | | | | | +-------|-+ |   | | |   |       +-----------+ | | | | | | 
   | |       | | | |   | | |   |     |     |         | |   | |       |   |         | | |   |   |         |   |   |   |       |   | | | | | | | | | |   |   |   | | |   |       |   |   |     | | | | | | 
   | |   +---------------------------------|-----------|---|-----------------------|-------|---|---------|---|---------------|---------|-|---|---|-------------|-|-|---+       |   |   |     | | | | | | 
   | |   |   | | | |   | | |   |     |     |         | |   | |       |   |         | | |   |   |         |   |   |   |       |   | | | | | | | | | |   |   |   | | |           |   |   |     | | | | | | 
   | |   |   | | +-------+ +---|-----|-----|-----------------|-----------+     +---------------|-------------|---------------|-----|-------|---|-+ |   |   |   | | |           |   +-+ |     | | +-+ | | 
   | |   |   | |   |   |       |     |     |         | |   | |       |         |   | | |   |   |         |   |   |   |       |   | | | | | | | |   |   |   |   | | |           |     | |     | |     | | 
   | |   +---|-----|---|---+   |     |     |         | |   | |       |         |   | | |   |   |         |   |   |   |       |   | | | | | | | |   |   |   |   | | |           |     | |     | |     | | 
   | |       | |   |   |   |   |     |     |         | |   | |       |         |   | | |   |   |         |   |   |   |       |   | | | | | | | |   |   |   |   | | |           |     | |     | |     | | 
   | |       | +---+ +---+ | +-|-----|-----|---------------|-|-------------------------|---|---------------------------------|-----|-|-----|-|-|---|-------|-------|-----------|-----|---------------+ | 
   | |       |       | | | | | |     |     |         | |   | |       |         |   | | |   |   |         |   |   |   |       |   | | | | | | | |   |   |   |   | | |           |     | |     | |       | 
   | |       |       | | | | | |     |     |         | |   | |       |         |   | | |   +---|---------|-------|---|-+     |   | | | | | | | |   |   |   |   | | | +-+       |   +---|-----|-+       | 
   | |       |       | | | | | |     |     |         | |   | |       |         |   | | |       |         |   |   |   | |     |   | | | | | | | |   |   |   |   | | | | |       |   | | |     |         | 
   | |       |       | | | | | |     |     |         | |   | |       |         |   | | +-------------------------|---|-|---------|-|---|-|-|-|-|-----------|---|---|---|-------|---|-|-|-----|-+       | 
   | |       |       | | | | | |     |     |         | |   | |       |         |   | |         |         |   |   |   | |     |   | | | | | | | |   |   |   |   | | | | |       |   | | |     | |       | 
   | |       +-+     | | | | | |     |     |         | +-----+       |         |   | |         |         +-------------|-----|-----|-|-------|-----|-----------|-+ | | |       |   | | |     | |       | 
   | |         |     | | | | | |     |     |         |     |         |         |   | |         |             |   |   | |     |   | | | | | | | |   |   |   |   |   | | |       |   | | |     | |       | 
   | |         |     | | | | | |     |     |         |     |         |         |   | |         |             |   |   | |     |   | | | | | | | |   |   |   +---|---|---|-----------+ | |     | |       | 
   | |         |     | | | | | |     |     |         |     |         |         |   | |         |             |   |   | |     |   | | | | | | | |   |   |       |   | | |       |     | |     | |       | 
   | |         |     | | | | | |   +-|-----+   +-----|-----|-------------------|-----------------------------|-------|-|-----|---|---|---|-------------|-------|---|-|---------|-+   | |     | | +-+   | 
   | |         |     | | | | | |   | |         |     |     |         |         |   | |         |             |   |   | |     |   | | | | | | | |   |   |       |   | | |       | |   | |     | | | |   | 
   | |         |     | | | | | |   | |         |     |     |         |         |   | |   +-------------------|---------------------|---|---|-|-|-------|-------|-----|---------|-----|-----------+ |   | 
   | |         |     | | | | | |   | |         |     |     |         |         |   | |   |     |             |   |   | |     |   | | | | | | | |   |   |       |   | | |       | |   | |     | |   |   | 
 +---|---------------|---|-|-|-|---|-|---------|-----|---------------|---------|---------|-------------------|---|-----|-------+ | | | | | | | |   |   |       | +-|-|-----+   | |   | |     | |   |   | 
 | | |         |     | | | | | |   | |         |     |     |         |         |   | |   |     |             |   |   | |     | | | | | | | | | |   |   |       | | | | |   |   | |   | |     | |   |   | 
 | | |         |     | | | | | |   | |         |     |     |         |   +-----|---------------|-----------------|---|-|-------|-|---|-+ | | +-----|-----+     | | | | |   |   | |   | |     | |   |   | 
 | | |         |     | | | | | |   | |         |     |     |         |   |     |   | |   |     |             |   |   | |     | | | | |   | |   |   |   | |     | | | | |   |   | |   | |     | |   |   | 
 | | |         |     | | | | | |   | |     +---------|-----|-------------|-----|---------|-----|-----------------|---|---------|-----|---------|---|-----------|-+ | | |   |   | |   | |     | |   +-+ | 
 | | |         |     | | | | | |   | |     |   |     |     |         |   |     |   | |   |     |             |   |   | |     | | | | |   | |   |   |   | |     |   | | |   |   | |   | |     | |     | | 
 | | |         |     | | | | | |   | |     |   |     |     |     +-----------------------|-------------------|---|---|-|-------|-------------------|-----|-------------|-------+ |   | |   +-----+   | | 
 | | |         |     | | | | | |   | |     |   |     |     |     |   |   |     |   | |   |     K             |   |   | |     | | | | |   | |   |   |   | |     |   | | |   |     |   | |   | | | |   | | 
 | | |         | +-------+ | | | +---|---------|-----------|-------------------------|--W--------------------|-------|-|-----|-|-|-|-----|-|-+ |   |   | |     |   | | |   |     |   | |   | | | |   | | 
 | | |         | |   | |   | | | | | |     |   |     |     |     |   |   |     |   | |   |     |             |   |   | |     | | | | |   | | | |   |   | |     |   | | |   |     |   | |   | | | |   | | 
 | | |         | |   | |   | | | | | |     |   |     | +---|-----|---|---------|---|-|---|---------------+   |   |   | |     | | | | |   | | | |   |   | |     |   | | |   |     |   | |   | | | |   | | 
 | | |         | |   | |   | | | | | |     |   |     | |   |     |   |   |     |   | |   |     |         |   |   |   | |     | | | | |   | | | |   |   | |     |   | | |   |     |   | |   | | | |   | | 
 | | |         | |   | |   | | | | | |     |   |     | |   |     |   |   |     |   | |   |     |         |   |   |   | |     | | | | |   | | | |   |   | |     |   | | |   |     |   | |   | | | |   | | 
 | | |         | |   | |   | | | | | |     |   |     | |   |     |   |   |     |   | |   |     |         |   |   |   | |     | | | | |   | | | |   |   | |     |   | | |   |     |   | |   | | | |   | | 
 | | |         | |   | |   +-|-|---|-|---------|-----|-----------|---|-------------|-|---|-----|-----------------|---|-|-------------------|-|-------+ | |     |   | +-|---|-----|---|-|---|---|---+ | | 
 | | |         | |   | |     | | | | |     |   |     | |   |     |   |   |     |   | |   |     |         |   |   |   | |     | | | | |   | | | |   | | | |     |   |   |   |     |   | |   | | | | | | | 
 | | |         +-+   | |   +-|---|---------|---------|-|---|-----|---|---|-----|---------|-----|-----------------------|-------|---|-|---|-|-|-|-----|---|-----|-------|---+     |   | |   | | | | | | | 
 | | |               | |   | | | | | |     |   |     | |   |     |   |   |     |   | |   |     |         |   |   |   | |     | | | | |   | | | |   | | | |     |   |   |         |   | |   | | | | | | | 
 | | |               | | +---|---|-------------------------|-----|---|---------|-------------------------+   |   |   | |     | | | | |   | | | +-----|-|-|-----|-------|-----+   |   | |   | | | | | | | 
 | | |               | | | | | | | | |     |   |     | |   |     |   |   |     |   | |   |     |             |   |   | |     | | | | |   | | |     | | | |     |   |   |     |   |   | |   | | | | | | | 
 | | |               | | | | | | | | |     |   |     | |   |     |   |   |     |   | |   |     | +-----------|-----------------|-|-|-|-+ | | |     | | | |     |   |   |     |   |   | |   | | | | | | | 
 | | |               | | | | | | | | |     |   |     | |   |     |   |   |     |   | |   |     | |           |   |   | |     | | | | | | | | |     | | | |     |   |   |     |   |   | |   | | | | | | | 
 | | |               | | | | | | | | |     |   |     | |   |     |   |   |     |   +-----+     | |           |   |   | |     | | | | | | | | |     | | | |     |   |   |     |   |   | |   | | | | | | | 
 | | |               | | | | | | | | |     |   |     | |   |     |   |   |     |     |         | |           |   |   | |     | | | | | | | | |     | | | |     |   |   |     |   |   | |   | | | | | | | 
 | | |           +---|-|---|---------------|---------|-|---|-----|---|---|-----|-----|---+     | |           |   |   | |     | | | | | | | | |     | | | |     |   +---------------+ | |   | | | | | | | 
 | | |           |   | | | | | | | | |     |   |     | |   |     |   |   |     |     |   |     | |           |   |   | |     | | | | | | | | |     | | | |     |       |     |   | | | |   | | | | | | | 
 | | |           |   +-------|-+ | | |     |   |     | |   |     |   |   |     |     |   |     | |           |   |   | |     | | | | | | | | |     | | | |     |       |     |   | | | |   | | | | | | | 
 | | |           |     | | | |   | | |     |   |     | |   |     |   |   |     |     |   |     | |           |   |   | |     | | | | | | | | |     | | | |     |       |     |   | | | |   | | | | | | | 
 | | |     +-----------|-|-|-----|---------|---|---------------------|---|-----------|---|-------|-------------------|-+     | | | | | | | | |     | | | |     |       |     |   | | | |   | | | | | | | 
 | | |     |     |     | | | |   | | |     |   |     | |   |     |   |   |     |     |   |     | |           |   |   |       | | | | | | | | |     | | | |     |       |     |   | | | |   | | | | | | | 
 | | |     |   +-|-----|-|-|---------|---------------|-----|-----|---|---------|-----|---|-------|-------------------|-----------|-----|-|---|-----|---|-|-----|-------|---------+ | | |   | | | | +-+ | 
 | | |     |   | |     | | | |   | | |     |   |     | |   |     |   |   |     |     |   |     | |           |   |   |       | | | | | | | | |     | | | |     |       |     |     | | |   | | | |     | 
 | | +---------|---------|---+   | | |     |   |     | |   |     |   |   |     |     |   |     | |           |   |   |       | | | | | | | | |     | | | |     |       |     |     | | |   | | | |     | 
 | |       |   | |     | | |     | | |     |   |     | |   |     |   |   |     |     |   |     | |           |   |   |       | | | | | | | | |     | | | |     |       |     |     | | |   | | | |     | 
 | | +-----|---|-+   +-|---+     | | |     |   |     | |   |     +-------|---------------------|-|-------+   |   |   |       | | | | | | | | |     | | | |     |       |     |     | | |   | | | |     | 
 | | |     |   |     | | |       | | |     |   |     | |   |         |   |     |     |   |     | |       |   |   |   |       | | | | | | | | |     | | | |     |       |     |     | | |   | | | |     | 
 | | |     |   |     +-|-----+   | | |     |   |     | |   |         |   |     |     |   |     | |       |   |   |   |       | +-|-|-+ | +---------|---|---+   |       |     |     | | |   | | | |     | 
 | | |     |   |       | |   |   | | |     |   |     | |   |         |   |     |     |   |     | |       |   |   |   |       |   | |   |   | |     | | | | |   |       |     |     | | |   | | | |     | 
 | | +-----------------|-|-------|-+ |     |   |     | |   |         |   |     |     |   |     | |       | +-|-------|-----------|---------|-|---------|---+   |       |     |     | | |   | | | |     | 
 | |       |   |       | |   |   |   |     |   |     | |   |         |   |     |     |   |     | |       | | |   |   |       |   | |   |   | |     | | | |     |       |     |     | | |   | | | |     | 
 | |       |   |       | |   |   |   |     |   |     | |   |         |   |     +-----|---|---------------------------|-------------+   |   | |     | | | |     |       |     |     | | |   | | | |     | 
 | |       |   |       | |   |   |   |     |   |     | |   |         |   |           |   |     | |       | | |   |   |       |   |     |   | |     | | | |     |       |     |     | | |   | | | |     | 
 | | +-----|---|-------|-|---|-------|---------|-----|-|---|-------------|-----------------------------------|---------------|---------|-------------+ | |     |       |     |     | | |   | | | |     | 
 | | |     |   |       | |   |   |   |     |   |     | |   |         R   |           |   |     | |       | | |   |   |       |   |     |   | |     |   | |     |       |     |     | | |   | | | |     | 
 | | |     |   |     +-|-|-------|---------|---|-------|---------------------------------------|-|-------|-|-|---------------|---|---------|-|-----|---|---------------|-+   |     | | |   | | | |     | 
 | | |     |   |     | | |   |   |   |     |   |     | |   |         |   |           |   |     | |       | | |   |   |       |   |     |   | |     |   | |     |       | |   |     | | |   | | | |     | 
 | | |     |   |     +-|-|---|-------------|---|-------|-------------+   |           |   |   +-|-|-------|-|-|---|---------------------|---+ |     |   | |     |       | |   |     | | |   | | | | +-+ | 
 | | |     |   |       | |   |   |   |     |   |     | |   |             |           |   |   | | |       | | |   |   |       |   |     |     |     |   | |     |       | |   |     | | |   | | | | | | | 
 | +-|-----|-------------------------------|---|-----|-|---------------------------------|---+ | | +-----|-|-|---|---|-----------------|-----------+   | |     |       | |   |     | | |   | | | | | | | 
 |   |     |   |       | |   |   |   |     |   |     | |   |             |           |   |     | | |     | | |   |   |       |   |     |     |         | |     |       | |   |     | | |   | | | | | | | 
 |   |     |   | +-----------|---|---|-----|-----------|---|-------------|-----------|---|-----+ | +-------|-|---|---|---------------------------------|-------|-----+ | | +---+   | | |   | | | | | | | 
 |   |     |   | |     | |   |   |   |     |   |     | |   |             |           |   |       |       | | |   |   |       |   |     |     |         | |     |     | | | | | |   | | |   | | | | | | | 
 |   |     |   | |     | |   |   |   |     |   |     | |   |             |           |   |   +-------------------------------|---|---------------+ +-+ | |     |     | | | | | |   | | |   | | | | | | | 
 |   |     |   | |     | |   |   |   |     |   |     | |   |             |           |   |   |   |       | | |   |   |       |   |     |     |   | | | | |     |     | | | | | |   | | |   | | | | | | | 
 |   |     |   | |     | |   |   |   |     |   |     | |   |             |           |   |   |   |       | | |   |   |       |   |     |     | +---|-+ | |     |     | | | | | |   | | |   | | | | | | | 
 |   |     |   | |     | |   |   |   |     |   |     | |   |             |           |   |   |   |       | | |   |   |       |   |     |     | | | |   | |     |     | | | | | |   | | |   | | | | | | | 
 |   |     |   | |     | |   |   | +-----------------|-|---|-------------+ +-+       |   |   |   |       | | |   |   |       |   |     |     | | | |   | |     |     | | | | | |   | | |   | | | | | | | 
 |   |     |   | |     | |   |   | | |     |   |     | |   |               | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   | |     |     | | | | | |   | | |   | | | | | | | 
 |   |     |   | |     | |   +-----|-----------|-----|-----------+         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   | |     |     | | | | | |   | | |   | | | | | | | 
 |   |     |   | |     | |       | | |     |   |     | |   |     |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   | |     |     | | | | | |   | | |   | | | | | | | 
 | +-|-----|---------+ | |       | | |     |   |     | |   |     |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   | |     |     | | | | | |   | | | +-+ | | | | | | 
 | | |     |   | |   | | |       | | |     |   |     | |   |     |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   | |     |     | | | | | |   | | | |   | | | | | | 
 | | |     |   | |   | | |       | | |     |   |     | |   |     |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   | |     |     | | | | | |   | | | |   | | | | | | 
 | | |     |   | |   | | |       | | |     |   |     | |   |     |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   | |     |     | | | | | |   | | | |   | | | | | | 
 +---|-----|-----|-----|-|-------+ | |     |   |     | |   |   +-|---------|-|-----------|-------|-------|-------|---|-------|---|-----|---------|-----|-+     | +-+ | | +-+ | |   | | | |   +-|-+ | | | 
   | |     |   | |   | | |         | |     |   |     | |   |   | |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   |       | | | | |     | |   | | | |     |   | | | 
 +---|-----|-----+   | | |         | | +---|---|-------|---|-----|---------|-|---------------|---|-------|-|-|---|---------------|-----|-----|-|-------|-------|---|-----+   | |   | | | |     |   | | | 
 | | |     |   |     | | |         | | |   |   |     | |   |   | |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   |       | | | | | |   | |   | | | |     |   | | | 
 | | |     |   |     | | |         | | |   |   |     | |   |   | |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   |       | | | | | |   | |   | | | |     |   | | | 
 | | |     |   |     | | |         | | |   |   |     | |   |   | |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   |       | | | | | |   | |   | | | |     |   | | | 
 | | |     |   |     | | |         | | |   |   |     | |   |   | |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   |       | | | | | |   | |   | | | |     |   | +-+ 
 | | |     |   |     | | |         | | |   |   |     | |   |   | |         | |       |   |   |   |       | | |   |   |       |   |     |     | | | |   |       | | | | | |   | |   | | | |     |   |     
 | | |     |   |     | | |         | | |   |   |     | |   |   +-|---------|-------+ |   | +-|---|-------|-------|-----------|---------|-----+ | | |   |       | | | | | |   | |   | | | |     | +-|---+ 
 | | |     |   |     | | |         | | |   |   |     | |   |     |         | |     | |   | | |   |       | | |   |   |       |   |     |       | | |   |       | | | | | |   | |   | | | |     | | |   | 
 | | |     |   |     | | |         | | |   |   |     | |   |     |         | |     | |   | | |   |       | | |   |   |       |   |     |       +-|-|-------+   | | | | | +---------|-----|-----|-+ |   | 
 | | |     |   |     | | |         | | |   |   |     | |   |     |         | |     | |   | | |   |       | | |   |   |       |   |     |         | |   |   |   | | | | |     | |   | | | |     |   |   | 
 | | |     |   |     | | |         | | |   | +---------|---------|---------|--Q------|-------------------|-----------------------------|---------|-----|---|---|---|-|---+   | |   | | | |     |   |   | 
 | | |     |   |     | | |         | | |   | | |     | |   |     |         | |     | |   | | |   |       | | |   |   |       |   |     |         | |   |   |   | | | | | |   | |   | | | |     |   |   | 
 | | |     |   |   +-|-----------------|-----|-|-----------------|---------+ |     | |   | | |   |       | | |   |   |       |   |     |         | |   |   |   | | | | | |   | |   | | | |     |   |   | 
 | | |     |   |   | | | |         | | |   | | |     | |   |     |           |     | |   | | |   |       | | |   |   |       |   |     |         | |   |   |   | | | | | |   | |   | | | |     |   |   | 
 | | |     |   |   | | | |         +---|---|-|-+     | |   |     |           |     +-----|-|-|-+ |       | | |   |   |       |   |     |         | |   +---|-------|---+ |   | |   | | | |     |   |   | 
 | | |     |   |   | | | |           | |   | |       | |   |     |           |       |   | | | | |       | | |   |   |       |   |     |         | |       |   | | | |   |   | |   | | | |     |   |   | 
 | | |     +-----------|-|-----------|-|-+ | |       | +---|-----|-----------|-------|-------------------|-------------------|---------|-------------------|---|-|-------+   | |   +-|-+ |     |   |   | 
 | | |         |   | | | |           | | | | |       |     |     |           |       |   | | | | |       | | |   |   |       |   |     |         | |       |   | | | |       | |     |   |     |   |   | 
 | | |         |   | | | |           | | +-----------------|-----|-----------------------|-|-----+       +----I--|-----------|-----------------------------|-----|-+ |       | |     |   |     |   |   | 
 | | |         |   | | | |           | |   | |       |     |     |           |       |   | | | |           | |   |   |       |   |     |         | |       |   | |   |       | |     |   |     |   |   | 
 | | |         |   | | | |           | |   | |       |     |     |           |       |   | | | |           | |   |   |       |   |   +-|-----------|---------------+ |       | |     |   |     |   |   | 
 | | |         |   | | | |           | |   | |       |     |     |           |       |   | | | |           | |   |   |       |   |   | |         | |       |   | | | |       | |     |   |     |   |   | 
 | | | +-+     | +---|---|-----------|-----|-|-------------|-----|-----------|---------------|-------------|---------+       |   |   | |         | |       |   | | | |       | |     |   |     |   |   | 
 | | | | |     | | | | | |           | |   | |       |     |     |           |       |   | | | |           | |   |           |   |   | |         | |       |   | | | |       | |     |   |     |   |   | 
 | | | | |     | | | | +-|-----------|-|---|-|-------------|-------------------------|---|-|-|-|---------+ | |   |           |   |   | |         | +-------|---|-|---|-------|-|---+ |   |     |   |   | 
 | | | | |     | | | |   |           | |   | |       |     |     |           |       |   | | | |         | | |   |           |   |   | |         |         |   | | | |       | |   | |   |     |   |   | 
 | | | | |     | | | |   |       +---+ |   | |       |     |     |           |       |   | | | |         | | |   |           |   |   | |         |         | +-----+ |       | |   +-|-----------------+ 
 | | | | |     | | | |   |       |     |   | |       |     |     |           |       |   | | | |         | | |   |           |   |   | |         |         | | | |   |       | |     |   |     |   |     
 | | | | |     +-+ +-------------|---+ |   | |       |     |     |           |       |   | | | |         | | |   |           |   |   | +---------------------+ | |   |       | |   +-----+     |   |     
 | | | | |           |   |       |   | |   | |       |     |     |           |       |   | | | |         | | |   |           |   |   |           |         |   | |   |       | |   | |         |   |     
 | | | | |           |   |       |   | +-+ | |       |     |     |           |       |   | | | |         | | |   |           |   |   |           |         |   | |   |       | |   | |         |   |     
 | | | | |           |   |       |   |   | | |       |     |     |           |       |   | | | |         | | |   |           |   |   |           |         |   | |   |       | |   | |         |   |     
 | | | | |           |   |       |   |   | +---------|---+ |     |           |       |   | | | |         | | |   |           |   |   |           |         |   | |   |       | |   | |         |   |     
 | | | | |           |   |       |   |   |   |       |   | |     |           |       |   | | | |         | | |   |           |   |   |           |         |   | |   |       | |   | |         |   |     
 | | | | |           |   |       |   |   |   |       |   | |     |           |       |   | | | |         | | |   |           |   |   |           |         |   | |   |       | |   | |         |   |     
 | | | | |           |   |       |   |   |   |       |   | |     |           |       |   | | | |         | | |   |           |   |   |           |         |   | |   |       | |   | |         |   |     
 | | | | |           |   |       |   |   |   |       |   | |     |           |       |   | | | |         | | |   |           |   |   |           |         |   | |   |       | |   | |         |   |     
 | | | | |           |   |       |   |   |   |       |   | |     |           |       |   | | | |         | | |   |           |   |   |           |         |   | |   |       | |   | |         |   |     
 | | | | | +---------|---|-------|-----------|-----------|-|-----|-----------|-------+   | | | |   +-------|-----|-----------|-------|-----------|---------|-----|---+       | |   | |         |   |     
 | | | | | |         |   |       |   |   |   |       |   | |     |           |           | | | |   |     | | |   |           |   |   |           |         |   | |           | |   | |         |   |     
 | | | | | | +-------+   |       |   |   |   |       |   | |     +-----------|-----------|-----|---|-----|-|-----|-----+     +---|---------------|---------+   +-------------|-----+ |         |   |     
 | | | | | | |           |       |   |   |   |       |   | |                 |           | | | |   |     | | |   |     |         |   |           |               |           | |     |         |   |     
 | | | | | | |           |       +-+ |   |   |       |   | |                 |           | | | |   |     | | |   |     |         |   |           |               |         +---|-+   |         |   |     
 | | | | | | |           |         | |   |   |       |   | |                 |           | | | |   |     | | |   |     |         |   |           |               |         | | | |   |         |   |     
 | | | | | | |     +---------------------|---|-------|-------------------------------------|-------|-------|-+   |     |         |   |           |               |         | | | |   |         |   |     
 | | | | | | |     |     |         | |   |   |       |   | |                 |           | | | |   |     | |     |     |         |   |           |               |         | M | |   |         |   |     
 +-+ | +---|-------------------------|---|---|-------------------------------------------|---|-------------------------------------------+       |           +-------+     | | | |   |         |   |     
     |   | | |     |     |         | |   |   |       |   | |                 |           | | | |   |     | |     |     |         |   |   |       |           |   |   |     | | | |   |         |   |     
   +---------+     |     |         | |   |   |       +-----------------------|-----------+ | | |   |     | |     |     |         |   |   |       |           |   |   |     | | | |   |         |   |     
   | |   | |       |     |         | |   |   |           | |                 |             | | |   |     | |     |     |         |   |   |       |           |   |   |     | | | |   |         |   |     
   | |   | |       |     |         | |   |   |           | |                 |             +-|-|---|-----|-+     |     |         |   |   |       |           |   |   |     | | | |   |         |   |     
   | |   | |       |     |         | |   |   |           | |                 |               | |   |     |       |     |         |   |   |       |           |   |   |     | | | |   |         |   |     
   | |   | |       +-----------------+   |   |           | |   +-----------------------------|-----|-----|-------|-----+         |   |   |       |           |   |   |     | | | |   |         |   |     
   | |   | |             |         |     |   |           | |   |             |               | |   |     |       |               |   |   |       |           |   |   |     | | | |   |         |   |     
 +-+ |   | |             |         |     |   |           +-----|-------------|---------------|-----------|---+   |               |   +---|-------|---------------|---+   +-|-|---+   |         |   |     
 |   |   | |             |         |     |   |             |   |             |               | |   |     |   |   |               |       |       |           |   |       | | | |     |         |   |     
 | +-|-----|-------------|---------|---------|-------------+   |             |               | +---|-----|---|-------------------+       |       |           +---|---------+ | |     |         |   |     
 | | |   | |             |         |     |   |                 |             |               |     |     |   |   |                       |       |               |       |   | |     |         |   |     
 | | |   +-|-----------------------|-+   +---|-----------+     |             |               +-----|-------------------------------------|-------|-----------------------|-----------+         |   |     
 | | |     |             |         | |       |           |     |             |                     |     |   |   |                       |       |               |       |   | |               |   |     
 | | |     | +---------+ |         | +---------------------------------------|--Y------------------|-----|---|-----------------------------------|-------------------+   |   +-----------------+   |     
 | | |     | |         | |         |         |           |     |             |                     |     |   |   |                       |       |               |   |   |     |                   |     
 | | |     | |         | |         |         |           +-----------------------------------------|---------+   |                       |       |               |   |   |     |                   |     
 | | |     | |         | |         |         |                 |             |                     |     |       |                       |       |               |   |   |     |                   |     
 | | |     +-----------|-|---------|---------|-------------+ +-+             +---------------------------------------------------------------------------------------+ +-+     |                   |     
 | | |       |         | |         |         |             | |                                     |     |       |                       |       |               |     |       |                   |     
 | | |       |         +-+         |         |             | +-+ +---------------------------------+     +-------------------------------|-------|---------------+     |       |                   |     
 | | |       |                     |         |             |   | |                                               |                       |       |                     |       |                   |     
 | | |       +-+                   +-----------------------|---|---------------------------------------------------------------------------------|---------------------|-------|-+                 |     
 | | |         |                             |             |   | |                                               |                       |       |                     |       | |                 |     
 +-+ +---------+                             +-------------+ P-+ +-----------------------------------------------+                       +-------+                     +-------+ +-----------------+     ")
