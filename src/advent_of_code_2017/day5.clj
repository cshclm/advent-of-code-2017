(ns advent-of-code-2017.day5
  (:require
   [clojure.string :as s]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [criterium.core :refer [quick-bench]]))

(defn read-input
  [input]
  (mapv edn/read-string (s/split input #"\s+")))

(defn part1
  [input]
  (loop [i input pos 0 steps 0]
    (if (or (>= pos (count i)) (< pos 0))
      steps
      (recur
       (update i pos inc)
       (+ pos (nth i pos))
       (inc steps)))))

(defn part2
  [input]
  (loop [i input pos 0 steps 0]
    (if (or (>= pos (count i)) (< pos 0))
      steps
      (let [offset (nth i pos)]
        (recur
         (update i pos (if (>= offset 3) dec inc))
         (+ pos offset)
         (inc steps))))))

(defn solve
  []
  ((juxt part1 part2) (read-input data/day5)))

(comment
  (let [i (read-input data/day5)]
    (quick-bench (part1 i)) ;; 128ms
    (quick-bench (part2 i)))) ;; 8.8ms
