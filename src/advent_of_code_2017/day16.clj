(ns advent-of-code-2017.day16
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [advent-of-code-2017.util :as util]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]))

(def readers
  {\x (fn [args]
        (map #(Integer/parseInt %) args))
   \s (fn [args]
        (first (map #(Integer/parseInt %) args)))
   \p (fn [args]
        (map first args))})

(defn read-input
  [input]
  (reduce
   (fn [acc i]
     (conj acc [(first i) ((readers (first i)) (second i))]))
   []
   (map #(vector (first %)
                 (cstr/split (apply str (rest %)) #"/"))
        (cstr/split input #","))))

(defn exchange
  [progs pa pb]
  (let [va (get progs pa)]
    (-> progs
        (assoc pa (get progs pb))
        (assoc pb va))))

(def mods
  {\x (fn [progs [pa pb]] (exchange progs pa pb))
   \s (fn [progs n]
        (let [headn (- (count progs) n)
              move (drop headn progs)]
          (into [] (concat move (take headn progs)))))
   \p (fn [progs [pa pb]]
        (let [a (.indexOf progs pa)
              b (.indexOf progs pb)]
          (exchange progs a b)))})

(defn part1
  [input]
  (fn [order]
    (reduce (fn [acc i]
              ((mods (first i)) acc (second i)))
            order
            input)))

(defn- find-cycle
  [reducer start]
  (reduce (fn [acc iter]
            (let [v (reducer (:val acc))]
              (if-let [seen-iter (get (:seen acc) v)]
                (reduced {:pos (inc iter)
                          :cycle-at seen-iter
                          :val v
                          :found true})
                (-> acc
                    (update :seen #(assoc % v (inc iter)))
                    (assoc :val v)))))
          {:seen {start 0}
           :val start}
          (range)))

(defn- solve-reduced
  [reducer n cycle-desc]
  (let [cycles (- (:pos cycle-desc) (:cycle-at cycle-desc))
        min-iter (mod (- n (:cycle-at cycle-desc)) cycles)]
    (reduce (fn [acc _] (reducer acc))
            (:val cycle-desc)
            (range min-iter))))

(defn part2
  [input programs n]
  (let [input (read-input input)
        reducer (part1 input)
        start (into [] programs)
        cycle-desc (find-cycle reducer start)]
    (apply str (if (:found cycle-desc)
                 (solve-reduced reducer n cycle-desc)
                 (:val cycle-desc)))))
