(ns advent-of-code-2017.day13
  "http://adventofcode.com/2017/day/13"
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [criterium.core :refer [quick-bench]]))

(defn- read-input
  [input]
  (let [ranges (->> (cstr/split-lines input)
                    (map #(mapv edn/read-string (cstr/split % #":\s*")) )
                    (into {}))]
    {:ranges ranges
     :steps 0
     :times-caught 0
     :severity 0}))

(defn- move
  [state]
  (update state :depth (fnil inc -1)))

(defn- scan
  [state]
  (let [d (:depth state)
        r (get-in state [:ranges d])]
    (if (and r (zero? (mod (:steps state) (* 2 (dec r)))))
      (update (update state :severity + (* d r))
              :times-caught inc)
      state)))

(defn- step
  [state]
  (update state :steps inc))

(defn- traverse
  [input delay key-range]
  (reduce (fn [acc n] (->> acc move scan step))
          (assoc input :steps delay)
          key-range))

(defn part1
  [input]
  (let [key-range (range (inc (apply max (-> input :ranges keys))))]
    (:severity (traverse input 0 key-range))))

(defn part2
  [input]
  (let [key-range (range (inc (apply max (-> input :ranges keys))))]
    (->> (range)
         (drop-while #(> (:times-caught (traverse input % key-range)) 0))
         first)))

(comment
  (quick-bench (part1 (read-input data/day13))) ;; 238 µs
  (quick-bench (part2 (read-input data/day13)))) ;; <long>
