(ns advent-of-code-2017.util)

(defn padded-binary
  [len n]
  (let [bn (Integer/toBinaryString n)]
    (if (= (count bn) len)
      bn
      (str (format (str "%0" (- len (count bn)) "d") 0) bn))))
