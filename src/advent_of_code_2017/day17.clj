(ns advent-of-code-2017.day17
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [advent-of-code-2017.util :as util]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]))

(defn read-input
  [input]
  nil)

(defn insert
  [arr idx v]
  (let [i (inc idx)]
    (concat (take i arr)
            (list v)
            (drop i arr))))

(defn part1
  [input]
  (let [r (reduce (fn [acc n]
                    (let [idx (mod (+ (:pos acc) input) (count (:arr acc)))]
                      (-> acc
                          (update :arr #(insert % idx n))
                          (assoc :pos (inc idx)))))
                  {:pos 0
                   :arr [0]}
                  (range 1 2018))]
    (nth (:arr r) (mod (inc (:pos r)) (count (:arr r))))))

(defn part2
  [insertions input]
  (:last (reduce (fn [acc n]
                   (let [idx (mod (+ (:pos acc) input) (:arr-len acc))]
                     (if (= idx 0)
                       (-> acc
                           (assoc :last n)
                           (update :arr-len inc)
                           (assoc :pos (inc idx)))
                       (-> acc
                           (update :arr-len inc)
                           (assoc :pos (inc idx))))))
                 {:pos 0
                  :last nil
                  :arr-len 1}
                 (range 1 insertions))))

(defn part2
  [input]
  nil)
