(ns advent-of-code-2017.day19
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [advent-of-code-2017.util :as util]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]))

(defn read-input
  [input]
  (mapv vec (cstr/split-lines input)))

(defn insert
  [arr idx v]
  (let [i (inc idx)]
    (concat (take i arr)
            (list v)
            (drop i arr))))

(def ^:private directions
  {:east #(vector (inc (first %)) (second %))
   :north #(vector (first %) (dec (second %)))
   :west #(vector (dec (first %)) (second %))
   :south #(vector (first %) (inc (second %)))})

(defn- get-start-x
  [input]
  (.indexOf (first input) \|))

(defn- move
  [pos dir]
  (let [mf (get directions dir)]
    (mf pos)))

(defn- char-in-dir
  [board pos dir]
  (get-in board (reverse (move pos dir))))

(defn- next-direction
  [board pos dir]
  {:post [(some #{%} [:north :east :south :west])]}
  (letfn [(search [sdir] (not= (char-in-dir board pos sdir) \space))]
    (if (some #{dir} [:north :south])
      (first (filter search [:east :west]))
      (first (filter search [:north :south])))))

(defn- solve
  [board]
  (loop [pos [(get-start-x board) 0] dir :south letters [] steps 0]
    (if (and (<= 0 (first pos) 1000)
             (<= 0 (second pos) 1000))
      (let [np (move pos dir)
            c (get-in board (reverse np))]
        (condp = c
          \+ (recur np
                    (next-direction board np dir)
                    letters
                    (inc steps))
          \- (recur np dir letters (inc steps))
          \| (recur np dir letters (inc steps))
          \space [letters (inc steps)]
          (recur np dir (conj letters c) (inc steps))))
      (throw (ex-info "Ran outside of bounds" {:pos pos})))))

(defn part1
  [board]
  (first (solve board)))

(defn part2
  [board]
  (second (solve board)))
