(ns advent-of-code-2017.day18
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [advent-of-code-2017.util :as util]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]))

(defn get-val
  [s v]
  (if (re-matches #"^-?\d+$" v)
    (Integer/parseInt v)
    (or (get-in s [:regs v]) 0)))

(def ^:private instructions
  {"snd" (fn [v]
           (fn [s] (assoc s :snd (get-val s v))))
   "set" (fn [reg v]
           (fn [s] (assoc-in s [:regs reg] (get-val s v))))
   "add" (fn [reg v]
           (fn [s] (update-in s [:regs reg] #(+ (or % 0) (get-val s v)))))
   "mul" (fn [reg v]
           (fn [s] (update-in s [:regs reg] #(* (or % 0) (get-val s v)))))
   "mod" (fn [reg v]
           (fn [s] (update-in s [:regs reg] #(mod (or % 0) (get-val s v)))))
   "rcv" (fn [v]
           (fn [s] (if (zero? (get-val s v))
                     s
                     (assoc s :rcvd (:snd s)))))
   "jgz" (fn [reg v]
           (fn [s] (if (pos-int? (get-val s reg))
                     (assoc s :jmp (Integer/parseInt v))
                     s)))})

(defn parse-instruction
  [inst]
  (let [f (get instructions (first inst))]
    (apply f (rest inst))))

(defn read-input
  [input]
  (map #(parse-instruction (cstr/split % #"\s+"))
       (cstr/split-lines input)))

(defn part1
  [input]
  (loop [state {} pc 0]
    (if (or (neg-int? pc) (>= pc (count input)))
      (throw (ex-info "Started executing garbage" {:pc pc :state state}))
      (let [ns ((nth input pc) state)]
        (if-let [j (:jmp ns)]
          (recur (dissoc ns :jmp) (+ pc j))
          (if-let [rcvd (:rcvd state)]
            rcvd
            (recur ns (inc pc))))))))

(defn part2
  [input]
  nil)
