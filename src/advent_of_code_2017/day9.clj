(ns advent-of-code-2017.day9
  (:require
   [clojure.string :as cstr]
   [clojure.edn :as edn]
   [advent-of-code-2017.data :as data]
   [clojure.spec.alpha :as s]
   [criterium.core :refer [quick-bench]]
   [clojure.spec.gen.alpha :as gen]))

(def init {::total 0
           ::depth 0
           ::garbage false
           ::escape false
           ::gchars 0})

(defn input-generator
  []
  (gen/fmap cstr/join
            (gen/vector (gen/frequency [[5 (gen/return "{")]
                                        [5 (gen/return "}")]
                                        [3 (gen/return "<")]
                                        [2 (gen/return ">")]
                                        [1 (gen/return "!")]
                                        [5 (s/gen string?)]])
                        0 50)))

(s/def ::total int?)
(s/def ::depth int?)
(s/def ::garbage boolean?)
(s/def ::escape boolean?)
(s/def ::gchars int?)
(s/def ::input (s/with-gen string? input-generator))
(s/def ::output (s/keys :req [::total ::depth ::garbage ::escape ::gchars]))
(s/fdef read-input
        :args (s/cat :input ::input)
        :ret ::output)

(defn read-input
  [input]
  (letfn [(reducer
            [{:keys [::garbage ::escape ::total ::depth ::gchars] :as acc} c]
            (cond
              escape
              (assoc acc ::escape false)
              garbage
              (condp = c
                \> (assoc acc ::garbage false)
                \! (assoc acc ::escape true)
                (update acc ::gchars inc))
              :else
              (condp = c
                \< (assoc acc ::garbage true)
                \{ (update acc ::depth inc)
                \} (-> acc
                       (update ::total (partial + depth))
                       (update ::depth dec))
                acc)))]
    (->> input
         seq
         (reduce reducer init))))

(s/fdef part1
        :args (s/cat :input string?)
        :ret int?)

(defn part1
  [input]
  (::total (read-input input)))

(s/fdef part2
        :args (s/cat :input string?)
        :ret int?)

(defn part2
  [input]
  (::gchars (read-input input)))

(comment
  (quick-bench (part1 (read-input data/day9))) ;; 1.8ms
  (quick-bench (part2 (read-input data/day9)))) ;; 1.8ms
